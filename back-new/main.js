require('dotenv').config()

const app = require('./src/app')
const pool = require('./src/pool')

;(async () => {
    try {   
        await pool.connect()
        app()
    } catch (error) {
        console.log('pool error')
        console.log(process.env.PGUSER)
        console.log(error)
    }
})()