const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {

    if (req.method === 'OPTIONS') {
        return next()
    }
  
    try {

        const token = req.headers.authorization.split(' ')[1]
        const decoded = jwt.verify(token, process.env.PKEY)
        req.auth = { username: decoded.username, email: decoded.email, token: token }

        next()

    } catch (err) {
        res.send(null)
        return
    }

}