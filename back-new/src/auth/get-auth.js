const jwt = require('jsonwebtoken')

module.exports = (authorization) => {
    const token = authorization?.split(' ')[1]
    let decoded
    if (token) {
        decoded = jwt.verify(token, process.env.PKEY)
    }

    return decoded
}