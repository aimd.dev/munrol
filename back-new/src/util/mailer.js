const aws = require('aws-sdk')
const welcomeActivateTemplate = require('../email-templates/welcome-activate')
const accountVerifiedTemplate = require('../email-templates/account-acctivated')
const passwordResetTemplate = require('../email-templates/password-reset')
const passwordChangedTemplate = require('../email-templates/password-changed')

const config = {
    accessKeyId: process.env.SES_ID,
    secretAccessKey: process.env.SES_SECRET,
    region: 'us-east-1',
}

const ses = new aws.SES(config)

const getParams = (email, template, subject) => {
    return {
        Source: process.env.SES_SOURCE,
        Destination: {ToAddresses: [email]},
        ReplyToAddresses: [process.env.SES_SOURCE],
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: template
                },
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        }
    }
}

const getTemplate = (params, template) => {
    Object.keys(params).forEach(key => {
        const regex = new RegExp(`{{${key.toUpperCase()}}}`, 'g')
        template = template.replace(regex, params[key]);
    })
    return template
}

class Mailer {

    static async sendVerifyAccountEmail(username, email, code) {
        let params = {user:username, domain:process.env.FRONT_DOMAIN, data:`?u=${username}&c=${code}`}
        let data
        try {
            data = await ses.sendEmail(
                getParams(email, getTemplate(params, welcomeActivateTemplate), 
                'Bienvenido a la comunidad de Munrol')
            ).promise()
        } catch (error) {
            console.log(error)
            return false
        }
        return data
    }

    static async sendAccountVerifiedEmail(username, email) {
        let params = {user:username, domain:process.env.FRONT_DOMAIN}
        let data
        try {
            data = await ses.sendEmail(
                getParams(email, getTemplate(params, accountVerifiedTemplate), 
                'Tu cuenta ha sido verificada')
            ).promise()
        } catch (error) {
            console.log(error)
            return false
        }
        return data
    }

    static async sendResetCode(username, email, code) {
        let params = {user:username, code:code, domain:process.env.FRONT_DOMAIN}
        let data
        try {
            data = await ses.sendEmail(
                getParams(email, getTemplate(params, passwordResetTemplate), 
                'Recuperar Contraseña')
            ).promise()
        } catch (error) {
            console.log(error)
            return false
        }
        return data
    }

    static async sendPasswordChanged(username, email) {
        let params = {user:username, domain:process.env.FRONT_DOMAIN}
        let data
        try {
            data = await ses.sendEmail(
                getParams(email, getTemplate(params, passwordChangedTemplate), 
                'Cambio de Contraseña')
            ).promise()
        } catch (error) {
            console.log(error)
            return false
        }
        return data
    }
}

module.exports = Mailer