const aws = require('aws-sdk')
const fetch = require('node-fetch')
// const fs = require('fs')

const config = {
    accessKeyId: process.env.S3_ID,
    secretAccessKey: process.env.S3_SECRET,
    region: 'us-east-1',
}
const s3 = new aws.S3(config)

const getParams = async (key, body, type) => {

    let buffer = Buffer.from(body.split(",")[1], 'base64')

    return {
        Bucket: process.env.S3_BUCKET,
        Key: key,
        Body: buffer,
        ACL: 'public-read',
        ContentType: type
    }
}

class S3Manager {

    static async upload(key, file, type) {

        let params = await getParams(key, file, type)

        try {
            await s3.upload(params).promise()
        } catch (err) {
            console.log(err)
            return false
        }

        return true
    }
}

module.exports = S3Manager