const express = require('express')

const checkAuth = require('../auth/check-auth')

const router = express.Router()

router.use(checkAuth)

// Get Current User
router.get('/current', (req, res, next) => {
    const { auth } = req
    return res.send(auth)
})

module.exports = router