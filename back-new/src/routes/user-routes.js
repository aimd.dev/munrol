const express = require('express')

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const checkAuth = require('../auth/check-auth')

const User = require('../repos/user-repo')
const Friendship = require('../repos/friendship-repo')
const Reply = require('../repos/reply-repo')
const Mailer = require('../util/mailer')

const { customAlphabet } = require('nanoid')
const S3Manager = require('../util/s3-uploader')

const router = express.Router()

// Check if user exists
router.get('/q/:username', async (req, res, next) => {
    const { username } = req.params
    const user = await User.getByUsername(username)
    return res.send({exists: !!user})
})

// Create new user
router.post('', async (req, res, next) => {
    const { username, email, password } = req.body

    let hash
    try {
        hash = await bcrypt.hash(password, 12)
    } catch (error) {
        return res.status(503).send({message:"Could Not Hash"})
    }

    let user
    try {
        user = await User.insert(username, email, hash)
    } catch (error) {
        return res.status(500).send({ message:"Could Not Insert" })
    }

    const nanoid = customAlphabet(process.env.NANOID_ALPHABET, 120)
    const code = nanoid()

    try {
        await User.insertVerificationCode(username, code)
    } catch (error) {
        return res.status(500).send({ message:"Verification Error" })
    }

    Mailer.sendVerifyAccountEmail(username, email, code)

    return res.send(user).status(201)
})

// Verify Account
router.post('/account-verify/', async (req, res, next) => {
    
    const { username, code } = req.body
    let result

    try {
        result = await User.getVerificationCode(username)
    } catch (error) {
        console.log(error)
        return res.status(400).send({message: 'Could Not Verify'})
    }

    if (!result) {
        return res.status(400).send({message: 'User is Verified'})
    }

    if (result.code !== code) {
        return res.status(403).send({message: 'Mismatch'})
    }

    await User.verifyAccount(username)
    Mailer.sendAccountVerifiedEmail(username, result.email)
    User.deleteVerificationCode(username)
    
    return res.send({message: 'User Verified'}).status(201)
})

// Login user
router.post('/auth/', async (req, res, next) => {

    const { username, password } = req.body
    let user

    try {
        user = await User.getCredentials(username)
    } catch (error) {
        return res.status(500).send({ message: "Could Not Get" })
    }

    if (!user) {
        return res.status(404).send({ message: "User Not Found" })
    }

    let match

    try {
        match = await bcrypt.compare(password, user.password)
    } catch (error) {
        return res.status(500).send({ message:"Could Not Match" })
    }

    if (!match) {
        return res.status(401).send({ message:"Password Mismatch" })
    }

    try {
        user.token = jwt.sign({username: user.username, email: user.email}, process.env.PKEY)
    } catch (error) {
        return res.status(503).send({ message: "Could Not Sign" })
    }

    delete user.password
    return res.send(user)
})

// Get user avatar
router.get('/avatar/:username', async (req, res, next) => {
    const { username } = req.params
    const avatar = await User.getAvatar(username)
    return res.send(avatar)
})

// Send Reset Code
router.get('/pr', async (req, res, next) => {
    
    const { u } = req.query
    let user

    try {
        user = await User.getCredentials(u)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Get'})
    }

    if (!user) {
        return res.status(404).send({message: 'User Does Not Exist'})
    }

    try {
        await User.deleteResetCode(u)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Delete'})
    }

    const nanoid = customAlphabet(process.env.NANOID_ALPHABET, 5)
    const code = nanoid() 

    try {
        await User.insertResetCode(u, code)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Insert'})
    }

    try {
        Mailer.sendResetCode(u, user.email, code)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Send Email'})
    }  
    
    return res.send({message: 'Code Sent'}).status(200)
})

// Restore Password
router.post('/pr', async (req, res, next) => {
    
    const { username, code, password } = req.body
    let dbcode

    try {
        dbcode = await User.getResetCode(username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Get'})
    }

    if (!dbcode) {
        return res.status(404).send({message: 'Not Found'})
    }

    const now = new Date().getTime()
    const then = new Date(dbcode.createdAt).getTime()
    if (now - then > (5*60*1000)) {
        return res.status(403).send({message: 'Code Expired'})
    }

    if (dbcode.code !== code) {
        return res.status(403).send({message: 'Mismatch'})
    }

    let hash
    try {
        hash = await bcrypt.hash(password, 12)
    } catch (error) {
        return res.status(503).send({message:"Could Not Hash"})
    }

    try {
        await User.alter(username, 'password', hash)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Alter' })
    }

    User.deleteResetCode(username)
    Mailer.sendPasswordChanged(username, dbcode.email)

    return res.send({message: 'Password Restored'}).status(200)
})

// Get user by username
router.get('/u/:username', async (req, res, next) => {

    const { username } = req.params
    const user = await User.getByUsername(username)
    return res.send(user)
})


router.use(checkAuth)


// Get Alerts
router.get('/alerts', async (req, res, next) => {

    const { auth } = req

    let data
    try {
        data = await Promise.all([Friendship.getAlerts(auth.username), Reply.getAlerts(auth.username)])
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Get' })
    }
    
    return res.send({friendships:data[0].count, replies:data[1].count})
})

// Clear Alerts
router.patch('/alerts', async (req, res, next) => {

    const { param } = req.body
    const { auth } = req

    try {
        if (param === 'f') {
            await Friendship.clearAlerts(auth.username)
        } else {
            await Reply.clearAlerts(auth.username)
        }
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Patch' })
    }
    
    return res.send({message: 'Alerts Cleared'})
})

// Update Avatar
router.post('/:username/avatar', async (req, res, next) => {

    const { username } = req.params
    const { key, file, type } = req.body
    const { auth } = req

    if (username !== auth.username) {
        return res.status(403).send({ message: 'Forbidden' })
    }

    let upload

    try {
        upload = await S3Manager.upload(key, file, type)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Upload' })
    }

    if (!upload) {
        return res.status(500).send({ message: 'Could Not Upload' })
    }

    try {
        await User.alter(username, 'avatar', process.env.S3_STATIC_ROOT+key)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Alter' })
    }

    return res.send({ key: process.env.S3_STATIC_ROOT+key })
    
})

// Update User
router.put('/:username/:prop', async (req, res, next) => {

    const { username, prop } = req.params
    const { value } = req.body
    const { auth } = req

    if (username !== auth.username) {
        return res.status(403).send({ message: 'Forbidden' })
    }

    try {
        await User.alter(username, prop, value)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Alter' })
    }

    return res.send({ message: 'Alter Successful' })
    
})

module.exports = router