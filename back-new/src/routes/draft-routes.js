const express = require('express')

const checkAuth = require('../auth/check-auth')

const Draft = require('../repos/draft-repo')

const router = express.Router()

router.use(checkAuth)

// Get Drafts By Author
router.get('/threads/', async (req, res, next) => {
    const { username } = req.auth
    let drafts

    try {
        drafts = await Draft.getThreadDraftsByUsername(username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Get'})
    }
   
    return res.send(drafts)
})

// Insert Draft
router.post('/threads/', async (req, res, next) => {
    const { username } = req.auth
    const { title, summary, body } = req.body
    let draft

    try {
        draft = await Draft.insertThreadDraft(username, title, summary, body)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Post'})
    }
   
    return res.status(201).send(draft)
})

// Update Draft
router.put('/threads/', async (req, res, next) => {
    const { username } = req.auth
    const { id, title, summary, body } = req.body
    let draft

    try {
        draft = await Draft.updateThreadDraft(id, new Date(), username, title, summary, body)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Put'})
    }
   
    return res.status(201).send(draft)
})

// Remove Draft
router.delete('/threads/:id', async (req, res, next) => {
    const { username } = req.auth
    const { id } = req.params

    // console.log(author, id)

    try {
        await Draft.removeThreadDraft(id, username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message: 'Could Not Delete'})
    }

    return res.status(200).send({message: 'Success'})
})

module.exports = router