const express = require('express')
const getAuth = require('../auth/get-auth')
const checkAuth = require('../auth/check-auth')

const Thread = require('../repos/thread-repo')
const Draft = require('../repos/draft-repo')
const Reply = require('../repos/reply-repo')

const { customAlphabet } = require('nanoid')

const router = express.Router()


// Get New Threads
router.get('/new', async (req, res, next) => {

    const auth = getAuth(req.headers.authorization)
    const { ts, pos } = req.query
    let threads

    try {
        threads = await Thread.getNew(new Date(parseInt(ts)), auth?.username, 10, pos)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message:'Could Not Get'})
    }
    
    return res.send(threads)
})

// Get Active Threads
router.get('/active', async (req, res, next) => {

    const auth = getAuth(req.headers.authorization)
    const { ts, pos } = req.query
    let threads

    try {
        threads = await Thread.getActive(new Date(parseInt(ts)), auth?.username, 10, pos)
    } catch (error) {
        console.log(error)
        return res.status(500).send({message:'Could Not Get'})
    }

    return res.send(threads)
})

// Get Threads by Author
router.get('/by-author/:username', async (req, res, next) => {

    const { username } = req.params
    const threads = await Thread.getByAuthor(username)
    return res.send(threads)
})

// Get Thread by id
router.get('/id/:id', async (req, res, next) => {

    const { id } = req.params
    const auth = getAuth(req.headers.authorization)
    let thread

    try {
        const data = await Promise.all([Thread.getById(id, auth?.username), Reply.getByThread(id, auth?.username)])
        thread = data[0]
        thread.replies = data[1]
    } catch (error) {
        console.log(error)
        res.status(500).send({message: 'Could Not Get'})
    }
    
    if (!thread) {
        res.status(404).send({message: 'Not Found'})
    }

    return res.send(thread)

})

// Get Thread Participants
router.get('/participants/:id', async (req, res, next) => {

    const { id } = req.params
    let participants
    try {
        participants = await Thread.getParticipants(id)
    } catch (error) {
        console.log(error)
        return res.status(404).send({message: 'Not Found'})
    }
    
    return res.send(participants)
})


// Authentication
router.use(checkAuth)


// Like
router.post('/likes', async (req, res, next) => {

    const { id } = req.body
    const { auth } = req

    try {
        await Thread.like(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Post' })
    }

    return res.send({ message: 'Like Successful' })
})

// Dislike
router.delete('/likes/:id', async (req, res, next) => {

    const { id } = req.params
    const { auth } = req

    try {
        await Thread.dislike(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Delete' })
    }

    return res.send({ message: 'Dislike Successful' })
})

// Save
router.post('/saved', async (req, res, next) => {

    const { id } = req.body
    const { auth } = req

    try {
        await Thread.save(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Post' })
    }

    return res.send({ message: 'Save Successful' })
})

// Unsave
router.delete('/saved/:id', async (req, res, next) => {

    const { id } = req.params
    const { auth } = req

    try {
        await Thread.unsave(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Delete' })
    }

    return res.send({ message: 'Unsave Successful' })
})

// Create New Thread
router.post('', async (req, res, next) => {
    
    const { title, summary, restricted, participants, body, draftId } = req.body
    const { auth } = req

    const nanoid = customAlphabet(process.env.NANOID_ALPHABET, 12)
    const id = nanoid()

    let thread

    if (!restricted) {

        try {
            thread = await Thread.insert(id, title, summary, restricted, body, auth.username)
        } catch (error) {
            console.log(error)
            return res.status(500).send({ message:"Could Not Insert" })
        }

        Draft.removeThreadDraft(draftId, auth.username)
        return res.send({message: 'Success', id:id}).status(201)

    } else {

        try {
            thread = await Thread.insertTransaction(id, title, summary, restricted, body, auth.username, participants)
        } catch (error) {
            console.log(error)
            return res.status(500).send({ message:"Could Not Insert" })
        }

        Draft.removeThreadDraft(draftId, auth.username)
        return res.send({message: 'Success', id:id}).status(201)
    }
    
})

// Update Thread
router.put('/:id', async (req, res, next) => {

    const { id } = req.params
    const { title, summary, restricted, body, prop, value } = req.body
    const { auth } = req

    if (author !== auth.username) {
        return res.status(403).send({ message: 'Forbidden' })
    }

    try {

        if (prop === 'contents') {
            await Thread.update(id, title, summary, restricted, body, auth.username)
        } else {
            await Thread.alter(id, prop, value, auth.username)
        }
        
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Update' })
    }

    return res.send({ message: 'Update Successful' })
    
})

module.exports = router