const express = require('express')

const checkAuth = require('../auth/check-auth')

const Friendship = require('../repos/friendship-repo')
const { customAlphabet } = require('nanoid')

const router = express.Router()

// Get Mutuals by Username
router.get('/mutuals/:username', async (req, res, next) => {
    
    const { username } = req.params
    let mutuals

    try {
        mutuals = await Friendship.getMutuals(username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Get" })
    }

    return res.send(mutuals)
})

// Authentication
router.use(checkAuth)

// Check Friendship
router.get('/check', async (req, res, next) => {

    const { target } = req.query
    const { auth } = req
    let friendship

    try {
        friendship = await Friendship.check(target, auth.username)
    } catch (error) {
        console.log(error)
        res.status(400).send({message: 'Could Not Get'})
    }

    res.status(200).send({friendship: !!friendship})

})

// Insert Friendship
router.post('', async (req, res, next) => {
    
    const { user } = req.body
    const { auth } = req

    const nanoid = customAlphabet(process.env.NANOID_ALPHABET, 12)
    const id = nanoid()

    try {
        await Friendship.insert(id, auth.username, user)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Insert" })
    }

    return res.send({message: 'Friendship request created'}).status(201)
})

// Get Requests by Username
router.get('/requests/', async (req, res, next) => {
    
    const { auth } = req
    let requests

    try {
        requests = await Friendship.getRequests(auth.username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Get" })
    }

    return res.send(requests)
})

// Accept Request
router.patch('/:id', async (req, res, next) => {
    
    const { id } = req.params
    const { auth } = req

    let friendship

    try {
        friendship = await Friendship.accept(id, auth.username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Update" })
    }

    if (friendship) {
        return res.send({ message: "Friendship Accepted" })
    } else {
        return res.status(400).send({ message: "User Mismatch" })
    }
    
})

// Unfriend
router.delete('/:id', async (req, res, next) => {
    
    const { id } = req.params
    const { auth } = req

    try {
        await Friendship.remove(id, auth.username)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Delete" })
    }

    return res.send({ message: "Friendship Deleted" })
})

module.exports = router