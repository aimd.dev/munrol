const express = require('express')

const checkAuth = require('../auth/check-auth')

const Reply = require('../repos/reply-repo')
const Thread = require('../repos/thread-repo')
const Participant = require('../repos/participant-repo')
const { customAlphabet } = require('nanoid')

const router = express.Router()


// Get replies by thread
router.get('/by-thread/:thread', async (req, res, next) => {
    
    const { thread } = req.params
    const replies = await Reply.getByThread(thread)
    return res.send(replies)
})


// Authentication
router.use(checkAuth)


// Like
router.post('/likes', async (req, res, next) => {

    const { id } = req.body
    const { auth } = req

    try {
        await Reply.like(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Post' })
    }

    return res.send({ message: 'Like Successful' })
})

// Dislike
router.delete('/likes/:id', async (req, res, next) => {

    const { id } = req.params
    const { auth } = req

    try {
        await Reply.dislike(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Delete' })
    }

    return res.send({ message: 'Dislike Successful' })
})

// Save
router.post('/saved', async (req, res, next) => {

    const { id } = req.body
    const { auth } = req

    try {
        await Reply.save(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Post' })
    }

    return res.send({ message: 'Save Successful' })
})

// Unsave
router.delete('/saved/:id', async (req, res, next) => {

    const { id } = req.params
    const { auth } = req

    try {
        await Reply.unsave(auth.username, id)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Could Not Delete' })
    }

    return res.send({ message: 'Unsave Successful' })
})

// Insert Reply
router.post('', async (req, res, next) => {
    
    const { thread, body, approved } = req.body
    const { auth } = req
    let approvalDate = approved ? new Date() : null

    const nanoid = customAlphabet(process.env.NANOID_ALPHABET, 12)
    const id = nanoid()

    let reply
    try {
        reply = await Reply.insert(id, thread, body, auth.username, approved, approvalDate)
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message:"Could Not Insert" })
    }

    return res.send({message: 'Reply was sent'}).status(201)
})

// Get Reply Requests by Author
router.get('/requests', async (req, res, next) => {
    
    const { auth } = req

    let requests
    try {
        requests = await Reply.getReplyRequestsByAuthor(auth.username)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Get" })
    }

    return res.send(requests).status(201)
})

// Approve Reply
router.patch('/requests', async (req, res, next) => {

    const { id, threadId, username } = req.body
    const { auth } = req
    let thread

    try {
        thread = await Thread.getById(threadId)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Get" })
    }

    if (!thread) {
        return res.status(404).send({ message:"Not Found" })
    }

    if (thread.author !== auth.username) {
        return res.status(403).send({ message:"Forbidden" })
    }

    const approvalDate = new Date()

    try {
        await Reply.approveReplyRequest(id, approvalDate)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Patch" })
    }

    try {
        await Participant.insert(threadId, username)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Insert" })
    }

    return res.send({message: 'Reply Approved'}).status(201)
})

// Reject Reply
router.delete('/requests/:threadId/:replyId', async (req, res, next) => {

    const { threadId, replyId } = req.params
    const { auth } = req
    let thread

    try {
        thread = await Thread.getById(threadId)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Get" })
    }

    if (!thread) {
        return res.status(404).send({ message:"Not Found" })
    }

    if (thread.author !== auth.username) {
        return res.status(403).send({ message:"Forbidden" })
    }

    try {
        await Reply.rejectReplyRequest(replyId)
    } catch (error) {
        console.log(error)
        return res.status(400).send({ message:"Could Not Delete" })
    }

    return res.send({message: 'Reply Deleted'}).status(201)
})

module.exports = router