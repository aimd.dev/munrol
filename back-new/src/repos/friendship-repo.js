const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class Friendship {

    static async insert(id, friend, befriends) {
        const sql = 'SELECT * FROM insert_friendship($1, $2, $3);'
        const { rows } = await pool.query(sql, [id, friend, befriends])
        return toCamelCase(rows)
    }

    static async accept(id, user) {
        const sql = 'SELECT * FROM accept_friendship($1, $2);'
        const { rows } = await pool.query(sql, [id, user])
        return toCamelCase(rows)[0]
    }

    static async remove(id, user) {
        const sql = 'SELECT * FROM remove_friendship($1, $2);'
        const { rows } = await pool.query(sql, [id, user])
        return toCamelCase(rows)[0]
    }

    static async getRequests(username) {
        const sql = 'SELECT * FROM get_friendship_requests($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async getMutuals(username) {
        const sql = 'SELECT * FROM get_friendships_by_user($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async check(user1, user2) {
        const sql = 'SELECT * FROM check_friendship($1, $2);'
        const { rows } = await pool.query(sql, [user1, user2])
        return toCamelCase(rows)[0]
    }

    static async getAlerts(username) {
        const sql = 'SELECT * FROM get_friendship_requests_alerts($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0]
    }

    static async clearAlerts(username) {
        const sql = 'SELECT * FROM clear_friendship_requests_alerts($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }
    
}

module.exports = Friendship