const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class Participant {

    static async insert(thread, username) {
        const sql = 'INSERT INTO participants (thread, username) VALUES ($1, $2);'
        const { rows } = await pool.query(sql, [thread, username])
        return toCamelCase(rows)
    }
    
}

module.exports = Participant