const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class Reply {

    static async getByThread(thread, username) {
        const sql = 'SELECT * FROM get_replies_by_thread($1, $2);'
        const { rows } = await pool.query(sql, [thread, username])
        return toCamelCase(rows)
    }

    static async insert(id, thread, body, author, approved, approvalDate) {
        const sql = 'SELECT * FROM insert_reply($1, $2, $3, $4, $5, $6);'
        const { rows } = await pool.query(sql, [id, thread, body, author, approvalDate, approved])
        return toCamelCase(rows)
    }

    static async getReplyRequestsByAuthor(username) {
        const sql = 'SELECT * FROM get_reply_requests_by_author($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async approveReplyRequest(id, approvalDate) {
        const sql = 'SELECT * FROM approve_reply($1, $2);'
        const { rows } = await pool.query(sql, [id, approvalDate])
        return toCamelCase(rows)
    }

    static async rejectReplyRequest(id) {
        const sql = 'SELECT * FROM reject_reply($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    // static async update(id, title, summary, body, author) {
    //     const sql = 'SELECT * FROM update_thread($1, $2, $3, $4, $5);'
    //     const { rows } = await pool.query(sql, [id, title, summary, body, author])
    //     return toCamelCase(rows)
    // }

    static async like(username, id) {
        const sql = `SELECT * FROM insert_fav_reply($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async dislike(username, id) {
        const sql = `SELECT * FROM delete_fav_reply($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async save(username, id) {
        const sql = `SELECT * FROM insert_saved_reply($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async unsave(username, id) {
        const sql = `SELECT * FROM delete_saved_reply($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async getAlerts(username) {
        const sql = 'SELECT * FROM get_reply_requests_alerts($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0]
    }

    static async clearAlerts(username) {
        const sql = 'SELECT * FROM clear_reply_requests_alerts($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }
    
}

module.exports = Reply