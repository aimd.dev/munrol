const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class Thread {

    static async getById(id, username) {
        const { rows } = await pool.query('SELECT * FROM get_thread_by_id($1, $2);', [id, username])
        return toCamelCase(rows)[0]
    }

    static async getByAuthor(username) {
        const { rows } = await pool.query('SELECT * FROM get_threads_by_user($1);', [username])
        return toCamelCase(rows)
    }

    static async getParticipants(id) {
        const { rows } = await pool.query('SELECT * FROM get_thread_participants($1);', [id])
        return toCamelCase(rows)
    }

    static async insert(id, title, summary, restricted, body, author) {
        const sql = 'SELECT * FROM create_thread($1, $2, $3, $4, $5, $6);'
        const { rows } = await pool.query(sql, [id, title, summary, restricted, body, author])
        return toCamelCase(rows)[0]
    }

    static async insertTransaction(id, title, summary, restricted, body, username, participants) {

        let thread
        let sql

        const client = await pool.connect()
        try {
            // Begin Transaction
            await client.query('BEGIN')

            // 1. Insert Thread
            sql = 'SELECT * FROM create_thread($1, $2, $3, $4, $5, $6);'
            const { rows } = await client.query(sql, [id, title, summary, restricted, body, username])
            thread = toCamelCase(rows)[0]

            // 2. Insert Participants
            sql = ''
            participants.forEach((p,i) => { sql += `($1,$${i+2})${i === participants.length - 1 ? '' : ','}` })
    
            let values = [...participants]
            values.unshift(id)
            
            await client.query(`INSERT INTO participants (thread, username) VALUES ${sql};`, values)

            // End Transaction
            await client.query('COMMIT;')
    
            return thread
    
        } catch (error) {

            console.log('error in transaction')
            client.query('ROLLBACK')
    
            console.log(error.code)
            console.log(error.detail)
            console.log(error.message)
    
            throw new Error(error.code)

        } finally {
            client.release()
        }
    }

    static async update(id, title, summary, restricted, body, author) {
        const sql = 'SELECT * FROM update_thread($1, $2, $3, $4, $5);'
        const { rows } = await pool.query(sql, [id, title, summary, restricted, body, author])
        return toCamelCase(rows)
    }

    static async alter(id, prop, value, author) {
        const sql = `SELECT * FROM update_thread_${prop}($1, $2, $3);`
        const { rows } = await pool.query(sql, [id, value, author])
        return toCamelCase(rows)
    }

    static async like(username, id) {
        const sql = `SELECT * FROM insert_fav_thread($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async dislike(username, id) {
        const sql = `SELECT * FROM delete_fav_thread($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async save(username, id) {
        const sql = `SELECT * FROM insert_saved_thread($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async unsave(username, id) {
        const sql = `SELECT * FROM delete_saved_thread($1, $2);`
        await pool.query(sql, [username, id])
        return true
    }

    static async getNew(date, username, limit, offset) {
        const sql = `SELECT * FROM get_new_threads($1, $2, $3, $4);`
        const { rows } = await pool.query(sql, [date, username, limit, offset])
        return toCamelCase(rows)
    }

    static async getActive(date, username, limit, offset) {
        const sql = `SELECT * FROM get_active_threads($1, $2, $3, $4);`
        const { rows } = await pool.query(sql, [date, username, limit, offset])
        return toCamelCase(rows)
    }
    
}

module.exports = Thread