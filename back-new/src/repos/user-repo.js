const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class User {

    static async getByUsername(username) {
        const { rows } = await pool.query('SELECT * FROM get_user_by_username($1);', [username])
        return toCamelCase(rows)[0]
    }

    static async getAvatar(username) {
        const { rows } = await pool.query('SELECT * FROM get_user_avatar($1);', [username])
        return toCamelCase(rows)[0]
    }

    static async insert(username, email, password) {
        const sql = 'SELECT * FROM insert_user($1, $2, $3);'
        const { rows } = await pool.query(sql, [username, email, password])
        return toCamelCase(rows)[0]
    }

    static async insertVerificationCode(username, code) {
        const sql = 'SELECT * FROM insert_verification_code($1, $2);'
        const { rows } = await pool.query(sql, [username, code])
        return toCamelCase(rows)
    }

    static async getVerificationCode(username) {
        const sql = 'SELECT * FROM get_verification_code($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0]
    }

    static async deleteVerificationCode(username) {
        const sql = 'SELECT * FROM delete_verification_code($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async getCredentials(username) {
        const { rows } = await pool.query('SELECT * FROM get_user_credentials($1);', [username])
        return toCamelCase(rows)[0]
    }

    static async alter(username, prop, value) {
        await pool.query(`SELECT * FROM update_user_${prop}($1, $2);`, [username, value])
        return true
    }

    static async verifyAccount(username) {
        await pool.query(`SELECT * FROM verify_account($1);`, [username])
        return true
    }

    static async deleteResetCode(username) {
        const sql = 'SELECT * FROM delete_reset_code($1);'
        await pool.query(sql, [username])
        return true
    }

    static async insertResetCode(username, code) {
        const sql = 'SELECT * FROM insert_reset_code($1, $2);'
        const { rows } = await pool.query(sql, [username, code])
        return toCamelCase(rows)
    }

    static async getResetCode(username) {
        const sql = 'SELECT * FROM get_reset_code($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0]
    }
}

module.exports = User