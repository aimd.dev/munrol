const pool = require('../pool')
const toCamelCase = require('../util/to-camel-case')


class Draft {

    static async getThreadDraftsByUsername(username) {
        const sql = `SELECT * FROM get_thread_drafts($1);`
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async insertThreadDraft(author, title, summary, body) {
        const sql = `SELECT * FROM insert_thread_draft($1, $2, $3, $4);`
        const { rows } = await pool.query(sql, [author, title, summary, body])
        return toCamelCase(rows)[0]
    }

    static async updateThreadDraft(id, date, author, title, summary, body) {
        const sql = `SELECT * FROM update_thread_draft($1, $2, $3, $4, $5, $6);`
        const { rows } = await pool.query(sql, [id, date, author, title, summary, body])
        return toCamelCase(rows)[0]
    }

    static async removeThreadDraft(id, author) {
        const sql = `SELECT * FROM delete_thread_draft($1, $2);`
        const { rows } = await pool.query(sql, [id, author])
        return toCamelCase(rows)[0]
    }
}

module.exports = Draft