const express = require('express')
const server = require('http')
const socket = require('socket.io')
// const fs = require('fs')

const authRouter = require('./routes/auth-routes')
const usersRouter = require('./routes/user-routes')
const threadsRouter = require('./routes/thread-routes')
const draftsRouter = require('./routes/draft-routes')
const repliesRouter = require('./routes/reply-routes')
const friendshipRouter = require('./routes/friendship-routes')

// const bcrypt = require('bcrypt')

// const log = function(entry:any) {
//     fs.appendFileSync('/tmp/sample-app.log', new Date().toISOString() + ' - ' + entry + '\n');
// };

module.exports = () => {

    const app = express()
    const http = server.createServer(app)
    const io = socket(http)
    
    // Cors headers
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
        next()
    })

    app.use('/api/status', (req, res, next) => {
        res.send({message:"Listening"}).status(200)
        next()
    })

    // Routing
    app.use(express.json({limit:'20mb'}))
    app.use('/api/users', usersRouter)
    app.use('/api/threads', threadsRouter)
    app.use('/api/drafts', draftsRouter)
    app.use('/api/replies', repliesRouter)
    app.use('/api/friendships', friendshipRouter)
    app.use('/api/auth', authRouter)

    // Sockets
    // io.on('connection', (socket) => {
    //     console.log('a user connected');
        
    //     socket.on('disconnect', () => {
    //         console.log('user disconnected');
    //     });

    //     socket.on('my message', (msg) => {
    //         console.log('message: ' + msg);
    //     });
    // });

    http.listen(process.env.PORT || 3000)
    return app
}