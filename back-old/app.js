const app = require('express')();
const bp = require('body-parser');
const http = require('http').createServer(app);

// const express = require('express')
// const app = express()

const io = require('socket.io')(http);


const userRoutes = require('./users/routes/userRoutes')
const characterRoutes = require('./characters/routes/characterRoutes')
const worldRoutes = require('./worlds/routes/worldRoutes')
const tagRoutes = require('./tags/routes/tagRoutes')


app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
  next()
})


// app.use((req, res, next) => {
// })
app.use(bp.json())
app.use('/api/users', userRoutes)
app.use('/api/characters', characterRoutes)
app.use('/api/worlds', worldRoutes)
app.use('/api/tags', tagRoutes)



io.on('connection', (socket) => {
  console.log('a user connected');
  
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

  socket.on('my message', (msg) => {
    console.log('message: ' + msg);
  });
});

http.listen(3000)
// app.listen(3000)
