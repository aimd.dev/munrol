const express = require('express')
const router = express.Router()

const userControllers = require('../controllers/userControllers')

router.get('/:uname', userControllers.getUserByName)
router.get('/worlds/:uname', userControllers.getUserWorlds)

router.post('/new', userControllers.postUser)
router.post('/auth', userControllers.loginUser)

module.exports = router