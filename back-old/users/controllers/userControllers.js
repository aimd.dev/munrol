const User = require('../../models/user')

const postUser = async (req, res, next) => {

    const username = req.body.username
    const email = req.body.email
    const password = req.body.password

    let newUser
    try {
        newUser = await User.create({ username, email, password })
    } catch (err) {
        return res.status(400).json({message: err})
    }
    res.status(201).json({message:'User created successfully', user: newUser})
}

const loginUser = async (req, res, next) => {

    // const username = req.body.username
    const email = req.body.email
    const password = req.body.password

    let loggedIn

    try {
        loggedIn = await User.login(email)
        // Not found
        if (!loggedIn) {
            return res.status(404).json({message: "UserDoesNotExist"})
        }
        if (password != loggedIn.password) {
            return res.status(401).json({message: "WrongCredentials"})
        }
        delete loggedIn.password
    } catch (err) {
        return res.status(400).json({message: err})
    }
    res.status(200);
    res.json({message:'Log in successful', user: loggedIn})
}

const getUserByName = async (req, res, next) => {
    let user
    try {
        user = await User.getByName(req.params.uname)
        // Not found
        if (!user) {
            return res.status(404).json({message: "UserDoesNotExist"})
        }
    } catch (err) {
        return res.status(400).json({error: err})
    }
    res.status(200).json(user)
}

const getUserWorlds = async (req, res, next) => {

    let worlds
    try {
        worlds = await User.getUserWorlds(req.params.uname)
    } catch (err) {
        console.log('error')
        return res.status(400).json({message: err})
    }
    res.status(200).json(worlds)
}

exports.postUser = postUser
exports.loginUser = loginUser
exports.getUserByName = getUserByName
exports.getUserWorlds = getUserWorlds