const Tag = require('../../models/tag')

const getByString = async (req, res, next) => {
    let tags
    try {
        tags = await Tag.getByString(req.params.q.toLowerCase())
    } catch (err) {
        return res.status(400).json({error: err})
    }
    res.status(200).json(tags.map(t => t.title))
}

const createIfNotExist = async (req, res, next) => {
    // console.log(req.body.tags)
    let tags = req.body.tags
    tags.forEach((t,i) => tags[i] = t.toLowerCase())

    try {
        tags = await Tag.createIfNotExist(req.body.created_by, tags)
    } catch (err) {
        return res.status(400).json({error: err})
    }
    res.status(201).json(tags)
}

// const getByWorld = async (req, res, next) => {
//     let tags
//     // console.log(req.params.wid)
//     try {
//         tags = await Tag.getByWorld(req.params.wid)
//     } catch (err) {
//         return res.status(400).json({error: err})
//     }
//     res.status(200).json(tags.map(t => t.tag))
// }

exports.getByString = getByString
exports.createIfNotExist = createIfNotExist
// exports.getByWorld = getByWorld