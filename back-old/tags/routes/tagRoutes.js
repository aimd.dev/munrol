const express = require('express')
const router = express.Router()

const tagControllers = require('../controllers/tagControllers')

router.get('/:q', tagControllers.getByString)
// router.get('/world/:wid', tagControllers.getByWorld)
// router.post('/new', tagControllers.createIfNotExist)

module.exports = router