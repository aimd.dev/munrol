const db = require('../util/db')

const create = async ({username, email, password}) => {
    
    const sql = `INSERT INTO users (username, email, password) VALUES ($1, $2, $3) RETURNING username, email;`
    console.log(sql)

    try {
        let result = await db.sqlToDB(sql, [username, email, password])
        return result.rows[0]
    } catch (err) {
        throw getErrorString(err.message)
    }
}

const login = async (email) => {

    const sql = `SELECT id, username, password FROM users WHERE email = $1;`
    console.log(sql)

    try {
        let result = await db.sqlToDB(sql, [email])
        return result.rows[0]
    } catch (err) {
        throw getErrorString(err.message)
    }
}

const getByName = async (username) => {
    let sql = `SELECT * FROM users WHERE username = $1;`
    try {
        let result = await db.sqlToDB(sql, [username])
        return result.rows[0]
    } catch (err) {
        console.log(err)
    }
}   

const getUserWorlds = async (username) => {
    let sql =  `SELECT worlds.id, world_url, title, world_picture FROM worlds 
                LEFT JOIN worlds_users ON worlds_users.world = worlds.world_url
                WHERE worlds_users.member = $1;`

    console.log(sql)
    try {
        let result = await db.sqlToDB(sql, [username])
        return result.rows
    } catch (err) {
        console.log(err)
    }
}

// const getUserNames = async () => {
//     let sql = 'SELECT username, email FROM users;'
//     try {
//         let result = await db.sqlToDB(sql)
//         return result.rows
//     } catch (err) {
//         console.log(err)
//     }
// }

exports.create = create
exports.login = login
exports.getByName = getByName
exports.getUserWorlds = getUserWorlds
// exports.getUsernames = getUserNames

const getErrorString = code => {
    let errorString = "UnkownError"
    if (code == 23505) errorString = "UserAlreadyExists"
    if (code == 23502) errorString = "MissingFields"
    return errorString
}