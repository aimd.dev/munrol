const db = require('../util/db')

const getByUrl = async (url) => {
    let sql = `SELECT * FROM characters WHERE character_url = $1`
    try {
        let result = await db.sqlToDB(sql, [url])
        return result.rows[0]
    } catch (err) {
        console.log(err)
    }
}   

exports.getByUrl = getByUrl