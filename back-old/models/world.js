const db = require('../util/db')

const getByUrl = async (url) => {
    // console.log(url)
    let sql = 'SELECT * FROM full_world WHERE url = $1;'
    try {
        let result = await db.sqlToDB(sql, [url])
        return result.rows[0]
    } catch (err) {
        console.log(err)
    }
} 

const getAllUrls = async () => {
    let sql = 'SELECT url FROM worlds;'
    try {
        let result = await db.sqlToDB(sql)
        return result.rows
    } catch (err) {
        console.log(err)
    }
}

const create = async ({title, url, picture, description, created_by, tags}) => {
    
    try {
        let result = await db.createWorldTransaction({title, url, picture, description, tags, created_by})
        return result
    } catch (err) {
        throw getErrorString(err.message)
    }
}


const getLastFour = async () => {

    let sql = 'SELECT title, url, picture, tags FROM tagged_worlds AS worlds FULL JOIN likes on worlds.id = likes.world_id GROUP BY title, url, picture, tags ORDER BY MAX(likes.created_at) DESC LIMIT 4'

    try {
        let result = await db.sqlToDB(sql)
        return result.rows
    } catch (err) {
        throw getErrorString(err.message)
    }
}

// 47ccb268-d966-4c1f-aed7-9e8f3e90f0fc

const getRecentEvents = async (world) => {
    let sql = 'SELECT * FROM recent_events WHERE world_id = $1 ORDER BY created_at LIMIT 2'

    try {
        let result = await db.sqlToDB(sql, [world])
        return result.rows
    } catch (err) {
        throw getErrorString(err.message)
    }
}

exports.getByUrl = getByUrl
exports.getAllUrls = getAllUrls
exports.create = create
exports.getLastFour = getLastFour
exports.getRecentEvents = getRecentEvents



const getErrorString = code => {
    console.log(code)
    let errorString = "UnkownError"
    if (code == 23505) errorString = "FieldAlreadyExists"
    if (code == 23502) errorString = "MissingFields"
    return errorString
}