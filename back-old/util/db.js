const { Pool } = require('pg')

const pool = new Pool()

pool.on('error', (err) => {
    console.log(err)
})

module.exports.sqlToDB = async (sql, params) => {
    // console.log(params)
    try {
        let result = await pool.query(sql, params);
        return result;
    } catch (error) {
        console.log('error in sql call')
        console.log(error.code)
        console.log(error.detail)
        console.log(error.message)

        throw new Error(error.code)
    }
}

module.exports.createWorldTransaction = async ({title, world_url, world_picture, description, created_by, tags}) => {
    // console.log(params)
    const client = await pool.connect()

    try {
        // start transaction
        await client.query('BEGIN')
        // 1. create world
        await client.query(
            `INSERT INTO worlds (title, world_url, world_picture, description, created_by) VALUES ($1, $2, $3, $4, $5)`,
            [title, world_url, world_picture, description, created_by]
        )
        // 2. create tags if they don't exist
        let tagsql = ''
        tags.forEach((t,i) => {
            tagsql += `($1, $${i+2})${i === tags.length - 1 ? ' ON CONFLICT (title) DO NOTHING;' : ', '}`
        })

        let vars = [...tags]
        vars.unshift(created_by)
        
        await client.query(
            `INSERT INTO tags (author, title) VALUES ${tagsql}`,
            vars
        )

        // 3. link world and tags
        let linksql = ''
        tags.forEach((t,i) => {
            linksql += `($1, $${i+2})${i === tags.length - 1 ? ';' : ', '}`
        })
        vars[0] = world_url;
        await client.query(
            `INSERT INTO world_tags (world, tag) VALUES ${linksql}`,
            vars
        )

        // 4. Link creator to world
        await client.query('INSERT INTO worlds_users (world, member) VALUES ($1, $2)', [world_url, created_by])    

        await client.query('COMMIT;')

        return {message: "World created successfully"};

    } catch (error) {
        console.log('error in transaction')
        client.query('ROLLBACK')

        console.log(error.code)
        console.log(error.detail)
        console.log(error.message)

        throw new Error(error.code)
    } finally {
        client.release()
    }
}