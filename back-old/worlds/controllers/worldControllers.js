const World = require('../../models/world')
// const Tag = require('../../models/tag')
const urlGen = require('../../util/urlgen')

const getWorldByUrl = async (req, res, next) => {
    let world
    try {
        world = await World.getByUrl(req.params.wid)
        if (!world) {
            return res.status(404).json({message: "WorldDoesNotExist"})
        }
    } catch (err) {
        return res.status(400).json({error: err})
    }
    res.status(200).json(world)
}

const getAllUrls = async (req, res, next) => {
    let worlds
    try {
        worlds = await World.getAllUrls()
    } catch (err) {
        return res.status(400).json({error: err})
    }
    res.status(200).json(worlds.map(w => w.url))
}

const createWorld = async (req, res, next) => {

    const title = req.body.title
    const url = urlGen(title)
    const picture = req.body.picture || null
    const description = req.body.description
    const created_by = req.body.created_by

    console.log(title,
        url,
        picture,
        description, tags, created_by)

    let tags = req.body.tags
    tags.forEach((t,i) => tags[i] = t.toLowerCase())

    let world
    try {
        world = await World.create({title, url, picture, description, created_by, tags})
    } catch (err) {
        return res.status(400).json({message: err})
    }
    return res.status(201).json({message: "World created successfully"})
}

const getLastFour = async (req, res, next) => {

    let worlds
    try {
        worlds = await World.getLastFour()
    } catch (err) {
        return res.status(400).json({message: err})
    }
    return res.status(200).json(worlds)
}

const getRecentEvents = async (req, res, next) => {

    let events
    try {
        events = await World.getRecentEvents(req.params.wid)
    } catch (err) {
        return res.status(400).json({message: err})
    }
    return res.status(200).json(events)
}

exports.getWorldByUrl = getWorldByUrl
exports.getAllUrls = getAllUrls
exports.createWorld = createWorld
exports.getLastFour = getLastFour
exports.getRecentEvents = getRecentEvents