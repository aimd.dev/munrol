const express = require('express')
const router = express.Router()

const worldControllers = require('../controllers/worldControllers')

router.get('/urls', worldControllers.getAllUrls)
router.get('/trending', worldControllers.getLastFour)
router.get('/:wid', worldControllers.getWorldByUrl)
router.get('/recent-events/:wid', worldControllers.getRecentEvents)

router.post('/new', worldControllers.createWorld)

module.exports = router