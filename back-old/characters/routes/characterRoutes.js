const express = require('express')
const router = express.Router()

const characterControllers = require('../controllers/characterControllers')

router.get('/:cid', characterControllers.getCharacterById)

module.exports = router