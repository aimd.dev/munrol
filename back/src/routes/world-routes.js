const express = require('express')
const checkAuth = require('../auth/check-auth')
const World = require('../repos/world-repo')
const Event = require('../repos/event-repo')

const router = express.Router()

// Get popular
router.get('/api/browse/worlds/popular', async (req, res) => {
    const worlds = await World.findPopular()
    res.send(worlds)
})
// Get trending
router.get('/api/browse/worlds/trending', async (req, res) => {
    const worlds = await World.findTrending()
    res.send(worlds)
})

// Check if url exists
router.get('/api/worlds/q/:url', async (req, res) => {

    const { url } = req.params
    const world = await World.findUrl(url)
    res.send(!!world)
})

// Get world by url
router.get('/api/worlds/:url', async (req, res) => {
    const { url } = req.params
    const world = await World.findByUrl(url)
    world.characters = await World.findWorldCharacters(world.id)
    world.events = await Event.findByWorldId(world.id) 
    world.posts = await World.findPostsInWorld(world.id, 5)
    world.moderation = await World.findWorldModeration(world.id)
    world ? res.send(world) : res.sendStatus(404)
})



// Auth check
router.use(checkAuth)



// Get world by url
router.get('/api/user-in-world/:id', async (req, res) => {

    const { id } = req.params
    const { uid } = req.auth

    const status = {}

    status.membership = await World.checkMembership(uid, id)
    const moderation = await World.checkModeration(uid, id) 
    status.moderation = moderation ? moderation : false
    status.liked = await World.checkLiked(uid, id)

    status ? res.send(status) : res.sendStatus(404)
})

// Like Interaction
router.post('/api/world-likes/', async (req, res) => {

    const { uid } = req.auth
    const { wid, like } = req.body
    await World.likeInteraction(uid, wid, like)
    res.status(201).send({message: 'success'})
})

// Join Interaction
router.post('/api/world-joins/', async (req, res) => {

    const { uid } = req.auth
    const { wid, like } = req.body
    await World.likeInteraction(uid, wid, like)
    res.status(201).send({message: 'success'})
})

// Insert world
router.post('/api/worlds', async (req, res) => {
    const { uid, username } = req.auth
    const { title, picture, description, tags } = req.body
    const world = await World.insert(title, picture, description, username, tags, uid)
    res.send(world)
})

module.exports = router