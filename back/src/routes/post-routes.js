const express = require('express')
const Post = require('../repos/post-repo')

const router = express.Router()

// Get tags by string
router.get('/api/posts/', async (req, res) => {
    const posts = await Post.findRecent()
    res.send(posts)
})

module.exports = router