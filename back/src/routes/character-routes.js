const express = require('express')
const Character = require('../repos/character-repo')

const router = express.Router()

// Get thread
router.get('/api/characters/:url', async (req, res) => {
    const { url } = req.params
    const character = await Character.findByUrl(url)
    res.send(character)
})

module.exports = router