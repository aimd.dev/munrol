const express = require('express')

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const checkAuth = require('../auth/check-auth')

const User = require('../repos/user-repo')
const World = require('../repos/world-repo')

const router = express.Router()

// Check if url exists
router.get('/api/users/q/:username', async (req, res, next) => {

    const { username } = req.params
    const user = await User.findUsername(username)
    res.send(!!user)
})

// Login user
router.post('/api/users/auth/', async (req, res, next) => {

    const { email, password } = req.body
    const user = await User.login(email)

    let auth

    try {
        auth = await bcrypt.compare(password, user.password)
    } catch (error) {
        console.log(error)
    }

    if (!auth) {
        res.sendStatus(401)
        return
    }

    try {
        user.token = jwt.sign({id: user.id, username: user.username, email: email}, process.env.PKEY)
    } catch (error) {
        console.log(error)
    }
    
    res.send(user)
})

// Get user by username
router.get('/api/users/:username', async (req, res, next) => {
    const { username } = req.params
    const user = await User.findByUsername(username)
    user.totalFriends = await User.findFriendCount(username)
    user.characters = await User.findUserCharacters(user.id)
    user.worlds = await World.findByUser(username)
    user.friends = await User.findUserFriends(user.id)
    user.posts = await User.findUserPosts(username)
    user.following = await User.findFollowing(user.id)
    user.followers = await User.findFollowers(user.id)
    user ? res.send(user) : res.sendStatus(404)
})

// Insert user
router.post('/api/users', async (req, res, next) => {
    const { username, email, password } = req.body

    let hash
    try {
        hash = await bcrypt.hash(password, 12)
    } catch (error) {
        console.log(error)
    }

    const user = await User.insert(username, email, hash)
    res.send(user)
})




// Auth check
router.use(checkAuth)



// Get user to user data
router.get('/api/user-to-user/:lid', async (req, res, next) => {

    const { uid } = req.auth
    const { lid } = req.params
    const data =  await User.checkUserToUser(lid, uid)
    res.send(data)
})

// Follow / Unfollow
router.post('/api/user-follow/', async (req, res, next) => {

    const { uid } = req.auth
    const { lid, f } = req.body

    await User.followInteraction(lid, uid, f)
    res.status(201).send({message: 'success'})
})

// // Update user
// router.put('/users/:id', async (req, res) => {
//     const { id } = req.params
//     const { username, bio } = req.body
//     const user = await User.update(id, username, bio)
//     user ? res.send(user) : res.sendStatus(404)
// })

// // Delete user
// router.delete('/users/:id', async (req, res) => {
//     const { id } = req.params
//     const user = await User.delete(id)
//     user ? res.send(user) : res.sendStatus(404)
// })

module.exports = router