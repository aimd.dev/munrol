const express = require('express')
const Tag = require('../repos/tag-repo')

const router = express.Router()

// Get tags by string
router.get('/api/tags/:title', async (req, res) => {
    const {title} = req.params
    const tags = await Tag.findLike(title.toLowerCase())
    res.send(tags.map(t => t.title))
})

module.exports = router