const express = require('express')
const Thread = require('../repos/thread-repo')

const router = express.Router()

// Get thread
router.get('/api/threads/:url', async (req, res) => {
    const { url } = req.params
    const thread = await Thread.findByUrl(url)
    thread.posts = await Thread.findThreadPosts(thread.id)
    res.send(thread)
})

module.exports = router