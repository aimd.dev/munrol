const express = require('express')
const { customAlphabet } = require('nanoid')
const Event = require('../repos/event-repo')

const router = express.Router()

// Get recent events
router.get('/api/events/', async (req, res) => {
    const events = await Event.findRecent()
    res.send(events)
})

// Get events in world
router.get('/api/events/world/:wid', async (req, res) => {
    const { wid } = req.params
    const events = await Event.findByWorldId(wid)
    res.send(events)
})

// Get event data
router.get('/api/events/:url', async (req, res) => {
    const { url } = req.params
    const event = await Event.findByUrl(url)
    event.threads = await Event.findEventThreads(event.id)
    res.send(event)
})

router.get('/api/nanoid', async (req, res) => {
    const nanoid = customAlphabet('0123456789ABCDFGHJKLMNOPQRSTVWXYZ', 11)
    const id = nanoid()
    res.send(id)
})

module.exports = router