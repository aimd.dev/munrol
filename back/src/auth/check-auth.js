const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {

    if (req.method === 'OPTIONS') {
        return next()
    }

    try {

        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            res.sendStatus(401)
            console.log(error)
        }

        const decoded = jwt.verify(token, process.env.PKEY)
        req.auth = { uid: decoded.id, username: decoded.username, email: decoded.email }
        next()

    } catch (err) {
        res.sendStatus(401)
        console.log(error)
        // return next(error)
    }

}