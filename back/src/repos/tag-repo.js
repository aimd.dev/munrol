const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class Tag {

    static async findLike(title) {

        const sql = 'SELECT title FROM tags WHERE title LIKE $1 LIMIT 5;'
        const { rows } = await pool.query(sql, [`%${title}%`])
        return toCamelCase(rows)
    }
}

module.exports = Tag