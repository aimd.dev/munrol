const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class Thread {

    static async findByUrl(url) {

        const sql = 'SELECT * FROM get_thread_data($1);'
        const { rows } = await pool.query(sql, [url])
        return toCamelCase(rows)[0]
    }

    static async findThreadPosts(id, qty = 10) {
        const sql = 'SELECT *, row_number() over () AS index FROM get_thread_posts($1, $2);'
        const { rows } = await pool.query(sql, [id, qty])
        return toCamelCase(rows)
    }
}

module.exports = Thread