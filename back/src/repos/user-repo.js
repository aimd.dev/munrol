const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class User {

    static async login(email) {

        const { rows } = await pool.query('SELECT id, username, password FROM users WHERE email = $1;', [email])
        return toCamelCase(rows)[0]
    }

    static async findUsername(username) {

        const { rows } = await pool.query('SELECT id FROM users WHERE username = $1;', [username])
        return toCamelCase(rows)[0]
    }

    static async findByUsername(username) {

        const sql = 'SELECT * FROM get_user_data($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0]
    }

    static async findFriendCount(username) {

        const sql = 'SELECT * FROM get_friend_count($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)[0].getFriendCount
    }

    static async findUserFriends(id) {
        const sql = 'SELECT * FROM get_user_friends($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findUserCharacters(id) {
        const sql = 'SELECT * FROM get_user_characters($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findFollowing(id) {
        const sql = 'SELECT * FROM get_user_following($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }
    static async findFollowers(id) {
        const sql = 'SELECT * FROM get_user_followers($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findUserPosts(username) {
        const sql = 'SELECT * FROM get_user_posts($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    // Insert
    static async insert(username, email, password) {

        const sql = 'INSERT INTO users (username, email, password) VALUES ($1, $2, $3) RETURNING *;'
        const { rows } = await pool.query(sql, [username, email, password])
        return toCamelCase(rows)[0]
    }

    // Check user to user data
    static async checkUserToUser(lid, fid) {

        const sql = 'SELECT * FROM get_user_to_user_data($1, $2);'
        const { rows } = await pool.query(sql, [lid, fid])
        return toCamelCase(rows)[0]
    }

    // User follow interaction
    static async followInteraction(lid, uid, f) {

        const sql = 'SELECT * FROM user_follow_interaction($1, $2, $3);'
        const { rows } = await pool.query(sql, [lid, uid, f])
        return toCamelCase(rows)[0]
    }

    // static async update(id, username, bio) {

    //     const sql = 'UPDATE users SET username = $1, bio = $2 WHERE id = $3 RETURNING *;'
    //     const { rows } = await pool.query(sql, [username, bio, id])
    //     return toCamelCase(rows)[0]
    // }

    // static async delete(id) {

    //     const sql = 'DELETE FROM users WHERE id = $1 RETURNING *;'
    //     const { rows } = await pool.query(sql, [id])
    //     return toCamelCase(rows)[0]
    // }

    // static async count() {

    //     const { rows } = await pool.query('SELECT COUNT(*) FROM users;')
    //     return parseInt(rows[0].count)
    // }


    // router.use(checkAuth)
}

module.exports = User