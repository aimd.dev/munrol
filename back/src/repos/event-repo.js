const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class Event {

    // static async findPopular() {
        
    //     const { rows } = await pool.query('SELECT * FROM worlds;')
    //     return toCamelCase(rows)
    // }

    // static async findTrending() {
        
    //     const { rows } = await pool.query('SELECT * FROM worlds;')
    //     return toCamelCase(rows)
    // }

    static async findRecent() {

        const sql = 'SELECT * FROM recent_events ORDER BY created_at DESC LIMIT 2;'
        const { rows } = await pool.query(sql)
        return toCamelCase(rows)
    }

    static async findByWorldId(id) {

        const sql = 'SELECT * FROM recent_events WHERE world_id = $1 ORDER BY created_at DESC LIMIT 3;'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findByUrl(url) {

        const sql = 'SELECT * FROM get_event_data($1);'
        const { rows } = await pool.query(sql, [url])
        return toCamelCase(rows)[0]
    }

    static async findEventThreads(id, qty = 10) {
        const sql = 'SELECT * FROM get_event_threads($1, $2);'
        const { rows } = await pool.query(sql, [id, qty])
        return toCamelCase(rows)
    }

    // static async insert(title, url, picture, description, created_by, tags) {

    //     const sql = 'INSERT INTO worlds (username, email, password) VALUES ($1, $2, $3) RETURNING *;'
    //     const { rows } = await pool.query(sql, [title, url, picture, description, created_by, tags])
    //     return toCamelCase(rows)[0]
    // }
}

module.exports = Event