const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')
const urlGen = require('./utils/url-gen')


class World {

    static async findPopular(qty = 4) {
        
        const { rows } = await pool.query('SELECT * FROM get_popular_worlds($1);', [qty])
        return toCamelCase(rows)
    }

    static async findTrending(qty = 4) {
        
        const { rows } = await pool.query('SELECT * FROM get_trending_worlds($1);', [qty])
        return toCamelCase(rows)
    }

    static async findUrl(url) {

        const { rows } = await pool.query('SELECT id FROM worlds WHERE url = $1;', [url])
        return toCamelCase(rows)[0]
    }

    static async findByUrl(url) {

        const sql = 'SELECT * FROM get_world_data($1);'
        const { rows } = await pool.query(sql, [url])
        return toCamelCase(rows)[0]
    }

    static async findPostsInWorld(id, qty = 4) {

        const sql = 'SELECT * FROM get_world_recent_posts($1, $2);'
        const { rows } = await pool.query(sql, [id, qty])
        return toCamelCase(rows)
    }

    static async findWorldModeration(id) {

        const sql = 'SELECT * FROM get_world_moderation_data($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findWorldCharacters(id) {

        const sql = 'SELECT * FROM get_world_characters($1);'
        const { rows } = await pool.query(sql, [id])
        return toCamelCase(rows)
    }

    static async findByUser(username) {

        const sql = 'SELECT title, picture, tags, url FROM get_user_worlds($1);'
        const { rows } = await pool.query(sql, [username])
        return toCamelCase(rows)
    }

    static async checkMembership(uid, wid) {

        const sql = 'SELECT * FROM get_user_in_world_data($1, $2);'
        const { rows } = await pool.query(sql, [uid, wid])
        return toCamelCase(rows)[0].isMember
    }

    static async checkModeration(uid, wid) {
        
        const sql = 'SELECT * FROM get_user_in_world_moderation_data($1, $2);'
        const { rows } = await pool.query(sql, [uid, wid])
        return toCamelCase(rows)[0]
    }

    static async checkLiked(uid, wid) {

        const sql = 'SELECT * FROM get_user_in_world_like_data($1, $2);'
        const { rows } = await pool.query(sql, [uid, wid])
        return toCamelCase(rows)[0].hasLiked
    }

    static async likeInteraction(uid, wid, like) {

        const sql = 'SELECT * FROM world_like_interaction($1, $2, $3);'
        const { rows } = await pool.query(sql, [uid, wid, like])
        return toCamelCase(rows)
    }

    static async joinInteraction(uid, wid, join) {

        const sql = 'SELECT * FROM world_join_interaction($1, $2, $3);'
        const { rows } = await pool.query(sql, [uid, wid, join])
        return toCamelCase(rows)
    }

    static async insert(title, picture, description, created_by, tags, user_id) {

        const client = await pool.connect()
        const url = urlGen(title)

        try {
            // start transaction
            await client.query('BEGIN')

            // 1. create world
            const { rows } = await client.query(
                `INSERT INTO worlds (title, url, picture, description, created_by) VALUES ($1, $2, $3, $4, $5) RETURNING id;`,
                [title, url, picture, description, created_by]
            )
            const id = rows[0].id
            // 2. create tags if they don't exist
            let tagsql = ''
            tags.forEach((t,i) => {
                tagsql += `($1, $${i+2})${i === tags.length - 1 ? ' ON CONFLICT (title) DO NOTHING;' : ', '}`
            })
    
            let vars = [...tags]
            vars.unshift(created_by)
            
            await client.query(
                `INSERT INTO tags (created_by, title) VALUES ${tagsql}`,
                vars
            )
    
            // 3. link world and tags
            let linksql = ''
            tags.forEach((t,i) => {
                linksql += `($1, $${i+2})${i === tags.length - 1 ? ';' : ', '}`
            })
            vars[0] = id;
            await client.query(
                `INSERT INTO world_tags (world_id, tag_title) VALUES ${linksql}`,
                vars
            )
    
            // 4. Link creator to world
            await client.query('INSERT INTO world_users (world_id, user_id) VALUES ($1, $2)', [id, user_id])  
            
            // 5. Make creator admin
            await client.query('INSERT INTO world_moderation (world_id, user_id, is_admin) VALUES ($1, $2, $3)', [id, user_id, true]) 
    
            await client.query('COMMIT;')
    
            return { message: "World created successfully" };
    
        } catch (error) {
            console.log('error in transaction')
            client.query('ROLLBACK')
    
            console.log(error.code)
            console.log(error.detail)
            console.log(error.message)
    
            throw new Error(error.code)
        } finally {
            client.release()
        }

    }
}

module.exports = World