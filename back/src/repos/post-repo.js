const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class Post {


    static async findRecent() {
        const sql = 'SELECT * FROM full_posts_data ORDER BY created_at DESC LIMIT 10;'
        const { rows } = await pool.query(sql)
        return toCamelCase(rows)
    }
}

module.exports = Post