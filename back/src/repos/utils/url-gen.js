const urlGen = (string) => {
    return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').toLowerCase();
}

module.exports = urlGen