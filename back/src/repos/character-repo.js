const pool = require('../pool')
const toCamelCase = require('./utils/to-camel-case')


class Character {

    static async findByUrl(url) {
        const sql = 'SELECT * FROM get_character_data($1);'
        const { rows } = await pool.query(sql, [url])
        return toCamelCase(rows)[0]
    }
}

module.exports = Character