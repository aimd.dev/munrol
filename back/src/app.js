const express = require('express')
const server = require('http')
const socket = require('socket.io')

const usersRouter = require('./routes/user-routes')
const worldsRouter = require('./routes/world-routes')
const tagsRouter = require('./routes/tag-routes')
const eventsRouter = require('./routes/event-routes')
const threadsRouter = require('./routes/thread-routes')
const postsRouter = require('./routes/post-routes')
const characterRouter = require('./routes/character-routes')

const bcrypt = require('bcrypt')

module.exports = () => {

    const app = express()
    const http = server.createServer(app)
    const io = socket(http)
    
    // Cors headers
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
        next()
    })

    // Routing
    app.use(express.json())
    app.use(usersRouter)
    app.use(worldsRouter)
    app.use(tagsRouter)
    app.use(eventsRouter)
    app.use(threadsRouter)
    app.use(postsRouter)
    app.use(characterRouter)

    // Sockets
    io.on('connection', (socket) => {
        console.log('a user connected');
        
        socket.on('disconnect', () => {
            console.log('user disconnected');
        });

        socket.on('my message', (msg) => {
            console.log('message: ' + msg);
        });
    });

    http.listen(3000)
    return app
}