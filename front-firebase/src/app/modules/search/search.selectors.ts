import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromSearch from "./search.reducer";

export const selectSearchState = createFeatureSelector<fromSearch.State>('search')

export const selectCurrentThreads = createSelector(selectSearchState, (search) => search.threads)