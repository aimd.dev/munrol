import { Injectable } from '@angular/core';
import { constants } from '@app/core/config/constants';
import { LoaderStateEnum } from '@app/shared/models';
import { AlertsService } from '@app/shared/services/alerts.service';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { EMPTY } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { ThreadBrief } from '../thread/models/ThreadBrief.model';
import * as SearchActions from "./search.actions";
import { SearchService } from './search.service';
import { SearchUiService } from './services/search-ui.service';

@Injectable()
export class SearchEffects {

    constructor(
        private actions$: Actions,
        private store: Store<any>,
        private service: SearchService,
        private ui: SearchUiService,
        private alert: AlertsService
    ) { }

    getThreads$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchActions.getThreads),
            tap((action) => {
                if (!action.last) this.store.dispatch(SearchActions.reset())
                this.ui.setGetLoading(LoaderStateEnum.ON)
            }),
            exhaustMap((action) => this.service.getThreads(action.tag, action.last)
                .pipe(
                    tap((data) => {
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                    }),
                    map((data) => {
                        const threads = data.docs.map((r) => {
                            const thread = r.data() as ThreadBrief
                            thread.id = r.id
                            return thread
                        })
                        if (threads.length === 0 || threads.length < constants.pageLength) {
                            this.ui.setEndOfDocument(true)
                        }
                        return SearchActions.getThreadsSuccess({ threads })
                    }),
                    catchError((err) => {
                        console.log(err);
                        this.alert.error('No se ha podido obtener el hilo')
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                        return EMPTY
                    })
                )
            )
        )
    )

    reset$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchActions.reset),
            tap(() => {
                this.ui.setEndOfDocument(false)
            })
        ), { dispatch: false }
    )

}
