import { Injectable } from '@angular/core'
import { query, getDocs, orderBy, limit, startAfter, where, collection, Firestore } from '@angular/fire/firestore'
import { constants } from '@app/core/config/constants'
import { from } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    /**
     * Constructor del servicio
     * @param firestore firebase firestore
     */
    constructor(
        private firestore: Firestore
    ) { }
    /**
     * Busqueda de hilos por tag
     * @param tag tag
     * @param last last fetched
     * @returns threads
     */
    getThreads(tag: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        where('tags', 'array-contains-any', [tag, tag.toLowerCase()]),
                        orderBy('createdAt', 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        where('tags', 'array-contains-any', [tag, tag.toLowerCase()]),
                        orderBy('createdAt', 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }
}
