import { createAction, props } from "@ngrx/store"
import { ThreadBrief } from "../thread/models/ThreadBrief.model"

export const getThreads = createAction(
    '[Search Page] Get Threads', 
    props<{tag:string, last?:any}>() 
)

export const getThreadsSuccess = createAction(
    '[Search Page] Get Threads Success', 
    props<{threads:ThreadBrief[]}>() 
)

export const reset = createAction(
    '[Search Page] Reset State'
)