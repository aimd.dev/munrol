import { Injectable } from '@angular/core';
import { loaderState, LoaderStateEnum } from '@app/shared/models';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchUiService {

  private _getLoading$:BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.ON)
  getLoading$:Observable<loaderState> = this._getLoading$.asObservable()

  private _endOfDocument$:Subject<boolean> = new Subject<boolean>()
  endOfDocument$:Observable<boolean> = this._endOfDocument$.asObservable()

  constructor() { }

  setGetLoading(state:loaderState) {
    this._getLoading$.next(state)
  }

  setEndOfDocument(state:boolean) {
    this._endOfDocument$.next(state)
  }
}
