import { Action, createReducer, on } from '@ngrx/store';
import { ThreadBrief } from '../thread/models/ThreadBrief.model';
import * as SearchActions from "./search.actions";

export interface State {
    threads: ThreadBrief[] | null
}

export const initialState: State = {
    threads: null
};

export const searchReducer = createReducer(

  initialState,

    on(SearchActions.getThreadsSuccess,
        (state, action) => {
            const reference = [...(state.threads ? state.threads : [])]
            return {
                ...state,
                threads: reference.concat(action.threads)
            }
        }
    ),

    on(SearchActions.reset,
        (state, action) => initialState
    )

)
