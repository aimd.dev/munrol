import { Component, OnDestroy, OnInit } from '@angular/core'
import { ThreadBrief } from '@app/modules/thread/models/ThreadBrief.model'
import { loaderState } from '@app/shared/models'
import { Store } from '@ngrx/store'
import * as SearchActions from '../search.actions'
import { combineLatest, Observable, of, Subject } from 'rxjs'
import { filter, map, share, takeUntil, tap } from 'rxjs/operators'
import { selectCurrentThreads } from '../search.selectors'
import { SearchUiService } from '../services/search-ui.service'
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
    selector: 'mr-search-page',
    templateUrl: './search-page.component.html',
    styles: [
    ]
})
export class SearchPageComponent implements OnInit, OnDestroy {

    form: FormGroup

    last: string | undefined

    tag$: Observable<string>
    threads$: Observable<ThreadBrief[] | null>

    loading$: Observable<loaderState>
    unsubscriber$: Subject<void> = new Subject<void>()

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private store: Store<any>,
        public ui: SearchUiService
    ) { }

    ngOnInit(): void {

        this.form = this.fb.group({
            query: ['', [Validators.required]]
        })

        this.threads$ = this.store.select(selectCurrentThreads)
            .pipe(
                share(),
                tap((data) => { data && data.length ? this.last = data[data.length - 1].createdAt : this.last = undefined }),
                map((data) => data)
            )

        this.tag$ = this.route.queryParams
            .pipe(
                filter(params => params && params.q),
                tap(params => {
                    this.query.setValue(params.q)
                    this.store.dispatch(SearchActions.getThreads({ tag: params.q }))
                }),
                map(params => params.q)
            )

        this.loading$ = combineLatest([of(false), this.ui.getLoading$])
            .pipe(
                takeUntil(this.unsubscriber$),
                map(([_, service]) => service)
            )
    }

    get query() { return this.form.get('query') as FormControl }

    search() {
        const query: string = this.query.value.trim()
        if (query !== '') this.router.navigate(['.'], { queryParams: { q: query }, relativeTo: this.route })
    }

    loadMore() {
        this.store.dispatch(SearchActions.getThreads({ tag: '', last: this.last }))
    }

    reset() {
        this.store.dispatch(SearchActions.reset())
    }

    ngOnDestroy(): void {
        this.reset()
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
