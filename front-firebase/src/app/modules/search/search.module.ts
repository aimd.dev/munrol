import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchRoutingModule } from './search-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './search.effects';
import { StoreModule } from '@ngrx/store';
import { searchReducer } from './search.reducer';
import { SearchPageComponent } from './search-page/search-page.component';
import { SharedModule } from '@app/shared/shared.module';



@NgModule({
  declarations: [
    SearchPageComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    StoreModule.forFeature('search', searchReducer),
    EffectsModule.forFeature([SearchEffects])
  ]
})
export class SearchModule { }
