import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NewThreadComponent } from './components/new-thread/new-thread.component';
import { ThreadPageComponent } from './components/thread-page/thread-page.component';
import { EditThreadComponent } from './components/edit-thread/edit-thread.component';
import { AuthGuard } from '@app/core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'new',
    component: NewThreadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: EditThreadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':id',
    component: ThreadPageComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ThreadRoutingModule { }
