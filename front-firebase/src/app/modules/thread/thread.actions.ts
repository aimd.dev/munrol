import { File } from '@app/shared/models'
import { createAction, props } from '@ngrx/store'
import { OrderByDirection } from 'firebase/firestore'
import { Comment } from './models/Comment.model'
import { NewComment } from './models/NewComment.model'
import { NewReply } from './models/NewReply.model'
import { NewThread } from './models/NewThread.model'
import { Reply } from './models/Reply.model'
import { ReplyUpdate } from './models/ReplyUpdate.model'
import { Thread } from './models/Thread.model'
import { ThreadPush } from './models/ThreadPush'
import { ThreadUpdate } from './models/ThreadUpdate.model'

export const createThread = createAction(
    '[New Thread Page] Create Thread',
    props<{thread: NewThread}>()
)

export const getThread = createAction(
    '[Thread Page] Get Thread',
    props<{id: string}>()
)

export const getThreadSuccess = createAction(
    '[Thread Page] Get Thread Success',
    props<{thread: Thread}>()
)

export const updateThread = createAction(
    '[Thread Edit Page] Update Thread',
    props<{id: string, thread: ThreadUpdate}>()
)

export const getPage = createAction(
    '[Thread Page] Get Reply Page',
    props<{index: number, paginate?: boolean}>()
)

export const getReplies = createAction(
    '[Thread Page] Get Thread Replies',
    props<{ids: string[]}>()
)

export const getRepliesSuccess = createAction(
    '[Thread Page] Get Thread Replies Success',
    props<{replies: Reply[]}>()
)

export const updateThreadState = createAction(
    '[Thread Page] Update Thread State',
    props<{state: ThreadPush}>()
)

export const addParticipant = createAction(
    '[Thread Page] Add Participant',
    props<{username: string, thread: string}>()
)

export const updateParticipants = createAction(
    '[Thread Page] Update Participants',
    props<{participants: string[], thread: string, diff: string[], author: string}>()
)

export const updateParticipantsSuccess = createAction(
    '[Thread Page] Update Participants Success',
    props<{participants: string[]}>()
)

export const addParticipantSuccess = createAction(
    '[Thread Page] Add Participant Success',
    props<{username: string}>()
)

export const postReply = createAction(
    '[Thread Page] Post Reply',
    props<{reply: NewReply}>()
)

export const postReplySuccess = createAction(
    '[Thread Page] Post Reply Success',
    props<{reply: Reply}>()
)

export const updateReply = createAction(
    '[Thread Page] Update Reply',
    props<{id: string, reply: ReplyUpdate}>()
)

export const updateReplySuccess = createAction(
    '[Thread Page] Update Reply',
    props<{id: string, reply: ReplyUpdate}>()
)

export const deleteReply = createAction(
    '[Thread Page] Delete Reply',
    props<{reply: Reply}>()
)

export const removeParticipant = createAction(
    '[Thread Page] Remove Participant',
    props<{username: string, thread: string}>()
)

export const deleteReplySuccess = createAction(
    '[Thread Page] Delete Reply Success',
    props<{id: string, thread: string}>()
)

export const updatePages = createAction(
    '[Thread Page] Update Pages',
    props<{total: number}>()
)

export const updatePagination = createAction(
    '[Thread Page] Update Pagination',
    props<{pages: number[]}>()
)

export const sendInviteNotifications = createAction(
    '[New Thread Page] Send Invite Notification',
    props<{usernames: string[], thread: string, author: string}>()
)

export const linkFiles = createAction(
    '[Thread Page] Link Files To Thread',
    props<{thread: string, files: string[]}>()
)

export const linkFilesSuccess = createAction(
    '[Thread Page] Link Files To Thread Success',
    props<{files: File[]}>()
)

export const getFiles = createAction(
    '[Thread Page] Get Thread Files',
    props<{ids: string[]}>()
)

export const getFilesSuccess = createAction(
    '[Thread Page] Get Thread Files Success',
    props<{files: File[]}>()
)

export const getReference = createAction(
    '[Content] Get Referenced File',
    props<{id: string}>()
)

export const getReferenceSuccess = createAction(
    '[Content] Get Referenced File Success',
    props<{file: File}>()
)

export const kickParticipant = createAction(
    '[Thread Page] Kick Participant',
    props<{participant: string, thread: string}>()
)

export const kickParticipantError = createAction(
    '[Thread Page] Kick Participant Error',
    props<{participant: string}>()
)

export const unlinkFile = createAction(
    '[Thread Page] Unlink File',
    props<{file: string, thread: string}>()
)

export const unlinkFileSuccess = createAction(
    '[Thread Page] Unlink File Success',
    props<{file: string}>()
)

export const getComments = createAction(
    '[Thread Page] Get Comments',
    props<{thread: string, order: OrderByDirection, last?: string}>()
)

export const getCommentsSuccess = createAction(
    '[Thread Page] Get Comments Success',
    props<{comments: Comment[]}>()
)

export const postComment = createAction(
    '[Thread Page] Post Comment',
    props<{comment: NewComment}>()
)

export const postCommentSuccess = createAction(
    '[Thread Page] Post Comment Success',
    props<{comment: Comment}>()
)

export const editComment = createAction(
    '[Thread Page] Edit Comment',
    props<{id: string, comment: Comment}>()
)

export const removeComment = createAction(
    '[Thread Page] Remove Comment',
    props<{comment: Comment}>()
)

export const removeCommentSuccess = createAction(
    '[Thread Page] Remove Comment Success',
    props<{comment: string}>()
)

export const resetComments = createAction(
    '[Thread Page] Reset Comments'
)

export const closeThread = createAction(
    '[Thread Page] Close Thread',
    props<{id: string}>()
)

export const closeThreadSuccess = createAction(
    '[Thread Page] Close Thread Success'
)

export const openThread = createAction(
    '[Thread Page] Open Thread',
    props<{id: string}>()
)

export const openThreadSuccess = createAction(
    '[Thread Page] Open Thread Success'
)
