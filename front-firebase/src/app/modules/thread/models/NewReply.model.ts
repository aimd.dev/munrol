export interface NewReply {
    author: string
    body: string
    createdAt: string
    thread: string
    owner: string
}
