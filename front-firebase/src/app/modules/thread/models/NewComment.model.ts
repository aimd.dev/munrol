export interface NewComment {
    author: string
    body: string
    createdAt: string,
    mentions: string[]
    thread: string
}
