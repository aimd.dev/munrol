export interface Comment {
    id: string
    author: string
    createdAt: string
    lastEdit?: string
    body: string
    thread: string
    mentions: string[]
}
