export interface ReplyUpdate {
    body: string
    lastEdit: string
}
