import { Reply } from './Reply.model'

export interface ThreadPush {
    lastUpdated: string
    linkedReplies: Reply[]
    nowTyping: string[]
}
