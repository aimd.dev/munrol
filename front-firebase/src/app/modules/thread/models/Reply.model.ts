export interface Reply {
    id: string
    author: string
    body: string
    createdAt: string
    lastEdit: string
    thread: string
    isAuthor: boolean
    owner: string
    isOwner: boolean
}
