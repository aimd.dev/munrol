import { ReplyReference } from './ReplyReference.model'
import { ThreadFormat } from './thread-format.enum'

export interface ThreadBrief {
    id: string
    title?: string
    summary: string
    body: string
    nsfw: boolean
    open: boolean
    restricted: boolean
    author: string
    createdAt: string
    lastEdit?: string
    lastUpdated?: string
    tags?: string[]
    participants: string[]
    replyCount: number
    commentCount: number
    linkedReplies: ReplyReference[]
    format: ThreadFormat
}
