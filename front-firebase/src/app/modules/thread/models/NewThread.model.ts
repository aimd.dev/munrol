import { ThreadFormat } from './thread-format.enum'

export interface NewThread {
    title?: string
    summary: string
    body: string
    nsfw: boolean
    open: boolean
    restricted: boolean
    author: string
    createdAt: string
    tags?: string[]
    participants: string[]
    linkedFiles: string[]
    commentCount: number
    replyCount: number
    format: ThreadFormat
}
