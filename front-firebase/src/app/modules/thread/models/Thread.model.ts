import { Reply } from './Reply.model'
import { ThreadFormat } from './thread-format.enum'

export interface Thread {
    id: string
    title?: string
    summary: string
    body: string
    nsfw: boolean
    open: boolean
    restricted: boolean
    author: string
    createdAt: string
    lastEdit?: string
    lastUpdated?: string
    tags?: string[]
    participants: string[]
    linkedReplies: Reply[]
    replies: Reply[]
    linkedFiles: string[]
    commentCount: number
    nowTyping: string[]
    format: ThreadFormat

    replyCount: number
    saved?: boolean
    following?: boolean
    isParticipant: boolean
    isAuthor: boolean
}
