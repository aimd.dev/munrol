export interface ReplyReference {
    id: string
    createdAt: string
}
