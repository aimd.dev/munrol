import { ThreadFormat } from './thread-format.enum'

export interface ThreadUpdate {
    title: string
    summary: string
    body: string
    nsfw: boolean
    participants: string[]
    tags: string[]
    restricted: boolean
    open: boolean
    lastEdit: string
    format: ThreadFormat
}
