import { Injectable } from '@angular/core'
import { doc, getDocs, where, collection, query, updateDoc, getDoc, documentId, arrayRemove, arrayUnion, Firestore, writeBatch, orderBy, OrderByDirection, startAfter, limit, increment, docSnapshots } from '@angular/fire/firestore'
import { constants } from '@app/core/config/constants'
import { Store } from '@ngrx/store'
import * as ThreadActions from './thread.actions'
import { setLogLevel } from 'firebase/firestore'
import { from, Subscription } from 'rxjs'
import { Comment } from './models/Comment.model'
import { NewComment } from './models/NewComment.model'
import { NewReply } from './models/NewReply.model'
import { NewThread } from './models/NewThread.model'
import { Reply } from './models/Reply.model'
import { ReplyUpdate } from './models/ReplyUpdate.model'
import { ThreadPush } from './models/ThreadPush'
import { ThreadUpdate } from './models/ThreadUpdate.model'


@Injectable({
    providedIn: 'root'
})
export class ThreadService {

    push: Subscription

    constructor(
        private firestore: Firestore,
        private store: Store<any>
    ) {
        // setLogLevel('debug')
    }

    createThread(thread: NewThread) {
        const batch = writeBatch(this.firestore)
        const id = doc(collection(this.firestore, 'threads')).id
        batch.set(doc(this.firestore, `threads/${id}`), thread)
        batch.set(doc(this.firestore, `threadReplies/${id}`), {linkedReplies: []})
        batch.update(doc(this.firestore, `users/${thread.author}`), { threadCount: increment(1) })
        if (thread.linkedFiles.length) {
            thread.linkedFiles.forEach(f => {
                batch.update(doc(this.firestore, `files/${f}`), { linkedThreads: arrayUnion(id) })
            })
        }
        return from(batch.commit().then(() => id))
    }

    updateThread(id: string, thread: ThreadUpdate) {
        return from(updateDoc(doc(this.firestore, `threads/${id}`), {
            ...thread
        }))
    }

    updateParticipants(participants: string[], thread: string) {
        return from(updateDoc(doc(this.firestore, `threads/${thread}`), {
            participants: [ ...participants ]
        }))
    }

    getThread(id: string) {
        return from(getDoc(doc(this.firestore, `threads/${id}`)))
    }

    threadSubscription(thread: string) {
        // unsubscribe
        if (this.push) this.push.unsubscribe()
        // init listener
        this.push = docSnapshots(doc(this.firestore, `threadReplies/${thread}`))
        .subscribe(data => {
            this.store.dispatch(ThreadActions.updateThreadState({
                state: data.data() as ThreadPush
            }))
        })
    }

    endPush() {
        this.push.unsubscribe()
    }

    getReplies(ids: string[]) {
        return from(getDocs(query(collection(this.firestore, 'replies'), where(documentId(), 'in', ids))))
    }

    addParticipant(username: string, thread: string) {
        return from(updateDoc(doc(this.firestore, `threads/${thread}`), { participants: arrayUnion(username)}))
    }

    postReply(reply: NewReply) {
        const batch = writeBatch(this.firestore)
        const id = doc(collection(this.firestore, 'replies')).id
        const replyReference = { id: id, createdAt: reply.createdAt }
        batch.set(doc(this.firestore, `replies/${id}`), reply)
        batch.update(doc(this.firestore, `users/${reply.author}`), { replyCount: increment(1) })
        batch.update(doc(this.firestore, `threads/${reply.thread}`), { replyCount: increment(1) })
        batch.update(doc(this.firestore, `threadReplies/${reply.thread}`), { linkedReplies: arrayUnion(replyReference), lastUpdated: reply.createdAt })
        return from(batch.commit().then(() => ({ ...reply, id })))
    }

    updateReply(id: string, reply: ReplyUpdate) {
        return from(updateDoc(doc(this.firestore, `replies/${id}`), { ...reply }))
    }

    deleteReply(reply: Reply) {
        const replyReference = { id: reply.id, createdAt: reply.createdAt }
        const batch = writeBatch(this.firestore)
        batch.delete(doc(this.firestore, `replies/${reply.id}`))
        if (reply.author !== 'deleted') {
            batch.update(doc(this.firestore, `users/${reply.author}`), { replyCount: increment(-1) })
        }
        if (reply.thread !== 'deleted') {
            batch.update(doc(this.firestore, `threads/${reply.thread}`), { replyCount: increment(-1) })
            batch.update(doc(this.firestore, `threadReplies/${reply.thread}`), { linkedReplies: arrayRemove(replyReference) })
        }
        return from(batch.commit())
    }

    removeParticipant(username: string, thread: string) {
        return from(updateDoc(doc(this.firestore, `threads/${thread}`), { participants: arrayRemove(username) }))
    }

    async sendInviteNotifications(usernames: string[], thread: string, author: string) {

        const participants = [ ...usernames ]

        while (participants.length) {
            const batch = writeBatch(this.firestore)
            const piece = participants.splice(0, 20)
            piece.forEach((p) => {
                batch.update(doc(this.firestore, `notifications/${p}`), { newInvites: true })
                batch.set(doc(this.firestore, `notifications/${p}/invites/${thread}`), {
                    createdAt: new Date().toISOString(),
                    author: author
                })
            })
            await batch.commit()
        }
    }

    linkFiles(id: string, files: string[]) {
        const batch = writeBatch(this.firestore)

        batch.update(doc(this.firestore, `threads/${id}`), { linkedFiles: arrayUnion(...files) })
        files.forEach(f => {
            batch.update(doc(this.firestore, `files/${f}`), { linkedThreads: arrayUnion(id) })
        })

        return from(batch.commit())
    }

    getFiles(ids: string[]) {
        return from(getDocs(query(collection(this.firestore, 'files'), where(documentId(), 'in', ids))))
    }

    getReference(id: string) {
        return from(getDoc(doc(this.firestore, `files/${id}`)))
    }

    kickParticipant(participant: string, thread: string) {
        return from(updateDoc(doc(this.firestore, `threads/${thread}`), {
            participants: arrayRemove(participant)
        }))
    }

    unlinkFile(file: string, thread: string) {
        const batch = writeBatch(this.firestore)
        batch.update(doc(this.firestore, `threads/${thread}`), { linkedFiles: arrayRemove(file) })
        batch.update(doc(this.firestore, `files/${file}`), { linkedThreads: arrayRemove(file) })
        return from(batch.commit())
    }

    getComments(thread: string, order: OrderByDirection, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'comments'),
                        where('thread', '==', thread),
                        orderBy('createdAt', order),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'comments'),
                        where('thread', '==', thread),
                        orderBy('createdAt', order),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }

    postComment(comment: NewComment) {
        const batch = writeBatch(this.firestore)
        const id = doc(collection(this.firestore, 'comments')).id
        batch.set(doc(this.firestore, `comments/${id}`), comment)
        batch.update(doc(this.firestore, `users/${comment.author}`), { commentCount: increment(1) })
        batch.update(doc(this.firestore, `threads/${comment.thread}`), { commentCount: increment(1) })
        return from(batch.commit().then(() => ({ ...comment, id })))
    }

    editComment(id: string, comment: Comment) {
        return from(updateDoc(doc(this.firestore, `comments/${id}`), { ...comment }))
    }

    removeComment(comment: Comment) {
        const batch = writeBatch(this.firestore)
        batch.delete(doc(this.firestore, `comments/${comment.id}`))
        batch.update(doc(this.firestore, `users/${comment.author}`), { commentCount: increment(-1) })
        if (comment.thread !== 'deleted') batch.update(doc(this.firestore, `threads/${comment.thread}`), { commentCount: increment(-1) })
        return from(batch.commit())
    }

    deleteThread(id: string, author: string) {
        const batch = writeBatch(this.firestore)
        batch.delete(doc(this.firestore, `threads/${id}`))
        batch.update(doc(this.firestore, `users/${author}`), { threadCount: increment(-1) })
        return batch.commit()
    }

    closeThread(id: string) {
        return from(updateDoc(doc(this.firestore, `threads/${id}`), { open: false }))
    }

    openThread(id: string) {
        return from(updateDoc(doc(this.firestore, `threads/${id}`), { open: true }))
    }
}
