import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, ofType, createEffect, concatLatestFrom } from '@ngrx/effects'
import { catchError, delay, exhaustMap, filter, map, mergeMap, tap } from 'rxjs/operators'
import * as ThreadActions from './thread.actions'
import { ThreadService } from './thread.service'
import { EMPTY } from 'rxjs'
import { Store } from '@ngrx/store'
import { AlertsService } from '@app/shared/services/alerts.service'
import * as fromAuth from '../auth/auth.reducer'
import { Reply } from './models/Reply.model'
import { selectCurrentPages, selectThreadReplies } from './thread.selectors'
import { calculateVisiblePages } from '@app/shared/util/helpers/calculate-visible-pages'
import { ThreadUiService } from './services/thread-ui.service'
import { File, LoaderStateEnum } from '@app/shared/models'
import { Thread } from './models/Thread.model'
import { selectCurrentUserFiles } from '../auth/auth.selectors'
import { getDoc } from '@angular/fire/firestore'
import { Comment } from './models/Comment.model'
import { constants } from '@app/core/config/constants'

@Injectable()
export class ThreadEffects {

    constructor(
        private router: Router,
        private actions$: Actions,
        private service: ThreadService,
        private ui: ThreadUiService,
        private store: Store<fromAuth.State>,
        private alert: AlertsService
    ) {}

    createThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.createThread),
            tap(() => this.ui.setPostThreadLoading(LoaderStateEnum.ON)),
            mergeMap((action) => this.service.createThread(action.thread)
                .pipe(
                    tap((id) => {
                        const notifications: string [] = action.thread.participants.filter(p => p !== action.thread.author)
                        this.service.sendInviteNotifications(notifications, id, action.thread.author)
                        this.alert.success('Hilo creado con éxito')
                        this.router.navigate([`/threads/${id}`])
                        this.ui.setPostThreadLoading(LoaderStateEnum.OFF)
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se pudo crear el hilo')
                        this.ui.setPostThreadLoading(LoaderStateEnum.OFF)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    updateThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.updateThread),
            tap(() => this.ui.setPostThreadLoading(LoaderStateEnum.ON)),
            mergeMap((action) => this.service.updateThread(action.id, action.thread)
                .pipe(
                    tap(() => {
                        this.alert.success('Hilo actualizado con éxito')
                        this.router.navigate([`/threads/${action.id}`])
                        this.ui.setPostThreadLoading(LoaderStateEnum.OFF)
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se pudo actualizar el hilo')
                        this.ui.setPostThreadLoading(LoaderStateEnum.OFF)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    getThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getThread),
            exhaustMap((action) => this.service.getThread(action.id)
                .pipe(
                    map((doc, auth) => {
                        const data = doc.data() as Thread
                        data.id = doc.id
                        return ThreadActions.getThreadSuccess({ thread: data })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se ha podido obtener el hilo')
                        return EMPTY
                    })
                )
            )
        )
    )

    // getThreadSuccess$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(ThreadActions.getThreadSuccess),
    //         map((action) => ThreadActions.updatePages({ total: action.thread.linkedReplies.length }))
    //     )
    // )

    getReplies$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getReplies),
            exhaustMap((action) => this.service.getReplies(action.ids)
                .pipe(
                    map((data) => {
                        const replies = data.docs.map((r) => {
                            const reply = r.data() as Reply
                            reply.id = r.id
                            return reply
                        })
                        return ThreadActions.getRepliesSuccess({ replies: replies })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se han podido cargar las respuestas')
                        return EMPTY
                    })
                )
            )
        )
    )

    addParticipant$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.addParticipant),
            exhaustMap((action) => this.service.addParticipant(action.username, action.thread)
                .pipe(
                    map(() => ThreadActions.addParticipantSuccess({username: action.username})),
                    catchError((err) => {
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    updateParticipants$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.updateParticipants),
            exhaustMap((action) => this.service.updateParticipants(action.participants, action.thread)
                .pipe(
                    map(() => ThreadActions.updateParticipantsSuccess({ participants: action.participants })),
                    tap(() => {
                        this.alert.success('Participantes actualizados')
                        this.service.sendInviteNotifications(action.diff, action.thread, action.author)
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('Ha ocurrido un error')
                        return EMPTY
                    })
                )
            )
        )
    )

    postReply$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.postReply),
            tap(() => this.ui.setPostReplyLoading(LoaderStateEnum.ON)),
            exhaustMap((action) => this.service.postReply(action.reply)
                .pipe(
                    tap(() => {
                        this.alert.success('Tu respuesta ha sido publicada con éxito')
                        this.ui.setPostReplyLoading(LoaderStateEnum.OFF)
                        this.ui.clearEditor()
                    }),
                    map((reply) => ThreadActions.postReplySuccess({ reply: reply as Reply })),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se ha podido publicar tu respuesta')
                        this.ui.setPostReplyLoading(LoaderStateEnum.OFF)
                        return EMPTY
                    })
                )
            )
        )
    )

    postReplySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.postReplySuccess),
            delay(100),
            concatLatestFrom(() => this.store.select(selectCurrentPages)),
            tap(([res, pages]) => {
                this.router.navigate([`/threads/${res.reply.thread}`], { queryParams: { page: pages.length } })
            })
        ), { dispatch: false }
    )

    updateReply$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.updateReply),
            exhaustMap((action) => this.service.updateReply(action.id, action.reply)
                .pipe(
                    tap(() => {
                        this.alert.success('Tu respuesta ha sido actualizada')
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se ha podido actualizar tu respuesta')
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    deleteReply$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.deleteReply),
            exhaustMap((action) => this.service.deleteReply(action.reply)
                .pipe(
                    tap(() => {
                        this.alert.success('Tu respuesta ha sido eliminada')
                    }),
                    map(() => ThreadActions.deleteReplySuccess({ id: action.reply.id, thread: action.reply.thread })),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se ha podido eliminar tu respuesta')
                        return EMPTY
                    })
                )
            )
        )
    )

    deleteReplySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.deleteReplySuccess),
            delay(100),
            concatLatestFrom(() => this.store.select(selectCurrentPages)),
            tap(([res, pages]) => {
                this.router.navigate([`/threads/${res.thread}`], { queryParams: { page: pages.length } })
            })
        ), { dispatch: false }
    )

    removeParticipant$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.removeParticipant),
            exhaustMap((action) => this.service.removeParticipant(action.username, action.thread)
                .pipe(
                    tap(() => this.alert.success('Cambios guardados')),
                    catchError((err) => {
                        this.alert.error('Ha ocurrido un error')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    getPage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getPage),
            concatLatestFrom(() => this.store.select(selectCurrentPages)),
            filter(([action, pages]) => !!action.paginate),
            map(([action, pages]) => ThreadActions.updatePagination({pages: calculateVisiblePages(action.index, pages)}))
        )
    )

    sendInviteNotifications$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.sendInviteNotifications),
            map((action) => this.service.sendInviteNotifications(action.usernames, action.thread, action.author))
        ), { dispatch: false }
    )

    linkFiles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.linkFiles),
            exhaustMap((action) => this.service.linkFiles(action.thread, action.files)
                .pipe(
                    tap(() => {
                        this.alert.success('Vinculación exitosa')
                    }),
                    concatLatestFrom(() => this.store.select(selectCurrentUserFiles)),
                    map(([res, archive]) => {
                        const files = action.files.map(f => archive?.find(file => file.id === f) as File)
                        return ThreadActions.linkFilesSuccess({ files })
                    }),
                    catchError((err) => {
                        this.alert.error('Ha ocurrido un error')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    getFiles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getFiles),
            exhaustMap((action) => this.service.getFiles(action.ids)
                .pipe(
                    map((data) => {
                        const files = data.docs.map((r) => {
                            const file = r.data() as File
                            file.id = r.id
                            return file
                        })
                        return ThreadActions.getFilesSuccess({ files })
                    }),
                    catchError((err) => {
                        this.alert.error('Ha ocurrido un error')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    getReference$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getReference),
            exhaustMap((action) => this.service.getReference(action.id)
                .pipe(
                    map((data) => {
                        const file = data.data() as File
                        file.id = data.id
                        return ThreadActions.getReferenceSuccess({ file })
                    })
                )
            )
        )
    )

    kickParticipant$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.kickParticipant),
            exhaustMap((action) => this.service.kickParticipant(action.participant, action.thread)
                .pipe(
                    tap(() => this.alert.success('Participante eliminado')),
                    catchError((err) => {
                        this.alert.error('No se ha podido eliminar al participante')
                        ThreadActions.kickParticipantError({ participant: action.participant })
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    unlinkFile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.unlinkFile),
            exhaustMap((action) => this.service.unlinkFile(action.file, action.thread)
                .pipe(
                    tap(() => this.alert.success('La ficha ha sido removida del hilo')),
                    map(() => ThreadActions.unlinkFileSuccess({ file: action.file })),
                    catchError((err) => {
                        this.alert.error('No se ha podido desvincular la ficha')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    getComments$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.getComments),
            exhaustMap((action) => this.service.getComments(action.thread, action.order, action.last)
                .pipe(
                    map((data) => {
                        const comments = data.docs.map(c => {
                            const comment = c.data() as Comment
                            comment.id = c.id
                            return comment
                        })
                        return ThreadActions.getCommentsSuccess({ comments })
                    }),
                    catchError((err) => {
                        this.alert.error('No se han podido obtener los comentarios')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    postComment$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.postComment),
            exhaustMap((action) => this.service.postComment(action.comment)
                .pipe(
                    tap(() => {
                        this.alert.success('Comentario creado con éxito')
                        this.ui.clearCommentEditor()
                        this.ui.setPostCommentLoading(LoaderStateEnum.OFF)
                    }),
                    map((comment) => ThreadActions.postCommentSuccess({ comment })),
                    catchError((err) => {
                        this.alert.error('No se ha podido crear el comentario')
                        this.ui.setPostCommentLoading(LoaderStateEnum.OFF)
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    editComment$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.editComment),
            exhaustMap((action) => this.service.editComment(action.id, action.comment)
                .pipe(
                    tap(() => this.alert.success('Edición exitosa')),
                    catchError((err) => {
                        this.alert.error('No se ha podido editar el comentario')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    removeComment$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.removeComment),
            exhaustMap((action) => this.service.removeComment(action.comment)
                .pipe(
                    tap(() => this.alert.success('Comentario eliminado')),
                    map(() => ThreadActions.removeCommentSuccess({ comment: action.comment.id })),
                    catchError((err) => {
                        this.alert.error('No se han podido eliminar el comentario')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    closeThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.closeThread),
            exhaustMap((action) => this.service.closeThread(action.id)
                .pipe(
                    tap(() => this.alert.success('El hilo ha sido cerrado')),
                    map(() => ThreadActions.closeThreadSuccess()),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se pudo cerrar el hilo')
                        return EMPTY
                    })
                )
            )
        )
    )

    openThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThreadActions.openThread),
            exhaustMap((action) => this.service.openThread(action.id)
                .pipe(
                    tap(() => this.alert.success('El hilo ha sido abierto')),
                    map(() => ThreadActions.openThreadSuccess()),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se pudo abrir el hilo')
                        return EMPTY
                    })
                )
            )
        )
    )
}
