import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'
import { ThreadRoutingModule } from './thread-routing.module'
import { NewThreadComponent } from './components/new-thread/new-thread.component'
import { ThreadPageComponent } from './components/thread-page/thread-page.component'
import { EditThreadComponent } from './components/edit-thread/edit-thread.component'
import { SharedModule } from '@app/shared/shared.module'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { threadReducer } from './thread.reducer'
import { ThreadEffects } from './thread.effects'

import { ParticipantsModalComponent } from './components/modals/participants-modal/participants-modal.component'
import { ThreadActionsComponent } from './components/thread-actions/thread-actions.component'
import { ThreadHeaderComponent } from './components/thread-header/thread-header.component'
import { ThreadFooterComponent } from './components/thread-footer/thread-footer.component'
import { ThreadRepliesComponent } from './components/thread-replies/thread-replies.component'
import { ThreadPostReplyComponent } from './components/thread-post-reply/thread-post-reply.component'
import { UpgradeThreadComponent } from './components/upgrade-thread/upgrade-thread.component'
import { ThreadSidebarComponent } from './components/thread-sidebar/thread-sidebar.component'
import { ThreadReplyEditComponent } from './components/thread-reply-edit/thread-reply-edit.component'
import { ThreadReplyComponent } from './components/thread-reply/thread-reply.component'
import { ConfirmDeleteReplyModalComponent } from './components/modals/confirm-delete-reply-modal/confirm-delete-reply-modal.component'
import { LinkFileModalComponent } from './components/modals/link-file-modal/link-file-modal.component'
import { KickParticipantModalComponent } from './components/modals/kick-participant-modal/kick-participant-modal.component'
import { FileSidebarComponent } from './components/file-sidebar/file-sidebar.component'
import { CommentSidebarComponent } from './components/comment-sidebar/comment-sidebar.component'
import { PostCommentComponent } from './components/post-comment/post-comment.component'
import { DeleteCommentModalComponent } from './components/modals/delete-comment-modal/delete-comment-modal.component';
import { DeleteThreadModalComponent } from './components/modals/delete-thread-modal/delete-thread-modal.component';
import { NewThreadFilesComponent } from './components/modals/new-thread-files/new-thread-files.component'

@NgModule({
  declarations: [
    NewThreadComponent,
    ThreadPageComponent,
    EditThreadComponent,
    ParticipantsModalComponent,
    ThreadActionsComponent,
    ThreadHeaderComponent,
    ThreadFooterComponent,
    ThreadRepliesComponent,
    ThreadPostReplyComponent,
    UpgradeThreadComponent,
    ThreadSidebarComponent,
    ThreadReplyEditComponent,
    ThreadReplyComponent,
    ConfirmDeleteReplyModalComponent,
    LinkFileModalComponent,
    KickParticipantModalComponent,
    FileSidebarComponent,
    CommentSidebarComponent,
    PostCommentComponent,
    DeleteCommentModalComponent,
    DeleteThreadModalComponent,
    NewThreadFilesComponent
  ],
  imports: [
    CommonModule,
    ThreadRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    StoreModule.forFeature('thread', threadReducer),
    EffectsModule.forFeature([ThreadEffects])
  ]
})
export class ThreadModule { }
