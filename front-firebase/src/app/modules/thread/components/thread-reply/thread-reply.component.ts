import { Component, Input } from '@angular/core'
import { Store } from '@ngrx/store'
import * as ThreadActions from '../../thread.actions'
import { Reply } from '../../models/Reply.model'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { ConfirmDeleteReplyModalComponent } from '../modals/confirm-delete-reply-modal/confirm-delete-reply-modal.component'

@Component({
    selector: 'mr-thread-reply',
    templateUrl: './thread-reply.component.html',
    styles: [
    ]
})
export class ThreadReplyComponent {

    editing: boolean

    @Input() reply: Reply
    @Input() id: string
    @Input() linkedFiles: string[]

    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }

    openDeleteModal() {
        this.modal.open(ConfirmDeleteReplyModalComponent)
        .then((result) => {
            if (result) this.store.dispatch(ThreadActions.deleteReply({ reply: this.reply }))
        })
    }
}
