import { Component, Input } from '@angular/core'
import { Store } from '@ngrx/store'
import * as AuthActions from '@modules/auth/auth.actions'
import * as ThreadActions from '../../thread.actions'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { DeleteThreadModalComponent } from '../modals/delete-thread-modal/delete-thread-modal.component'

@Component({
    selector: 'mr-thread-actions',
    templateUrl: './thread-actions.component.html',
    styles: [
    ]
})
export class ThreadActionsComponent {

    @Input()
    id: string
    @Input()
    following: boolean
    @Input()
    saved: boolean
    @Input()
    isAuthor: boolean
    @Input()
    open: boolean
    @Input()
    author: string

    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }

    saveThread() {
        this.store.dispatch(AuthActions.saveThread({ id: this.id }))
    }

    unsaveThread() {
        this.store.dispatch(AuthActions.unsaveThread({ id: this.id }))
    }

    followThread() {
        this.store.dispatch(AuthActions.followThread({ id: this.id }))
    }

    unfollowThread() {
        this.store.dispatch(AuthActions.unfollowThread({ id: this.id }))
    }

    closeThread() {
        this.store.dispatch(ThreadActions.closeThread({ id: this.id }))
    }

    openThread() {
        this.store.dispatch(ThreadActions.openThread({ id: this.id }))
    }

    deleteThread() {
        this.modal.open(DeleteThreadModalComponent, [{ key: 'id', value: this.id }, { key: 'author', value: this.author }])
    }
}
