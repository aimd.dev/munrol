import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { combineLatest, Observable, of, Subject } from 'rxjs'
import { map, take, takeUntil } from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as fromAuth from '@modules/auth/auth.reducer'
import * as ThreadActions from '../../thread.actions'
import { selectCurrentUserAuth } from '@app/modules/auth/auth.selectors'

import { NewThread } from '../../models/NewThread.model'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { ThreadUiService } from '../../services/thread-ui.service'
import { constants } from '@app/core/config/constants'
import { UserAuth } from '@app/modules/auth/models'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'
import { ThreadFormat } from '../../models/thread-format.enum'


@Component({
    selector: 'mr-new-thread',
    templateUrl: './new-thread.component.html',
    styles: [
    ]
})
export class NewThreadComponent implements OnInit, OnDestroy {

    formats = ThreadFormat
    username: string
    form: FormGroup
    queuedFiles: string[] = []
    loading$: Observable<loaderState>
    unsubscriber$: Subject<void> = new Subject()

    public toolbar: typeof EditorToolbar = EditorToolbar

    constructor(
        private fb: FormBuilder,
        private store: Store<fromAuth.State>,
        private vldt: EditorBodyRequiredValidatorService,
        private ui: ThreadUiService
    ) { }

    ngOnInit(): void {
        this.store.select(selectCurrentUserAuth)
            .pipe(takeUntil(this.unsubscriber$), take(1))
            .subscribe(user => { if (user) this.username = (user as UserAuth).username })

        this.form = this.fb.group({
            author: [this.username, [Validators.required]],
            title: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(60)]],
            summary: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(constants.summaryMaxLength)]],
            body: ['', [this.vldt.editorBodyRequired()]],
            nsfw: [false],
            participants: [[this.username]],
            tags: [[]],
            restricted: [false],
            open: [true],
            format: [ThreadFormat.Rol]
        })

        this.loading$ = combineLatest([of(LoaderStateEnum.OFF), this.ui.postThreadLoading$])
            .pipe(
                takeUntil(this.unsubscriber$),
                map(([_, service]) => service)
            )

        this.restricted.valueChanges.subscribe(data => console.log(data))
    }

    get summary() { return this.form.get('summary') as FormControl }
    get restricted() { return this.form.get('restricted') as FormControl }

    submit(): void {
        this.form.markAllAsTouched()
        this.form.updateValueAndValidity()
        if (this.form.invalid) return

        const thread: NewThread = { ...this.form.value }
        thread.createdAt = new Date().toISOString()
        thread.linkedFiles = this.queuedFiles
        thread.commentCount = 0
        thread.replyCount = 0

        this.store.dispatch(ThreadActions.createThread({ thread }))
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
