import {
    Component,
    Input
} from '@angular/core'

@Component({
    selector: 'mr-thread-header',
    templateUrl: './thread-header.component.html',
    styles: [
    ]
})
export class ThreadHeaderComponent {
    /**
     * thread id
     */
    @Input()
    id: string
    /**
     * Thread title
     */
    @Input()
    title: string | undefined
    /**
     * Thread status
     */
    @Input()
    open: boolean
    /**
     * Thread privacy
     */
    @Input()
    restricted: boolean
    /**
     * Created at
     */
    @Input()
    createdAt: string
    /**
     * Last edit
     */
    @Input()
    lastEdit: string | undefined
    /**
     * Is author
     */
    @Input()
    isAuthor: boolean
    /**
     * is following
     */
    @Input()
    following: boolean
    /**
     * is saved
     */
    @Input()
    saved: boolean
    /**
     * Thread author
     */
    @Input()
    author: string
}
