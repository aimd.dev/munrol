import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'

import { Store } from '@ngrx/store'
import * as ThreadActions from '../../thread.actions'

import { NewReply } from '../../models/NewReply.model'
import { map, takeUntil } from 'rxjs/operators'
import { combineLatest, Observable, of, Subject } from 'rxjs'
import { ThreadUiService } from '../../services/thread-ui.service'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'
import { ThreadService } from '../../thread.service'

enum ThreadViews {
    OPEN_UNRESTRICTED = 'OPEN_UNRESTRICTED',
    OPEN_RESTRICTED_GUEST = 'OPEN_RESTRICTED_GUEST',
    OPEN_RESTRICTED_MEMBER = 'OPEN_RESTRICTED_MEMBER',
    APP_GUEST = 'APP_GUEST',
    CLOSED = 'CLOSED'
}
type viewKey = keyof typeof ThreadViews

@Component({
    selector: 'mr-thread-post-reply',
    templateUrl: './thread-post-reply.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThreadPostReplyComponent implements OnInit, OnDestroy {

    view: viewKey
    form: FormGroup

    localOpen: boolean
    localRestricted: boolean
    localIsParticipant: boolean
    localAuthor: string | null

    loading$: Observable<loaderState>

    unsubscriber$: Subject<void> = new Subject<void>()

    public toolbar: typeof EditorToolbar = EditorToolbar

    @Input() set open(open: boolean) {
        this.localOpen = open
        this.setView()
    }
    @Input() set restricted(restricted: boolean) {
        this.localRestricted = restricted
        this.setView()
    }
    @Input() set isParticipant(isParticipant: boolean) {
        this.localIsParticipant = isParticipant
        this.setView()
    }

    @Input() set author(author: string | null | undefined) {
        this.localAuthor = author ? author : null
        this.setView()
    }

    @Input() thread: string
    @Input() owner: string
    @Input() linkedFiles: string[]

    constructor(
        private fb: FormBuilder,
        private store: Store<any>,
        private service: ThreadService,
        private vldt: EditorBodyRequiredValidatorService,
        public ui: ThreadUiService
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            author: [this.localAuthor, [Validators.required]],
            body: ['', [this.vldt.editorBodyRequired()]],
            thread: [this.thread, [Validators.required]],
            owner: [this.owner, [Validators.required]]
        })

        this.loading$ = combineLatest([of(LoaderStateEnum.OFF), this.ui.postReplyLoading$])
            .pipe(
                takeUntil(this.unsubscriber$),
                map(([_, service]) => service)
            )

        this.ui.clearEditor$
            .pipe(takeUntil(this.unsubscriber$)).subscribe(() => this.body.setValue(''))

    }

    get body() { return this.form.get('body') as FormControl }

    setView(): void {
        if (this.localOpen) {

            if (this.localRestricted) {
                this.view = this.localIsParticipant ? ThreadViews.OPEN_RESTRICTED_MEMBER : ThreadViews.OPEN_RESTRICTED_GUEST
            } else {
                this.view = this.localAuthor ? ThreadViews.OPEN_UNRESTRICTED : ThreadViews.APP_GUEST
            }

        } else { this.view = ThreadViews.CLOSED }
    }

    send(): void {

        if (this.form.invalid || ! this.localAuthor) return

        const reply: NewReply = { ...this.form.value }
        reply.createdAt = new Date().toISOString()

        if (!this.localIsParticipant) this.store.dispatch(ThreadActions.addParticipant({ username: this.localAuthor, thread: this.thread }))
        this.store.dispatch(ThreadActions.postReply({ reply }))
    }

    ngOnDestroy() {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
