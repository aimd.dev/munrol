import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Store } from '@ngrx/store'
import { combineLatest, Observable } from 'rxjs'
import { Thread } from '../../models/Thread.model'
import * as fromThread from '../../thread.reducer'
import * as ThreadActions from '../../thread.actions'
import { selectCurrentThread, selectThreadFiles, selectThreadReplies } from '../../thread.selectors'
import { filter, map, skip } from 'rxjs/operators'
import { selectCurrentUserAuth, selectCurrentUserFollowing, selectCurrentUserSaved, selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'
import { UserAuth } from '@app/modules/auth/models'
import { SidebarService } from '@app/shared/components/sidebar/sidebar.service'
import { File } from '@app/shared/models'
import { Reply } from '../../models/Reply.model'
import { ThreadService } from '../../thread.service'
import { Title } from '@angular/platform-browser'

@Component({
    selector: 'mr-thread-page',
    templateUrl: './thread-page.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThreadPageComponent implements OnInit, OnDestroy {

    page$: Observable<number>

    files$: Observable<File[]>

    thread$: Observable<Thread | null>

    replies$: Observable<Reply[]>

    currentUser$: Observable<string | undefined>

    constructor(
        private route: ActivatedRoute,
        private store: Store<fromThread.State>,
        private sidebar: SidebarService,
        private service: ThreadService,
        private title: Title
    ) { }

    ngOnInit(): void {

        this.page$ = this.route.queryParams
        .pipe(
            map((params) => params && params.page ? Number(params.page) : 1)
        )

        this.files$ = this.store.select(selectThreadFiles)

        this.replies$ = this.store.select(selectThreadReplies)

        this.thread$ = combineLatest([
            this.store.select(selectCurrentThread),
            this.store.select(selectCurrentUserUsername),
            this.store.select(selectCurrentUserSaved),
            this.store.select(selectCurrentUserFollowing),
            this.replies$
        ])
            .pipe(
                skip(1),
                filter(([thread, auth, saved, following, replies]) => !!thread),
                map(([thread, auth, saved, following, replies]) => {
                    const copy = { ...thread }
                    if (copy) {
                        copy.isAuthor = copy.author === auth
                        copy.isParticipant = !!auth && copy.participants!.includes(auth)
                        copy.replyCount = replies ? replies.length : 0
                        copy.saved = saved.includes(copy.id as string)
                        copy.following = following.includes(copy.id as string)
                    }
                    this.title.setTitle(`${copy.title} | Munrol`)
                    return copy as Thread
                })
            )

        this.route.params.subscribe((params) => {
            if (params.id) {
                this.sidebar.hide()
                this.store.dispatch(ThreadActions.getThread({ id: params.id }))
                this.service.threadSubscription(params.id)
            }
        })

        this.currentUser$ = this.store.select(selectCurrentUserAuth).pipe(map((user: UserAuth | null) => user?.username))
    }

    ngOnDestroy(): void {
        this.sidebar.hide()
        this.service.endPush()
    }
}
