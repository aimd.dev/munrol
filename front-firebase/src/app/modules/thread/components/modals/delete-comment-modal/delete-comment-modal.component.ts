import { Component } from '@angular/core'
import { ModalService } from '@app/shared/components/modal/modal.service'

@Component({
  selector: 'mr-delete-comment-modal',
  templateUrl: './delete-comment-modal.component.html',
  styles: [
  ]
})
export class DeleteCommentModalComponent {

    constructor(
        private modal: ModalService
    ) { }

    confirm(): void {
        this.modal.close(true)
    }

}
