import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import * as fromAuth from '@modules/auth/auth.reducer'
import { selectCurrentUserFriends } from '@app/modules/auth/auth.selectors'
import { Observable, Subject } from 'rxjs'
import { map, takeUntil } from 'rxjs/operators'
import { ListUser } from '@app/shared/models'
import { ModalService } from '@shared/components/modal/modal.service'

@Component({
    selector: 'mr-participants-modal',
    templateUrl: './participants-modal.component.html',
    styles: [
    ]
})
export class ParticipantsModalComponent implements OnInit, OnDestroy {

    query = ''

    friends$: Observable<ListUser[]>

    unsubscriber$: Subject<void> = new Subject()

    @Input()
    participants: string[] = []

    constructor(
        private store: Store<fromAuth.State>,
        private modal: ModalService
    ) { }

    ngOnInit(): void {
        this.friends$ = this.store.select(selectCurrentUserFriends)
        .pipe(
            takeUntil(this.unsubscriber$),
            map((friends) => Object.keys(friends).map(f => ({
                        username: f,
                        checked: this.participants.includes(f)
                    })
                )
            )
        )
    }

    send(): void {
        this.modal.close(this.participants)
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
