import { Component } from '@angular/core';
import { ModalService } from '@app/shared/components/modal/modal.service';

@Component({
    selector: 'mr-confirm-delete-reply-modal',
    templateUrl: './confirm-delete-reply-modal.component.html',
    styles: [
    ]
})
export class ConfirmDeleteReplyModalComponent {

    constructor(
        private modal: ModalService
    ) { }

    confirm(): void {
        this.modal.close(true)
    }
}
