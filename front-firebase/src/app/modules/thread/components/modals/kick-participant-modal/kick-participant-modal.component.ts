import { Component, Input } from '@angular/core'
import { ModalService } from '@app/shared/components/modal/modal.service'

@Component({
    selector: 'mr-kick-participant-modal',
    templateUrl: './kick-participant-modal.component.html',
    styles: [
    ]
})
export class KickParticipantModalComponent {
    /**
     * Participant
     */
    @Input()
    participant: string
    /**
     * Constructor del componente
     * @param modal modal service
     */
    constructor(
        private modal: ModalService
    ) { }
    /**
     * Confirm kick
     */
    kick() {
        this.modal.close(true)
    }
}
