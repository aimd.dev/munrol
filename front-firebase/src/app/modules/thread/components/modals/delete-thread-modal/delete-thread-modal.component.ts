import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { ThreadService } from '@app/modules/thread/thread.service'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { loaderState } from '@app/shared/models'
import { AlertsService } from '@app/shared/services/alerts.service'
import { Subject } from 'rxjs'

@Component({
  selector: 'mr-delete-thread-modal',
  templateUrl: './delete-thread-modal.component.html',
  styleUrls: ['./delete-thread-modal.component.scss']
})
export class DeleteThreadModalComponent {

    loading$: Subject<loaderState> = new Subject<loaderState>()

    @Input() id: string

    @Input() author: string

    constructor(
        private router: Router,
        private modal: ModalService,
        private service: ThreadService,
        private alert: AlertsService
    ) { }

    confirm(): void {
        this.service.deleteThread(this.id, this.author)
        .then(() => {
            this.alert.success('El hilo ha sido eliminado')
            this.router.navigate(['/'])
            this.modal.close()
        })
        .catch(() => {
            this.alert.error('No se pudo eliminar el hilo')
        })
    }
}
