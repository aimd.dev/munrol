import {
    Component,
    Input,
    OnInit
} from '@angular/core'

import { File } from '@app/shared/models'

import {
    Observable
} from 'rxjs'

import {
    map,
    tap
} from 'rxjs/operators'

import {
    Store
} from '@ngrx/store'

import * as AuthActions from '@modules/auth/auth.actions'

import {
    selectCurrentUserFiles
} from '@app/modules/auth/auth.selectors'

import {
    ModalService
} from '@app/shared/components/modal/modal.service'

@Component({
    selector: 'mr-link-file-modal',
    templateUrl: './link-file-modal.component.html',
    styles: [
    ]
})
export class LinkFileModalComponent implements OnInit {
    /**
     * Selected files for linking
     */
    selection: string[] = []
    /**
     * User files
     */
    files$: Observable<File[] | undefined>

    @Input()
    threadFiles: string[]
    /**
     * Constructor del componente
     * @param store ngrx store
     * @param modal modal service
     */
    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.files$ = this.store.select(selectCurrentUserFiles)
        .pipe(
            tap(files => !files ? this.store.dispatch(AuthActions.getFiles()) : null),
            map(files => files?.map(f => ({ ...f, inThread: !!this.threadFiles.find(tf => tf === f.id) })))
        )
    }
    /**
     * Confirm files to link
     */
    link() {
        this.modal.close(this.selection)
    }
}
