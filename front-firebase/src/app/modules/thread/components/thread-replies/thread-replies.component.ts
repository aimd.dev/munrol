import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    ActivatedRoute, Router
} from '@angular/router'

import {
    combineLatest,
    Observable,
    Subject
} from 'rxjs'

import {
    delay,
    filter,
    map,
    takeUntil,
    tap
} from 'rxjs/operators'

import { Store } from '@ngrx/store'

import * as fromThread from '../../thread.reducer'
import * as ThreadActions from '../../thread.actions'

import {
    selectCurrentUserUsername
} from '@app/modules/auth/auth.selectors'

import {
    selectCurrentPages,
    selectCurrentReplies,
    selectCurrentVisiblePages
} from '../../thread.selectors'

import {
    Reply
} from '../../models/Reply.model'
import { calculateVisiblePages } from '@app/shared/util/helpers/calculate-visible-pages'
import { ThreadFormat } from '../../models/thread-format.enum'

@Component({
    selector: 'mr-thread-replies',
    templateUrl: './thread-replies.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThreadRepliesComponent implements OnInit, OnDestroy {

    page$: Observable<Reply[]>

    pages$: Observable<number[] | null>

    currentPageIndex$: Observable<number>

    totalThreadReplies$: Observable<number>

    unsubscriber$: Subject<void> = new Subject<void>()

    @Input() linkedFiles: string[]

    @Input() set replies(replies: Reply[] | null) {
        if (replies)
        this.store.dispatch(ThreadActions.updatePages({ total: replies.length }))
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private store: Store<fromThread.State>
    ) { }

    ngOnInit(): void {
        // para cuando cambie la url
        this.currentPageIndex$ = this.route.queryParams
        .pipe(
            map((params) => params.page ? parseInt(params.page) : 1),
            tap(page => this.store.dispatch(ThreadActions.getPage({ index: page ? page : 1 })))
        )

        this.store.select(selectCurrentPages)
        .pipe(
            takeUntil(this.unsubscriber$),
            tap((pages) => {
                const index = this.route.snapshot.queryParams['page']
                this.store.dispatch(ThreadActions.updatePagination({
                    pages: calculateVisiblePages(index ? index : 1, pages)
                }))
            })
        ).subscribe()

        this.pages$ = this.store.select(selectCurrentVisiblePages)
        .pipe(
            tap(() => {
                const index = this.route.snapshot.queryParams['page']
                this.store.dispatch(ThreadActions.getPage({ index: index ? index : 1 }))
            })
        )

        // observable con la pagina actual
        this.page$ = combineLatest([this.store.select(selectCurrentReplies), this.store.select(selectCurrentUserUsername)])
        .pipe(
            // filter(([replies, auth]) => !!replies.length),
            delay(0),
            map(([replies, auth]) => {
                const toDownload = replies.filter(r => !r.body).map(r => r.id)
                if (replies.length === 0 && this.route.snapshot.queryParams['page'] !== 1) {
                    this.router.navigate(['.'], { queryParams: { page: 1 }, relativeTo: this.route })
                }
                if (toDownload.length) this.store.dispatch(ThreadActions.getReplies({ ids: toDownload }))
                return replies.map(r => r.author ? { ...r, isAuthor: r.author === auth, isOwner: r.owner === auth } : r)
            })
        )
    }

    ngOnDestroy() {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
