import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { Store } from '@ngrx/store'
import * as ThreadActions from "../../thread.actions"

import { ReplyUpdate } from '../../models/ReplyUpdate.model'
import { Reply } from '../../models/Reply.model'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'

@Component({
    selector: 'mr-thread-reply-edit',
    templateUrl: './thread-reply-edit.component.html',
    styles: [
    ]
})
export class ThreadReplyEditComponent implements OnInit {

    replyForm: FormGroup

    public toolbar: typeof EditorToolbar = EditorToolbar

    @Input() reply: Reply

    @Input() linkedFiles: string[]

    @Output()
    end: EventEmitter<void> = new EventEmitter<void>()

    constructor(
        private fb: FormBuilder,
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        this.replyForm = this.fb.group({
            body: [this.reply.body, [Validators.required]]
        })
    }

    send() {

        if (this.replyForm.invalid) return

        // return
        const reply: ReplyUpdate = { ...this.replyForm.value }
        reply.lastEdit = new Date().toISOString()
        this.store.dispatch(ThreadActions.updateReply({ id: this.reply.id, reply: reply }))
    }
}
