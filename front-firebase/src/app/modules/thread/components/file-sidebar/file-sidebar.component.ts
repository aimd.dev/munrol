import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import { selectThreadFiles } from '../../thread.selectors'
import { File } from '@app/shared/models'
import { constants } from '@app/core/config/constants'

@Component({
    selector: 'mr-file-sidebar',
    templateUrl: './file-sidebar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileSidebarComponent implements OnInit {
    /**
     * Last fetched
     */
    last$: BehaviorSubject<number> = new BehaviorSubject<number>(0)
    /**
     * Thread files
     */
    files$: Observable<File[]>
    /**
     * End of files
     */
    end$: Subject<boolean> = new Subject<boolean>()
    /**
     * Subject para la vista
     */
    expanded$: BehaviorSubject<File | null> = new BehaviorSubject<File | null>(null)
    /**
     * Is current user the author
     */
    @Input()
    isAuthor: boolean
    /**
     * Current user
     */
    @Input()
    currentUser: string | undefined
    /**
     * Thread id
     */
    @Input()
    thread: string
    /**
     * Vista unica o multiple
     */
    @Input()
    set expand(expanded: File) {
        this.expanded$.next(expanded)
    }
    /**
     * Constructor del componente
     * @param store ngrx store
     */
    constructor(
        private store: Store
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.files$ = combineLatest([this.last$, this.store.select(selectThreadFiles)])
        .pipe(
            tap(([_, saved]) => {
                if (saved.every(s => !!s.body)) this.end$.next(true)
            }),
            map(([last, files]) => files.slice(0, last + constants.pageLength)),
            tap((files) => {
                const toDownload = files.filter(f => !f.body).map(f => f.id)
                if (toDownload.length) {
                    this.store.dispatch(ThreadActions.getFiles({ ids: toDownload.slice(0, 9) }))
                }
            })
        )
    }
    /**
     * Load next batch
     */
    load() {
        const currentValue = this.last$.getValue()
        this.last$.next(currentValue + constants.pageLength)
    }
}
