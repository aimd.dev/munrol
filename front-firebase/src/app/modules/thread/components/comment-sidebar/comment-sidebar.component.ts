import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import * as ThreadActions from '../../thread.actions'
import { BehaviorSubject, Observable, Subject } from 'rxjs'
import { Comment } from '../../models/Comment.model'
import { selectThreadComments } from '../../thread.selectors'
import { map } from 'rxjs/operators'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { constants } from '@app/core/config/constants'
import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'

@Component({
    selector: 'mr-comment-sidebar',
    templateUrl: './comment-sidebar.component.html',
    styles: [
    ]
})
export class CommentSidebarComponent implements OnInit, OnDestroy {
    /**
     * Last fetched
     */
    last: string | undefined

    lastLength = 0

    comments$: Observable<Comment[] | null>

    end$: Subject<boolean> = new Subject<boolean>()

    user$: Observable<string | undefined | null>

    loading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.ON)

    unsubscriber$: Subject<void> = new Subject<void>()

    @Input()
    thread: string

    @Input()
    author: string

    constructor(
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        this.user$ = this.store.select(selectCurrentUserUsername)

        this.comments$ = this.store.select(selectThreadComments)
        .pipe(
            map((comments) => {
                console.log(comments)
                if (!comments) return []
                this.loading$.next(LoaderStateEnum.OFF)
                this.last = comments.length ? comments[comments.length - 1].createdAt : undefined
                if ((comments.length - this.lastLength) < constants.pageLength) {
                    this.end$.next(true)
                }
                this.lastLength = comments.length
                return comments
            })
        )
        this.store.dispatch(ThreadActions.getComments({ thread: this.thread, order: 'desc', last: this.last }))
    }
    /**
     * Load more threads
     */
    loadMore() {
        this.store.dispatch(ThreadActions.getComments({ thread: this.thread, order: 'desc', last: this.last }))
    }
    /**
     * Destructor del componente
     */
    ngOnDestroy(): void {
        this.store.dispatch(ThreadActions.resetComments())
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
