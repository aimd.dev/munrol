import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { combineLatest, Observable, of, Subject } from 'rxjs'
import { map, takeUntil, takeWhile } from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as fromAuth from '@modules/auth/auth.reducer'
import * as ThreadActions from '../../thread.actions'

import { ModalService } from '@shared/components/modal/modal.service'
import { ActivatedRoute } from '@angular/router'
import { selectCurrentThread } from '../../thread.selectors'
import { ThreadUpdate } from '../../models/ThreadUpdate.model'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { ThreadUiService } from '../../services/thread-ui.service'
import { constants } from '@app/core/config/constants'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { ThreadFormat } from '../../models/thread-format.enum'

@Component({
    selector: 'mr-edit-thread',
    templateUrl: './edit-thread.component.html',
    styles: [
    ]
})
export class EditThreadComponent implements OnInit, OnDestroy {

    formats = ThreadFormat
    toolbar = EditorToolbar

    username: string
    form: FormGroup

    id$: Observable<string>
    loading$: Observable<loaderState>
    unsubscriber$: Subject<void> = new Subject()

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private modal: ModalService,
        private store: Store<fromAuth.State>,
        private ui: ThreadUiService
    ) { }

    ngOnInit(): void {

        this.id$ = this.route.params.pipe(map(params => params.id))

        this.route.params.subscribe((params) => {
            if (params.id) {

                this.store.dispatch(ThreadActions.getThread({ id: params.id }))
                this.store.select(selectCurrentThread)
                    .pipe(
                        takeUntil(this.unsubscriber$),
                        takeWhile((thread) => !thread || thread?.id === params.id)
                        // distinctUntilKeyChanged('id'),
                    ).subscribe((thread) => {

                        if (!thread) return
                        if (thread.id !== params.id) return

                        this.form = this.fb.group({
                            title: [thread.title, [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
                            summary: [thread.summary, [Validators.required, Validators.minLength(20), Validators.maxLength(constants.summaryMaxLength)]],
                            body: [thread.body, [Validators.required]],
                            nsfw: [thread.nsfw],
                            tags: [thread.tags],
                            restricted: [thread.restricted],
                            open: [thread.open],
                            format: [thread.format]
                        })

                    })
            }

            this.loading$ = combineLatest([of(LoaderStateEnum.OFF), this.ui.postThreadLoading$])
                .pipe(
                    takeUntil(this.unsubscriber$),
                    map(([_, service]) => service)
                )
        })
    }

    debounce() { }

    submit(): void {

        if (this.form.invalid) return

        const thread: ThreadUpdate = { ...this.form.value }
        thread.lastEdit = new Date().toISOString()
        const id: string = this.route.snapshot.params.id

        this.store.dispatch(ThreadActions.updateThread({ id, thread }))
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
