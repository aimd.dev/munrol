import {
    Component,
    Input,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms'

import { Store } from '@ngrx/store'
import * as ThreadActions from '../../thread.actions'

import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'
import { ThreadUiService } from '../../services/thread-ui.service'
import { catchError, takeUntil } from 'rxjs/operators'
import { BehaviorSubject, EMPTY, Subject } from 'rxjs'
import { loaderState, LoaderStateEnum } from '@app/shared/models'

@Component({
    selector: 'mr-post-comment',
    templateUrl: './post-comment.component.html'
})
export class PostCommentComponent implements OnInit, OnDestroy {

    form: FormGroup

    toolbar: typeof EditorToolbar = EditorToolbar

    loading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)

    unsubscriber$: Subject<void> = new Subject<void>()

    @Input()
    author: string

    @Input()
    thread: string

    constructor(
        private fb: FormBuilder,
        private vldt: EditorBodyRequiredValidatorService,
        private store: Store<any>,
        private ui: ThreadUiService
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            author: [this.author, [Validators.required]],
            body: ['', this.vldt.editorBodyRequired()],
            thread: [this.thread, [Validators.required]],
            mentions: [[]]
        })

        this.ui.clearCommentEditor$
            .pipe(takeUntil(this.unsubscriber$)).subscribe(() => {
                this.body.setValue('')
            })

        this.ui.postCommentLoading$
            .pipe(takeUntil(this.unsubscriber$)).subscribe(() => {
                this.loading$.next(LoaderStateEnum.OFF)
            })
    }

    get body() { return this.form.get('body') as FormControl }

    send() {
        if (this.form.invalid) return
        this.loading$.next(LoaderStateEnum.ON)
        const comment = { ...this.form.value }
        comment.createdAt = new Date().toISOString()
        this.store.dispatch(ThreadActions.postComment({ comment }))
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
