import {
    Component,
    Input
} from '@angular/core'

@Component({
    selector: 'mr-thread-footer',
    templateUrl: './thread-footer.component.html',
    styles: [
    ]
})
export class ThreadFooterComponent {
    /**
     * Last update (replies)
     */
    @Input()
    lastUpdated: string | undefined
    /**
     * Reply count
     */
    @Input()
    replyCount: number
    /**
     * Is user the author
     */
    @Input()
    isAuthor: boolean
    /**
     * Thread id
     */
    @Input()
    id: string
}
