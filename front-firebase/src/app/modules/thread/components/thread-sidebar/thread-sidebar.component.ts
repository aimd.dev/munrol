import {
    ChangeDetectionStrategy,
    Component,
    Input
} from '@angular/core'

import { Store } from '@ngrx/store'
import * as ThreadActions from '../../thread.actions'

import {
    ModalService
} from '@app/shared/components/modal/modal.service'

import {
    LinkFileModalComponent
} from '../modals/link-file-modal/link-file-modal.component'

import {
    File,
    ModalProp
} from '@app/shared/models'

import {
    SidebarService
} from '@app/shared/components/sidebar/sidebar.service'

import {
    FileSidebarComponent
} from '../file-sidebar/file-sidebar.component'

import {
    CommentSidebarComponent
} from '../comment-sidebar/comment-sidebar.component'
import { ParticipantsModalComponent } from '../modals/participants-modal/participants-modal.component'
import { Thread } from '../../models/Thread.model'

@Component({
    selector: 'mr-thread-sidebar',
    templateUrl: './thread-sidebar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThreadSidebarComponent {
    /**
     * File ids
     */
    linkedFiles: string[] = []
    /**
     * current user
     */
    @Input()
    currentUser: string | undefined | null
    /**
     * thread id
     */
    @Input()
    thread: Thread
    /**
     * is user the author
     */
    @Input()
    isAuthor: boolean
    /**
     * is user participant
     */
    @Input()
    isParticipant: boolean
    /**
     * Fichas
     */
    @Input()
    set files(files: File[] | null) {
        this.linkedFiles = files ? files.map(f => f.id) : []
    }
    /**
     * Constructor del componente
     * @param store ngrx store
     * @param modal modal service,
     * @param sidebar sidebar service
     */
    constructor(
        private store: Store<any>,
        private modal: ModalService,
        private sidebar: SidebarService
    ) { }
    /**
     * Open file linking modal
     */
    linkFile() {
        const props: ModalProp[] = [{ key: 'threadFiles', value: this.linkedFiles }]
        this.modal.open(LinkFileModalComponent, props)
        .then((files) => files && this.store.dispatch(ThreadActions.linkFiles({ thread: this.thread.id, files })))
    }
    /**
     * Editar participants
     */
    manageParticipants() {
        const previousParticipants = [ ...this.thread.participants ]
        const props: ModalProp[] = [{ key: 'participants', value: previousParticipants }]
        this.modal.open(ParticipantsModalComponent, props)
        .then((changes) => {
            if (changes) {
                const diff: string[] = []
                changes.unshift(this.currentUser)
                changes.forEach((p: string) => {
                    if (!previousParticipants.includes(p)) diff.push(p)
                })
                this.store.dispatch(ThreadActions.updateParticipants({
                    participants: changes,
                    thread: this.thread.id,
                    diff: diff,
                    author: this.thread.author
                }))
            }
        })
    }
    /**
     * Open file sidebar
     */
    openFileSidebar() {
        const props = [
            { key: 'isAuthor', value: this.isAuthor },
            { key: 'currentUser', value: this.currentUser },
            { key: 'thread', value: this.thread.id }
        ]
        this.sidebar.show(FileSidebarComponent, props)
    }
    /**
     * Open comment sidebar
     */
    openCommentSidebar() {
        const props = [
            { key: 'thread', value: this.thread.id },
            { key: 'author', value: this.currentUser }
        ]
        this.sidebar.show(CommentSidebarComponent, props)
    }
}
