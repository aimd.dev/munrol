import { createReducer, on } from '@ngrx/store'
import * as ThreadActions from './thread.actions'
import { Thread } from './models/Thread.model'
import { Reply } from './models/Reply.model'
import { constants } from '@app/core/config/constants'
import { File } from '@app/shared/models'
import { Comment } from './models/Comment.model'
import { ThreadFormat } from './models/thread-format.enum'

export interface State {
    currentThread: Thread | null
    threadReplies: Reply[] | []
    currentReplies: Reply[] | []
    currentPages: number[] | []
    currentVisiblePages: number[] | []
    threadFiles: File[]
    threadComments: Comment[] | null
}

export const initialState: State = {
    currentThread: null,
    threadReplies: [],
    currentReplies: [],
    currentPages: [],
    currentVisiblePages: [],
    threadFiles: [],
    threadComments: null
}

export const threadReducer = createReducer(

    initialState,

    on(ThreadActions.getThreadSuccess,
        (state, action) => {
            const reference = { ...state }
            reference.threadFiles = action.thread.linkedFiles.map(id => ({ id }) as File)
            reference.currentThread = { ...action.thread }
            return { ...reference }
        }
    ),

    on(ThreadActions.getRepliesSuccess,
        (state, action) => {

            const ids: string[] = action.replies.map(r => r.id)
            const reference: Reply[] = [...action.replies]
            const oldThreadReplies: Reply[] = [...state.threadReplies]

            const newThreadReplies: Reply[] | [] = oldThreadReplies.map(r => {
                if (ids.includes(r.id)) {
                    return reference.find(old => old.id === r.id) as Reply
                } else {
                    return r
                }
            })
            return {
                ...state,
                threadReplies: newThreadReplies.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            }
        }
    ),

    on(ThreadActions.updateThreadState,
        (state, action) => {
            const thread: Thread = state.currentThread ? state.currentThread : {} as Thread
            const newReplies: Reply[] = [ ...action.state.linkedReplies ]
            const oldReplies: Reply[] = [ ...state.threadReplies ]
            const updatedReplies: Reply[] = newReplies.map(r => {
                const existing = oldReplies.find(rep => rep.id === r.id)
                return existing ? existing : r
            })
            return {
                ...state,
                currentThread: { ...thread, lastUpdated: action.state.lastUpdated, nowTyping: action.state.nowTyping },
                threadReplies: updatedReplies.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            }
        }
    ),

    on(ThreadActions.getPage,
        (state, action) => {
            const reference = [...state.threadReplies]
            const pageLength = state.currentThread?.format === ThreadFormat.Rol ? constants.pageLength : 1

            let start = (action.index * pageLength) - pageLength
            let end = action.index * pageLength

            // console.log('first', start, end)

            if (state.currentThread?.format === ThreadFormat.Fanfic) {
                end = start === 0 ? 0 : end - 1
                start = start === 0 ? start : start - 1
            }

            // console.log('second', start, end)

            const page = reference
            .sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            .slice(start, end)

            // console.log('reference', reference)
            // console.log('page', page)

            return {
                ...state,
                currentReplies: [...page]
            }
        }
    ),

    on(ThreadActions.addParticipantSuccess,
        (state, action) => {
            const reference = { ...state.currentThread as Thread }
            reference.participants = [ ...reference.participants, action.username ]
            return {
                ...state,
                currentThread: reference
            }
        }
    ),

    on(ThreadActions.updateParticipants,
        (state, action) => {

            const reference = { ...state.currentThread as Thread }
            reference.participants = action.participants

            return {
                ...state,
                currentThread: reference
            }
        }
    ),

    // on(ThreadActions.postReplySuccess,
    //     (state, action) => {
    //         const reply: Reply = { ...action.reply }
    //         reply.isAuthor = true
    //         const reference: Reply[] = [...state.threadReplies, reply]
    //         return {
    //             ...state,
    //             threadReplies: reference.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
    //         }
    //     }
    // ),

    on(ThreadActions.updateReply,
        (state, action) => {
            const reference: Reply[] = [...state.threadReplies]
            const reply: Reply = { ...reference.find(r => r.id === action.id) as Reply }
            reply.body = action.reply.body
            reply.lastEdit = action.reply.lastEdit
            const newReplies: Reply[] = [...reference.filter(r => r.id !== action.id), reply]
            return {
                ...state,
                threadReplies: newReplies.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            }
        }
    ),

    // on(ThreadActions.deleteReplySuccess,
    //     (state, action) => {
    //         const reference: Reply[] = [...state.threadReplies]
    //         return {
    //             ...state,
    //             threadReplies: reference.filter(r => r.id !== action.id).sort((a, b) => a.createdAt.localeCompare(b.createdAt))
    //         }
    //     }
    // ),

    on(ThreadActions.removeParticipant,
        (state, action) => {
            const reference = { ...state.currentThread as Thread }
            reference.participants = reference.participants.filter(p => p !== action.username)
            return {
                ...state,
                currentThread: reference
            }
        }
    ),

    on(ThreadActions.updatePages,
        (state, action) => {
            const pageLength = state.currentThread?.format === ThreadFormat.Rol ? constants.pageLength : 1
            const length: number = Math.ceil(action.total / pageLength)
            const pages = Array.from({length: length}, (x, i) => i+1)
            return {
                ...state,
                currentPages: [...pages]
            }
        }
    ),

    on(ThreadActions.updatePagination,
        (state, action) => {
            return {
                ...state,
                currentVisiblePages: [...action.pages]
            }
        }
    ),

    on(ThreadActions.linkFilesSuccess,
        (state, action) => {
            const reference = { ...state }
            const ids = action.files.map(f => f.id)
            return {
                ...state,
                currentThread: {
                    ...state.currentThread as Thread,
                    linkedFiles: (reference.currentThread as Thread).linkedFiles.concat(ids)
                },
                threadFiles: reference.threadFiles.concat(action.files)
            }
        }
    ),

    on(ThreadActions.getFilesSuccess,
        (state, action) => {

            const ids: string[] = action.files.map(f => f.id)
            const reference: File[] = [...action.files]
            const oldThreadFiles: File[] = [...state.threadFiles]

            const newThreadFiles: File[] | [] = oldThreadFiles.map(r => {
                if (ids.includes(r.id)) {
                    return reference.find(old => old.id === r.id) as File
                } else {
                    return r
                }
            })

            return {
                ...state,
                threadFiles: newThreadFiles
            }
        }
    ),

    on(ThreadActions.getReferenceSuccess,
        (state, action) => {
            const reference = [ ...state.threadFiles ]
            const index = reference.findIndex(f => f.id === action.file.id) as number
            reference[index] = { ...action.file }
            return {
                ...state,
                threadFiles: reference
            }
        }
    ),

    on(ThreadActions.kickParticipant,
        (state, action) => {
            const reference = { ...state.currentThread } as Thread
            reference.participants = reference.participants.filter(p => p !== action.participant)
            return {
                ...state,
                currentThread: reference
            }
        }
    ),

    on(ThreadActions.kickParticipantError,
        (state, action) => {
            const reference = { ...state.currentThread as Thread }
            reference.participants = [ ...reference.participants, action.participant ]
            return {
                ...state,
                currentThread: reference
            }
        }
    ),

    on(ThreadActions.unlinkFileSuccess,
        (state, action) => {
            const files = [ ...state.threadFiles ]
            return {
                ...state,
                threadFiles: files.filter(f => f.id !== action.file)
            }
        }
    ),

    on(ThreadActions.getCommentsSuccess,
        (state, action) => {
            const comments = state.threadComments ? [...state.threadComments] : null
            return {
                ...state,
                threadComments: comments ? comments.concat(action.comments) : action.comments
            }
        }
    ),

    on(ThreadActions.postCommentSuccess,
        (state, action) => {
            const comments = state.threadComments ? [...state.threadComments] : []
            const count = (state.currentThread as Thread).commentCount
            return {
                ...state,
                currentThread: { ...state.currentThread as Thread, commentCount: count + 1 },
                threadComments: [action.comment].concat(comments)
            }
        }
    ),

    on(ThreadActions.editComment,
        (state, action) => {
            const comments = [...state.threadComments as Comment[]]
            const index = comments.findIndex(c => c.id === action.id) as number
            let comment = comments[index]
            comment = Object.assign({ ...comment }, action.comment)
            comments[index] = comment
            return {
                ...state,
                threadComments: comments
            }
        }
    ),

    on(ThreadActions.removeCommentSuccess,
        (state, action) => {
            if (!state.currentThread) { return { ...state } }
            const comments = [...state.threadComments as Comment[]]
            const count = (state.currentThread as Thread).commentCount
            return {
                ...state,
                currentThread: { ...state.currentThread as Thread, commentCount: count - 1 },
                threadComments: comments.filter(c => c.id !== action.comment)
            }
        }
    ),

    on(ThreadActions.resetComments,
        (state) => {
            return {
                ...state,
                threadComments: null
            }
        }
    ),

    on(ThreadActions.closeThreadSuccess,
        (state) => {
            return {
                ...state,
                currentThread: { ...state.currentThread as Thread, open: false }
            }
        }
    ),

    on(ThreadActions.openThreadSuccess,
        (state) => {
            return {
                ...state,
                currentThread: { ...state.currentThread as Thread, open: true }
            }
        }
    )

)
