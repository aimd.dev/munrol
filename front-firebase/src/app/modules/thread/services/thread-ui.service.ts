import { Injectable } from '@angular/core'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { BehaviorSubject, Observable, Subject } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class ThreadUiService {

    private _postThreadLoading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
    postThreadLoading$: Observable<loaderState> = this._postThreadLoading$.asObservable()

    private _postCommentLoading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
    postCommentLoading$: Observable<loaderState> = this._postCommentLoading$.asObservable()

    private _clearEditor$: Subject<void> = new Subject<void>()
    clearEditor$: Observable<void> = this._clearEditor$.asObservable()

    private _clearCommentEditor$: Subject<void> = new Subject<void>()
    clearCommentEditor$: Observable<void> = this._clearCommentEditor$.asObservable()

    private _postReplyLoading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
    postReplyLoading$: Observable<loaderState> = this._postReplyLoading$.asObservable()

    constructor() { }

    setPostThreadLoading(state: loaderState) {
        this._postThreadLoading$.next(state)
    }

    setPostReplyLoading(state: loaderState) {
        this._postReplyLoading$.next(state)
    }

    setPostCommentLoading(state: loaderState) {
        this._postCommentLoading$.next(state)
    }

    clearEditor() {
        this._clearEditor$.next()
    }

    clearCommentEditor() {
        this._clearCommentEditor$.next()
    }
}
