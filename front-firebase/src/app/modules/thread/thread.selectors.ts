import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromThreads from './thread.reducer'

export const selectThreadState = createFeatureSelector<fromThreads.State>('thread')

export const selectCurrentThread = createSelector(selectThreadState, (thread) => thread.currentThread)
export const selectThreadReplies = createSelector(selectThreadState, (thread) => thread.threadReplies)
export const selectCurrentReplies = createSelector(selectThreadState, (thread) => thread.currentReplies)
export const selectThreadFiles = createSelector(selectThreadState, (thread) => thread.threadFiles)
export const selectThreadComments = createSelector(selectThreadState, (thread) => thread.threadComments)
export const selectCurrentPages = createSelector(selectThreadState, (thread) => thread.currentPages)
export const selectCurrentVisiblePages = createSelector(selectThreadState, (thread) => thread.currentVisiblePages)
