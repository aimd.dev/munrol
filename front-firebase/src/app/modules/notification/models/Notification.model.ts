export interface Notification {
    createdAt: string
    pointer: string
    author?: string
}
