import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromNotification from './notification.reducer'

export const selectNotificationState = createFeatureSelector<fromNotification.State>('notification')

export const selectPushState = createSelector(selectNotificationState, (notification) => notification.push)
export const selectCurrentView = createSelector(selectNotificationState, (notification) => notification.view)
export const selectCurrentNotifications = createSelector(selectNotificationState, (notification) => notification.notifications)
