import { PushNotifications } from "@app/shared/models";
import { createReducer, on } from "@ngrx/store";
import { Notification } from "./models/Notification.model";
import * as NotificationsActions from "./notification.actions";

export interface State {
    view: string | null,
    push: PushNotifications,
    notifications: Notification[]
}

export const initialState: State = {
    view: null,
    push: { newInvites: false, newRequests: false, newMessages: false },
    notifications: []
}

export const notificationReducer = createReducer(

    initialState,

    on(NotificationsActions.updateNotifications,
        (state, action) => {
            return {
                ...state,
                push: { ...state.push, ...action.state }
            }
        }
    ),

    on(NotificationsActions.getNotificationsSuccess,
        (state, action) => {
            return {
                ...state,
                notifications: [...action.notifications]
            }
        }
    ),

    on(NotificationsActions.dismissNotification,
        (state, action) => {
            const notifications = [...state.notifications]
            return {
                ...state,
                notifications: notifications.filter(n => n.pointer !== action.id)
            }
        }
    ),

    on(NotificationsActions.setView,
        (state, action) => {
            return {
                ...state,
                view: action.view,
                notifications: []
            }
        }
    ),

    on(NotificationsActions.reset,
        (state, action) => {
            return {
                ...state,
                view: 'requests',
                notifications: []
            }
        }
    )
)
