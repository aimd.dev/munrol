import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Notification } from '../../models/Notification.model';
import * as NotificationsActions from "../../notification.actions";

@Component({
  selector: '[mr-request-notification]',
  templateUrl: './request-notification.component.html',
  styles: [
  ]
})
export class RequestNotificationComponent implements OnInit {

  @Input() notification: Notification

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit(): void {
  }

  accept(id:string) {
    this.store.dispatch(NotificationsActions.acceptFriendshipRequest({source:id}))
  }

  dismiss(id:string) {
    this.store.dispatch(NotificationsActions.dismissNotification({collection:'invites', id:id}))
  }
}
