import {
    Component,
    OnInit
} from '@angular/core'

import {
    Observable
} from 'rxjs'

import {
    map,
    take,
    tap
} from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as NotificationActions from '../../notification.actions'

import { selectCurrentNotifications, selectCurrentView, selectPushState } from '../../notification.selectors'
import { Notification } from '../../models/Notification.model'
import { PushNotifications } from '@app/shared/models'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
    selector: 'mr-notification-page',
    templateUrl: './notification-page.component.html',
    styles: [
    ]
})
export class NotificationPageComponent implements OnInit {

    active$: Observable<string>

    notifications$: Observable<Notification[]>

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private store: Store<any>
    ) { }

    ngOnInit(): void {

        this.route.queryParams.subscribe(params => {
            this.store.dispatch(NotificationActions.setView({ view: params.tab ? params.tab : 'requests' }))
        })

        this.active$ = this.store.select(selectCurrentView)
            .pipe(
                map((tab) => tab ? tab : 'requests'),
                tap((tab) => {
                    this.store.dispatch(NotificationActions.getNotifications({ collection: tab }))
                    this.store.select(selectPushState).pipe(take(1)).subscribe((push: PushNotifications) => {
                        const key = ('new' + tab[0].toUpperCase() + tab.slice(1)) as keyof PushNotifications
                        if (push[key]) {
                            this.store.dispatch(NotificationActions.markAsRead({ property: key }))
                        }
                    })
                })
            )

        this.notifications$ = this.store.select(selectCurrentNotifications)
            .pipe(map((res) => res ? res : []))
    }

    acceptFriendship(source: string) {
        this.store.dispatch(NotificationActions.acceptFriendshipRequest({ source }))
    }

    rejectFriendship(source: string) {
        this.store.dispatch(NotificationActions.rejectFriendshipRequest({ source }))
    }

    select(tab: string) {
        this.router.navigate(['.'], { queryParams: { tab: tab }, relativeTo: this.route })
    }

    loadMore() {
        //
    }
}
