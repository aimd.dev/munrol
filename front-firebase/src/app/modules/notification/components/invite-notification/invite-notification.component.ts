import { Component, Input } from '@angular/core'
import { Store } from '@ngrx/store'
import { Notification } from '../../models/Notification.model'
import * as NotificationsActions from '../../notification.actions'

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: '[mr-invite-notification]',
    templateUrl: './invite-notification.component.html',
    styles: [
    ]
})
export class InviteNotificationComponent {

    @Input() notification: Notification

    constructor(
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        console.log(this.notification)
    }

    dismiss(id: string) {
        this.store.dispatch(NotificationsActions.dismissNotification({ collection: 'invites', id: id }))
    }
}
