import { Injectable } from '@angular/core'
import { AlertsService } from '@app/shared/services/alerts.service'
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { EMPTY } from 'rxjs'
import { catchError, exhaustMap, map, tap } from 'rxjs/operators'
import { selectCurrentUserUsername } from '../auth/auth.selectors'
import { Notification } from './models/Notification.model'
import * as NotificationActions from './notification.actions'
import { NotificationService } from './notification.service'

@Injectable()
export class NotificationEffects {

    constructor(
        private actions$: Actions,
        private service: NotificationService,
        private store: Store<any>,
        private alert: AlertsService
    ) {}

    markAsRead$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationActions.markAsRead),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.markAsRead(auth!, action.property)
                .pipe(
                    catchError((err) => {
                        console.log(err)
                        this.alert.error(err.code)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    getNotifications$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationActions.getNotifications),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.getNotifications(auth!, action.collection)
                .pipe(
                    map((res) => {
                        const notifications: Notification[] = res.docs.map((doc) => {
                            const notification = doc.data() as Notification
                            notification.pointer = doc.id
                            return notification
                        })
                        console.log(notifications)
                        return NotificationActions.getNotificationsSuccess({ notifications })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error(err.code)
                        return EMPTY
                    })
                )
            )
        )
    )

    dismissNotification$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationActions.dismissNotification),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.dismissNotification(auth!, action.collection, action.id)
                .pipe(
                    catchError((err) => {
                        console.log(err)
                        this.alert.error(err.code)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    acceptFriendshipRequest$ = createEffect(() =>
        this.actions$.pipe(
            ofType(NotificationActions.acceptFriendshipRequest),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.acceptFriendshipRequest(action.source, auth!)
                .pipe(
                    tap(() => this.alert.success(`${action.source} y tu ahora son amigos!`)),
                    map(() => NotificationActions.dismissNotification({collection: 'requests', id: action.source})),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error(err.code)
                        return EMPTY
                    })
                )
            )
        )
    )
}
