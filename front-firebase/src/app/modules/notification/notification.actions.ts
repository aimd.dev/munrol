import { PushNotifications } from '@app/shared/models'
import { createAction, props } from '@ngrx/store'
import { Notification } from './models/Notification.model'

export const updateNotifications = createAction(
    '[Tob Bar] Update Notifications',
    props<{state: PushNotifications}>()
)

export const markAsRead = createAction(
    '[Notifications Page] Mark As Read',
    props<{property: string}>()
)

export const setView = createAction(
    '[Notifications Page] Set View',
    props<{view: string}>()
)

export const getNotifications = createAction(
    '[Notifications Page] Get Notifications',
    props<{collection: string}>()
)

export const getNotificationsSuccess = createAction(
    '[Notifications Page] Get Notifications Success',
    props<{notifications: Notification[]}>()
)

export const dismissNotification = createAction(
    '[Notifications Page] Dismiss Notification',
    props<{collection: string, id: string}>()
)

export const acceptFriendshipRequest = createAction(
    '[Notifications Page] Accept Friendship Request',
    props<{source: string}>()
)

export const rejectFriendshipRequest = createAction(
    '[Notifications Page] Accept Friendship Request',
    props<{source: string}>()
)

export const reset = createAction(
    '[Notifications Page] Reset'
)
