import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NotificationRoutingModule } from './notification-routing.module'
import { NotificationPageComponent } from './components/notification-page/notification-page.component'
import { SharedModule } from '@app/shared/shared.module'
import { RequestNotificationComponent } from './components/request-notification/request-notification.component'
import { InviteNotificationComponent } from './components/invite-notification/invite-notification.component'

@NgModule({
  declarations: [
    NotificationPageComponent,
    RequestNotificationComponent,
    InviteNotificationComponent
  ],
  imports: [
    CommonModule,
    NotificationRoutingModule,
    SharedModule
  ]
})
export class NotificationModule { }
