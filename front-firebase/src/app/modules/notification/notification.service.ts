import { Injectable } from '@angular/core'
import { updateDoc, collection, doc, getDocs, deleteDoc, Firestore, writeBatch, docSnapshots } from '@angular/fire/firestore'
import { PushNotifications } from '@app/shared/models'
import { Store } from '@ngrx/store'
import { from, Subscription } from 'rxjs'
import * as NotificationActions from '@modules/notification/notification.actions'

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(
        private firestore: Firestore,
        private store: Store<any>
    ) { }

    private push: Subscription

    // Notifications
    pushSubscription(username: string) {
        // unsubscribe
        if (this.push) this.push.unsubscribe()
        // init listener
        this.push = docSnapshots(doc(this.firestore, `notifications/${username}`))
        .subscribe(data => {
            this.store.dispatch(NotificationActions.updateNotifications({
                state: data.data() as PushNotifications
            }))
        })
    }

    endPush() {
        this.push.unsubscribe()
    }

    markAsRead(username: string, property: string) {
        return from(updateDoc(doc(this.firestore, `notifications/${username}`), { [property]: false }))
    }

    getNotifications(username: string, subcollection: string) {
        return from(getDocs(collection(this.firestore, `notifications/${username}/${subcollection}`)))
    }

    dismissNotification(username: string, subcollection: string, id: string) {
        return from(deleteDoc(doc(this.firestore, `notifications/${username}/${subcollection}/${id}`)))
    }

    acceptFriendshipRequest(source: string, target: string) {
        const createdAt: string = new Date().toISOString()

        const batch = writeBatch(this.firestore)
        batch.delete(doc(this.firestore, `notifications/${target}/requests/${source}`))
        batch.update(doc(this.firestore, `users/${target}`), { [`friends.${source}`]: createdAt })
        batch.update(doc(this.firestore, `users/${source}`), { [`friends.${target}`]: createdAt })

        return from(batch.commit())
    }

    rejectFriendshipRequest(source: string, target: string) {
        return from(deleteDoc(doc(this.firestore, `notifications/${target}/requests/${source}`)))
    }
}
