import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { AuthRoutingModule } from './auth-routing.module'

import { SharedModule } from '@shared/shared.module'

import { LoginComponent } from './components/login/login.component'
import { SignupComponent } from './components/signup/signup.component'
import { AuthMainComponent } from './components/auth-main/auth-main.component'
import { PasswordResetModalComponent } from './components/modals/password-reset-modal/password-reset-modal.component'


@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    AuthMainComponent,
    PasswordResetModalComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    RouterModule,
    SharedModule
  ],
  providers: []
})
export class AuthModule { }
