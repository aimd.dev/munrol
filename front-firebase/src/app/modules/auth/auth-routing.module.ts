import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthMainComponent } from './components/auth-main/auth-main.component'

import { LoginComponent } from './components/login/login.component'
import { SignupComponent } from './components/signup/signup.component'

const routes: Routes = [
    {
        path: '',
        component: AuthMainComponent,
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'signup',
                component: SignupComponent
            }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {

}
