import { File } from '@app/shared/models'

export interface UserData {
    friends: {[key: string]: number},
    following: string[]
    saved: string[]
    files?: File[]
}
