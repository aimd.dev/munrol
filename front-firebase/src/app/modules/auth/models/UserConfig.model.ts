export interface UserConfig {
    email?: string
    replyRequestEmail: boolean
    threadInviteEmail: boolean
}