import { File } from '@app/shared/models'
import { createAction, props } from '@ngrx/store'

import {
    UserAuth,
    UserConfig,
    UserData,
    UserLogin,
    UserSignup
} from './models'

export const signup = createAction(
    '[Signup Page] User Signup',
    props<{user: UserSignup}>()
)

export const authenticate = createAction(
    '[Login Page] User Auth',
    props<{user: UserLogin}>()
)

export const setCurrentUser = createAction(
    '[Auth Service] Set Current User',
    props<{user: UserAuth | null}>()
)

export const getUserData = createAction(
    '[Auth Service] Get Current User Data',
    props<{username: string}>()
)

export const getUserDataSuccess = createAction(
    '[Auth Service] Get Current User Data Success',
    props<{user: UserData | null}>()
)

export const sendPasswordResetEmail = createAction(
    '[Login Page] Send Password Reset Email',
    props<{email: string}>()
)

export const getUserConfig = createAction(
    '[Settings Page] Get User Config',
    props<{username: string}>()
)

export const getUserConfigSuccess = createAction(
    '[Settings Page] Get User Config Success',
    props<{config: UserConfig}>()
)

export const updateEmail = createAction(
    '[User Settings Page] Update Email',
    props<{email: string, password: string}>()
)

export const updateEmailSuccess = createAction(
    '[User Settings Page] Update Email Success',
    props<{email: string}>()
)

export const updatePassword = createAction(
    '[User Settings Page] Update Password',
    props<{oldPassword: string, newPassword: string}>()
)

export const updateConfig = createAction(
    '[User Settings Page] Update Config Switch',
    props<{key: string, value: boolean}>()
)

export const updateConfigSuccess = createAction(
    '[User Settings Page] Update Config Switch'
)

export const updateConfigError = createAction(
    '[User Settings Page] Update Config Switch Error',
    props<{key: string, value: boolean}>()
)

export const logout = createAction(
    '[Top Menu] User Logout'
)

export const saveThread = createAction(
    '[Thread Page] Save Thread',
    props<{id: string}>()
)

export const saveThreadError = createAction(
    '[Thread Page] Save Thread Success',
    props<{id: string}>()
)

export const unsaveThread = createAction(
    '[Thread Page] Unsave Thread',
    props<{id: string}>()
)

export const unsaveThreadError = createAction(
    '[Thread Page] Unsave Thread Success',
    props<{id: string}>()
)

export const followThread = createAction(
    '[Thread Page] Follow Thread',
    props<{id: string}>()
)

export const followThreadError = createAction(
    '[Thread Page] Follow Thread Success',
    props<{id: string}>()
)

export const unfollowThread = createAction(
    '[Thread Page] Unfollow Thread',
    props<{id: string}>()
)

export const unfollowThreadError = createAction(
    '[Thread Page] Unfollow Thread Success',
    props<{id: string}>()
)

export const getFiles = createAction(
    '[Thread Page] Get User Files'
)

export const getFilesSuccess = createAction(
    '[Thread Page] Get User Files Success',
    props<{files: File[]}>()
)
