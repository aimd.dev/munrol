import { Injectable } from '@angular/core';
import { loaderState, LoaderStateEnum } from '@app/shared/models';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthUiService {

  private _submitLoading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
  submitLoading$: Observable<loaderState> = this._submitLoading$.asObservable()

  private _errors$: Subject<string> = new Subject<string>()
  errors$: Observable<string> = this._errors$.asObservable()

  setSubmitLoading(state: loaderState) {
    this._submitLoading$.next(state)
  }

  setError(code: string) {
    this._errors$.next(code)
  }
}
