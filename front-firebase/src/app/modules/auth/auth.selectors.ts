import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromAuth from './auth.reducer'

export const selectAuthState = createFeatureSelector<fromAuth.State>('auth')

export const selectCurrentUserAuth = createSelector(selectAuthState, auth => auth.userAuth)
export const selectCurrentUserEmail = createSelector(selectAuthState, auth => auth.userAuth?.email)
export const selectCurrentUserUsername = createSelector(selectAuthState, auth => auth.userAuth?.username)
export const selectCurrentUserData = createSelector(selectAuthState, auth => auth.userData)
export const selectCurrentUserFriends = createSelector(selectCurrentUserData, data => data ? data.friends : [])
export const selectCurrentUserConfig = createSelector(selectAuthState, auth => auth.userConfig)
export const selectCurrentUserFiles = createSelector(selectCurrentUserData, data => data ? data.files : [])

export const selectCurrentUserSaved = createSelector(selectAuthState, auth => auth.userData? auth.userData.saved : [])
export const selectCurrentUserFollowing = createSelector(selectAuthState, auth => auth.userData? auth.userData.following : [])
