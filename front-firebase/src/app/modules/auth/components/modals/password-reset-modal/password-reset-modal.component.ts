import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalService } from '@app/shared/components/modal/modal.service';

@Component({
  selector: 'mr-password-reset-modal',
  templateUrl: './password-reset-modal.component.html',
  styles: [
  ]
})
export class PasswordResetModalComponent implements OnInit {

  form:FormGroup

  constructor(
    private fb: FormBuilder,
    private modal: ModalService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }

  get email() { return this.form.get('email')!.value }

  send() {
    if (this.form.invalid) return
    this.modal.close(this.email)
  }

}
