import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms'

import {
    combineLatest,
    Observable,
    of,
    Subject
} from 'rxjs'

import {
    map,
    takeUntil
} from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as fromAuth from '../../auth.reducer'
import * as AuthActions from '../../auth.actions'

import {
    AuthUiService
} from '../../services/auth-ui.service'

import {
    ModalService
} from '@app/shared/components/modal/modal.service'

import {
    PasswordResetModalComponent
} from '../modals/password-reset-modal/password-reset-modal.component'

import {
    loaderState,
    LoaderStateEnum
} from '@app/shared/models'

@Component({
    selector: 'mr-login',
    templateUrl: './login.component.html',
    styles: []
})
export class LoginComponent implements OnInit, OnDestroy {
    /**
     * Login form
     */
    form: FormGroup
    /**
     * Loading state
     */
    loading$: Observable<loaderState>
    /**
     * Subject for destruction
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Constructor del componente
     * @param {FormBuilder} fb angular form builder
     * @param {Store} store ngrx store
     * @param {Modal Service} modal modal service
     * @param {AuthUiService} ui ui service
     */
    constructor(
        private fb: FormBuilder,
        private store: Store<fromAuth.State>,
        private modal: ModalService,
        private ui: AuthUiService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.form = this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', [Validators.required]]
        })
        // Loading state
        this.loading$ = combineLatest([of(LoaderStateEnum.OFF), this.ui.submitLoading$])
        .pipe(
            takeUntil(this.unsubscriber$),
            map(([_, service]) => service)
        )
    }

    get email() { return this.form.get('email') as FormControl }
    /**
     * Open password reset modal
     */
    openPasswordResetModal() {
        this.modal.open(PasswordResetModalComponent)
        .then((email) => {
            if (email) this.store.dispatch(AuthActions.sendPasswordResetEmail({ email }))
        })
    }
    /**
     * Login
     */
    send() {
        if (this.form.invalid) return
        this.store.dispatch(AuthActions.authenticate({ user: this.form.value }))
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
