import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import * as fromAuth from "../../auth.reducer";
import * as AuthActions from "../../auth.actions";
import { combineLatest, Observable, of, Subject } from "rxjs";
import { loaderState, LoaderStateEnum } from "@app/shared/models";
import { map, takeUntil } from "rxjs/operators";
import { AuthUiService } from "../../services/auth-ui.service";

@Component({
  selector: 'mr-signup',
  templateUrl: './signup.component.html',
  styles: []
})
export class SignupComponent implements OnInit {

  form: FormGroup

  loading$: Observable<loaderState>

  unsubscriber$: Subject<void> = new Subject<void>()

  constructor(
    private fb: FormBuilder,
    private store: Store<fromAuth.State>,
    private ui: AuthUiService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[A-Za-z0-9]+$')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })

    this.loading$ = combineLatest([of(LoaderStateEnum.OFF), this.ui.submitLoading$])
    .pipe(
      takeUntil(this.unsubscriber$),
      map(([_, service]) => service)
    )
  }

  get username() { return this.form.get('username') as FormControl }
  get email() { return this.form.get('email') as FormControl }

  send() {
    if (this.form.invalid) return
    this.store.dispatch(AuthActions.signup({ user: this.form.value }))
  }

  ngOnDestroy(): void {
    this.unsubscriber$.next()
    this.unsubscriber$.complete()
  }
}
