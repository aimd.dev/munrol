import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, ofType, createEffect, concatLatestFrom } from '@ngrx/effects'
import { EMPTY, from } from 'rxjs'
import { catchError, exhaustMap, map, mergeMap, tap } from 'rxjs/operators'
import * as AuthActions from './auth.actions'
import { AuthService } from './auth.service'
import { UserData } from './models/UserData.model'
import { AlertsService } from '@app/shared/services/alerts.service'
import { UserConfig } from './models/UserConfig.model'
import { Store } from '@ngrx/store'
import { selectCurrentUserEmail, selectCurrentUserUsername } from './auth.selectors'
import { AuthUiService } from './services/auth-ui.service'
import { File, LoaderStateEnum } from '@app/shared/models'
import { getErrorMessage } from './helpers'

@Injectable()
export class AuthEffects {

    constructor(
        private router: Router,
        private actions$: Actions,
        private service: AuthService,
        private store: Store<any>,
        private alerts: AlertsService,
        private ui: AuthUiService
    ) {}

    signup$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.signup),
            tap(() => this.ui.setSubmitLoading(LoaderStateEnum.ON)),
            exhaustMap((action) => this.service.signup(action.user)
                .pipe(
                    tap(() => {
                        this.router.navigate(['/'])
                    }),
                    catchError((err) => {
                        this.alerts.error(getErrorMessage(err.code))
                        this.ui.setError(err.code)
                        this.ui.setSubmitLoading(LoaderStateEnum.OFF)
                        throw err
                    })
                )
            )
        ), { dispatch: false }
    )

    authenticate$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.authenticate),
            tap(() => this.ui.setSubmitLoading(LoaderStateEnum.ON)),
            exhaustMap((action) => this.service.login(action.user)
                .pipe(
                    tap(() => {
                        this.router.navigate(['/'])
                    }),
                    catchError((err) => {
                        this.alerts.error(getErrorMessage(err.code))
                        this.ui.setError(err.code)
                        this.ui.setSubmitLoading(LoaderStateEnum.OFF)
                        throw err
                    })
                )
            )
        ), { dispatch: false }
    )

    setCurrentUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.setCurrentUser),
            tap(() => this.ui.setSubmitLoading(LoaderStateEnum.OFF)),
            map((action) => {
                if (action.user) return AuthActions.getUserData({ username: action.user.username })
                else return AuthActions.getUserDataSuccess({ user: null })
            })
        )
    )

    getCurrentUserData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.getUserData),
            mergeMap((action) => this.service.getUserData(action.username)
                .pipe(
                    map(([
                        doc,
                        following,
                        saved
                    ]) => {
                        const user: UserData = {
                            friends: (doc.data() as UserData).friends,
                            following: (following.data() as any)?.threads as string[] || [],
                            saved: (saved.data() as any)?.threads as string[] || []
                        }
                        return AuthActions.getUserDataSuccess({ user })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Hubo un problema al obtener el usuario')
                        return EMPTY
                    })
                )
            )
        )
    )

    sendPasswordResetEmail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.sendPasswordResetEmail),
            exhaustMap((action) => from(this.service.passwordReset(action.email))
                .pipe(
                    tap(() => this.alerts.success('El correo de confirmación ha sido enviado')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error(err.code)
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    getUserConfig$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.getUserConfig),
            exhaustMap((action) => this.service.getUserConfig(action.username)
                .pipe(
                    map((doc) => AuthActions.getUserConfigSuccess({ config: doc.data() as UserConfig })),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error(err.code)
                        return EMPTY
                    })
                )
            )
        )
    )

    updateEmail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.updateEmail),
            concatLatestFrom(() => this.store.select(selectCurrentUserEmail)),
            exhaustMap(([action, email]) => this.service.updateEmail(email!, action.email, action.password)
                .pipe(
                    tap(() => this.alerts.success('Cambios guardados')),
                    map(() => AuthActions.updateEmailSuccess({ email: action.email })),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se pudo actualizar el email')
                        return EMPTY
                    })
                )
            )
        )
    )

    updatePassword$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.updatePassword),
            concatLatestFrom(() => this.store.select(selectCurrentUserEmail)),
            exhaustMap(([action, email]) => this.service.updatePassword(email!, action.oldPassword, action.newPassword)
                .pipe(
                    tap(() => this.alerts.success('Cambios guardados')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se pudo actualizar la contraseña')
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    updateConfig$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.updateConfig),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => from(this.service.updateConfig(action.key, action.value, auth!))
                .pipe(
                    tap(() => this.alerts.success('Cambios guardados')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error(err.code)
                        this.store.dispatch(AuthActions.updateConfigError({ key: action.key, value: !action.value }))
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.logout),
            exhaustMap(() => from(this.service.logout())
                .pipe(tap(() => this.router.navigate(['/'])))
            )
        ), { dispatch: false }
    )

    saveThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.saveThread),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.saveThread(auth!, action.id)
                .pipe(
                    tap(() => this.alerts.success('El hilo ha sido guardado en tu archivo')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Ha ocurrido un error')
                        this.store.dispatch(AuthActions.saveThreadError({ id: action.id }))
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    unsaveThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.unsaveThread),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.unsaveThread(auth!, action.id)
                .pipe(
                    tap(() => this.alerts.success('El hilo ha sido removido de tu archivo')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Ha ocurrido un error')
                        this.store.dispatch(AuthActions.unsaveThreadError({ id: action.id }))
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    followThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.followThread),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.followThread(auth!, action.id)
                .pipe(
                    tap(() => this.alerts.success('Estas siguiendo este hilo')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Ha ocurrido un error')
                        this.store.dispatch(AuthActions.followThreadError({ id: action.id }))
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    unfollowThread$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.unfollowThread),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.unfollowThread(auth!, action.id)
                .pipe(
                    tap(() => this.alerts.success('Ya no estas siguiendo este hilo')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Ha ocurrido un error')
                        this.store.dispatch(AuthActions.unfollowThreadError({ id: action.id }))
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    getUserFiles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.getFiles),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.getUserFiles(auth)
                .pipe(
                    map((data) => {
                        const files = data.docs.map((r) => {
                            const file = r.data() as File
                            file.id = r.id
                            return file
                        })
                        return AuthActions.getFilesSuccess({ files })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('Ha ocurrido un error')
                        return EMPTY
                    })
                )
            )
        )
    )
}
