import { createReducer, on } from '@ngrx/store'
import * as AuthActions from './auth.actions'
import { UserAuth, UserData, UserConfig } from './models'

export interface State {
    userAuth: UserAuth | null
    userData: UserData | null
    userConfig: UserConfig | null
}

export const initialState: State = {
    userAuth: null,
    userData: null,
    userConfig: null
}

export const authReducer = createReducer(

    initialState,

    on(AuthActions.setCurrentUser,
        (state, action) => {
            return { ...state, userAuth: action.user }
        }
    ),

    on(AuthActions.getUserDataSuccess,
        (state, action) => {
            return { ...state, userData: action.user }
        }
    ),

    on(AuthActions.getUserConfigSuccess,
        (state, action) => {
            return { ...state, userConfig: action.config }
        }
    ),

    on(AuthActions.updateEmailSuccess,
        (state, action) => {
            const config = { ...state.userConfig, email: action.email } as UserConfig
            const auth = { ...state.userAuth, email: action.email } as UserAuth
            return { ...state, userConfig: config, userAuth: auth }
        }
    ),

    on(AuthActions.updateConfig,
        (state, action) => {
            const config = { ...state.userConfig, [action.key]: action.value } as UserConfig
            return { ...state, userConfig: config }
        }
    ),

    on(AuthActions.updateConfigError,
        (state, action) => {
            const config = { ...state.userConfig, [action.key]: action.value } as UserConfig
            return { ...state, userConfig: config }
        }
    ),

    on(AuthActions.logout,
        () => {
            return { ...initialState }
        }
    ),

    on(AuthActions.saveThread,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const saved = [ ...data.saved ? data.saved : [], action.id ]
            data.saved = saved
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.saveThreadError,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const saved = data.saved.filter(thread => thread !== action.id)
            data.saved = saved
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.unsaveThread,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const saved = data.saved.filter(thread => thread !== action.id)
            data.saved = saved
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.unsaveThreadError,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const saved = [ ...data.saved ? data.saved : [], action.id ]
            data.saved = saved
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.followThread,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const following = [ ...data.following ? data.following : [], action.id ]
            data.following = following
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.followThreadError,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const following = data.following.filter(thread => thread !== action.id)
            data.following = following
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.unfollowThread,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const following = data.following.filter(thread => thread !== action.id)
            data.following = following
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.unfollowThreadError,
        (state, action) => {
            const data = { ...state.userData as UserData }
            const following = [ ...data.following ? data.following : [], action.id ]
            data.following = following
            return { ...state, userData: data }
        }
    ),

    on(AuthActions.getFilesSuccess,
        (state, action) => {
            const data = { ...state.userData as UserData }
            return {
                ...state,
                userData: {
                    ...data,
                    files: action.files
                }
            }
        }
    )

)
