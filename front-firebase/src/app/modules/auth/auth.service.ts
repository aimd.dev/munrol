import {
    Injectable
} from '@angular/core'

import {
    EMPTY,
    forkJoin,
    from
} from 'rxjs'

import {
    take
} from 'rxjs/operators'

import {
    onAuthStateChanged,
    signInWithEmailAndPassword,
    sendPasswordResetEmail,
    reauthenticateWithCredential,
    signOut,
    updateEmail,
    updatePassword,
    EmailAuthProvider,
    user,
    Auth,
    authState
} from '@angular/fire/auth'

import {
    Functions,
    httpsCallable
} from '@angular/fire/functions'

import {
    doc,
    getDoc,
    updateDoc,
    arrayUnion,
    query,
    arrayRemove,
    collection,
    where,
    getDocs,
    Firestore,
    writeBatch
} from '@angular/fire/firestore'

import { Store } from '@ngrx/store'
import * as fromAuth from './auth.reducer'
import * as AuthActions from './auth.actions'

import {
    UserSignup,
    UserAuth,
    UserLogin
} from './models'

import {
    NotificationService
} from '../notification/notification.service'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    /**
     * Constructor del servicio
     * @param auth firebase auth
     * @param functions firebase functions
     * @param firestore firebase firestore
     * @param store firebase storage
     */
    constructor(
        private auth: Auth,
        private functions: Functions,
        private firestore: Firestore,
        private store: Store<fromAuth.State>,
        private notifications: NotificationService
    ) {}
    /**
     * App initializer
    */
    init() {
        this.initAuthListener()
        return authState(this.auth).pipe(take(1)).toPromise()
    }
    /**
     * Auth listener
     */
    initAuthListener() {
        onAuthStateChanged(this.auth, (user) => {
            if (user) {
                this.setCurrentUser(user)
                this.notifications.pushSubscription(user.displayName as string)
            }
        })
    }
    /**
     * Setear el usuario conectado
     * @param user usuario
     */
    private setCurrentUser(user: any): void {
        const currentUser: UserAuth = {
            username: user.displayName as string,
            email: user.email as string
        }
        this.store.dispatch(AuthActions.setCurrentUser({ user: currentUser }))
    }
    /**
     * Obtener la data del usuario logeado
     * @param username nombre de usuario
     * @returns observables
     */
    getUserData(username: string) {
        return forkJoin([
            from(getDoc(doc(this.firestore, `users/${username}`))),
            from(getDoc(doc(this.firestore, `userFollowing/${username}`))),
            from(getDoc(doc(this.firestore, `userSaved/${username}`)))
        ])
    }
    /**
     * User sign up
     * @param user user
     */
    signup(user: UserSignup) {
        return from(
            httpsCallable(this.functions, 'signUp')({ user })
            .then(() => {
                const batch = writeBatch(this.firestore)
                // User document
                batch.set(doc(this.firestore, `users/${user.username}`), {
                    createdAt: new Date().toISOString(),
                    friends: {},
                    replyCount: 0,
                    threadCount: 0,
                    commentCount: 0
                })
                // User config
                batch.set(doc(this.firestore, `users/${user.username}/private/config`), {
                    replyRequestEmail: true,
                    threadInviteEmail: true
                })
                // User notifications
                batch.set(doc(this.firestore, `notifications/${user.username}`), {
                    newInvites: false,
                    newRequests: false,
                    newMentions: false
                })
                // Archive
                batch.set(doc(this.firestore, `userSaved/${user.username}`), { threads: [] })
                batch.set(doc(this.firestore, `userFollowing/${user.username}`), { threads: [] })
                // Inbox
                batch.set(doc(this.firestore, `chats/${user.username}`), { newMessages: false })
                // Commit
                return batch.commit()
            })
            .then(() => signInWithEmailAndPassword(this.auth, user.email, user.password))
        )
    }

    // singup2(user: UserSignup) {
    //     const batch = writeBatch(this.firestore)

    // }
    /**
     * User login
     * @param user user
     */
    login(user: UserLogin) {
        return from(signInWithEmailAndPassword(this.auth, user.email, user.password))
    }
    /**
     * Reset password
     * @param email email
     */
    passwordReset(email: string) {
        return from(sendPasswordResetEmail(this.auth, email))
    }

    getUserConfig(username: string) {
        return from(getDoc(doc(this.firestore, `users/${username}/private/config`)))
    }

    updateEmail(oldEmail: string, newEmail: string, password: string) {
        return from(user(this.auth).toPromise()
        .then((user) => {
            return user && reauthenticateWithCredential(user, EmailAuthProvider.credential(oldEmail, password))
            .then(() => updateEmail(user, newEmail))
        }))
    }

    updatePassword(email: string, oldPassword: string, newPassword: string) {
        return from(user(this.auth).toPromise()
        .then((user) => {
            return user && reauthenticateWithCredential(user, EmailAuthProvider.credential(email, oldPassword))
            .then(() => updatePassword(user, newPassword))
        }))
    }

    updateConfig(key: string, value: boolean, username: string) {
        return from(updateDoc(doc(this.firestore, `users/${username}/private/config`), {
            [key]: value
        }))
    }

    // Logout
    logout() {
        return signOut(this.auth)
    }

    saveThread(username: string, id: string) {
        return from(updateDoc(doc(this.firestore, `userSaved/${username}`), {
            threads: arrayUnion(id)
        }))
    }

    unsaveThread(username: string, id: string) {
        return from(updateDoc(doc(this.firestore, `userSaved/${username}`), {
            threads: arrayRemove(id)
        }))
    }

    followThread(username: string, id: string) {
        return from(updateDoc(doc(this.firestore, `userFollowing/${username}`), {
            threads: arrayUnion(id)
        }))
    }

    unfollowThread(username: string, id: string) {
        return from(updateDoc(doc(this.firestore, `userFollowing/${username}`), {
            threads: arrayRemove(id)
        }))
    }

    getUserFiles(username: string | undefined) {
        if (!username) return EMPTY
        return from(getDocs(query(collection(this.firestore, 'files'), where('author', '==', username))))
    }
}
