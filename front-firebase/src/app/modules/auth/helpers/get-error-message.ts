export const getErrorMessage = (code: string): string => {
    switch (code) {
        case 'auth/user-not-found':
        case 'auth/wrong-password':
            return 'Error de credenciales'
        case 'functions/already-exists':
            return 'El usuario ya existe'
        case 'functions/internal':
            return 'Puede que el correo ya este registrado'
        default:
            return 'Error inesperado'
    }
}
