import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromInbox from './inbox.reducer'

export const selectInboxState = createFeatureSelector<fromInbox.State>('inbox')

export const selectRoomList = createSelector(selectInboxState, (inbox) => inbox.roomList ? inbox.roomList : [])
export const selectActiveRoom = createSelector(selectInboxState, (inbox) => inbox.active)
export const selectMessages = createSelector(selectInboxState, (inbox) => inbox.messages)
