import { ChangeDetectionStrategy, Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
    selector: 'mr-inbox-home',
    templateUrl: './inbox-home.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxHomeComponent {

    constructor(
        private router: Router
    ) { }

    navigate(to: string) {
        this.router.navigate([`/inbox/${to}`])
    }

    reset() {
        this.router.navigate(['/inbox'])
    }
}
