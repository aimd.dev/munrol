import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit
} from '@angular/core'

import { Observable, Subject } from 'rxjs'
import { map, takeUntil, tap } from 'rxjs/operators'

import { ActivatedRoute } from '@angular/router'
import { Store } from '@ngrx/store'
import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'
import { selectActiveRoom } from '../../inbox.selectors'
import { Chatroom } from '@app/shared/models'
import { InboxService } from '../../inbox.service'

@Component({
    selector: 'mr-inbox-room',
    templateUrl: './inbox-room.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxRoomComponent implements OnInit, OnDestroy {

    user$: Observable<string | undefined>

    room$: Observable<string | undefined>

    unsubscriber$: Subject<void> = new Subject<void>()

    constructor(
        private route: ActivatedRoute,
        private store: Store<any>,
        private service: InboxService
    ) { }

    ngOnInit(): void {
        this.user$ = this.store.select(selectCurrentUserUsername)

        this.room$ = this.route.params
        .pipe(
            takeUntil(this.unsubscriber$),
            map(params => params.username)
        )
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
