import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { InboxRoutingModule } from './inbox-routing.module'
import { SharedModule } from '@app/shared/shared.module'

import { InboxHomeComponent } from './components/inbox-home/inbox-home.component'
import { InboxRoomComponent } from './components/inbox-room/inbox-room.component'

@NgModule({
  declarations: [
    InboxHomeComponent,
    InboxRoomComponent
  ],
  imports: [
    CommonModule,
    InboxRoutingModule,
    SharedModule
  ]
})
export class InboxModule { }
