import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'

import { AuthGuard } from '@app/core/guards/auth.guard'

import { InboxHomeComponent } from './components/inbox-home/inbox-home.component'
import { InboxRoomComponent } from './components/inbox-room/inbox-room.component'
import { IsFriendGuard } from '@app/core/guards/is-friend.guard'

const routes: Routes = [
    {
        path: '',
        component: InboxHomeComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: InboxRoomComponent
            },
            {
                path: ':username',
                component: InboxRoomComponent,
                canActivate: [IsFriendGuard]
            }
        ]
    }
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class InboxRoutingModule { }
