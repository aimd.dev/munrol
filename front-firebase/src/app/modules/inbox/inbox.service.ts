import { Injectable } from '@angular/core'
import { collection, deleteField, doc, Firestore, limitToLast, onSnapshot, orderBy, query, setDoc, writeBatch, updateDoc, docSnapshots } from '@angular/fire/firestore'
import { constants } from '@app/core/config/constants'
import { Chatroom, Message, PushNotifications } from '@app/shared/models'
import { HtmlSlicePipe } from '@app/shared/pipes/html-slice.pipe'
import { Store } from '@ngrx/store'
import { Unsubscribe } from 'firebase/auth'
import * as InboxActions from './inbox.actions'
import * as NotificationActions from '@modules/notification/notification.actions'
import { Subscription } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class InboxService {

    private limit: number = constants.roomPageLength

    private inbox: Subscription

    private list: Unsubscribe

    private log: Unsubscribe

    constructor(
        private firestore: Firestore,
        private store: Store<any>,
        private htmlSlice: HtmlSlicePipe
    ) { }

    init(username: string) {
        // unsubscribe
        if (this.inbox) this.inbox.unsubscribe()
        // init listener
        this.inbox = docSnapshots(doc(this.firestore, `chats/${username}`))
        .subscribe(res => {
        const doc = res.data()
            this.store.dispatch(NotificationActions.updateNotifications({
                state: doc as PushNotifications
            }))
        })
    }

    setActiveRoom(room: string) {
        this.store.dispatch(InboxActions.setActiveRoom({ room }))
    }

    initRoomList(username: string) {
        // unsubscribe
        if (this.list) this.list()
        // init listener
        this.list = onSnapshot(collection(this.firestore, `chats/${username}/rooms`), (res) => {
            const docs = res.docs.map((d) => {
                const doc = d.data()
                doc.id = d.id
                return doc
            })
            this.store.dispatch(InboxActions.setRoomList({ list: docs as Chatroom[] }))
        })
    }

    createRoom(from: string, to: string) {
        return setDoc(doc(this.firestore, `chats/${from}/rooms/${to}`), {
            newMessages: true,
            lastMessage: '',
            lastUpdated: null,
            typing: false
        })
    }

    initRoom(username: string, room: string) {
        if (this.log) this.log()
        this.log = onSnapshot(
            query(
                collection(this.firestore, `chats/${username}/rooms/${room}/messages`),
                orderBy('createdAt', 'asc'),
                limitToLast(constants.roomPageLength)
            ), (res) => {
            this.limit = this.limit + constants.roomPageLength
            const docs = res.docs.map((doc) => doc.data()).sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            this.store.dispatch(InboxActions.setMessages({ messages: docs as Message[] }))
        })
    }

    loadMore(username: string, room: string) {
        if (this.log) this.log()
        this.log = onSnapshot(
            query(
                collection(this.firestore, `chats/${username}/rooms/${room}/messages`),
                orderBy('createdAt', 'asc'),
                limitToLast(this.limit)
            ), (res) => {
            this.limit = this.limit + constants.roomPageLength
            const docs = res.docs.map((doc) => doc.data()).sort((a, b) => a.createdAt.localeCompare(b.createdAt))
            this.store.dispatch(InboxActions.setMessages({ messages: docs as Message[] }))
        })
    }

    terminateInbox() {
        if (this.inbox) this.inbox.unsubscribe()
    }

    terminateList() {
        if (this.list) this.list()
    }

    terminateRoom() {
        if (this.log) this.log()
    }

    deleteRoom(username: string, room: string) {
        const batch = writeBatch(this.firestore)
        batch.delete(doc(this.firestore, `chats/${username}/rooms/${room}`))
        batch.update(doc(this.firestore, `chats/${username}`), {
            [`newMessages.${room}`]: deleteField()
        })
        return batch.commit()
    }

    updateTyping(from: string, to: string, nowTyping: boolean) {
        return updateDoc(doc(this.firestore, `chats/${to}/rooms/${from}`), { nowTyping })
    }

    sendMessage(from: string, to: string, message: Message) {
        const batch = writeBatch(this.firestore)

        // set new messages to true in to room
        batch.set(doc(this.firestore, `chats/${to}`), { newMessages: true })

        // update last message in from room
        batch.set(doc(this.firestore, `chats/${from}/rooms/${to}`), {
            newMessages: true,
            lastMessage: this.htmlSlice.transform(message.body, 30),
            lastUpdated: message.createdAt,
            typing: false
        })

        // update last message in to room
        batch.set(doc(this.firestore, `chats/${to}/rooms/${from}`), {
            newMessages: true,
            lastMessage: this.htmlSlice.transform(message.body, 30),
            lastUpdated: message.createdAt,
            typing: false
        })

        // add message in to room log
        const toRef = doc(collection(this.firestore, `chats/${to}/rooms/${from}/messages/`))
        batch.set(toRef, { ...message })

        // add message in from room log
        const fromRef = doc(collection(this.firestore, `chats/${from}/rooms/${to}/messages/`))
        batch.set(fromRef, { ...message })

        return batch.commit()
    }

    markAsRead(from: string) {
        return updateDoc(doc(this.firestore, `chats/${from}`), { newMessages: false })
    }

    markRoomAsRead(from: string, to: string) {
        return updateDoc(doc(this.firestore, `chats/${from}/rooms/${to}`), { newMessages: false })
    }
}
