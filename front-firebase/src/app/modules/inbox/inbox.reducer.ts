import { Chatroom, Message } from '@app/shared/models'
import { createReducer, on } from '@ngrx/store'
import * as InboxActions from './inbox.actions'

export interface State {
    roomList: Chatroom[] | null
    messages: Message[] | null
    active: Chatroom | null
}

export const initialState: State = {
    roomList: null,
    messages: null,
    active: null
}

export const inboxReducer = createReducer(

    initialState,

    on(InboxActions.setRoomList,
        (state, action) => {
            return {
                ...state,
                roomList: action.list
            }
        }
    ),

    on(InboxActions.setActiveRoom,
        (state, action) => {
            const list = [ ...state.roomList as Chatroom[] ]
            const room = list.find(r => r.id === action.room) as Chatroom
            return {
                ...state,
                active: room
            }
        }
    ),

    on(InboxActions.setMessages,
        (state, action) => {
            return {
                ...state,
                messages: action.messages
            }
        }
    )

)
