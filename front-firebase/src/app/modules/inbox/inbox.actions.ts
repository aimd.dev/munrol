import { Chatroom, Message } from '@app/shared/models'
import { createAction, props } from '@ngrx/store'

export const setRoomList = createAction(
    '[Chatroom List] Set Chatroom List',
    props<{list: Chatroom[]}>()
)

export const setMessages = createAction(
    '[Chatroom] Set Messages',
    props<{messages: Message[]}>()
)

export const setActiveRoom = createAction(
    '[Inbox] Set Active Room',
    props<{room: string}>()
)
