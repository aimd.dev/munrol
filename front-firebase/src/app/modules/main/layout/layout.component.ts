import { Component, OnDestroy, OnInit } from '@angular/core'
import { Subject } from 'rxjs'

import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'
import { Store } from '@ngrx/store'

import { InboxService } from '@app/modules/inbox/inbox.service'
import { takeUntil, tap } from 'rxjs/operators'

@Component({
  selector: 'mr-layout',
  templateUrl: './layout.component.html',
  styles: []
})
export class LayoutComponent implements OnInit, OnDestroy {
    /**
     * Component unsubscriber
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Component constructor
     * @param store ngrx store
     * @param inbox inbox service
     */
    constructor(
        private store: Store<any>,
        private inbox: InboxService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.store.select(selectCurrentUserUsername)
        .pipe(
            takeUntil(this.unsubscriber$),
            tap(username => {
                if (username) this.inbox.init(username)
                else this.inbox.terminateInbox()
            })
        ).subscribe()
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
