import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AboutComponent } from '@app/core/components/about/about.component'
import { ContactComponent } from '@app/core/components/contact/contact.component'
import { PrivacyPolicyComponent } from '@app/core/components/privacy-policy/privacy-policy.component'
import { TermsAndConditionsComponent } from '@app/core/components/terms-and-conditions/terms-and-conditions.component'
import { LayoutComponent } from './layout/layout.component'

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            // Browse
            {
                path: '',
                loadChildren: () => import('../browse/browse.module').then(m => m.BrowseModule)
            },

            // Search
            {
                path: 'search',
                loadChildren: () => import('../search/search.module').then(m => m.SearchModule)
            },

            // Thread
            {
                path: 'threads',
                loadChildren: () => import('../thread/thread.module').then(m => m.ThreadModule)
            },

            // Profile
            {
                path: 'users',
                loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule)
            },

            // File
            {
                path: 'files',
                loadChildren: () => import('../file/file.module').then(m => m.FileModule)
            },

            // Notification
            {
                path: 'notifications',
                loadChildren: () => import('../notification/notification.module').then(m => m.NotificationModule)
            },

            // Inbox
            {
                path: 'inbox',
                loadChildren: () => import('../inbox/inbox.module').then(m => m.InboxModule)
            },

            // Privacy
            {
                path: 'privacy-policy',
                component: PrivacyPolicyComponent
            },

            // Terms
            {
                path: 'terms-and-conditions',
                component: TermsAndConditionsComponent
            },

            // About
            {
                path: 'about',
                component: AboutComponent
            },

            // Contact
            {
                path: 'contact',
                component: ContactComponent
            }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
    providers: [

    ]
})
export class MainRoutingModule {

}
