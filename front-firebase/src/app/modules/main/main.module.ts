import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { LayoutComponent } from './layout/layout.component'
import { MainRoutingModule } from './main-routing.module'
import { CoreModule } from '@core/core.module'
import { SharedModule } from '@app/shared/shared.module'
import { StoreModule } from '@ngrx/store'
import { inboxReducer } from '../inbox/inbox.reducer'

@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MainRoutingModule,
    CoreModule,
    SharedModule,
    StoreModule.forFeature('inbox', inboxReducer)
  ],
  providers: []
})
export class MainModule { }
