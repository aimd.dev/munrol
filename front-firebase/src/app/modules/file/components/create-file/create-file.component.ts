import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { selectCurrentUserAuth } from '@app/modules/auth/auth.selectors'
import { UserAuth } from '@app/modules/auth/models'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'
import { Store } from '@ngrx/store'
import { BehaviorSubject, Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { FileService } from '../../file.service'
import { NewFile } from '../../models/new-file.model'
import { FileUiService } from '../../services/file-ui.service'

@Component({
    selector: 'mr-create-file',
    templateUrl: './create-file.component.html',
    styles: [
    ]
})
export class CreateFileComponent implements OnInit, OnDestroy {

    username: string

    form: FormGroup

    loading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.ON)

    unsubscriber$: Subject<void> = new Subject()

    public toolbar: typeof EditorToolbar = EditorToolbar

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private store: Store<any>,
        private service: FileService,
        private vldt: EditorBodyRequiredValidatorService,
        public ui: FileUiService
    ) { }

    ngOnInit(): void {
        this.store.select(selectCurrentUserAuth)
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe(user => {
                this.loading$.next(LoaderStateEnum.OFF)
                user ? this.username = (user as UserAuth).username : null
            })

        this.form = this.fb.group({
            author: [this.username, [Validators.required]],
            title: ['', [Validators.required, Validators.maxLength(60)]],
            body: ['', [this.vldt.editorBodyRequired()]],
            image: ['', [Validators.required]]
        })
    }

    submit(): void {
        this.form.markAllAsTouched()
        this.form.updateValueAndValidity()
        if (this.form.invalid) return

        const file: NewFile = { ...this.form.value }
        file.createdAt = new Date().toISOString()
        file.linkedThreads = []
        file.linkedFiles = []

        this.service.createFile(file)
        .then((res) => this.router.navigate([`/files/${res}`]))
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
