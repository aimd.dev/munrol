import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms'

import {
    ActivatedRoute, Router
} from '@angular/router'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    takeUntil
} from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as FileActions from '../../file.actions'
import { selectCurrentFile } from '../../file.selectors'

import {
    loaderState
} from '@app/shared/models'

import {
    editorBodyRequired
} from '@app/shared/util/custom-validators/editor-body-required-validator'
import { FileService } from '../../file.service'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'
import { FileUiService } from '../../services/file-ui.service'

@Component({
    selector: 'mr-edit-file',
    templateUrl: './edit-file.component.html',
    styles: [
    ]
})
export class EditFileComponent implements OnInit, OnDestroy {

    form: FormGroup

    loading$: Observable<loaderState>

    unsubscriber$: Subject<void> = new Subject()

    public toolbar: typeof EditorToolbar = EditorToolbar

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private store: Store<any>,
        private vldt: EditorBodyRequiredValidatorService,
        private service: FileService,
        public ui: FileUiService
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe((params) => {
            if (params.id) {
                this.store.dispatch(FileActions.getFile({ id: params.id }))
                // subscribe to select
                this.store.select(selectCurrentFile)
                .pipe(
                    takeUntil(this.unsubscriber$)
                ).subscribe((file) => {
                    if (!file) return
                    if (file.id !== params.id) return

                    this.form = this.fb.group({
                        id: [file.id],
                        title: [file.title, [Validators.required, Validators.maxLength(60)]],
                        body: [file.body, [this.vldt.editorBodyRequired()]],
                        image: [file.picture, [Validators.required]]
                    })
                })
            }
        })
    }

    submit(): void {
        this.form.markAllAsTouched()
        this.form.updateValueAndValidity()
        if (this.form.invalid) return

        const file = { ...this.form.value }
        file.lastEdit = new Date().toISOString()
        if (typeof file.image === 'string') delete file.image

        this.service.editFile(file)
        .then((res) => this.router.navigate([`/files/${res}`]))
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
