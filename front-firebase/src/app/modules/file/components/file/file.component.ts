import {
    Component,
    OnInit
} from '@angular/core'

import {
    ActivatedRoute, Router
} from '@angular/router'

import {
    combineLatest,
    Observable
} from 'rxjs'

import { Store } from '@ngrx/store'
import * as FileActions from '../../file.actions'
import { selectCurrentFile } from '../../file.selectors'

import {
    ModalService
} from '@app/shared/components/modal/modal.service'

import {
    DeleteFileModalComponent
} from '../delete-file-modal/delete-file-modal.component'

import {
    File
} from '@app/shared/models'
import { map, tap } from 'rxjs/operators'
import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'

@Component({
    selector: 'mr-file',
    templateUrl: './file.component.html',
    styles: [
    ]
})
export class FileComponent implements OnInit {
    /**
     * Current file
     */
    file$: Observable<File | null> = new Observable<File | null>()
    /**
     * Constructor del componente
     * @param route activated route
     * @param store ngrx store
     * @param modal modal service
     */
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private store: Store<any>,
        private modal: ModalService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.route.params.subscribe(params => {
            if (params.id) {
                this.file$ = combineLatest([this.store.select(selectCurrentFile), this.store.select(selectCurrentUserUsername)])
                .pipe(
                    map(([file, user]) => {
                        if (file) file = { ...file, isAuthor: user === file?.author }
                        return file
                    })
                )
                this.store.dispatch(FileActions.getFile({ id: params.id }))
            }
        })
    }
    editFile(id: string) {
        this.router.navigate([`/files/edit/${id}`])
    }
    /**
     * Delete file
     * @param id file id
     * @param img file picture
     */
    deleteFile(id: string, img: string) {
        const props = [{key: 'id', value: id}, {key: 'img', value: img}]
        this.modal.open(DeleteFileModalComponent, props)
    }
}
