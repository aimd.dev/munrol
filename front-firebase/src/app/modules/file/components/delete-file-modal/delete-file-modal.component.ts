import {
    Component,
    Input
} from '@angular/core'

import {
    Location
} from '@angular/common'

import {
    BehaviorSubject
} from 'rxjs'

import { Store } from '@ngrx/store'
import * as FileActions from '../../file.actions'

import { FileService } from '../../file.service'
import { AlertsService } from '@app/shared/services/alerts.service'

import {
    loaderState,
    LoaderStateEnum
} from '@app/shared/models'

@Component({
    selector: 'mr-delete-file-modal',
    templateUrl: './delete-file-modal.component.html'
})
export class DeleteFileModalComponent {
    /**
     * Loader state
     */
    loading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
    /**
     * File id
     */
    @Input()
    id: string
    /**
     * File image
     */
    @Input()
    img: string
    /**
     * Constructor del componente
     * @param _location location
     * @param store ngrx store
     * @param service file service
     * @param alerts alerts service
     */
    constructor(
        private _location: Location,
        private store: Store<any>,
        private service: FileService,
        private alerts: AlertsService
    ) { }
    /**
     * Delete file
     */
    deleteFile() {
        this.loading$.next(LoaderStateEnum.ON)
        this.service.deleteFile(this.id, this.img)
        .then(() => {
            this._location.back()
            this.store.dispatch(FileActions.reset())
            this.alerts.success('Ficha eliminada con éxito')
        })
        .catch((err) => {
            console.log(err)
            this.alerts.error('No se pudo eliminar la ficha')
            this.loading$.next(LoaderStateEnum.OFF)
        })
    }
}
