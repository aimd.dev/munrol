import { createAction, props } from '@ngrx/store'
import { File } from '@app/shared/models'
import { Thread } from '../thread/models/Thread.model'

export const getFile = createAction(
    '[Edit File] Get File',
    props<{id: string}>()
)

export const getFileSuccess = createAction(
    '[Edit File] Get File Success',
    props<{file: File}>()
)

export const setFileThreads = createAction(
    '[Expanded File] Set File Threads Initial',
    props<{threads: Thread[]}>()
)

export const getFileThreads = createAction(
    '[Expanded File] Get File Threads',
    props<{ids: string[]}>()
)

export const getFileThreadsSuccess = createAction(
    '[Expanded File] Get File Threads Success',
    props<{threads: Thread[]}>()
)

export const reset = createAction(
    '[Expanded File] Reset'
)
