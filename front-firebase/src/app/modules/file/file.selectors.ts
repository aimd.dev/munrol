import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromFile from './file.reducer'

export const selectFileState = createFeatureSelector<fromFile.State>('file')

export const selectFileThreads = createSelector(selectFileState, (file) => file.threads)
export const selectCurrentFile = createSelector(selectFileState, (file) => file.expanded)
