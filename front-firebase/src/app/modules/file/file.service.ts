import {
    Injectable
} from '@angular/core'

import {
    from
} from 'rxjs'

import {
    addDoc,
    collection,
    deleteDoc,
    doc,
    Firestore,
    getDoc,
    getDocs,
    query,
    updateDoc,
    where
} from '@angular/fire/firestore'

import {
    uploadBytes,
    ref,
    Storage,
    deleteObject
} from '@angular/fire/storage'

import {
    FileUiService
} from './services/file-ui.service'

import {
    AlertsService
} from '@app/shared/services/alerts.service'

import {
    LoaderStateEnum
} from '@app/shared/models'

import {
    NewFile
} from './models/new-file.model'
import { documentId } from 'firebase/firestore'

@Injectable({
    providedIn: 'root'
})
export class FileService {
    /**
     * Constructor del servicio
     * @param firestore firebase firestore
     * @param storage firebase storage
     * @param alerts alert service
     * @param ui ui service
     */
    constructor(
        private firestore: Firestore,
        private storage: Storage,
        private alerts: AlertsService,
        private ui: FileUiService
    ) { }
    /**
     * Create File
     * @param file file
     */
    createFile(file: NewFile) {
        // Start loading
        this.ui.setPostFileLoading(LoaderStateEnum.ON)
        // Save image file
        const img = file.image as File
        file.picture = `files/${img.name}`
        delete file.image
        let id: string
        // Return promise
        return addDoc(collection(this.firestore, 'files'), file)
            .then((res) => {
                console.log(res)
                id = res.id
                return uploadBytes(ref(this.storage, file.picture), img)
            })
            .then(() => {
                this.ui.setPostFileLoading(LoaderStateEnum.OFF)
                this.alerts.success('Ficha creada con éxito')
                return id
            })
            .catch((err) => {
                console.log(err)
                this.ui.setPostFileLoading(LoaderStateEnum.OFF)
                this.alerts.error('No se pudo publicar la ficha')
            })
    }
    /**
     * Edit file
     * @param file
     */
    async editFile(file: any) {
        // Start loading
        this.ui.setPostFileLoading(LoaderStateEnum.ON)
        // Save image file
        if (file.image) {
            const img = file.image as File
            file.picture = `files/${img.name}`
            await uploadBytes(ref(this.storage, file.picture), img)
        }
        delete file.image
        const id = file.id
        delete file.id
        // Return promise
        return updateDoc(doc(this.firestore, `files/${id}`), file)
            .then(() => {
                this.ui.setPostFileLoading(LoaderStateEnum.OFF)
                this.alerts.success('Ficha actualizada')
            })
            .catch((err) => {
                console.log(err)
                this.ui.setPostFileLoading(LoaderStateEnum.OFF)
                this.alerts.error('No se pudo publicar la ficha')
            })
    }
    /**
     * Delete file
     * @param id file id
     * @param image file picture
     */
    deleteFile(id: string, image: string) {
        return deleteDoc(doc(this.firestore, `files/${id}`))
        .then(() => deleteObject(ref(this.storage, image)))
    }
    /**
     * Get file by id
     * @param id file id
     */
    getFile(id: string) {
        return from(getDoc(doc(this.firestore, `files/${id}`)))
    }
    /**
     * Get linked threads in a file
     * @param ids id list
     */
    getFileThreads(ids: string[]) {
        return from(getDocs(query(collection(this.firestore, 'threads'), where(documentId(), 'in', ids))))
    }
}
