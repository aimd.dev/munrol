export interface NewFile {
    author: string,
    body: string,
    createdAt: string,
    title: string,
    image?: File,
    picture: string,
    linkedThreads: [],
    linkedFiles: []
}
