import { Injectable } from '@angular/core'
import { File } from '@app/shared/models'
import { AlertsService } from '@app/shared/services/alerts.service'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { EMPTY } from 'rxjs'
import { catchError, exhaustMap, map } from 'rxjs/operators'
import { Thread } from '../thread/models/Thread.model'
import * as FileActions from './file.actions'
import { FileService } from './file.service'

@Injectable()
export class FileEffects {

    constructor(
        private actions$: Actions,
        private service: FileService,
        private store: Store<any>,
        private alert: AlertsService
    ) {}

    getFile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FileActions.getFile),
            exhaustMap((action) => this.service.getFile(action.id)
                .pipe(
                    map((res) => {
                        const file = res.data() as File
                        file.id = res.id
                        return FileActions.getFileSuccess({ file })
                    }),
                    catchError((err) => {
                        this.alert.error('Ha ocurrido un error')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    getFileThreads$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FileActions.getFileThreads),
            exhaustMap((action) => this.service.getFileThreads(action.ids)
                .pipe(
                    map((res) => {
                        const docs = res.docs
                        const threads = docs.map((t) => {
                            const thread = t.data() as Thread
                            thread.id = t.id
                            return thread
                        })
                        return FileActions.getFileThreadsSuccess({ threads })
                    }),
                    catchError((err) => {
                        this.alert.error('Ha ocurrido un error')
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )
}
