import { File } from '@app/shared/models'
import { createReducer, on } from '@ngrx/store'
import { Thread } from '../thread/models/Thread.model'
import * as FileActions from './file.actions'

export interface State {
    expanded: File | null
    threads: Thread[] | null
}

export const initialState: State = {
    expanded: null,
    threads: null
}

export const fileReducer = createReducer(

    initialState,

    on(FileActions.getFileSuccess,
        (state, action) => {
            return {
                ...state,
                expanded: action.file
            }
        }
    ),

    on(FileActions.setFileThreads,
        (state, action) => {
            return {
                ...state,
                threads: action.threads
            }
        }
    ),

    on(FileActions.getFileThreadsSuccess,
        (state, action) => {
            const ids = action.threads.map(t => t.id)
            const threads = [ ...state.threads as Thread[] ]

            const newThreads: Thread[] | [] = threads.map(t => {
                if (ids.includes(t.id)) {
                    return action.threads.find(old => old.id === t.id) as Thread
                } else {
                    return t
                }
            })

            return {
                ...state,
                threads: newThreads
            }
        }
    ),

    on(FileActions.reset,
        () => initialState
    )
)
