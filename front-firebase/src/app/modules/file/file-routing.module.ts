import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'

import { CreateFileComponent } from './components/create-file/create-file.component'
import { EditFileComponent } from './components/edit-file/edit-file.component'
import { FileComponent } from './components/file/file.component'

import { AuthGuard } from '@app/core/guards/auth.guard'

const routes: Routes = [
    {
        path: 'new',
        component: CreateFileComponent,
        canActivate: [AuthGuard]
    },
    {
        path: ':id',
        component: FileComponent
    },
    {
        path: 'edit/:id',
        component: EditFileComponent,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class FileRoutingModule { }
