import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { FileRoutingModule } from './file-routing.module'
import { SharedModule } from '@app/shared/shared.module'

import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { FileEffects } from './file.effects'

import { CreateFileComponent } from './components/create-file/create-file.component'
import { EditFileComponent } from './components/edit-file/edit-file.component'
import { fileReducer } from './file.reducer';
import { FileComponent } from './components/file/file.component';
import { DeleteFileModalComponent } from './components/delete-file-modal/delete-file-modal.component'

@NgModule({
  declarations: [
    CreateFileComponent,
    EditFileComponent,
    FileComponent,
    DeleteFileModalComponent
  ],
  imports: [
    CommonModule,
    FileRoutingModule,
    SharedModule,
    StoreModule.forFeature('file', fileReducer),
    EffectsModule.forFeature([FileEffects])
  ]
})
export class FileModule { }
