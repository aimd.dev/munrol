import { Injectable } from '@angular/core'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class FileUiService {

    private _postFileLoading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.OFF)
    postFileLoading$: Observable<loaderState> = this._postFileLoading$.asObservable()

    constructor() { }

    setPostFileLoading(state: loaderState) {
        this._postFileLoading$.next(state)
    }
}
