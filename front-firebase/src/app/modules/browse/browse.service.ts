import { Injectable } from '@angular/core'
import { from } from 'rxjs'
import { Firestore, collection, query, orderBy, startAfter, limit, getDocs } from '@angular/fire/firestore'
import { constants } from '@app/core/config/constants'

@Injectable({
    providedIn: 'root'
})
export class BrowseService {
    /**
     * Constructor del servicio
     * @param firestore angular firestore
     */
    constructor(
        private firestore: Firestore
    ) { }
    /**
     * Obtener lista de threads para el home
     * @param query ordenar por
     * @param last last fetched
     * @returns lista de threads
     */
    getThreads(order: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        orderBy(order, 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        orderBy(order, 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }
}
