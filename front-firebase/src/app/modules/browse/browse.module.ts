import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowseRoutingModule } from './browse-routing.module';
import { HomeComponent } from './home/home.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BrowseEffects } from './browse.effects';
import { browseReducer } from './browse.reducer';
import { SharedModule } from '@app/shared/shared.module';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    BrowseRoutingModule,
    SharedModule,
    StoreModule.forFeature('browse', browseReducer),
    EffectsModule.forFeature([BrowseEffects])
  ]
})
export class BrowseModule { }
