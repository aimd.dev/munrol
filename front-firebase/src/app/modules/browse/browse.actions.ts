import { createAction, props } from '@ngrx/store'
import { ThreadBrief } from '../thread/models/ThreadBrief.model'

export const setView = createAction(
    '[Browse Page] Set View',
    props<{view: string}>()
)

export const getThreads = createAction(
    '[Browse Page] Get Threads',
    props<{last?: string, view: string}>()
)

export const getThreadsSuccess = createAction(
    '[Browse Page] Get Threads Success',
    props<{threads: ThreadBrief[]}>()
)

export const reset = createAction(
    '[Browse Page] Reset State'
)
