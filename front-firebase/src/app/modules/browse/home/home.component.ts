import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { selectCurrentUserAuth } from '@app/modules/auth/auth.selectors'
import { UserAuth } from '@app/modules/auth/models'
import { ThreadBrief } from '@app/modules/thread/models/ThreadBrief.model'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { concatLatestFrom } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { Observable, Subject } from 'rxjs'
import { map, skip, takeUntil, tap } from 'rxjs/operators'
import * as BrowseActions from '../browse.actions'
import { selectCurrentThreads, selectCurrentView } from '../browse.selectors'
import { BrowseUiService } from '../services/browse-ui.service'

@Component({
    selector: 'mr-home',
    templateUrl: './home.component.html',
    styles: []
})
export class HomeComponent implements OnInit, OnDestroy {
    /**
     * Timestamp of last fetched
     */
    last: string | undefined
    /**
     * Active tab
     */
    active$: Observable<string>
    /**
     * Thread list
     */
    threads$: Observable<ThreadBrief[] | null>
    /**
     * Loading state
     */
    loading$: Observable<loaderState>
    /**
     * Usuario conectado
     */
    currentUser$: Observable<UserAuth | null>
    /**
     * Component unsubscriber
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Component constructor
     * @param store ngrx store
     * @param ui ui service
     */
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private store: Store<any>,
        public ui: BrowseUiService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        // Suscribirse al usuario conectado
        this.currentUser$ = this.store.select(selectCurrentUserAuth)

        this.ui.setGetLoading(LoaderStateEnum.ON)

        this.route.queryParams.subscribe(params => {
            const view = params.tab && params.tab === 'lastUpdated' ? params.tab : 'createdAt'
            this.store.dispatch(BrowseActions.setView({ view }))
        })

        this.threads$ = this.store.select(selectCurrentThreads)
            .pipe(
                skip(1),
                concatLatestFrom(() => this.store.select(selectCurrentView)),
                tap(([data, view]) => {
                    data && data.length ? this.last = data[data.length - 1][view as 'createdAt' | 'lastUpdated'] : this.last = undefined
                }),
                map(([data, view]) => data)
            )

        this.active$ = this.store.select(selectCurrentView)
            .pipe(
                map((tab) => tab ? tab : 'createdAt'),
                tap((tab) => this.store.dispatch(BrowseActions.getThreads({ view: tab })))
            )

        this.loading$ = this.ui.getLoading$
            .pipe(
                takeUntil(this.unsubscriber$),
                map((service) => service)
            )
    }
    /**
     * Selects a tab
     * @param tab tab to select
     */
    select(tab: string) {
        this.router.navigate(['.'], { queryParams: { tab: tab }, relativeTo: this.route })
    }
    /**
     * Load more threads
     */
    loadMore(view: string) {
        this.store.dispatch(BrowseActions.getThreads({ last: this.last, view }))
    }
    /**
     * Reset store
     */
    reset() {
        this.store.dispatch(BrowseActions.reset())
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.reset()
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
