import { createReducer, on } from "@ngrx/store";
import { ThreadBrief } from "../thread/models/ThreadBrief.model";
import * as BrowseActions from "./browse.actions";

export interface State {
    threads: ThreadBrief[] | null
    view: string | null
}

export const initialState: State = {
    threads: null,
    view: null
}

export const browseReducer = createReducer(

    initialState,

    on(BrowseActions.setView,
        (state, action) => {
            return {
                ...initialState,
                view: action.view
            }
        }
    ),

    on(BrowseActions.getThreadsSuccess,
        (state, action) => {
            const reference = state.threads ? [...state.threads] : []
            return {
                ...state,
                threads: reference.concat(action.threads)
            }
        }
    ),

    on(BrowseActions.reset,
        (state, action) => initialState
    )
)
