import { Injectable } from '@angular/core'
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { catchError, exhaustMap, map, tap } from 'rxjs/operators'
import { BrowseService } from './browse.service'
import * as BrowseActions from './browse.actions'
import { AlertsService } from '@app/shared/services/alerts.service'
import { EMPTY } from 'rxjs'
import { ThreadBrief } from '../thread/models/ThreadBrief.model'
import { selectCurrentView } from './browse.selectors'
import { BrowseUiService } from './services/browse-ui.service'
import { LoaderStateEnum } from '@app/shared/models'

@Injectable()
export class BrowseEffects {

    constructor(
        private actions$: Actions,
        private service: BrowseService,
        private store: Store<any>,
        private alert: AlertsService,
        private ui: BrowseUiService
    ) {}

    getThreads$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BrowseActions.getThreads),
            tap(() => this.ui.setGetLoading(LoaderStateEnum.ON)),
            exhaustMap((action) => this.service.getThreads(action.view, action.last)
                .pipe(
                    tap(() => {
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                    }),
                    map((data) => {
                        const threads = data.docs.map((r) => {
                            const thread = r.data() as ThreadBrief
                            thread.id = r.id
                            return thread
                        })
                        if (threads.length === 0) {
                            this.ui.setEndOfDocument(true)
                        }
                        return BrowseActions.getThreadsSuccess({ threads })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alert.error('No se ha podido obtener el hilo')
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                        return EMPTY
                    })
                )
            )
        )
    )

    setView$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BrowseActions.setView),
            tap(() => {
                this.ui.setEndOfDocument(false)
            })
        ), { dispatch: false }
    )

    reset$ = createEffect(() =>
        this.actions$.pipe(
            ofType(BrowseActions.reset),
            tap(() => {
                this.ui.setEndOfDocument(false)
            })
        ), { dispatch: false }
    )

}
