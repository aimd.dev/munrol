import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromBrowse from "./browse.reducer";

export const selectBrowseState = createFeatureSelector<fromBrowse.State>('browse')

export const selectCurrentView = createSelector(selectBrowseState, (browse) => browse.view)
export const selectCurrentThreads = createSelector(selectBrowseState, (browse) => browse.threads)