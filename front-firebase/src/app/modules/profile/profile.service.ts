import { Injectable } from '@angular/core'
import { query, collection, getDocs, orderBy, startAfter, limit, where, getDoc, doc, updateDoc, documentId, Firestore, writeBatch, deleteField } from '@angular/fire/firestore'
import { ref, Storage, uploadBytes } from '@angular/fire/storage'
import { constants } from '@app/core/config/constants'
import { Store } from '@ngrx/store'
import { from } from 'rxjs'
import * as ProfileActions from './profile.actions'

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor(
        private firestore: Firestore,
        private storage: Storage,
        private store: Store<any>
    ) { }

    getProfile(username: string) {
        return from(getDoc(doc(this.firestore, `users/${username}`)))
    }

    getProfileThreads(username: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'threads'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }

    getProfileReplies(username: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'replies'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'replies'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }

    getProfileFiles(username: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'files'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'files'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }

    getProfileComments(username: string, last?: string) {
        if (last) {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'comments'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        startAfter(last),
                        limit(constants.pageLength)
                    )
                )
            )
        } else {
            return from(
                getDocs(
                    query(
                        collection(this.firestore, 'comments'),
                        where('author', '==', username),
                        orderBy('createdAt', 'desc'),
                        limit(constants.pageLength)
                    )
                )
            )
        }
    }

    getProfileSaved(ids: string[]) {
        return from(getDocs(query(collection(this.firestore, 'threads'), where(documentId(), 'in', ids))))
    }

    updateBio(username: string, bio: string) {
        return from(updateDoc(doc(this.firestore, `users/${username}`), { bio }))
    }

    async updateAvatar(username: string, avatar: File) {
        await uploadBytes(ref(this.storage, `users/${username}/${username}.png`), avatar)
            .then(() => this.store.dispatch(ProfileActions.updateAvatarSuccess()))
            .catch((error) => this.store.dispatch(ProfileActions.updateAvatarError({ error })))
    }

    sendFriendShipRequest(source: string, target: string) {
        const batch = writeBatch(this.firestore)
        batch.set(doc(this.firestore, `notifications/${target}/requests/${source}`), { createdAt: new Date().toISOString() })
        batch.update(doc(this.firestore, `notifications/${target}`), { newRequests: true })
        return from(batch.commit())
    }

    endFriendship(source: string, target: string) {
        const batch = writeBatch(this.firestore)

        batch.update(doc(this.firestore, `users/${source}`), {
            [`friends.${target}`]: deleteField()
        })
        batch.update(doc(this.firestore, `users/${target}`), {
            [`friends.${source}`]: deleteField()
        })

        return from(batch.commit())
    }
}
