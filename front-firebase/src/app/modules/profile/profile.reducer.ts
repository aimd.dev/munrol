import { createReducer, on } from '@ngrx/store'
import * as ProfileActions from './profile.actions'
import { ThreadBrief } from '../thread/models/ThreadBrief.model'
import { ProfileUser } from './models/ProfileUser.model'
import { Reply } from '../thread/models/Reply.model'
import { File } from '@app/shared/models'
import { Comment } from '../thread/models/Comment.model'
import * as ThreadActions from '@modules/thread/thread.actions'

export interface State {
    user: ProfileUser | null
    threads: ThreadBrief[] | null
    replies: Reply[] | null
    files: File[] | null
    comments: Comment[] |  null
    saved: ThreadBrief[] | null
    view: string | null
}

export const initialState: State = {
    user: null,
    threads: null,
    replies: null,
    files: null,
    comments: null,
    saved: null,
    view: null
}

export const profileReducer = createReducer(

    initialState,

    on(ProfileActions.setView,
        (state, action) => {
            return {
                ...state,
                view: action.view
            }
        }
    ),

    on(ProfileActions.getProfileSuccess,
        (state, action) => {
            return {
                ...state,
                user: action.profile,
                threads: null,
                replies: null,
                files: null,
                comments: null,
                saved: null
            }
        }
    ),

    on(ProfileActions.getProfileThreadsSuccess,
        (state, action) => {
            const threads = [ ...(state.threads ? state.threads : []) ]
            return {
                ...state, threads: threads.concat(action.threads)
            }
        }
    ),

    on(ProfileActions.getProfileRepliesSuccess,
        (state, action) => {
            const replies = [ ...(state.replies ? state.replies : []) ]
            return {
                ...state, replies: replies.concat(action.replies)
            }
        }
    ),

    on(ProfileActions.getProfileFilesSuccess,
        (state, action) => {
            const files = [ ...(state.files ? state.files : []) ]
            return {
                ...state, files: files.concat(action.files)
            }
        }
    ),

    on(ProfileActions.getProfileCommentsSuccess,
        (state, action) => {
            const comments = [ ...(state.comments ? state.comments : []) ]
            return {
                ...state, comments: comments.concat(action.comments)
            }
        }
    ),

    on(ProfileActions.getProfileSavedInitial,
        (state, action) => {
            return {
                ...state, saved: action.ids as any
            }
        }
    ),

    on(ProfileActions.getProfileSavedSuccess,
        (state, action) => {
            const ids = action.threads.map(t => t.id)
            const threads = [ ...(state.saved ? state.saved : []) ]

            const newThreads: ThreadBrief[] | [] = threads.map(t => {
                if (ids.includes(t.id)) {
                    return action.threads.find(old => old.id === t.id) as ThreadBrief
                } else {
                    return t
                }
            })

            return {
                ...state, saved: newThreads
            }
        }
    ),

    on(ProfileActions.updateBio,
        (state, action) => {
            return {
                ...state,
                user: {
                    ...state.user as ProfileUser,
                    bio: action.bio
                }
            }
        }
    ),

    on(ProfileActions.endFriendshipSuccess,
        (state, action) => {
            const newFriends = { ...(state.user as ProfileUser).friends }
            delete newFriends[action.target]
            return {
                ...state,
                user: {
                    ...state.user as ProfileUser,
                    friends: newFriends
                }
            }
        }
    ),

    on(ProfileActions.resetProfile,
        () => {
            return {
                ...initialState
            }
        }
    ),

    on(ThreadActions.removeCommentSuccess,
        (state, action) => {
            if (!state.comments) { return { ...state } }
            const comments = [...state.comments as Comment[]]
            return {
                ...state,
                user: { ...state.user as ProfileUser, commentCount: (state.user as ProfileUser).commentCount - 1 },
                comments: comments.filter(c => c.id !== action.comment)
            }
        }
    ),

    on(ThreadActions.deleteReplySuccess,
        (state, action) => {
            if (!state.replies) { return { ...state } }
            const replies = [ ...state.replies as Reply[]]
            return {
                ...state,
                user: { ...state.user as ProfileUser, replyCount: (state.user as ProfileUser).replyCount - 1 },
                replies: replies.filter(r => r.id !== action.id)
            }
        }
    )

)
