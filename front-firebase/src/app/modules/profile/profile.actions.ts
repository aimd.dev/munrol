import { File } from '@app/shared/models'
import { createAction, props } from '@ngrx/store'
import { Comment } from '../thread/models/Comment.model'
import { Reply } from '../thread/models/Reply.model'
import { ThreadBrief } from '../thread/models/ThreadBrief.model'
import { ProfileUser } from './models/ProfileUser.model'

export const setView = createAction(
    '[Profile Page] Set View',
    props<{view: string}>()
)

export const getProfile = createAction(
    '[Profile Page] Get User Profile',
    props<{username: string}>()
)

export const getProfileSuccess = createAction(
    '[Profile Page] Get User Profile Success',
    props<{profile: ProfileUser}>()
)

export const getProfileThreads = createAction(
    '[Profile Page] Get User Threads',
    props<{username: string, last?: string, force?: boolean}>()
)

export const getProfileThreadsSuccess = createAction(
    '[Profile Page] Get User Threads Success',
    props<{threads: ThreadBrief[]}>()
)

export const getProfileReplies = createAction(
    '[Profile Page] Get User Replies',
    props<{username: string, last?: string, force?: boolean}>()
)

export const getProfileRepliesSuccess = createAction(
    '[Profile Page] Get User Replies Success',
    props<{replies: Reply[]}>()
)

export const getProfileFiles = createAction(
    '[Profile Page] Get User Files',
    props<{username: string, last?: string, force?: boolean}>()
)

export const getProfileFilesSuccess = createAction(
    '[Profile Page] Get User Files Success',
    props<{files: File[]}>()
)

export const getProfileComments = createAction(
    '[Profile Page] Get User Comments',
    props<{username: string, last?: string, force?: boolean}>()
)

export const getProfileCommentsSuccess = createAction(
    '[Profile Page] Get User Comments Success',
    props<{comments: Comment[]}>()
)

export const getProfileSavedInitial = createAction(
    '[Profile Page] Get User Saved Initial',
    props<{ids: ThreadBrief[]}>()
)

export const getProfileSaved = createAction(
    '[Profile Page] Get User Saved',
    props<{ids: string[]}>()
)

export const getProfileSavedSuccess = createAction(
    '[Profile Page] Get User Saved Success',
    props<{threads: ThreadBrief[]}>()
)

export const updateBio = createAction(
    '[Profile Page] Update User Bio',
    props<{username: string, bio: string}>()
)

export const updateAvatarSuccess = createAction(
    '[Profile Page] Update User Avatar Success'
)

export const updateAvatarError = createAction(
    '[Profile Page] Update User Avatar Error',
    props<{error: any}>()
)

export const sendFriendShipRequest = createAction(
    '[Profile Page] Send Friendship Request',
    props<{target: string}>()
)

export const resetProfile = createAction(
    '[Profile Page] Reset Profile Page'
)

export const endFriendship = createAction(
    '[Profile Page] End Friendship',
    props<{target: string}>()
)

export const endFriendshipSuccess = createAction(
    '[Profile Page] End Friendship Success',
    props<{target: string}>()
)
