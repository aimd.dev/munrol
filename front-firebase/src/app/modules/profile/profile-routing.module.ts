import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AuthGuard } from '@app/core/guards/auth.guard';
import { IsUserGuard } from '@app/core/guards/is-user.guard';

const routes: Routes = [
  {
    path: ':username',
    component: ProfilePageComponent
  },
  {
    path: ':username/settings',
    component: SettingsComponent,
    canActivate: [AuthGuard, IsUserGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProfileRoutingModule { }
