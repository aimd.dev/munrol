import { ProfileUser } from './ProfileUser.model';

export interface Profile {
    user: ProfileUser
}
