import { Friend } from '@app/shared/models'

export interface ProfileUser {
    id: string
    createdAt: string
    username: string
    bio: string
    isCurrentUser: boolean
    friends: {[key: string]: string}
    replyCount: number
    threadCount: number
    commentCount: number
    friendsMapped: Friend[]
}
