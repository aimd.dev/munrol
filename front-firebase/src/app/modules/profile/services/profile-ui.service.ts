import { Injectable } from '@angular/core'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { BehaviorSubject, Observable, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ProfileUiService {

  private _reloadAvatar$: Subject<void> = new Subject<void>()
  reloadAvatar$: Observable<void> = this._reloadAvatar$.asObservable()

  private _loading$: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(LoaderStateEnum.ON)
  loading$: Observable<loaderState> = this._loading$.asObservable()

  private _endOfThreads$: Subject<boolean> = new Subject<boolean>()
  endOfThreads$: Observable<boolean> = this._endOfThreads$.asObservable()

  private _endOfReplies$: Subject<boolean> = new Subject<boolean>()
  endOfReplies$: Observable<boolean> = this._endOfReplies$.asObservable()

  private _endOfSaved$: Subject<boolean> = new Subject<boolean>()
  endOfSaved$: Observable<boolean> = this._endOfSaved$.asObservable()

  private _endOfFiles$: Subject<boolean> = new Subject<boolean>()
  endOfFiles$: Observable<boolean> = this._endOfFiles$.asObservable()

  private _endOfComments$: Subject<boolean> = new Subject<boolean>()
  endOfComments$: Observable<boolean> = this._endOfComments$.asObservable()


  reloadAvatar() {
    this._reloadAvatar$.next()
  }

  setGetLoading(state: loaderState) {
    this._loading$.next(state)
  }

  setEndOfThreads(state: boolean) {
    this._endOfThreads$.next(state)
  }

  setEndOfReplies(state: boolean) {
    this._endOfReplies$.next(state)
  }

  setEndOfSaved(state: boolean) {
    this._endOfSaved$.next(state)
  }

  setEndOfFiles(state: boolean) {
    this._endOfFiles$.next(state)
  }

  setEndOfComments(state: boolean) {
    this._endOfComments$.next(state)
  }
}
