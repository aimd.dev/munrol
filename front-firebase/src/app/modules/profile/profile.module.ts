import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ProfileRoutingModule } from './profile-routing.module'

import { SharedModule } from '@shared/shared.module'

import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { profileReducer } from './profile.reducer'
import { ProfileEffects } from './profile.effects'

import { ProfilePageComponent } from './components/profile-page/profile-page.component'
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component'
import { ProfileCountersComponent } from './components/profile-counters/profile-counters.component'
import { ProfileFriendsComponent } from './components/profile-friends/profile-friends.component'
import { ProfileTabsComponent } from './components/profile-tabs/profile-tabs.component'
import { ProfileActionsComponent } from './components/profile-actions/profile-actions.component'
import { ProfileBioComponent } from './components/profile-bio/profile-bio.component'
import { ProfileAvatarComponent } from './components/profile-avatar/profile-avatar.component'
import { ProfileAvatarUploadModalComponent } from './components/modals/profile-avatar-upload/profile-avatar-upload-modal.component'
import { threadReducer } from '../thread/thread.reducer'
import { ThreadEffects } from '../thread/thread.effects'
import { ThreadUrlPipe } from '@app/shared/pipes/thread-url.pipe'
import { SettingsComponent } from './components/settings/settings.component'
import { ChangeEmailModalComponent } from './components/modals/change-email-modal/change-email-modal.component'
import { ChangePasswordModalComponent } from './components/modals/change-password-modal/change-password-modal.component'
import { ProfileSavedComponent } from './components/profile-saved/profile-saved.component'
import { FileModalComponent } from './components/modals/file-modal/file-modal.component'
import { EndFriendshipModalComponent } from './components/modals/end-friendship-modal/end-friendship-modal.component'

@NgModule({
  declarations: [
    ProfilePageComponent,
    ProfileHeaderComponent,
    ProfileCountersComponent,
    ProfileFriendsComponent,
    ProfileTabsComponent,
    ProfileActionsComponent,
    ProfileBioComponent,
    ProfileAvatarComponent,
    ProfileAvatarUploadModalComponent,
    SettingsComponent,
    ChangeEmailModalComponent,
    ChangePasswordModalComponent,
    ProfileSavedComponent,
    FileModalComponent,
    EndFriendshipModalComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    StoreModule.forFeature('profile', profileReducer),
    EffectsModule.forFeature([ProfileEffects]),
    StoreModule.forFeature('threads', threadReducer),
    EffectsModule.forFeature([ThreadEffects])
  ],
  providers: [
    ThreadUrlPipe
  ]
})
export class ProfileModule { }
