import { Component, Input } from '@angular/core'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { File } from '@app/shared/models'
import { Store } from '@ngrx/store'

@Component({
    selector: 'mr-file-modal',
    templateUrl: './file-modal.component.html',
    styles: [
    ]
})
export class FileModalComponent {

    @Input()
    file: File

    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }

    //   ngOnInit(): void {
    //   }

}
