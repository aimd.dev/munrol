import { Component, Input } from '@angular/core'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { loaderState } from '@app/shared/models'
import { Store } from '@ngrx/store'
import { Subject } from 'rxjs'
import * as ProfileActions from '../../../profile.actions'

@Component({
  selector: 'mr-end-friendship-modal',
  templateUrl: './end-friendship-modal.component.html',
  styles: [
  ]
})
export class EndFriendshipModalComponent {

    @Input()
    friend: string

    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }

    endFriendship() {
        this.store.dispatch(ProfileActions.endFriendship({ target: this.friend }))
    }
}
