import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ModalService } from '@app/shared/components/modal/modal.service'

@Component({
    selector: 'mr-change-password-modal',
    templateUrl: './change-password-modal.component.html',
    styles: [
    ]
})
export class ChangePasswordModalComponent implements OnInit {

    form: FormGroup

    constructor(
        private modal: ModalService,
        private fb: FormBuilder
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required]]
        })
    }

    confirm() {
        this.form.markAllAsTouched()
        this.form.updateValueAndValidity()
        if (this.form.invalid) return
        this.modal.close(this.form.value)
    }
}
