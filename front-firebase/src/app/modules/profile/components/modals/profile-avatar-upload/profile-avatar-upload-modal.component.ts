import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { ProfileUiService } from '@app/modules/profile/services/profile-ui.service'
import { ModalService } from '@app/shared/components/modal/modal.service'
import { loaderState, LoaderStateEnum } from '@app/shared/models'
import { fileMaxSize } from '@app/shared/util/custom-validators/file-size.validator'
import { fileType } from '@app/shared/util/custom-validators/file-type.validators'
import { typeFromExtension } from '@app/shared/util/helpers/file-types'
import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { ProfileService } from '../../../profile.service'

@Component({
    selector: 'mr-profile-avatar-upload-modal',
    templateUrl: './profile-avatar-upload-modal.component.html',
    styles: [
    ]
})
export class ProfileAvatarUploadModalComponent implements OnInit, OnDestroy {

    form: FormGroup

    loading: loaderState = LoaderStateEnum.OFF

    loadingState$: Subject<loaderState> = new Subject<loaderState>()

    unsubscriber$: Subject<void> = new Subject<void>()

    @Input() username: string

    constructor(
        private fb: FormBuilder,
        private service: ProfileService,
        private modal: ModalService,
        private ui: ProfileUiService
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            avatar: ['', [Validators.required, fileMaxSize(2), fileType([typeFromExtension('png'), typeFromExtension('jpg')])]]
        })

        this.ui.reloadAvatar$.pipe(takeUntil(this.unsubscriber$)).subscribe(() => this.modal.close())
        this.loadingState$.pipe(takeUntil(this.unsubscriber$)).subscribe((state) => this.loading = state)
    }

    get avatar() { return this.form.get('avatar') as FormControl }

    fileChange(event: Event): void {
        const target = event.target as HTMLInputElement
        if (target.files) {
            this.avatar.markAsTouched()
            this.avatar.setValue(target.files[0])
        }
    }

    upload() {
        this.loadingState$.next(LoaderStateEnum.ON)
        if (this.avatar.value) this.service.updateAvatar(this.username, this.avatar.value)
    }

    ngOnDestroy() {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
