import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ModalService } from '@app/shared/components/modal/modal.service'

@Component({
    selector: 'mr-change-email-modal',
    templateUrl: './change-email-modal.component.html',
    styles: [
    ]
})
export class ChangeEmailModalComponent implements OnInit {

    form: FormGroup

    constructor(
        private modal: ModalService,
        private fb: FormBuilder
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        })
    }

    confirm() {
        this.form.markAllAsTouched()
        this.form.updateValueAndValidity()
        if (this.form.invalid) return
        this.modal.close(this.form.value)
    }

}
