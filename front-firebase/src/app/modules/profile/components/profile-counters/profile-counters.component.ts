import { Component, Input } from '@angular/core'

@Component({
    selector: 'mr-profile-counters',
    templateUrl: './profile-counters.component.html',
    styles: [
    ]
})
export class ProfileCountersComponent {
    /**
     * Total replies
     */
    @Input()
    replyCount: number
    /**
     * Total threads
     */
    @Input()
    threadCount: number
    /**
     * Total comments
     */
    @Input()
    commentCount: number
    /**
     * Total friends
     */
    @Input()
    friendCount: number
}
