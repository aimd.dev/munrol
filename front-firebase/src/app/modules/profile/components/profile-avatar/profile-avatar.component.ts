import {
    Component,
    Input,
    ViewChild
} from '@angular/core'

import {
    ImageComponent
} from '@app/shared/components/image/image.component'
import {
    ModalService
} from '@app/shared/components/modal/modal.service'

import {
    ProfileAvatarUploadModalComponent
} from '../modals/profile-avatar-upload/profile-avatar-upload-modal.component'

@Component({
    selector: 'mr-profile-avatar',
    templateUrl: './profile-avatar.component.html',
    styles: [
    ]
})
export class ProfileAvatarComponent {

    @Input()
    username: string

    @Input()
    isCurrentUser: boolean

    @ViewChild('avatar') avatar: ImageComponent

    constructor(
        private modal: ModalService
    ) { }

    upload() {
        this.modal.open(ProfileAvatarUploadModalComponent, [{ key: 'username', value: this.username }])
            .then(() => this.avatar.reload())
    }
}
