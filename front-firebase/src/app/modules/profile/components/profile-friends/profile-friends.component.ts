import {
    Component,
    Input
} from '@angular/core'
import { ModalService } from '@app/shared/components/modal/modal.service'

import {
    Friend, ModalProp
} from '@app/shared/models'

import { EndFriendshipModalComponent } from '../modals/end-friendship-modal/end-friendship-modal.component'

@Component({
    selector: 'mr-profile-friends',
    templateUrl: './profile-friends.component.html',
    styles: [
    ]
})
export class ProfileFriendsComponent {
    /**
     * Friend list
     */
    @Input()
    friends: Friend[] = []
    /**
     * Is current logged in user
     */
    @Input()
    isCurrentUser: boolean

    constructor(
        private modal: ModalService
    ) {}

    endFriendship(friend: string) {
        const props: ModalProp[] = [{ key: 'friend', value: friend }]
        this.modal.open(EndFriendshipModalComponent, props)
    }
}
