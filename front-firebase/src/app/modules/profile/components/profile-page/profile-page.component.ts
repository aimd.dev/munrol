import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    ActivatedRoute
} from '@angular/router'

import {
    combineLatest,
    Observable,
    Subject
} from 'rxjs'

import {
    map,
    takeUntil
} from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as UserActions from '@modules/profile/profile.actions'
import * as fromUser from '@modules/profile/profile.reducer'
import { selectCurrentProfile } from '@modules/profile/profile.selectors'
import { selectCurrentUserData, selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'

import { ProfileUser } from '../../models/ProfileUser.model'
import { UserData } from '@app/modules/auth/models'
import { Title } from '@angular/platform-browser'

@Component({
    selector: 'mr-profile',
    templateUrl: './profile-page.component.html',
    styles: [
    ]
})
export class ProfilePageComponent implements OnInit, OnDestroy {
    /**
     * Current user
     */
    auth$: Observable<UserData | null>
    /**
     * Profile
     */
    profile$: Observable<ProfileUser | null>
    /**
     * Subject for destruction
     */
    unsubscriber$: Subject<void> = new Subject()
    /**
     * Constructor del componente
     * @param route angular router
     * @param store ngrx store
     */
    constructor(
        private route: ActivatedRoute,
        private store: Store<fromUser.State>,
        private title: Title
    ) { }
    /**
     * Inicializador del componente
     */
    ngOnInit(): void {
        this.auth$ = this.store.select(selectCurrentUserData)
        // Se suscribe al perfil
        this.profile$ = combineLatest([this.store.select(selectCurrentProfile), this.store.select(selectCurrentUserUsername)])
        .pipe(
            takeUntil(this.unsubscriber$),
            map(([profile, auth]) => {
                if (profile) {
                    this.title.setTitle(`${profile.username} | Munrol`)
                    return {
                        ...profile,
                        isCurrentUser: profile.username === auth,
                        friendsMapped: Object.keys(profile.friends).map(k => ({ username: k, createdAt: profile.friends[k] }))
                    }
                }
                return null
            })
        )
        // Dispatch para obtener el perfil
        this.route.params.subscribe((params) => {
            if (params.username) {
                this.store.dispatch(UserActions.getProfile({ username: params.username }))
            }
        })
    }
    /**
     * Destructor del componente
     */
    ngOnDestroy(): void {
        this.store.dispatch(UserActions.resetProfile())
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
