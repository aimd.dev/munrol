import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core'
import { constants } from '@app/core/config/constants'
import { selectCurrentUserSaved } from '@app/modules/auth/auth.selectors'
import { ThreadBrief } from '@app/modules/thread/models/ThreadBrief.model'
import { Store } from '@ngrx/store'
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs'
import { map, skip, takeUntil, tap } from 'rxjs/operators'
import { ProfileUiService } from '../../services/profile-ui.service'
import * as ProfileActions from '../../profile.actions'
import { selectCurrentSaved } from '../../profile.selectors'

@Component({
    selector: 'mr-profile-saved',
    templateUrl: './profile-saved.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileSavedComponent implements OnInit, OnDestroy {

    last$: BehaviorSubject<number> = new BehaviorSubject<number>(0)

    saved$: Observable<ThreadBrief[] | null>

    unsubscriber$: Subject<void> = new Subject<void>()

    constructor(
        private store: Store<any>,
        public ui: ProfileUiService
    ) { }

    ngOnInit(): void {
        this.store.select(selectCurrentUserSaved)
        .pipe(
            takeUntil(this.unsubscriber$),
            map(ids => ids.map(id=> ({ id })) as ThreadBrief[])
        )
        .subscribe(ids => this.store.dispatch(ProfileActions.getProfileSavedInitial({ ids })))

        this.saved$ = combineLatest([this.last$, this.store.select(selectCurrentSaved)])
        .pipe(
            tap(([_, saved]) => {
                if (saved && saved.every(s => !!s.body)) this.ui.setEndOfSaved(true)
            }),
            map(([last, saved]) => saved && saved.slice(0, last + constants.pageLength)),
            tap(saved => {
                const download: string[] = []
                saved && saved.forEach(s => !s.body && download.push(s.id))
                if (download.length) {
                    this.store.dispatch(ProfileActions.getProfileSaved({ ids: download }))
                }
            })
        )
    }

    loadMore() {
        const currentValue = this.last$.getValue()
        this.last$.next(currentValue + constants.pageLength)
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
