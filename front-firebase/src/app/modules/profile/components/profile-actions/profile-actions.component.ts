import { Component, Input } from '@angular/core';
import { ModalService } from '@app/shared/components/modal/modal.service';
import { ModalProp } from '@app/shared/models';
import { Store } from '@ngrx/store';
import * as ProfileActions from '../../profile.actions';
import { EndFriendshipModalComponent } from '../modals/end-friendship-modal/end-friendship-modal.component';

@Component({
    selector: 'mr-profile-actions',
    templateUrl: './profile-actions.component.html',
    styles: [
    ]
})
export class ProfileActionsComponent {

    @Input() username: string
    @Input() isCurrentUser: boolean
    @Input() isFriend: boolean

    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }

    requestFriendship() {
        this.store.dispatch(ProfileActions.sendFriendShipRequest({ target: this.username }))
    }

    endFriendship() {
        const props: ModalProp[] = [{ key: 'friend', value: this.username }]
        this.modal.open(EndFriendshipModalComponent, props)
    }
}
