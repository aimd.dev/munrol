import { Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'
import { Comment } from '@app/modules/thread/models/Comment.model'
import { Reply } from '@app/modules/thread/models/Reply.model'
import { ThreadBrief } from '@app/modules/thread/models/ThreadBrief.model'
import { LoaderStateEnum } from '@app/shared/models'
import { File } from '@app/shared/models'
import { Store } from '@ngrx/store'
import { combineLatest, Observable } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import * as ProfileActions from '../../profile.actions'
import { selectCurrentcomments, selectCurrentFiles, selectCurrentProfile, selectCurrentReplies, selectCurrentThreads, selectCurrentView } from '../../profile.selectors'
import { ProfileUiService } from '../../services/profile-ui.service'

@Component({
    selector: 'mr-profile-tabs',
    templateUrl: './profile-tabs.component.html',
    styles: [
    ]
})
export class ProfileTabsComponent implements OnInit {

    lastThread: string

    lastReply: string

    lastFile: string

    lastComment: string

    active$: Observable<string>

    threads$: Observable<ThreadBrief[] | null>

    replies$: Observable<Reply[] | null>

    files$: Observable<File[] | null>

    comments$: Observable<Comment[] | null>

    @Input() username: string

    @Input() isCurrentUser: boolean

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private store: Store<any>,
        public ui: ProfileUiService
    ) { }

    ngOnInit(): void {

        this.ui.setGetLoading(LoaderStateEnum.ON)

        this.route.queryParams.subscribe(params => {
            this.store.dispatch(ProfileActions.setView({ view: params.tab ? params.tab : 'threads' }))
        })

        this.active$ = combineLatest([this.store.select(selectCurrentView), this.store.select(selectCurrentProfile)])
        .pipe(
            map(([tab, profile]) => [tab ? tab : 'threads', profile?.username as string]),
            tap(([tab, username]) => {
                switch (tab) {
                    case 'replies':
                        this.store.dispatch(ProfileActions.getProfileReplies({ username: username }))
                        break
                    case 'files':
                        this.store.dispatch(ProfileActions.getProfileFiles({ username: username }))
                        break
                    case 'comments':
                        this.store.dispatch(ProfileActions.getProfileComments({ username: username }))
                        break
                    default:
                        this.store.dispatch(ProfileActions.getProfileThreads({ username: username }))
                        break
                }
            }),
            map(([tab, _]) => tab)
        )

        this.threads$ = this.store.select(selectCurrentThreads)
        .pipe(
            tap((data) => { data && data.length ? this.lastThread = data[data.length - 1]['createdAt'] : null })
        )

        this.replies$ = this.store.select(selectCurrentReplies)
        .pipe(
            tap((data) => { data && data.length ? this.lastReply = data[data.length - 1]['createdAt'] : null })
        )

        this.files$ = this.store.select(selectCurrentFiles)
        .pipe(
            tap((data) => { data && data.length ? this.lastFile = data[data.length - 1]['createdAt'] : null })
        )

        this.comments$ = this.store.select(selectCurrentcomments)
        .pipe(
            tap((data) => { data && data.length ? this.lastComment = data[data.length - 1]['createdAt'] : null })
        )
    }

    select(tab: string) {
        this.router.navigate(['.'], { queryParams: { tab: tab }, relativeTo: this.route })
    }

    loadMore(tab: string) {
        switch (tab) {
            case 'threads':
                this.store.dispatch(ProfileActions.getProfileThreads({ username: this.username, last: this.lastThread, force: true }))
                break
            case 'replies':
                this.store.dispatch(ProfileActions.getProfileReplies({ username: this.username, last: this.lastReply, force: true }))
                break
            case 'files':
                this.store.dispatch(ProfileActions.getProfileFiles({ username: this.username, last: this.lastFile, force: true }))
                break
            case 'comments':
                this.store.dispatch(ProfileActions.getProfileComments({ username: this.username, last: this.lastComment, force: true }))
                break
        }
    }
}
