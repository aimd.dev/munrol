import { Component, OnInit } from '@angular/core';
import { ModalService } from '@app/shared/components/modal/modal.service';
import { Store } from '@ngrx/store';
import * as AuthActions from "@modules/auth/auth.actions";
import { ChangeEmailModalComponent } from '../modals/change-email-modal/change-email-modal.component';
import { ChangePasswordModalComponent } from '../modals/change-password-modal/change-password-modal.component';
import { combineLatest, config, Observable } from 'rxjs';
import { UserConfig } from '@app/modules/auth/models/UserConfig.model';
import { selectCurrentUserConfig, selectCurrentUserEmail } from '@app/modules/auth/auth.selectors';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mr-settings',
  templateUrl: './settings.component.html',
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private modal: ModalService,
    private store: Store<any>
  ) { }

  config$:Observable<UserConfig>

  ngOnInit(): void {
    this.config$ = combineLatest([this.store.select(selectCurrentUserConfig), this.store.select(selectCurrentUserEmail)])
    .pipe(
      filter(([config, email]) => !!config),
      map(([config, email]) => { 
        let conf = { ...config, email: email }
        return conf as UserConfig
      })
    )
    this.store.dispatch(AuthActions.getUserConfig({ username: this.route.snapshot.params.username }))
  }

  openEmailModal() {
    this.modal.open(ChangeEmailModalComponent)
    .then((result) => {
      if (result) this.store.dispatch(AuthActions.updateEmail({ ...result }))
    })
  }

  openPasswordModal() {
    this.modal.open(ChangePasswordModalComponent)
    .then((result) => {
      if (result) this.store.dispatch(AuthActions.updatePassword({ ...result }))
    })
  }

  updateConfig(key:string, value:boolean) {
    this.store.dispatch(AuthActions.updateConfig({ key, value }))
  }
}
