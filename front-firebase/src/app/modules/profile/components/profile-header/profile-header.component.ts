import {
    Component,
    Input
} from '@angular/core'
import { UserData } from '@app/modules/auth/models'

import { ProfileUser } from '../../models/ProfileUser.model'

@Component({
    selector: 'mr-profile-header',
    templateUrl: './profile-header.component.html',
    styles: [
    ]
})
export class ProfileHeaderComponent {
    edit: boolean
    bioRef: string
    @Input()
    user: ProfileUser
    @Input()
    auth: UserData | null
}
