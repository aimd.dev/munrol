import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Input,
    ViewChild
} from '@angular/core'

import {
    Subject
} from 'rxjs'

import { Store } from '@ngrx/store'
import * as fromProfile from '../../profile.reducer'
import * as ProfileActions from '../../profile.actions'

@Component({
    selector: 'mr-profile-bio',
    templateUrl: './profile-bio.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileBioComponent {
    /**
     * Ngmodel
     */
    ref: string
    /**
     * Bio editing subject
     */
    editing$: Subject<boolean> = new Subject<boolean>()
    /**
     * Bio input string
     */
    @Input()
    bio: string
    /**
     * Username
     */
    @Input()
    username: string
    /**
     * Is logged in user
     */
    @Input()
    isCurrentUser: boolean
    /**
     * Textarea ref
     */
    @ViewChild('textarea', { static: true }) textarea: ElementRef
    /**
     * Constructor del componente
     * @param store ngrx store
     */
    constructor(
        private store: Store<fromProfile.State>
    ) { }
    /**
     * Init bio editing
     */
    editBio() {
        this.ref = this.bio
        this.editing$.next(true)
    }
    /**
     * Save changes
     */
    saveBio(): void {
        this.store.dispatch(ProfileActions.updateBio({ username: this.username, bio: this.ref }))
        this.closeEdit()
    }
    /**
     * End editing
     */
    closeEdit() {
        this.editing$.next(false)
    }
}
