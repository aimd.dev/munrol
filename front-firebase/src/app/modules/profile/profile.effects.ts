import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, ofType, createEffect, concatLatestFrom } from '@ngrx/effects'
import { catchError, exhaustMap, filter, map, tap } from 'rxjs/operators'
import * as fromAuth from '../auth/auth.reducer'
import * as ProfileActions from './profile.actions'
import { ProfileService } from './profile.service'
import { EMPTY } from 'rxjs'
import { Store } from '@ngrx/store'
import { selectCurrentUserUsername } from '../auth/auth.selectors'
import { ThreadBrief } from '../thread/models/ThreadBrief.model'
import { AlertsService } from '@app/shared/services/alerts.service'
import { ProfileUiService } from './services/profile-ui.service'
import { Reply } from '../thread/models/Reply.model'
import { selectCurrentcomments, selectCurrentFiles, selectCurrentReplies, selectCurrentThreads } from './profile.selectors'
import { LoaderStateEnum, File } from '@app/shared/models'
import { constants } from '@app/core/config/constants'
import { Comment } from '../thread/models/Comment.model'

@Injectable()
export class ProfileEffects {

    constructor(
        private router: Router,
        private actions$: Actions,
        private service: ProfileService,
        private store: Store<fromAuth.State>,
        private alerts: AlertsService,
        private ui: ProfileUiService
    ) {}

    getProfile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.getProfile),
            exhaustMap((action) => this.service.getProfile(action.username)
                .pipe(
                    map((doc) => {
                        const user: any = doc.data()
                        user.username = doc.id
                        return ProfileActions.getProfileSuccess({ profile: user })
                    }),
                    catchError((err) => {
                        console.log(err)
                        return EMPTY
                    })
                )
            )
        )
    )

    getProfileThreads$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.getProfileThreads),
            concatLatestFrom(() => this.store.select(selectCurrentThreads)),
            filter(([action, threads]) => action.force || !threads || threads.length === 0),
            tap(() => { this.ui.setGetLoading(LoaderStateEnum.ON) }),
            exhaustMap(([action, threads]) => this.service.getProfileThreads(action.username, action.last)
                .pipe(
                    tap(() => {
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                    }),
                    map((res) => {
                        const threads = res.docs.map((d) => {
                            const data = d.data() as ThreadBrief
                            return {
                                ...data,
                                id: d.id
                            }
                        })
                        if (threads.length < constants.pageLength) { this.ui.setEndOfThreads(true) }
                        return ProfileActions.getProfileThreadsSuccess({ threads })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                        this.alerts.error('No se han podido obtener los hilos')
                        return EMPTY
                    })
                )
            )
        )
    )

    getProfileReplies$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.getProfileReplies),
            concatLatestFrom(() => this.store.select(selectCurrentReplies)),
            filter(([action, replies]) => action.force || !replies || replies.length === 0),
            tap(() => { this.ui.setGetLoading(LoaderStateEnum.ON) }),
            exhaustMap(([action, replies]) => this.service.getProfileReplies(action.username, action.last)
                .pipe(
                    tap(() => {
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                    }),
                    map((res) => {
                        const replies = res.docs.map((d) => {
                            const data = d.data() as Reply
                            return {
                                ...data,
                                id: d.id
                            }
                        })
                        if (replies.length < constants.pageLength) { this.ui.setEndOfReplies(true) }
                        return ProfileActions.getProfileRepliesSuccess({ replies })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                        this.alerts.error('No se han podido obtener las respuestas')
                        return EMPTY
                    })
                )
            )
        )
    )

    getProfileFiles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.getProfileFiles),
            concatLatestFrom(() => this.store.select(selectCurrentFiles)),
            filter(([action, files]) => action.force || !files || files.length === 0),
            tap(() => { this.ui.setGetLoading(LoaderStateEnum.ON) }),
            exhaustMap(([action, files]) => this.service.getProfileFiles(action.username, action.last)
                .pipe(
                    tap(() => this.ui.setGetLoading(LoaderStateEnum.OFF)),
                    map((res) => {
                        const files = res.docs.map((d) => {
                            const data = d.data() as File
                            return {
                                ...data,
                                id: d.id
                            }
                        })
                        if (files.length < constants.pageLength) { this.ui.setEndOfFiles(true) }
                        return ProfileActions.getProfileFilesSuccess({ files })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.ui.setGetLoading(LoaderStateEnum.OFF)
                        this.alerts.error('No se han podido obtener las fichas')
                        return EMPTY
                    })
                )
            )
        )
    )

    getProfileComments$ = createEffect(() =>
    this.actions$.pipe(
        ofType(ProfileActions.getProfileComments),
        concatLatestFrom(() => this.store.select(selectCurrentcomments)),
        filter(([action, comments]) => action.force || !comments || comments.length === 0),
        tap(() => { this.ui.setGetLoading(LoaderStateEnum.ON) }),
        exhaustMap(([action, comments]) => this.service.getProfileComments(action.username, action.last)
            .pipe(
                tap(() => this.ui.setGetLoading(LoaderStateEnum.OFF)),
                map((res) => {
                    const comments = res.docs.map((d) => {
                        const data = d.data() as Comment
                        return {
                            ...data,
                            id: d.id
                        }
                    })
                    if (comments.length < constants.pageLength) { this.ui.setEndOfComments(true) }
                    return ProfileActions.getProfileCommentsSuccess({ comments })
                }),
                catchError((err) => {
                    console.log(err)
                    this.ui.setGetLoading(LoaderStateEnum.OFF)
                    this.alerts.error('No se han podido obtener los comentarios')
                    return EMPTY
                })
            )
        )
    )
)

    getProfileSaved$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.getProfileSaved),
            exhaustMap((action) => this.service.getProfileSaved(action.ids)
                .pipe(
                    map((res) => {
                        const docs = res.docs
                        const threads = docs.map((t) => {
                            const thread = t.data() as ThreadBrief
                            thread.id = t.id
                            return thread
                        })
                        if (threads.length < constants.pageLength) { this.ui.setEndOfSaved(true) }
                        return ProfileActions.getProfileSavedSuccess({ threads })
                    }),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se han podido obtener los hilos')
                        return EMPTY
                    })
                )
            )
        )
    )

    updateBio$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.updateBio),
            exhaustMap((action) => this.service.updateBio(action.username, action.bio)
                .pipe(
                    tap(() => this.alerts.success('Cambios guardados')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se pudo actualizar la biografía')
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    updateAvatarSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.updateAvatarSuccess),
            tap(() => {
                this.ui.reloadAvatar()
                this.alerts.success('Cambios guardados con éxito')
            })
        ), { dispatch: false }
    )

    updateAvatarError$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.updateAvatarError),
            tap(() => this.alerts.error('Ha ocurrido un error'))
        ), { dispatch: false }
    )

    sendFriendshipRequest$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.sendFriendShipRequest),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.sendFriendShipRequest(auth as string, action.target)
                .pipe(
                    tap(() => this.alerts.success('Tu solicitud fue enviada')),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se pudo enviar la solicitud')
                        return EMPTY
                    })
                )
            )
        ), { dispatch: false }
    )

    endFriendship$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.endFriendship),
            concatLatestFrom(() => this.store.select(selectCurrentUserUsername)),
            exhaustMap(([action, auth]) => this.service.endFriendship(auth as string, action.target)
                .pipe(
                    tap(() => this.alerts.success('Se ha terminado la amistad')),
                    map(() => ProfileActions.endFriendshipSuccess({ target: action.target })),
                    catchError((err) => {
                        console.log(err)
                        this.alerts.error('No se pudo enviar la solicitud')
                        return EMPTY
                    })
                )
            )
        )
    )

}
