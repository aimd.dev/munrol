import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromProfile from './profile.reducer'

export const selectProfileState = createFeatureSelector<fromProfile.State>('profile')

export const selectCurrentProfile = createSelector(selectProfileState, (profile) => profile.user)
export const selectCurrentView = createSelector(selectProfileState, (profile) => profile.view)
export const selectCurrentThreads = createSelector(selectProfileState, (profile) => profile.threads)
export const selectCurrentReplies = createSelector(selectProfileState, (profile) => profile.replies)
export const selectCurrentFiles = createSelector(selectProfileState, (profile) => profile.files)
export const selectCurrentcomments = createSelector(selectProfileState, (profile) => profile.comments)

export const selectCurrentSaved = createSelector(selectProfileState, (profile) => profile.saved)

