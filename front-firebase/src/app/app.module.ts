import { APP_INITIALIZER, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
// Libs
import { provideFirebaseApp, initializeApp } from '@angular/fire/app'
import { provideFirestore, getFirestore } from '@angular/fire/firestore'
import { provideStorage, getStorage } from '@angular/fire/storage'
import { getAuth, provideAuth } from '@angular/fire/auth'
import { getFunctions, provideFunctions } from '@angular/fire/functions'
// Ngrx
import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { reducers } from './app.reducer'
import { notificationReducer } from './modules/notification/notification.reducer'
import { EffectsModule } from '@ngrx/effects'
import { AuthEffects } from '@modules/auth/auth.effects'
import { NotificationEffects } from './modules/notification/notification.effects'
import { FileEffects } from './modules/file/file.effects'
import { fileReducer } from './modules/file/file.reducer'
// Modules
import { AppRoutingModule } from './app-routing.module'
import { SharedModule } from './shared/shared.module'
// Env
import { environment } from 'src/environments/environment'
// Services
import { AuthService } from '@modules/auth/auth.service'
// Components
import { AppComponent } from './app.component'

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'serverApp' }),
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        provideFirestore(() => getFirestore()),
        provideStorage(() => getStorage()),
        provideAuth(() => getAuth()),
        provideFunctions(() => getFunctions()),
        StoreModule.forRoot(reducers, {
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictActionSerializability: true,
                strictStateSerializability: true
            }
        }),
        StoreModule.forFeature('notification', notificationReducer),
        StoreModule.forFeature('file', fileReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([AuthEffects, NotificationEffects, FileEffects]),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        SharedModule
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: (auth: AuthService) => () => auth.init(),
            deps: [AuthService],
            multi: true
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
