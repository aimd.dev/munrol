import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
// Guards
import { UnauthGuard } from './core/guards/unauth.guard'

/**
 * Rutas en el root
 */
const routes: Routes = [
    // Auth
    {
        path: 'auth',
        loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
        canActivate: [UnauthGuard]
    },
    // Main
    {
        path: '',
        loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule)
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            anchorScrolling: 'enabled',
            scrollOffset: [0, 100],
            scrollPositionRestoration: 'enabled',
            initialNavigation: 'enabledBlocking'
        })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
