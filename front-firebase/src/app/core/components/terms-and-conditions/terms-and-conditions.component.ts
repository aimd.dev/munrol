import { Component } from '@angular/core'

@Component({
  selector: 'mr-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styles: ['.terms{ width: 100%; max-width: 700px; margin: 0 auto; font-size: .9rem; text-align: justify; }']
})
export class TermsAndConditionsComponent { }
