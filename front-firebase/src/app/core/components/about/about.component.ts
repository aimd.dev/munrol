import { Component } from '@angular/core';

@Component({
  selector: 'mr-about',
  templateUrl: './about.component.html',
  styles: ['.about {width: 100%; max-width: 700px; margin: 0 auto;}']
})
export class AboutComponent { }
