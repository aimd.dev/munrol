import { Component } from '@angular/core'

@Component({
  selector: 'mr-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styles: ['.privacy-policy { width: 100%; max-width: 700px; margin: 0 auto; font-size: .9rem; text-align: justify; }']
})
export class PrivacyPolicyComponent { }
