import {
    Component,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    takeUntil
} from 'rxjs/operators'

import { Store } from '@ngrx/store'

import * as fromAuth from '@modules/auth/auth.reducer'
import * as AuthActions from '@modules/auth/auth.actions'

import {
    selectPushState
} from '@app/modules/notification/notification.selectors'

import {
    selectCurrentUserAuth
} from '@modules/auth/auth.selectors'

import {
    ProfileUiService
} from '@app/modules/profile/services/profile-ui.service'

import {
    UserAuth
} from '@app/modules/auth/models'

import {
    PushNotifications
} from '@app/shared/models'

import {
    ImageComponent
} from '@app/shared/components/image/image.component'

@Component({
    selector: 'mr-topbar',
    templateUrl: './topbar.component.html'
})
export class TopbarComponent implements OnInit, OnDestroy {
    /**
     * Usuario conectado
     */
    currentUser$: Observable<UserAuth | null>
    /**
     * Estado de las notificaciones
     */
    pushState$: Observable<PushNotifications>
    /**
     * Subject de la destruccion del componente
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Componente para el avatar
     */
    @ViewChild('avatar') avatar: ImageComponent
    /**
     * Constructor del componente
     * @param {Store<fromAuth.State>} store ngrx store
     * @param {ProfileUiService} ui servicio para el ui
     */
    constructor(
        private store: Store<fromAuth.State>,
        private ui: ProfileUiService
    ) { }
    /**
     * Inicializador del componente
     */
    ngOnInit(): void {
        // Suscribirse al usuario conectado
        this.currentUser$ = this.store.select(selectCurrentUserAuth)
        // Suscribirse a las notificaciones
        this.pushState$ = this.store.select(selectPushState)
        // Suscribirse a los cambios en el avatar
        this.ui.reloadAvatar$
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe(() => this.avatar.reload())
    }
    /**
     * Cerrar sesion
     */
    logout(): void {
        this.store.dispatch(AuthActions.logout())
    }
    /**
     * Destructor del componente
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
