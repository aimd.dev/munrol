export const constants = {
    // Thread Page
    pageLength: 10,
    visiblePages: 10,
    // New Threads
    summaryMaxLength: 600,
    // Chats
    roomPageLength: 20
} as const
