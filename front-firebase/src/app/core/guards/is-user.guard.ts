import {
    Inject,
    Injectable,
    PLATFORM_ID
} from '@angular/core'

import {
    isPlatformServer
} from '@angular/common'

import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    UrlTree
} from '@angular/router'

import {
    Observable
} from 'rxjs'

import {
    map,
    take
} from 'rxjs/operators'

import {
    Auth, authState
} from '@angular/fire/auth'

@Injectable({
  providedIn: 'root'
})
export class IsUserGuard implements CanActivate {
    /**
     * Constructor del guard
     * @param {AngularFireAuth} afauth angular fire auth
     * @param {Router} router angular router
     */
    constructor(
        @Inject(PLATFORM_ID) private pid: any,
        private auth: Auth,
        private router: Router
    ) {}
    /**
     * Can activate si el usuario es el de la url
     * @param {ActivatedRouteSnapshot} route ruta activa
     * @returns true | false
     */
    canActivate(
        route: ActivatedRouteSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (isPlatformServer(this.pid)) {
            return true
        }
        return authState(this.auth).pipe(take(1),
        map((user) => {
            // if (user && user.uid !== route.params.username) this.router.navigate(['/'])
            return !!(user && user.uid === route.params.username)
        }))
    }
}
