import {
    Inject,
    Injectable, PLATFORM_ID
} from '@angular/core'

import {
    isPlatformServer
} from '@angular/common'

import {
    CanActivate,
    Router,
    UrlTree
} from '@angular/router'

import {
    Observable
} from 'rxjs'

import {
    map,
    take
} from 'rxjs/operators'

import {
    Auth, authState
} from '@angular/fire/auth'

@Injectable({
  providedIn: 'root'
})
export class UnauthGuard implements CanActivate {
    /**
     * Constructor del guard
     * @param {AngularFireAuth} afauth angular fire auth
     * @param {Router} router angular router
     */
    constructor(
        @Inject(PLATFORM_ID) private pid: any,
        private auth: Auth,
        private router: Router
    ) { }
    /**
     * Can activate si el user esta autenticado
     * @returns true | false
     */
    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (isPlatformServer(this.pid)) {
            return true
        }
        return authState(this.auth).pipe(take(1),
        map((user) => {
            // if (!!user) this.router.navigate(['/'])
            return !!!user
        }))
    }
}
