import { isPlatformServer } from '@angular/common'
import { Inject, Injectable, PLATFORM_ID } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate, Router, UrlTree } from '@angular/router'
import { selectCurrentUserFriends } from '@app/modules/auth/auth.selectors'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { map, take } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})
export class IsFriendGuard implements CanActivate {

    constructor(
        @Inject(PLATFORM_ID) private pid: any,
        private store: Store<any>,
        private router: Router
    ) { }

    forbid(): boolean {
        this.router.navigate(['/inbox'])
        return false
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (isPlatformServer(this.pid)) {
            return true
        }
        return this.store.select(selectCurrentUserFriends)
        .pipe(
            take(1),
            map(friends => {
                const user = route.paramMap.get('username') as string
                const exists = !!(friends as any)[user]
                if (exists) return true
                else return this.forbid()
            })
        )
    }
}
