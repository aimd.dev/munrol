import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { AlertType } from '../models'
import { Alert } from '../models/alert.model'

@Injectable({
    providedIn: 'root'
})
export class AlertsService {
    /**
     * Subject privado de las alertas
     */
    private _alerts$: BehaviorSubject<Alert[]> = new BehaviorSubject<Alert[]>([])
    /**
     * Obserble publico de las alertas
     */
    alerts$: Observable<Alert[]> = this._alerts$.asObservable()
    /**
     * Crear alerta de exito
     * @param {string} message mensaje de la alerta
     */
    success(message: string): void {
        this.add(AlertType.SUCCESS, message)
    }
    /**
     * Crear alerta de error
     * @param {string} message mensaje de la alerta
     */
    error(message: string): void {
        this.add(AlertType.ERROR, message)
    }
    /**
     * Crear alerta de info
     * @param {string} message mensaje de la alerta
     */
    info(message: string): void {
        this.add(AlertType.INFO, message)
    }
    /**
     * Crear una alerta
     * @param {AlertType} type tipo de alerta
     * @param {string} message mensaje de la alerta
     */
    add(type: AlertType, message: string): void {
        const alert: Alert = { id: Math.floor(Math.random() * (1000000 - 1)) + 1, type, message }
        const current: Alert[] = [...this._alerts$.getValue().concat([alert])]
        this._alerts$.next(current)
        setTimeout(() => { this.dismiss(alert.id) }, 10000)
    }
    /**
     * Cerrar una alerta
     * @param {number} id id de la alerta
     */
    dismiss(id: number): void {
        const current: Alert[] = [...this._alerts$.getValue().filter((a: Alert) => a.id !== id)]
        this._alerts$.next(current)
    }
}
