import { Component, Input, Output, EventEmitter } from '@angular/core'
import { File } from '@app/shared/models'
import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'
import { Subject } from 'rxjs'

@Component({
    selector: 'mr-file-expander',
    templateUrl: './file-expander.component.html',
    styles: [
    ]
})
export class FileExpanderComponent {

    fullText$: Subject<boolean> = new Subject<boolean>()

    @Input()
    file: File

    @Input()
    isAuthor: boolean

    @Input()
    currentUser: string | undefined

    @Input()
    thread: string

    @Input()
    sidebar = true

    @Output()
    expand: EventEmitter<File> = new EventEmitter<File>()

    constructor(
        private store: Store<any>
    ) { }

    unlinkFile(file: string) {
        this.store.dispatch(ThreadActions.unlinkFile({ file, thread: this.thread }))
    }
}
