import {
    ChangeDetectionStrategy,
    Component,
    Input
} from '@angular/core'

import {
    Store
} from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'

import {
    ModalService
} from '../modal/modal.service'

import {
    KickParticipantModalComponent
} from '@app/modules/thread/components/modals/kick-participant-modal/kick-participant-modal.component'

import {
    ModalProp
} from '@app/shared/models'

@Component({
    selector: 'mr-participant-list',
    templateUrl: './participant-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParticipantListComponent {
    /**
     * Current user
     */
    @Input()
    user: string | undefined | null
    /**
     * Thread id
     */
    @Input()
    thread: string
    /**
     * Participant list
     */
    @Input()
    participants: string[] = []
    /**
     * Is user the author
     */
    @Input()
    isAuthor: boolean
    /**
     * thread author
     */
    @Input()
    author: string
    /**
     * constructor del componente
     * @param store ngrx store
     */
    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }
    /**
     * Kick participant
     * @param participant person
     */
    kickUser(participant: string) {
        const props: ModalProp[] = [{value: participant, key: 'participant'}]
        this.modal.open(KickParticipantModalComponent, props)
        .then(result => {
            if (result) this.store.dispatch(ThreadActions.kickParticipant({ participant, thread: this.thread }))
        })
    }
}
