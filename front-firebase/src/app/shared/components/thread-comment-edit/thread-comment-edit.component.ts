import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'
import { Comment } from '@modules/thread/models/Comment.model'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'

@Component({
  selector: 'mr-thread-comment-edit',
  templateUrl: './thread-comment-edit.component.html',
  styles: [
  ]
})
export class ThreadCommentEditComponent implements OnInit {

    form: FormGroup

    @Input() comment: Comment

    @Output()
    cancel: EventEmitter<void> = new EventEmitter<void>()

    public toolbar: typeof EditorToolbar = EditorToolbar

    constructor(
        private fb: FormBuilder,
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            body: [this.comment.body, [Validators.required]]
        })
    }

    send() {

        if (this.form.invalid) return

        // return
        const comment: Comment = { ...this.form.value }
        comment.lastEdit = new Date().toISOString()
        this.store.dispatch(ThreadActions.editComment({ id: this.comment.id, comment: comment }))
    }
}
