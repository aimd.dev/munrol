import {
    Component,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    AlertsService
} from '@app/shared/services/alerts.service'

import {
    Alert
} from '@app/shared/models'

@Component({
    selector: 'mr-alerts',
    templateUrl: './alerts.component.html'
})
export class AlertsComponent implements OnInit, OnDestroy {
    /**
     * Observable de alertas
     */
    alerts$: Observable<Alert[]>
    /**
     * Subject para la destruccion del componente
     */
    unsubscriber$: Subject<void> = new Subject()
    /**
     * constructor del componente
     * @param service servicio de alertas
     */
    constructor(
        private service: AlertsService
    ) { }
    /**
     * Inicializador del componente
     */
    ngOnInit(): void {
        this.alerts$ = this.service.alerts$
    }
    /**
     * Cerrar una alerta
     * @param id id de la alerta
     */
    dismiss(id: number) {
        this.service.dismiss(id)
    }
    /**
     * Destructor del componente
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
