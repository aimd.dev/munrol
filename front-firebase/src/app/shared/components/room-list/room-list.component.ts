import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core'
import { selectCurrentUserUsername } from '@app/modules/auth/auth.selectors'
import { selectRoomList } from '@app/modules/inbox/inbox.selectors'
import { InboxService } from '@app/modules/inbox/inbox.service'
import { selectPushState } from '@app/modules/notification/notification.selectors'
import { AlertsService } from '@app/shared/services/alerts.service'
import { concatLatestFrom } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { Subject } from 'rxjs'
import { map, take, takeUntil, tap } from 'rxjs/operators'
import { AddRoomModalComponent } from '../add-room-modal/add-room-modal.component'
import { ModalService } from '../modal/modal.service'

@Component({
    selector: 'mr-room-list',
    templateUrl: './room-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomListComponent implements OnInit, OnDestroy {

    query = ''

    rooms$: Subject<any[]> = new Subject<any[]>()

    unsubscriber$: Subject<void> = new Subject<void>()

    @Output()
    addition: EventEmitter<string> = new EventEmitter<string>()

    @Output()
    deletion: EventEmitter<void> = new EventEmitter<void>()

    constructor(
        private service: InboxService,
        private store: Store<any>,
        private modal: ModalService,
        private alert: AlertsService
    ) { }

    ngOnInit(): void {
        this.store.select(selectCurrentUserUsername)
        .pipe(
            takeUntil(this.unsubscriber$),
            tap((username) => {
                if (username) this.service.initRoomList(username)
                else this.service.terminateList()
            })
        ).subscribe()

        this.store.select(selectRoomList)
        .pipe(
            takeUntil(this.unsubscriber$),
            concatLatestFrom(() => this.store.select(selectPushState)),
            tap(([list, push]) => {
                this.store.select(selectCurrentUserUsername)
                .pipe(take(1)).toPromise()
                .then(username => {
                    if (push.newMessages) {
                        const read = list.every(room => room.newMessages === false)
                        if (read) {
                            this.service.markAsRead(username as string)
                        }
                    }
                })
            }),
            map(([list, _]) => list)
        )
        .subscribe((list) => this.rooms$.next(list))
    }

    add() {
        let to: string
        let from: string
        this.store.select(selectCurrentUserUsername)
        .pipe(take(1)).toPromise()
        .then((username) => {
            from = username as string
            return this.modal.open(AddRoomModalComponent)
        })
        .then((selection) => {
            to = selection
            return selection && this.service.createRoom(from, selection)
        })
        .then(() => this.addition.emit(to))
    }

    remove(id: string) {
        this.store.select(selectCurrentUserUsername)
        .pipe(take(1)).toPromise()
        .then((username) => this.service.deleteRoom(username as string, id))
        .then(() => {
            this.alert.success('El chat ha sido removido')
            this.deletion.emit()
        })
        .catch((err) => {
            console.log(err)
            this.alert.error('No se pudo eliminar el chat')
        })
    }

    ngOnDestroy(): void {
        this.service.terminateList()
    }
}
