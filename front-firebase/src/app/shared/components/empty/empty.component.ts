import {
    Component,
    Input
} from '@angular/core'

@Component({
    selector: 'mr-empty',
    templateUrl: './empty.component.html',
    styles: [
    ]
})
export class EmptyComponent {
    /**
     * Icon
     */
    @Input()
    icon = 'la-times-circle'
    /**
     * Message
     */
    @Input()
    message = 'Sin resultados'
    /**
     * Small style
     */
    @Input()
    small: boolean
    /**
     * Text for button
     */
    @Input()
    actionText: string
    /**
     * Route for action button
     */
    @Input()
    actionRoute: string
}
