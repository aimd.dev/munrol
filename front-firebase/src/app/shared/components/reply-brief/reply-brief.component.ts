import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { ConfirmDeleteReplyModalComponent } from '@app/modules/thread/components/modals/confirm-delete-reply-modal/confirm-delete-reply-modal.component'
import { Reply } from '@app/modules/thread/models/Reply.model'
import { Store } from '@ngrx/store'
import { BehaviorSubject } from 'rxjs'
import { ModalService } from '../modal/modal.service'
import * as ThreadActions from '@modules/thread/thread.actions'

@Component({
    selector: 'mr-reply-brief',
    templateUrl: './reply-brief.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReplyBriefComponent {
    compact$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true)

    @Input() reply: Reply

    @Input() menu = false

    constructor(
        private modal: ModalService,
        private store: Store<any>
    ) {}

    toggle() {
        this.compact$.next(!this.compact$.getValue())
    }

    deleteReply() {
        this.modal.open(ConfirmDeleteReplyModalComponent)
        .then((result) => {
            if (result) this.store.dispatch(ThreadActions.deleteReply({ reply: this.reply }))
        })
    }
}
