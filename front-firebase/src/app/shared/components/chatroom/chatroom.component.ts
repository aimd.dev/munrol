import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnDestroy,
    OnInit
} from '@angular/core'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    map,
    takeUntil,
    tap
} from 'rxjs/operators'

import {
    Store
} from '@ngrx/store'

import {
    selectMessages,
    selectRoomList
} from '@app/modules/inbox/inbox.selectors'

import { InboxService } from '@app/modules/inbox/inbox.service'

import { Message } from '@app/shared/models'

@Component({
    selector: 'mr-chatroom',
    templateUrl: './chatroom.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatroomComponent implements OnInit, OnDestroy {
    /**
     * Room context
     */
    localContext: {from: string, to: string}
    /**
     * Message log
     */
    log$: Observable<Message[] | null>
    /**
     * Component unsubscriber
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Room context
     */
    @Input()
    set context(ctx: {from: string, to: string}) {
        if (ctx.from && ctx.to) this.service.initRoom(ctx.from, ctx.to)
        else this.service.terminateRoom()
        this.localContext = ctx
    }
    /**
     * Component constructor
     * @param service inbox service
     * @param store ngrx store
     */
    constructor(
        private service: InboxService,
        private store: Store<any>
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.log$ = this.store.select(selectMessages)
        .pipe(tap(res => console.log(res, res && res.length)))
        // Update push
        this.store.select(selectRoomList)
        .pipe(
            takeUntil(this.unsubscriber$),
            map(list => list.find(room => room.id === this.localContext.to)),
            tap((room) => {
                if (room && room.newMessages) {
                    this.service.markRoomAsRead(this.localContext.from, this.localContext.to)
                }
            })
        ).subscribe()
    }
    /**
     * Get older messages
     */
    loadOlder() {
        this.service.loadMore(this.localContext.from, this.localContext.to)
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
