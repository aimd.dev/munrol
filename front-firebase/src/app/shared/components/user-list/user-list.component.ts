import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core"
import { ListUser } from "@app/shared/models"


@Component({
    selector: 'mr-user-list',
    templateUrl: './user-list.component.html',
    styles: [
    ]
})
export class UserListComponent {

    @Input() users: ListUser[] = []

    @Input() selectable: boolean

    @Output()
    onchange: EventEmitter<string[]> = new EventEmitter<string[]>()

    handleCheck(friend: ListUser) {
        friend.checked = !friend.checked
        this.onchange.emit(this.users.filter((u: ListUser) => u.checked).map((u: ListUser) => u.username))
    }
}
