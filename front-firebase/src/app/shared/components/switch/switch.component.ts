import { Component, ElementRef, Input, Output, EventEmitter, Renderer2, Self, Optional } from '@angular/core'
import { ControlValueAccessor, NgControl } from '@angular/forms'

@Component({
    selector: 'mr-switch',
    templateUrl: './switch.component.html',
    styles: [
    ]
})
export class SwitchComponent implements ControlValueAccessor {

    @Input() preventDefault: boolean
    @Input() noMargin: boolean
    @Input() label: string
    @Input() help: string

    @Output()
    onactivate: EventEmitter<any> = new EventEmitter()
    @Output()
    ondeactivate: EventEmitter<any> = new EventEmitter()

    value: boolean

    constructor(
        private element: ElementRef,
        private renderer: Renderer2,
        @Self()
        @Optional()
        public ngControl: NgControl) {
        if (this.ngControl) { this.ngControl.valueAccessor = this }
    }

    onChangeCallback: any = () => { }
    onTouchedCallback: any = () => { }

    writeValue(value: any) {
        this.value = value
    }
    registerOnChange(fn: (_: any) => void): void {
        this.onChangeCallback = fn
    }
    registerOnTouched(fn: () => void): void {
        this.onTouchedCallback = fn
    }
    setDisabledState(disabled: boolean) {
        this.renderer.setProperty(this.element.nativeElement, 'disabled', disabled)
    }

    handleClick(e: MouseEvent) {
        e.preventDefault()
        this.value ? this.ondeactivate.emit() : this.onactivate.emit()
    }

    handleChange(e: Event) {
        const val: any = (e.target as HTMLInputElement).checked
        val ? this.onactivate.emit() : this.ondeactivate.emit()
        this.onChangeCallback(val)
    }
}
