import { DOCUMENT } from '@angular/common'
import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Inject,
    Input,
    OnInit
} from '@angular/core'

import { selectThreadFiles } from '@app/modules/thread/thread.selectors'

import {
    allowedTagNames,
    AllowedTagNames,
    File,
    HTMLMapElement
} from '@app/shared/models'
import { Store } from '@ngrx/store'
import { Observable, Subject } from 'rxjs'
import { map } from 'rxjs/operators'

@Component({
    selector: 'mr-editor-readonly',
    templateUrl: './editor-readonly.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditorReadonlyComponent implements OnInit {

    bodyMap: HTMLMapElement[] = []

    files$: Observable<{[key: string]: boolean}>

    unsubscriber$: Subject<void> = new Subject<void>()

    @Input()
    set html(html: string) {
        this.bodyMap = this.buildBodyMap(html)
    }

    constructor(
        public host: ElementRef,
        private store: Store<any>,
        @Inject(DOCUMENT) private document: Document
    ) { }

    ngOnInit(): void {
        this.files$ = this.store.select(selectThreadFiles)
        .pipe(
            map(files => files.reduce((initial, next) => ({ ...initial, [next.id]: true }), {}))
        )
    }

    buildBodyMap(html: string): HTMLMapElement[] {

        const map: HTMLMapElement[] = []
        const body = this.document.createElement('div')
        body.innerHTML = html

        body.childNodes.forEach((node) => {
            this.pushChildren(node, map)
        })

        body.remove()
        return map
    }

    pushChildren(node: Node, list: any[]) {

        const mapElement: HTMLMapElement = {
            tagName: allowedTagNames.includes(node.nodeName as AllowedTagNames) ? node.nodeName : '#text'
        }

        if (node.nodeName === 'A') {
            const element = node as HTMLAnchorElement
            if (element.dataset.file) mapElement.file = element.dataset.file
        }

        if (node.hasChildNodes()) {
            mapElement.children = []
            node.childNodes.forEach((n) => {
                this.pushChildren(n, mapElement.children as any)
            })
        } else {
            mapElement.text = node.textContent ? node.textContent : undefined
        }

        list.push(mapElement)
    }
}
