import { Component, Input } from '@angular/core'

@Component({
  selector: 'mr-timestamp',
  templateUrl: './timestamp.component.html',
  styles: [
  ]
})
export class TimestampComponent {

    @Input()
    createdAt: string

    @Input()
    lastEdit: string | undefined

    @Input()
    lastUpdated: string | undefined
}
