import {
    Directive,
    ViewContainerRef
} from '@angular/core'

import {
    SidebarService
} from './sidebar.service'

@Directive({
    selector: '[mrSidebarHost]'
})
export class SidebarHostDirective {
    /**
     * Constructor de la directiva
     * @param view view container ref de la directiva
     * @param service servicio para generar el sidebar
     */
    constructor(
        public view: ViewContainerRef,
        private service: SidebarService
    ) { this.service.view = view }
}
