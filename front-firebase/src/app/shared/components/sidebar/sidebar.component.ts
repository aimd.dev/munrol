import {
    Component
} from '@angular/core'

import {
    animate,
    style,
    transition,
    trigger
} from '@angular/animations'

import {
    SidebarService
} from './sidebar.service'

@Component({
    selector: 'mr-sidebar',
    templateUrl: './sidebar.component.html',
    animations: [
        trigger('slide', [
            transition(':enter', [
                style({ width: 0 }),
                animate(150, style({ width: '*' }))
            ]),
            transition(':leave', [
                animate(150, style({ width: 0 }))
            ])
        ])
    ]
})
export class SidebarComponent {
    /**
     * Constructor del componente
     * @param sidebar servicio sidebar
     */
    constructor(
        private sidebar: SidebarService
    ) { }
    /**
     * Ocultar el sidebar
     */
    hide() {
        this.sidebar.hide()
    }
}
