import {
    Injectable,
    Type,
    ViewChild,
    ViewContainerRef
} from '@angular/core'

import {
    SidebarHostDirective
} from './sidebar-host.directive'

import {
    SidebarProp
} from '@app/shared/models/sidebar-prop'

@Injectable({
    providedIn: 'root'
})
export class SidebarService {
    /**
     * View container ref
     */
    view: ViewContainerRef
    /**
     * Directive host element
     */
    @ViewChild(SidebarHostDirective, { static: true }) host: SidebarHostDirective
    /**
     * Constructor del servicio
     */
    constructor() { }
    /**
     * Generar el sidebar
     * @param componentType componente a generar
     * @param props inputs del componente
     */
    show(componentType: Type<any>, props: SidebarProp[] = []) {
        // Clear the view
        if (this.view) this.view.clear()
        // Create component
        const componentRef = this.view.createComponent(componentType)
        // Assign inputs
        props.forEach((p: SidebarProp) => { componentRef.instance[p.key] = p.value })
    }
    /**
     * Ocultar el sidebar
     */
    hide() {
        this.view.clear()
    }
}
