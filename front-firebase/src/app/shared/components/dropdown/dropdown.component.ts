import {
    Component,
    ElementRef,
    HostListener,
    Input
} from '@angular/core'

import {
    animate,
    style,
    transition,
    trigger
} from '@angular/animations'

import {
    BehaviorSubject
} from 'rxjs'

@Component({
    selector: 'mr-dropdown',
    templateUrl: './dropdown.component.html',
    animations: [
        trigger('shrink', [
            transition(':enter', [
                style({ transform: 'scale(0)', opacity: 0 }),
                animate(100, style({ transform: 'scale(1)', opacity: 1 }))
            ]),
            transition(':leave', [
                animate(100, style({ transform: 'scale(0)', opacity: 0 }))
            ])
        ])
    ]
})
export class DropdownComponent {
    /**
     * Drop status
     */
    shown$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
    /**
     * Icon for the button
     */
    @Input()
    icon = 'las la-ellipsis-h'
    /**
     * CSS transform origin
     */
    @Input()
    transformOrigin = 'top left'
    /**
     * Clickout event
     */
    @HostListener('document:click', ['$event'])
    clickout(event: MouseEvent) {
        if (!this.host.nativeElement.contains(event.target) && this.shown$.getValue()) this.shown$.next(false)
    }
    /** */
    constructor(
        private host: ElementRef
    ) {}
    /**
     * Toggle dropdown
     * @param event mouse event
     */
    toggle() {
        const current = this.shown$.getValue()
        this.shown$.next(!current)
    }
}
