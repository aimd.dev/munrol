import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mr-tag-list',
  templateUrl: './tag-list.component.html',
  styles: [
  ]
})
export class TagListComponent implements OnInit {

  @Input() tags:string[] | undefined = []

  @Input() plain:boolean

  constructor() { }

  ngOnInit(): void {
  }

}
