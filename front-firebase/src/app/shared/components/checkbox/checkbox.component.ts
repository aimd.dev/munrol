import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Optional,
    Renderer2,
    Self
} from '@angular/core'

import {
    ControlValueAccessor,
    NgControl
} from '@angular/forms'

import {
    BehaviorSubject
} from 'rxjs'

@Component({
    selector: 'mr-checkbox',
    templateUrl: './checkbox.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent implements ControlValueAccessor {

    value$: BehaviorSubject<any> = new BehaviorSubject<any>(false)

    constructor(
        private host: ElementRef,
        private renderer: Renderer2,
        @Self()
        @Optional()
        public ngControl: NgControl
    ) { if (this.ngControl) { this.ngControl.valueAccessor = this } }
    /**
     * Control value accessor implementation
     */
    writeValue(value: any) {
        this.value$.next(!!value)
    }
    propagateChange = (_: any) => { }
    propagateTouched = () => { }
    registerOnChange(fn: (value: any) => any): void {
        this.propagateChange = fn
    }
    registerOnTouched(fn: () => void): void {
        this.propagateTouched = fn
    }
    setDisabledState(disabled: boolean): void {
        if (disabled) this.renderer.addClass(this.host.nativeElement.children[0], 'disabled')
        else this.renderer.removeClass(this.host.nativeElement.children[0], 'disabled')
    }
    /**
     * Change checkbox value
     */
    handleChange() {
        const value = this.value$.getValue()
        this.value$.next(!value)
        this.propagateChange(!value)
    }
}
