import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostListener,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Renderer2,
    Self,
    TemplateRef
} from '@angular/core'

import {
    animate,
    style,
    transition,
    trigger
} from '@angular/animations'

import {
    ControlValueAccessor,
    FormGroup,
    NgControl
} from '@angular/forms'

import {
    BehaviorSubject,
    Subject
} from 'rxjs'

import {
    skip,
    takeUntil
} from 'rxjs/operators'

@Component({
    selector: 'mr-custom-select-single',
    templateUrl: './custom-select-single.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slide', [
            transition(':enter', [
                style({ height: 0 }),
                animate(100, style({}))
            ]),
            transition(':leave', [
                animate(100, style({ height: 0 }))
            ])
        ])
    ]
})
export class CustomSelectSingleComponent implements OnInit, OnDestroy, ControlValueAccessor {
    /**
     * Formulario de checks
     */
    form: FormGroup
    /**
     * Filter query
     */
    query = ''
    /**
     * Elementos
     */
    _items: any[] = []
    /**
     * Visibility status
     */
    open$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
    /**
     * local value
     */
    value$: BehaviorSubject<any> = new BehaviorSubject<any>(null)
    /**
     * Subject for destroy
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Select placeholder
     */
    @Input()
    placeholder: string
    /**
     * Elements of the dropdown
     */
    @Input()
    set items(items: any[]) {
        this._items = [ ...items ]
    }
    /**
     * Item template
     */
    @Input()
    template: TemplateRef<any>
    /**
     * Selected item template
     */
    @Input()
    selectedTemplate: TemplateRef<any>
    /**
     * Clickout event
     */
    @HostListener('document:click', ['$event'])
    clickout(event: MouseEvent) {
        if (!this.host.nativeElement.contains(event.target) && this.open$.getValue()) this.open$.next(false)
    }
    /**
     * Control value accessor implementation
     */
    writeValue(value: any) {
        if (value && value.length) this.value$.next(value)
    }
    propagateChange = (_: any) => { }
    propagateTouched = () => { }
    registerOnChange(fn: (value: any) => any): void {
        this.propagateChange = fn
    }
    registerOnTouched(fn: () => void): void {
        this.propagateTouched = fn
    }
    setDisabledState(disabled: boolean): void {
        this.renderer.setProperty(this.host.nativeElement, 'disabled', disabled)
    }
    /**
     * Constructor del componente
     * @param host host element
     * @param renderer angular renderer
     * @param fb angular form builder
     * @param ngControl form control
     */
    constructor(
        private host: ElementRef,
        private renderer: Renderer2,
        @Self()
        @Optional()
        public ngControl: NgControl
    ) { if (this.ngControl) { this.ngControl.valueAccessor = this } }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.value$
        .pipe(takeUntil(this.unsubscriber$), skip(1))
        .subscribe(value => {
            this.propagateChange(value)
        })
    }
    /**
     * Open the dropdown
     */
    open() {
        this.open$.next(true)
    }
    /**
     * Single select change
     * @param id element id
     */
    singleSelect(id: string) {
        this.value$.next(id)
        this.open$.next(false)
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
