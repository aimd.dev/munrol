import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostListener,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Renderer2,
    Self,
    TemplateRef
} from '@angular/core'

import {
    animate,
    style,
    transition,
    trigger
} from '@angular/animations'

import {
    ControlValueAccessor,
    FormBuilder,
    FormControl,
    FormGroup,
    NgControl
} from '@angular/forms'

import {
    BehaviorSubject,
    Subject
} from 'rxjs'

import {
    takeUntil
} from 'rxjs/operators'

@Component({
    selector: 'mr-custom-select-multi',
    templateUrl: './custom-select-multi.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slide', [
            transition(':enter', [
                style({ height: 0 }),
                animate(100, style({}))
            ]),
            transition(':leave', [
                animate(100, style({ height: 0 }))
            ])
        ])
    ]
})
export class CustomSelectMultiComponent implements OnInit, OnDestroy, ControlValueAccessor {
    /**
     * Formulario de checks
     */
    form: FormGroup
    /**
     * Filter query
     */
    query = ''
    /**
     * Elementos
     */
    _items: any[]
    /**
     * Visibility status
     */
    open$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)
    /**
     * local value
     */
    value$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([])
    /**
     * Subject for destroy
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * Select placeholder
     */
    @Input()
    placeholder: string
    /**
     * Elements of the dropdown
     */
    @Input()
    set items(items: any) {
        const controls: any = {}
        items.forEach((item: any) => controls[item.id] = new FormControl({
            value: item.inThread ? true : false,
            disabled: item.inThread ? true : false
        }))
        this.form = this.fb.group(controls)
        this._items = [ ...items ]
    }
    /**
     * Item template
     */
    @Input()
    template: TemplateRef<any>
    /**
     * Selected item template
     */
    @Input()
    selectedTemplate: TemplateRef<any>
    /**
     * Clickout event
     */
    @HostListener('document:click', ['$event'])
    clickout(event: MouseEvent) {
        if (!this.host.nativeElement.contains(event.target) && this.open$.getValue()) this.open$.next(false)
    }
    /**
     * Control value accessor implementation
     */
    writeValue(value: any) {
        if (value && value.length) {
            this.calculateFormValue(value)
            this.value$.next(value)
        }
    }
    propagateChange = (_: any) => { }
    propagateTouched = () => { }
    registerOnChange(fn: (value: any) => any): void {
        this.propagateChange = fn
    }
    registerOnTouched(fn: () => void): void {
        this.propagateTouched = fn
    }
    setDisabledState(disabled: boolean): void {
        this.renderer.setProperty(this.host.nativeElement, 'disabled', disabled)
    }
    /**
     * Constructor del componente
     * @param host host element
     * @param renderer angular renderer
     * @param fb angular form builder
     * @param ngControl form control
     */
    constructor(
        private host: ElementRef,
        private renderer: Renderer2,
        private fb: FormBuilder,
        @Self()
        @Optional()
        public ngControl: NgControl
    ) { if (this.ngControl) { this.ngControl.valueAccessor = this } }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.form.valueChanges
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe(value => {
            const ids = this.calculateSelection(value)
            this.value$.next(ids)
            this.propagateChange(ids)
        })
    }
    /**
     * Open the dropdown
     */
    open() {
        this.open$.next(true)
    }
    /**
     * Returns array of element ids
     * @param data form value
     * @returns array of selected items id
     */
    calculateSelection(data: {[key: string]: boolean}): string[] {
        return Object.keys(data).filter(k => data[k]).map(e => e)
    }
    /**
     * Sets the form value
     * @param selection value
     */
    calculateFormValue(selection: string[]) {
        const current = { ...this.form.value }
        const source = selection.reduce((result, next) =>  Object.assign(result, {[next]: true}), {})
        this.form.setValue(Object.assign(current, source))
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
