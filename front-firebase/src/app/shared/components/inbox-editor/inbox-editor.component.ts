import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { InboxService } from '@app/modules/inbox/inbox.service'
import { Message } from '@app/shared/models'
import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { editorBodyRequired } from '@app/shared/util/custom-validators/editor-body-required-validator'
import { EditorBodyRequiredValidatorService } from '@app/shared/util/custom-validators/editor-body-required-validator.service'

@Component({
    selector: 'mr-inbox-editor',
    templateUrl: './inbox-editor.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxEditorComponent {

    to: string

    from: string

    form: FormGroup

    public toolbar: typeof EditorToolbar = EditorToolbar

    @Input()
    set context(ctx: {from: string, to: string | null}) {
        this.form = this.fb.group({
            author: [ctx.from, [Validators.required]],
            body: ['', [this.vldt.editorBodyRequired()]]
        })
        this.from = ctx.from
        this.to = ctx.to as string
    }

    constructor(
        private fb: FormBuilder,
        private vldt: EditorBodyRequiredValidatorService,
        private service: InboxService
    ) { }

    postMessage() {

        if (this.form.invalid) return
        const message = {
            ...this.form.value,
            createdAt: new Date().toISOString()
        } as Message
        this.service.sendMessage(this.from, this.to, message)
        .catch((err) => console.log(err));

        (this.form.get('body') as FormControl).reset()
    }

    setTyping(typing: boolean) {
        this.service.updateTyping(this.from, this.to, typing)
    }
}
