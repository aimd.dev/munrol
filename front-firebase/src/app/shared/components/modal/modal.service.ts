import {
    Injectable,
    Type,
    ViewChild,
    ViewContainerRef
} from '@angular/core'

import {
    ModalHostDirective
} from './modal-host.directive'

import {
    ModalProp
} from '@app/shared/models'

@Injectable({
    providedIn: 'root'
})
export class ModalService {
    /**
     * View container ref
     */
    view: ViewContainerRef
    /**
     * Directive host element
     */
    @ViewChild(ModalHostDirective, { static: true }) host: ModalHostDirective
    /**
     * Promise vars
     */
    private result: Promise<any>
    private resolve: any
    private reject: any
    /**
     * Constructor del servicio
     */
    constructor() { }
    /**
     * Init promise
     */
    init(): void {
        this.result = new Promise((resolve, reject) => {
            this.resolve = resolve
            this.reject = reject
        })
    }
    /**
     * Generar modal
     * @param componentType modal a generar
     * @param props inputs for modal
     * @returns promise
     */
    open(componentType: Type<any>, props: ModalProp[] = []): Promise<any> {
        // Clear previous view
        if (this.view) this.view.clear()
        // Init promise
        this.init()
        // Create component
        const componentRef = this.view.createComponent(componentType)
        // Assign inputs
        props.forEach((p: ModalProp) => { componentRef.instance[p.key] = p.value })
        // Return the promise
        return this.result
    }
    /**
     * Close modal
     * @param response modal payload
     * @returns promise
     */
    close(response: any = null): Promise<any> {
        this.view.clear()
        return this.resolve(response)
    }
}
