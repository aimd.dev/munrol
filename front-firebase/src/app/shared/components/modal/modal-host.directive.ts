import {
    Directive,
    ViewContainerRef
} from '@angular/core'

import {
    ModalService
} from './modal.service'

@Directive({
    selector: '[mrModalHost]'
})
export class ModalHostDirective {
    /**
     * Constructor de la directiva
     * @param view view container ref de la directiva
     * @param service servicio para generar el modal
     */
    constructor(
        public view: ViewContainerRef,
        private service: ModalService
    ) { this.service.view = view }
}
