import {
    Component,
    Input,
    Output,
    EventEmitter
} from '@angular/core'

import {
    animate,
    style,
    transition,
    trigger
} from '@angular/animations'

import {
    ModalService
} from './modal.service'

import {
    loaderState
} from '@app/shared/models'

@Component({
    selector: 'mr-modal',
    templateUrl: './modal.component.html',
    animations: [
        trigger('fade', [
            transition(':enter', [
                style({ opacity: 0 }),
                animate(100, style({ opacity: 1 }))
            ]),
            transition(':leave', [
                animate(100, style({ opacity: 0 }))
            ])
        ]),
        trigger('slide', [
            transition(':enter', [
                style({ transform: 'translateY(-100px)', opacity: 0 }),
                animate(200, style({ transform: 'translateY(0)', opacity: 1 }))
            ]),
            transition(':leave', [
                animate(200, style({ transform: 'translateY(-100px)', opacity: 0 }))
            ])
        ])
    ]
})
export class ModalComponent {
    /**
     * Titulo
     */
    @Input()
    title = ''
    /**
     * Subtitulo
     */
    @Input()
    subtitle: string
    /**
     * Texto en el boton de confirmar
     */
    @Input()
    confirmText = 'Confirmar'
    /**
     * Tamaño grande
     */
    @Input()
    large: boolean
    /**
     * Scroller dentro del cuerpo
     */
    @Input()
    scroller: boolean
    /**
     * Retirar header y footer
     */
    @Input()
    customLayout: boolean
    /**
     * Estilo del modal
     */
    @Input()
    ui = 'DEFAULT'
    /**
     * Cerrar el modal al confirmar
     */
    @Input()
    closeOnConfirm: boolean
    /**
     * Deshabilitar confirmar
     */
    @Input()
    disableConfirm: boolean
    /**
     * Loading state
     */
    @Input()
    loading: loaderState
    /**
     * Evento al cerrar
     */
    @Output()
    onclose: EventEmitter<void> = new EventEmitter<any>()
    /**
     * Evento al confirmar
     */
    @Output()
    onconfirm: EventEmitter<void> = new EventEmitter<void>()
    /**
     * Constructor del componente
     * @param service servicio del modal
     */
    constructor(
        private service: ModalService
    ) { }
    /**
     * Evento para el click en el overlay
     * @param e mouse click
     */
    overlayClick(e: MouseEvent) {
        if ((e.target as HTMLElement).classList.contains('overlay')) this.close()
    }
    /**
     * Cerrar modal
     */
    close() {
        this.service.close()
    }
    /**
     * Confirmar
     */
    confirm() {
        this.onconfirm.emit()
    }
}
