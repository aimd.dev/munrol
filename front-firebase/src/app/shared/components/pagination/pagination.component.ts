import {
    Component,
    Input,
    ChangeDetectionStrategy
} from '@angular/core'

import {
    ActivatedRoute,
    Router
} from '@angular/router'


@Component({
    selector: 'mr-pagination',
    templateUrl: './pagination.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {

    active: number

    @Input() pages: number[] | null

    @Input() set current(index: number | null) {
        if (index) this.active = index
        else this.active = 1
    }

    constructor(
        private router: Router,
        public route: ActivatedRoute
    ) { }

    prev() {
        if (this.active > 1)
            this.router.navigate(['.'], { queryParams: { page: this.active - 1 }, relativeTo: this.route })
    }

    next() {
        if (this.active < (this.pages as number[]).length)
            this.router.navigate(['.'], { queryParams: { page: this.active + 1 }, relativeTo: this.route })
    }
}
