import {
    Component,
    ElementRef,
    HostListener,
    Input,
    OnInit
} from '@angular/core'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    map,
    tap
} from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'
import { selectThreadFiles } from '@app/modules/thread/thread.selectors'

import {
    SidebarService
} from '../sidebar/sidebar.service'

import {
    FileSidebarComponent
} from '@app/modules/thread/components/file-sidebar/file-sidebar.component'

import {
    File,
    SidebarProp
} from '@app/shared/models'

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: '[mr-file-pointer]',
    templateUrl: './file-pointer.component.html',
    styles: []
})
export class FilePointerComponent implements OnInit {
    /**
     * Posicion file
     */
    fileX = 0
    fileY = 0
    left = false
    right = false
    /**
     * Display status
     */
    open$: Subject<boolean> = new Subject<boolean>()
    /**
     * Referenced file
     */
    file$: Observable<File>
    /**
     * Referenced file
     */
    @Input() id: string
    /**
     * Editor element ref
     */
    @Input() editorHost: ElementRef
    /**
     * Component constructor
     * @param host host element
     * @param store ngrx store
     * @param sidebar sidebar service
     */
    constructor(
        private host: ElementRef,
        private store: Store<any>,
        private sidebar: SidebarService
    ) { }
    /**
     * Component init
     */
    ngOnInit(): void {
        this.host.nativeElement.classList.add('file-pointer')
    }
    /**
     * Hover in listener
     */
    @HostListener('mouseenter', ['$event']) in() {
        // Positioning
        const host = this.host.nativeElement.getBoundingClientRect()
        const editor = this.editorHost.nativeElement.getBoundingClientRect()
        const width = this.host.nativeElement.clientWidth
        const rightLimit = editor.left + this.editorHost.nativeElement.clientWidth

        let x = (host.left - editor.left) + (width + (width / 2)) - 150

        if (x < 0) {
            this.left = true
            x = 20
        }
        if ((editor.left + x + 300) > rightLimit) {
            this.right = true
            x = rightLimit - 280 - editor.left
        }

        this.fileY = this.host.nativeElement.offsetTop - 180
        this.fileX = x
        // Open
        this.open$.next(true)
        this.file$ = this.store.select(selectThreadFiles)
        .pipe(
            map((files) => files.find(f => f.id === this.id) as File),
            tap((file) => console.log(file)),
            tap((file) => !file.body && this.store.dispatch(ThreadActions.getReference({ id: this.id })))
        )
    }
    /**
     * Hover out listener
     */
    @HostListener('mouseleave') out() {
        // console.log('mouse leave')
        this.open$.next(false)
        this.left = false
        this.right = false
    }
    /**
     * Expand file
     * @param file file
     */
    showInSidebar(file: File) {
        const props: SidebarProp[] = [{ key: 'expand', value: file }]
        this.sidebar.show(FileSidebarComponent, props)
    }
}
