import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core'

import {
    fromEvent,
    Subject
} from 'rxjs'

import {
    debounceTime,
    delay,
    takeUntil,
    tap
} from 'rxjs/operators'

import {
    Message
} from '@app/shared/models'

@Component({
    selector: 'mr-chatlog',
    templateUrl: './chatlog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatlogComponent implements OnInit, AfterViewInit, OnDestroy {
    /**
     * Previous scrolltop
     */
    previousHeight: number
    previousTop: number
    /**
     * local message log
     */
    localLog: Message[]
    /**
     * Subject to scroll to bottom
     */
    scrollToBottom$: Subject<void> = new Subject<void>()
    /**
     * Component unsubscriber
     */
    unsubscriber$: Subject<void> = new Subject<void>()
    /**
     * User
     */
    @Input()
    from: string
    /**
     * Message log
     */
    @Input()
    set log(log: Message[]) {
        this.localLog = log
        this.scrollToBottom$.next()
    }
    /**
     * Scroll top output
     */
    @Output()
    loadOlder: EventEmitter<any> = new EventEmitter<any>()
    /**
     * Component constructor
     * @param host host element
     */
    constructor(private host: ElementRef) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        // Update scroll future behavior
        fromEvent(this.host.nativeElement, 'scroll')
        .pipe(
            takeUntil(this.unsubscriber$),
            debounceTime(300),
            tap(() => {
                const el = this.host.nativeElement
                this.previousHeight = el.scrollHeight
                if (el.scrollTop < 100) this.loadOlder.emit()
            })
        ).subscribe()
        // Subscribe to the scroll to bottom
        this.scrollToBottom$
        .pipe(takeUntil(this.unsubscriber$), delay(0))
        .subscribe(() => {
            const el = this.host.nativeElement
            const top = el.scrollTop
            const height = el.scrollHeight
            const client = el.clientHeight

            // console.log('scroll top', this.host.nativeElement.scrollTop)
            // console.log('scroll height', this.host.nativeElement.scrollHeight)
            // console.log('prev top', this.previousHeight)

            if (height - top === client || !this.previousHeight) {
                this.host.nativeElement.scrollTop = height
            } else {
                this.host.nativeElement.scrollTop = height - (this.previousHeight - top)
            }

            // this.scroll = el.scrollHeight - el.scrollTop === el.clientHeight

            // if (top !== 0) this.host.nativeElement.scrollTop = height
            // else this.host.nativeElement.scrollTop = this.previousHeight
            // this.previousTop = top
        })
    }
    getTimestamp(index: number, row: Message) {
        return row.createdAt + row.author
    }
    /**
     * On view load, scroll to bottom
     */
    ngAfterViewInit(): void {
        this.scrollToBottom$.next()
    }
    /**
     * Component destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
