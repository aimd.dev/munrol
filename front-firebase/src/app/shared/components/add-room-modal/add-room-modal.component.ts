import { Component, OnInit } from '@angular/core'
import { selectCurrentUserFriends } from '@app/modules/auth/auth.selectors'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import { ModalService } from '../modal/modal.service'

@Component({
    selector: 'mr-add-room-modal',
    templateUrl: './add-room-modal.component.html',
    styles: [
    ]
})
export class AddRoomModalComponent implements OnInit {
    /**
     * Selected friend
     */
    selection: {title: string}
    /**
     * Thread files
     */
    friends$: Observable<{title: string}[]>
    /**
     * Constructor del componente
     * @param store ngrx store
     * @param modal modal service
     */
    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.friends$ = this.store.select(selectCurrentUserFriends)
        .pipe(
            map(friends => Object.keys(friends).map(f => ({title: f})))
        )
    }
    /**
     * Confirm files to link
     */
    create() {
        this.modal.close(this.selection.title)
    }
}
