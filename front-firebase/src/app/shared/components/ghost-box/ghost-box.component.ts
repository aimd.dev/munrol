import { Component, Input } from '@angular/core'

@Component({
  selector: 'mr-ghost-box',
  templateUrl: './ghost-box.component.html',
  styles: [
  ]
})
export class GhostBoxComponent {

  @Input() height = 200

  constructor() { }
}
