import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core'

import { Thread } from '@app/modules/thread/models/Thread.model'

import { File, loaderState } from '@app/shared/models'
import { Store } from '@ngrx/store'
import * as FileActions from '@modules/file/file.actions'
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs'
import { selectFileThreads } from '@app/modules/file/file.selectors'
import { filter, map, tap } from 'rxjs/operators'
import { constants } from '@app/core/config/constants'

@Component({
    selector: 'mr-expanded-file',
    templateUrl: './expanded-file.component.html',
    styles: [
    ]
})
export class ExpandedFileComponent implements OnInit, OnDestroy {
    /**
     * Last fetched
     */
    last$: BehaviorSubject<number> = new BehaviorSubject<number>(0)
    /**
     * End of files
     */
    end$: Subject<boolean> = new Subject<boolean>()

    loading$: Subject<loaderState>

    threads$: Observable<Thread[] | null>

    @Input()
    file: File

    @Input()
    showMenu: boolean

    @Output()
    edit: EventEmitter<void> = new EventEmitter<void>()

    @Output()
    remove: EventEmitter<void> = new EventEmitter<void>()

    constructor(
        private store: Store<any>
    ) { }

    ngOnInit(): void {
        this.threads$ = combineLatest([this.last$, this.store.select(selectFileThreads)])
        .pipe(
            filter(([_, threads]) => threads !== null),
            tap(([_, threads]) => {
                if ((threads as Thread[]).every(s => !!s.body)) this.end$.next(true)
            }),
            map(([last, threads]) => (threads as Thread[]).slice(0, last + constants.pageLength)),
            tap((threads) => {
                const toDownload = threads.filter(f => !f.body).map(f => f.id)
                if (toDownload.length) {
                    this.store.dispatch(FileActions.getFileThreads({ ids: toDownload.slice(0, 9) }))
                }
            })
        )
    }

    showThreads() {
        const threads = this.file.linkedThreads.map(id => ({ id })) as Thread[]
        this.store.dispatch(FileActions.setFileThreads({ threads }))
    }

    getThreads() {
        // this.loading$.next(LoaderStateEnum.ON)
        const currentValue = this.last$.getValue()
        this.last$.next(currentValue + constants.pageLength)
    }

    ngOnDestroy(): void {
        this.store.dispatch(FileActions.reset())
    }
}
