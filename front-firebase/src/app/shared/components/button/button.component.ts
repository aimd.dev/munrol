import {
    Component,
    Input,
    ElementRef
} from '@angular/core'

import {
    loaderState,
    LoaderStateEnum
} from '@app/shared/models'

@Component({
    selector: '[mr-button]',
    templateUrl: './button.component.html',
    styles: []
})
export class ButtonComponent {
    /**
     * Show spinner
     */
    spinner: boolean
    /**
     * Loading state
     */
    @Input() set loading(value: loaderState | null) {
        const loading: boolean = value === LoaderStateEnum.ON
        this.spinner = loading
        if (loading) this.el.nativeElement.classList.add('loading')
        else this.el.nativeElement.classList.remove('loading')
    }
    /**
     * Constructor del componente
     * @param el host element
     */
    constructor(
        private el: ElementRef
    ) { }
}
