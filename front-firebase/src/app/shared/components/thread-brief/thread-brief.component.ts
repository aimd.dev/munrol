import { Component, Input } from '@angular/core'
import { ThreadBrief } from '@modules/thread/models/ThreadBrief.model'

@Component({
  selector: 'mr-thread-brief',
  templateUrl: './thread-brief.component.html',
  styles: [
  ]
})
export class ThreadBriefComponent {

  @Input()
  thread: ThreadBrief
}
