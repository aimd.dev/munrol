import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChildren, Input, Output, QueryList, EventEmitter } from '@angular/core'
import { Tab } from './models/Tab.model'
import { TabContentComponent } from './tab-content/tab-content.component'
import { TabService } from './tab.service'

@Component({
  selector: 'mr-tabs',
  templateUrl: './tabs.component.html',
  providers: [TabService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements AfterViewInit {

  @Input() centered:boolean

  @Input() active:string

  @Output()
  select: EventEmitter<string> = new EventEmitter<string>()

  @ContentChildren(TabContentComponent) screens:QueryList<TabContentComponent>;

  constructor(
    private service: TabService,
    private cf: ChangeDetectorRef
  ) { }

  tabs: Tab[] = []

  ngAfterViewInit():void {
    this.tabs = this.screens.map((screen:TabContentComponent) => ({ title: screen.tabTitle, id: screen.id }))
    this.cf.detectChanges()
  }

}
