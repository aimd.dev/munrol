import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Tab } from './models/Tab.model';

@Injectable({
  providedIn: 'root'
})
export class TabService {

  constructor() { }

  active$:ReplaySubject<string> = new ReplaySubject<string>(1)
  tabs$:ReplaySubject<Tab[]> = new ReplaySubject<Tab[]>(1)

  setTabs(tabs:Tab[], active:number | null = null):void {
    this.tabs$.next([...tabs])
    active ? this.active$.next(tabs[active].id) : this.active$.next(tabs[0].id)
  }
}
