import { ChangeDetectionStrategy, Component, Input } from '@angular/core'

@Component({
  selector: 'mr-tab-content',
  templateUrl: './tab-content.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabContentComponent {
  @Input() tabTitle: string
  @Input() id: string
  @Input() active: boolean

  constructor() { }
}
