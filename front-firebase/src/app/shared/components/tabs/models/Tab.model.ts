export interface Tab {
    id:string,
    title:string,
    alert?:boolean
}