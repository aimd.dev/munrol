import { DOCUMENT } from '@angular/common'
import {
    Component,
    Input,
    OnInit,
    ElementRef,
    ViewChild,
    HostListener,
    Renderer2,
    Self,
    Optional,
    Output,
    EventEmitter,
    Inject
} from '@angular/core'

import {
    ControlValueAccessor,
    NgControl
} from '@angular/forms'

import { EditorToolbar } from '@app/shared/models/editor-toolbar.enum'
import { InsertFileModalComponent } from '../insert-file-modal/insert-file-modal.component'
import { ModalService } from '../modal/modal.service'

import { position } from 'caret-pos'
import { NewThreadFilesComponent } from '@app/modules/thread/components/modals/new-thread-files/new-thread-files.component'

@Component({
    selector: 'mr-editor',
    templateUrl: './editor.component.html',
    styles: []
})
export class EditorComponent implements OnInit, ControlValueAccessor {
    /**
     * Mostrar el toolbar
     */
    show: boolean
    /**
     * Valor interno del control
     */
    value: string
    /**
     * Visibilidad del placeholder
     */
    placeholderHidden: boolean
    /**
     * Posicion toolbar X
     */
    toolbarX: number
    /**
     * Posicion toolbar Y
     */
    toolbarY: number
    /**
     * Visibilidad del toolbar
     */
    rendered: boolean
    /**
     * Text selection for file insert
     */
    fileSelectionRange: Range
    /**
     * Injected document window
     */
    window: Window

    files: string[] = []
    /**
     * Px min height of editor
     */
    @Input() minHeight = 300
    /**
     * Enable file button
     */
    @Input() enableFiles = false
    @Input() userFiles = false
    /**
     * Control placeholder
     */
    @Input() placeholder = 'Comienza a escribir aquí...'
    /**
     * Toolbar buttons
     */
    @Input() toolbarButtons: EditorToolbar = EditorToolbar.MIN

    @Input() enterKeyEvent = false

    @Output() typingStart: EventEmitter<void> = new EventEmitter<void>()
    @Output() typingEnd: EventEmitter<void> = new EventEmitter<void>()
    @Output() enterKey: EventEmitter<void> = new EventEmitter<void>()
    @Output() fileChanges: EventEmitter<string[]> = new EventEmitter<string[]>()

    /**
     * Container element ref
     */
    @ViewChild('el') el: ElementRef
    /**
     * Toolbar element ref
     */
    @ViewChild('toolbar') toolbar: ElementRef
    /**
     * Editor element ref
     */
    @ViewChild('editor') editor: ElementRef
    /**
     * Control Value Accessor implementation
     */
    onChange = (_: any) => { }
    onTouched = () => { }
    writeValue(value: any) {
        setTimeout(() => this.setValue(value))
    }
    registerOnChange(fn: (_: any) => unknown): void {
        this.onChange = fn
    }
    registerOnTouched(fn: () => void): void {
        this.onTouched = fn
    }
    setDisabledState(disabled: boolean) {
        if (disabled) this.renderer.addClass(this.el.nativeElement, 'disabled')
        else this.renderer.removeClass(this.el.nativeElement, 'disabled')
    }
    /**
     * Constructor del componente
     * @param renderer angular renderer
     * @param ngControl form control
     */
    constructor(
        private modal: ModalService,
        private renderer: Renderer2,
        @Inject(DOCUMENT)
        private document: Document,
        @Self()
        @Optional()
        public ngControl: NgControl
    ) {
        if (this.ngControl) this.ngControl.valueAccessor = this
        this.window = this.document.defaultView as Window
    }
    /**
     * Click host listener for clickout
     */
    @HostListener('document:click')
    clickout() {
        if (this.show) {
            this.rendered = false
            this.show = false
        }
    }
    /**
     * Inicializador del componente
     */
    ngOnInit(): void {
        setTimeout(() => this.document.execCommand('defaultParagraphSeparator', false, 'p'), 100)
    }
    /**
     * Handler para mouse up
     * @param event mouse up event
     */
    mouseUp(event: MouseEvent): void {
        const selection = this.document.getSelection()
        const show: boolean = selection?.anchorOffset !== selection?.focusOffset
        if (show) {
            this.rendered = true
            const bounds = this.editor.nativeElement.getBoundingClientRect()
            this.toolbarX = (event.clientX - bounds.left) - 130
            this.toolbarY = (event.clientY - bounds.top) - 100
            this.show = show
        }
    }
    /**
     * Aplicar formato
     * @param command comando del format
     */
    format(command: string): void {
        this.document.execCommand(command)
    }
    /**
     * Convertir
     * @param tag HTML tag
     */
    convert(tag: string): void {
        this.document.execCommand('formatBlock', false, `<${tag}>`)
    }
    /**
     * Open file modal
     */
    openFileModal() {
        const window = this.document.defaultView as Window
        const selection = window.getSelection() as Selection
        this.fileSelectionRange = new Range()
        const start = selection.anchorOffset
        const end = selection.focusOffset
        // set range
        if (end > start) {
            this.fileSelectionRange.setStart(selection.anchorNode as Node, selection?.anchorOffset)
            this.fileSelectionRange.setEnd(selection.focusNode as Node, selection.focusOffset)
        } else {
            this.fileSelectionRange.setStart(selection.focusNode as Node, selection.focusOffset)
            this.fileSelectionRange.setEnd(selection.anchorNode as Node, selection?.anchorOffset)
        }
        // return
        this.modal.open(InsertFileModalComponent)
        .then((file) => {
            if (file) this.addFile(file.id)
        })
    }
    /**
     * Agregar ficha
     */
    addFile(id: string) {
        const window = this.document.defaultView as Window
        const selection = window.getSelection()
        selection?.removeAllRanges()
        selection?.addRange(this.fileSelectionRange)
        this.document.execCommand('createLink', false, id)
        const anchor = this.editor.nativeElement.querySelector(`a[href="${id}"]`) as HTMLAnchorElement
        anchor.href = ''
        anchor.dataset.file = id
        selection?.removeAllRanges()
        this.onChange(this.editor.nativeElement.innerHTML)
    }
    /**
     * Remove file reference
     * @param anchor html anchor
     */
    removeFile(anchor: HTMLAnchorElement) {

        this.files = this.files.filter(f => f !== anchor.href)
        this.fileChanges.emit(this.files)

        const window = this.document.defaultView as Window
        const selection = window.getSelection()
        selection?.removeAllRanges()
        const range = this.document.createRange()
        range.selectNodeContents(anchor)
        selection?.addRange(range)
        this.document.execCommand('unlink')
        this.onChange(this.editor.nativeElement.innerHTML)
    }
    /**
     * No me acuerdo para que es esto.
     * @param event mouse click
     */
    checkLength(event: MouseEvent): void {
        event.stopPropagation()
    }
    /**
     * Enter key press event
     * @param e keyboard event
     */
    enterCheck(e: Event): void {
        if (this.enterKeyEvent) {
            e.preventDefault()
            this.enterKey.emit()
        }
    }
    /**
     * Paste handler
     * @param event paste
     */
    handlePaste(event: ClipboardEvent): void {
        event.preventDefault()
        const data = event.clipboardData
        const text = (data as DataTransfer).getData('text')
        this.document.execCommand('insertText', false, text)
    }
    /**
     * Input handler
     * @param event input
     */
    handleInput(event: Event): void
    handleInput(event: InputEvent): void {
        event.stopPropagation()
        // Show placeholder ?
        if ((event.target as HTMLInputElement).innerText.trim() !== '' || event.data === ' ') this.placeholderHidden = true
        else this.placeholderHidden = false
        // Add first paragraph
        if ((event.target as HTMLInputElement).innerHTML.length === 1) {
            this.document.execCommand('formatBlock', false, '<p>')
        }

        this.onChange(this.editor.nativeElement.innerHTML)
    }

    async parse(event: KeyboardEvent) {

        if (event.key === 'Shift' || event.key === 'Enter') { return }

        let parsed = false
        const input = this.editor.nativeElement
        const pos = position(input)

        let newPos = pos.pos
        if (this.editor.nativeElement.innerHTML.match(/--/g)) {
            this.editor.nativeElement.innerHTML = this.editor.nativeElement.innerHTML.replace(/--/g, '—')
            newPos -= 1
            parsed = true
        }

        if (this.editor.nativeElement.innerHTML.match(/:link:/g)) {

            let file
            this.editor.nativeElement.blur()

            if (this.userFiles) {
                file = await this.modal.open(NewThreadFilesComponent)
            } else {
                file = await this.modal.open(InsertFileModalComponent)
            }

            if (file) {
                this.editor.nativeElement.innerHTML = this.editor.nativeElement.innerHTML
                .replace(/:link:/g, `<a data-file="${file.id}">${file.title}</a>&nbsp;`)
                this.files.push(file.id)
                this.fileChanges.emit(this.files)

            } else {
                this.editor.nativeElement.innerHTML = this.editor.nativeElement.innerHTML
                .replace(/:link:/g, '')
            }

            parsed = true
        }

        if (parsed) {
            position(input, newPos)
            this.onChange(this.editor.nativeElement.innerHTML)
        }
    }
    /**
     * Actualizar el valor del contenido
     * @param html valor
     */
    setValue(html: string) {
        this.editor.nativeElement.innerHTML = html
        // Show placeholder ?
        if ((this.editor.nativeElement.innerText && this.editor.nativeElement.innerText.trim() !== '') ||
            html === ' ') this.placeholderHidden = true
        else this.placeholderHidden = false
        // Add first paragraph
        if (this.editor.nativeElement.innerHTML.length === 1) {
            this.document.execCommand('formatBlock', false, '<p>')
        }
    }
}
