import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mr-loader',
  templateUrl: './loader.component.html',
  styles: []
})
export class LoaderComponent implements OnInit {

  @Input('inline') inline:boolean
  @Input('height') height:number = 500
  @Input('grey') grey:boolean

  constructor() { }

  ngOnInit(): void {
  }

}
