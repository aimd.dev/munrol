import {
    Component,
    Input,
    ViewEncapsulation
} from '@angular/core'

import {
    getDownloadURL,
    listAll,
    ref,
    Storage
} from '@angular/fire/storage'

import {
    BehaviorSubject
} from 'rxjs'

@Component({
    selector: 'mr-image',
    templateUrl: './image.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ImageComponent {
    /**
     * Image path
     */
    src$: BehaviorSubject<string> = new BehaviorSubject<string>('')
    /**
     * Get img url
     */
    @Input() set path(path: string) {
        if (path) {
            getDownloadURL(ref(this.storage, path))
            .then((res) => this.src$.next(res))
            .catch(err => console.log(err))
        }
    }
    /**
     * Get avatar url
     */
    @Input() set avatar(username: string) {
        if (username) {
            listAll(ref(this.storage, `users/${username}`))
            .then((list) => {
                getDownloadURL(list.items[0])
                .then((res) => this.src$.next(res))
                .catch(err => console.log(err))
            })
        }
    }
    /**
     * Constructor del componente
     * @param storage firebase storage
     */
    constructor(
        private storage: Storage
    ) { }
    /**
     * Reload image
     */
    reload() {
        const current: string = this.src$.getValue()
        this.src$.next(`${current}?t=${new Date().toISOString()}`)
    }
}
