import { Component, OnInit } from '@angular/core'

import { Observable } from 'rxjs'
import { map, tap } from 'rxjs/operators'

import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'

import { ModalService } from '../modal/modal.service'
import { selectThreadFiles } from '@app/modules/thread/thread.selectors'
import { File } from '@app/shared/models'

@Component({
    selector: 'mr-insert-file-modal',
    templateUrl: './insert-file-modal.component.html',
    styles: [
    ]
})
export class InsertFileModalComponent implements OnInit {
    /**
     * Selected files for linking
     */
    selection: string
    /**
     * Thread files
     */
    files$: Observable<File[]>
    /**
     * Constructor del componente
     * @param store ngrx store
     * @param modal modal service
     */
    constructor(
        private store: Store<any>,
        private modal: ModalService
    ) { }
    /**
     * Component initializer
     */
    ngOnInit(): void {
        this.files$ = this.store.select(selectThreadFiles)
        .pipe(
            tap(files => {
                const toDownload = files.filter(f => !f.body).map(f => f.id)
                if (toDownload.length) {
                    this.store.dispatch(ThreadActions.getFiles({ ids: toDownload.slice(0, 9) }))
                }
            })
        )
    }
    /**
     * Confirm files to link
     */
    link() {
        this.modal.close(this.selection)
    }
}
