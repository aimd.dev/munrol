import {
    Component,
    Output,
    EventEmitter,
    Input
} from '@angular/core'

import {
    loaderState
} from '@app/shared/models'

@Component({
    selector: 'mr-load-more-feed',
    templateUrl: './load-more-feed.component.html',
    styles: [
    ]
})
export class LoadMoreFeedComponent {
    empty = false
    /**
     * list is empty
     */
    @Input()
    set data(data: unknown[] | null) {
        this.empty = !!data && data.length === 0
    }
    /**
     * is loading
     */
    @Input()
    loading: loaderState | null
    /**
     * is end of list
     */
    @Input()
    end: boolean | null
    /**
     * Message for empty list
     */
    @Input()
    emptyMessage = 'No hay resultados'
    /**
     * Icon for empty list
     */
    @Input()
    emptyIcon = 'la-comment'
    /**
     * Message for end of list
     */
    @Input()
    endMessage = 'Has llegado al final del feed'
    /**
     * Text for button
     */
    @Input()
    actionText: string
    /**
     * Route for action button
     */
    @Input()
    actionRoute: string
    /**
     * Load more event emitter
     */
    @Output()
    loadMore: EventEmitter<void> = new EventEmitter<void>()
}
