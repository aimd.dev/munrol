import { Component, ElementRef, Input, OnInit, Optional, Renderer2, Self, ViewChild } from '@angular/core'
import { ControlValueAccessor, NgControl } from '@angular/forms'

@Component({
  selector: 'mr-multi-input',
  templateUrl: './multi-input.component.html'
})
export class MultiInputComponent implements ControlValueAccessor {

  @ViewChild('el') el: ElementRef

  @Input() placeholder = ''

  constructor(
    private renderer: Renderer2,
    @Self()
    @Optional()
    public ngControl: NgControl) {
      if (this.ngControl) { this.ngControl.valueAccessor = this }
  }

  onChange = (_: any) => {}
  onTouched = () => {}

  writeValue(value: string[]) {
    this.value = value
  }
  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn
  }
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn
  }
  setDisabledState(disabled: boolean) {
    if (disabled) this.renderer.addClass(this.el.nativeElement, 'disabled')
    else this.renderer.removeClass(this.el.nativeElement, 'disabled')
  }

  value: string[] = []
  current = ''

  handleKey(event: KeyboardEvent): void {
    if (event.key === 'Enter' || event.key === ',') {
      event.preventDefault()
      if (this.current.trim() === '' || this.value.includes(this.current)) return
      this.value = [...this.value, this.current]
      this.current = ''
      this.onChange(this.value)
    }
  }

  remove(current: string) {
    this.value = [...this.value.filter((item: string) => item !== current)]
    this.onChange(this.value)
  }

}
