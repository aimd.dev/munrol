import { Component, Input } from '@angular/core'
import { DeleteCommentModalComponent } from '@app/modules/thread/components/modals/delete-comment-modal/delete-comment-modal.component'
import { Comment } from '@app/modules/thread/models/Comment.model'
import { Store } from '@ngrx/store'
import * as ThreadActions from '@modules/thread/thread.actions'
import { Subject } from 'rxjs'
import { ModalService } from '../modal/modal.service'

@Component({
    selector: 'mr-comment',
    templateUrl: './comment.component.html',
    styles: [
    ]
})
export class CommentComponent {

    full$: Subject<boolean> = new Subject<boolean>()

    edit$: Subject<boolean> = new Subject<boolean>()

    @Input()
    comment: Comment

    @Input()
    isAuthor: boolean

    @Input()
    sidebar = true

    constructor(
        private modal: ModalService,
        private store: Store<any>
    ) { }

    deleteComment(id: string) {
        const props = [{key: 'id', value: id}]
        this.modal.open(DeleteCommentModalComponent, props)
        .then((res) => res && this.store.dispatch(ThreadActions.removeComment({ comment: this.comment })))
    }
}
