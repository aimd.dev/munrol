import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Optional,
    Renderer2,
    Self,
    ViewChild
} from '@angular/core'
import { getDownloadURL, ref, Storage } from '@angular/fire/storage'

import {
    ControlValueAccessor,
    NgControl
} from '@angular/forms'
import { Subject } from 'rxjs'

@Component({
    selector: 'mr-dropzone',
    templateUrl: './dropzone.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropzoneComponent implements ControlValueAccessor {

    file$: Subject<ArrayBuffer | string | null> = new Subject<ArrayBuffer | string | null>()

    @ViewChild('fileInput') fileInput: ElementRef

    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private storage: Storage,
        @Self()
        @Optional()
        public ngControl: NgControl
    ) { if (this.ngControl) this.ngControl.valueAccessor = this }

    onChange = (_: any) => {}
    onTouched = () => {}

    writeValue(value: any) {
        if (!value) return
        if (typeof value === 'string') this.downloadImage(value)
    }
    // eslint-disable-next-line @typescript-eslint/ban-types
    registerOnChange(fn: (_: any) => {}): void {
      this.onChange = fn
    }
    registerOnTouched(fn: () => void): void {
      this.onTouched = fn
    }
    setDisabledState(disabled: boolean) {
      if (disabled) this.renderer.addClass(this.el.nativeElement, 'disabled')
      else this.renderer.removeClass(this.el.nativeElement, 'disabled')
    }

    allowDrop(e: DragEvent) {
        e.preventDefault()
    }

    openFileInput() {
        this.onTouched()
        this.fileInput.nativeElement.click()
    }

    inputChange(event: Event) {
        const files = (event.target as HTMLInputElement).files
        if (files) {
            this.loadImage(files[0])
            this.onChange(files[0])
        }
    }

    drop(event: DragEvent) {
        event.preventDefault()
        const files = (event.dataTransfer as DataTransfer).files
        this.loadImage(files[0])
        this.onChange(files[0])
    }

    reset() {
        this.file$.next(null)
    }

    loadImage(file: File) {
        const reader = new FileReader()
        reader.onload = (e: ProgressEvent<FileReader>) => this.file$.next((e.target as FileReader).result)
        reader.readAsDataURL(file)
    }

    downloadImage(path: string) {
        getDownloadURL(ref(this.storage, path))
            .then((res) => this.file$.next(res))
            .catch(err => console.log(err))
    }
}
