/**
 * Retorna la extension de un mime type
 * @param type mime type
 * @returns la extension
 */
export const extensionFromType = (type: string): string => {
    let extension = ''
    switch (type) {
        case 'image/jpeg':
            extension = 'jpg'
            break
        case 'image/png':
            extension = 'png'
            break
        case 'image/gif':
            extension = 'gif'
            break
    }
    return extension
}
/**
 * Retorna el mime type de una extension
 * @param extension la extension
 * @returns el mime type
 */
export const typeFromExtension = (extension: string): string => {
    let type = ''
    switch (extension) {
        case 'jpg':
            type = 'image/jpeg'
            break
        case 'png':
            type = 'image/png'
            break
        case 'gif':
            type = 'image/gif'
            break
    }
    return type
}
