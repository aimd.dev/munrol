/**
 * Retorna los nombres de los errores en un objeto de errores
 * @param errors errores del form control
 * @returns nombres de los errores en un objeto de errores
 */
export const validatorFlattener = (errors: {[key: string]: any} | null): string => {
    return JSON.stringify(errors ? Object.keys(errors).map((k) => k) : errors)
}
