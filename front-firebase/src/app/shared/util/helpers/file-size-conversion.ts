/**
 * Convierte bytes a MB
 * @param bytes total de bytes
 * @returns bytes en MB
 */
export const bytesToMB = (bytes: number): number => {
    return bytes / (1024*1024)
}
/**
 * Convierte MBs a bytes
 * @param mb total de MBs
 * @returns MBs en bytes
 */
export const MBToBytes = (mb: number): number => {
    return mb * (1024*1024)
}
