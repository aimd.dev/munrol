import { constants } from '@app/core/config/constants'

/**
 * Retorna las paginas que deberian estar visibles en la paginacion
 * @param active pagina activa
 * @param pages paginas
 * @returns paginas visibles en la paginacion
 */
export const calculateVisiblePages = (active: number, pages: number[]): number[] => {
    if (pages.length <= constants.visiblePages) return pages
    if (active < 4) return pages.slice(0, constants.visiblePages)
    if (active > (pages.length - 4)) return pages.slice(pages.length - 5, pages.length)
    return pages.slice(active - 3, active + 2)
}
