/**
 * Retorna un mensaje de error
 * @param key nombre del error
 * @param error el objeto del error
 * @returns el mensaje adecuado para el error
 */
export const getControlErrorMessage = (key: string, error: {[key: string]: any}) => {
    let message = ''
    console.log(error)
    switch (key) {
        case 'required':
            message += 'Este campo es requerido'
            break
        case 'minlength':
            message += `El campo debe tener al menos ${error.requiredLength} caracteres`
            break
        case 'maxlength':
            message += `El campo no debe tener más de ${error.requiredLength} caracteres`
            break
        case 'pattern':
            message += 'Caracter no permitido'
            break
        case 'maxSize':
            message += `El peso no debe superar los ${error} MB`
            break
        case 'types':
            message += 'El formato es incorrecto'
            break
        case 'email':
            message += 'Correo electrónico inválido'
            break
        default:
            message += 'Valor inválido'
    }
    return message
}
