import { AbstractControl } from '@angular/forms'

/**
 * Valida el formato de un archivo
 * @param types formatos permitidos
 * @returns si el archivo tiene el formato permitido
 */
export const fileType = (types: string[]) => {
    return (control: AbstractControl) => {
        if (!types.includes(control.value.type)) return ({ types: types })
        return null
    }
}
