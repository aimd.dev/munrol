import { DOCUMENT } from '@angular/common'
import { Inject, Injectable } from '@angular/core'
import { AbstractControl, ValidatorFn } from '@angular/forms'

@Injectable({
    providedIn: 'root'
})
export class EditorBodyRequiredValidatorService {

    constructor(
        @Inject(DOCUMENT) private document: Document
    ) { }

    editorBodyRequired(): ValidatorFn {
        return (control: AbstractControl): { required: boolean } | null => {
            const aux = this.document.createElement('div')
            aux.innerHTML = control.value
            const value = aux.innerText.trim()
            aux.remove()
            if (value === '') { return { required: true } }
            else { return null }
        }
    }
}
