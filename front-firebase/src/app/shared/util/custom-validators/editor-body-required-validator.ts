import { AbstractControl } from '@angular/forms'

/**
 * Valida si el cuerpo del editor esta vacio
 * @param control form control
 * @returns error de required
 */
export const editorBodyRequired = (control: AbstractControl): { required: boolean } | null => {
    const aux = document.createElement('div')
    aux.innerHTML = control.value
    const value = aux.innerText.trim()
    aux.remove()
    if (value === '') { return { required: true } }
    else { return null }
}
