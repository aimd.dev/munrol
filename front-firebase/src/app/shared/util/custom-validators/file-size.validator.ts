import { AbstractControl } from '@angular/forms'
import { MBToBytes } from '../helpers/file-size-conversion'

/**
 * Valida el tamaño de un archivo
 * @param max max size
 * @returns si el archivo es muy pesado
 */
export const fileMaxSize = (max: number) => {
    return (control: AbstractControl) => {
        if (control.value.size > MBToBytes(max)) return ({ maxSize: max })
        return null
    }
}
