import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// Componentes
import { AlertsComponent } from './components/alerts/alerts.component'
import { BlinkingDotComponent } from './components/blinking-dot/blinking-dot.component'
import { ButtonComponent } from './components/button/button.component'
import { EditorComponent } from './components/editor/editor.component'
import { EditorReadonlyComponent } from './components/editor-readonly/editor-readonly.component'
import { EmptyComponent } from './components/empty/empty.component'
import { FilePointerComponent } from './components/file-pointer/file-pointer.component'
import { GhostBoxComponent } from './components/ghost-box/ghost-box.component'
import { ImageComponent } from './components/image/image.component'
import { InputComponent } from './components/input/input.component'
import { LoaderComponent } from './components/loader/loader.component'
import { LoadMoreFeedComponent } from './components/load-more-feed/load-more-feed.component'
import { LogoComponent } from './components/logo/logo.component'
import { ModalComponent } from './components/modal/modal.component'
import { MultiInputComponent } from './components/multi-input/multi-input.component'
import { PaginationComponent } from './components/pagination/pagination.component'
import { ParticipantListComponent } from './components/participant-list/participant-list.component'
import { ReplyBriefComponent } from './components/reply-brief/reply-brief.component'
import { SwitchComponent } from './components/switch/switch.component'
import { TabContentComponent } from './components/tabs/tab-content/tab-content.component'
import { TabsComponent } from './components/tabs/tabs.component'
import { TagListComponent } from './components/tag-list/tag-list.component'
import { ThreadBriefComponent } from './components/thread-brief/thread-brief.component'
import { UserListComponent } from './components/user-list/user-list.component'
import { DropzoneComponent } from './components/dropzone/dropzone.component'
import { DropdownComponent } from './components/dropdown/dropdown.component'
import { CustomSelectSingleComponent } from './components/custom-select/custom-select-single.component'
import { CustomSelectMultiComponent } from './components/custom-select/custom-select-multi.component'
import { CheckboxComponent } from './components/checkbox/checkbox.component'
import { InsertFileModalComponent } from './components/insert-file-modal/insert-file-modal.component'
import { SidebarComponent } from './components/sidebar/sidebar.component'
import { FileExpanderComponent } from './components/file-expander/file-expander.component'
import { ExpandedFileComponent } from './components/expanded-file/expanded-file.component'
import { CommentComponent } from './components/comment/comment.component'
import { ThreadCommentEditComponent } from './components/thread-comment-edit/thread-comment-edit.component'
import { RoomListComponent } from './components/room-list/room-list.component'
import { ChatroomComponent } from './components/chatroom/chatroom.component'
import { InboxEditorComponent } from './components/inbox-editor/inbox-editor.component'
import { ChatlogComponent } from './components/chatlog/chatlog.component'
import { AddRoomModalComponent } from './components/add-room-modal/add-room-modal.component'
import { TimestampComponent } from './components/timestamp/timestamp.component'
// Pipes
import { ArrayFilterPipe } from './pipes/array-filter.pipe'
import { AvatarUrlPipe } from './pipes/avatar-url.pipe'
import { FileBriefPipe } from './pipes/file-brief.pipe'
import { FilterPipe } from './pipes/filter.pipe'
import { HtmlSlicePipe } from './pipes/html-slice.pipe'
import { PluralPipe } from './pipes/plural.pipe'
import { ProfileUrlPipe } from './pipes/profile-url.pipe'
import { ThreadUrlPipe } from './pipes/thread-url.pipe'
import { PropFromArrayPipe } from './pipes/prop-from-array.pipe'
import { TimeAgoPipe } from './pipes/time-ago.pipe'
// Directives
import { AutoFocusDirective } from './directives/auto-focus.directive'
import { FormControlDirective } from './directives/form-control.directive'
import { FormControlTypeLimitDirective } from './directives/form-control-type-limit.directive'
import { FormControlValidationDirective } from './directives/form-control-validation.directive'
import { EditorInlineFileFinderDirective } from './directives/editor-inline-file-finder.directive'
import { SidebarHostDirective } from './components/sidebar/sidebar-host.directive'
import { ModalHostDirective } from './components/modal/modal-host.directive'
import { KeyupDebouncerDirective } from './directives/keyup-debouncer.directive'
import { TooltipDirective } from './directives/tooltip.directive'


@NgModule({
    declarations: [
        // Components
        AlertsComponent,
        BlinkingDotComponent,
        ButtonComponent,
        EditorComponent,
        EditorReadonlyComponent,
        EmptyComponent,
        FilePointerComponent,
        GhostBoxComponent,
        ImageComponent,
        InputComponent,
        LoaderComponent,
        LoadMoreFeedComponent,
        LogoComponent,
        ModalComponent,
        MultiInputComponent,
        PaginationComponent,
        ParticipantListComponent,
        ReplyBriefComponent,
        SwitchComponent,
        TabContentComponent,
        TabsComponent,
        TagListComponent,
        ThreadBriefComponent,
        UserListComponent,
        DropzoneComponent,
        DropdownComponent,
        CustomSelectSingleComponent,
        CustomSelectMultiComponent,
        CheckboxComponent,
        InsertFileModalComponent,
        SidebarComponent,
        FileExpanderComponent,
        ExpandedFileComponent,
        CommentComponent,
        ThreadCommentEditComponent,
        RoomListComponent,
        ChatroomComponent,
        InboxEditorComponent,
        TimestampComponent,
        // Pipe
        ArrayFilterPipe,
        AvatarUrlPipe,
        FileBriefPipe,
        FilterPipe,
        HtmlSlicePipe,
        PluralPipe,
        ProfileUrlPipe,
        ThreadUrlPipe,
        PropFromArrayPipe,
        TimeAgoPipe,
        // Directives
        AutoFocusDirective,
        FormControlDirective,
        FormControlTypeLimitDirective,
        FormControlValidationDirective,
        EditorInlineFileFinderDirective,
        SidebarHostDirective,
        ModalHostDirective,
        InboxEditorComponent,
        KeyupDebouncerDirective,
        ChatlogComponent,
        AddRoomModalComponent,
        TooltipDirective
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule
    ],
    exports: [
        // Modules
        FormsModule,
        ReactiveFormsModule,
        // Components
        AlertsComponent,
        BlinkingDotComponent,
        ButtonComponent,
        EditorComponent,
        EditorReadonlyComponent,
        EmptyComponent,
        FilePointerComponent,
        GhostBoxComponent,
        ImageComponent,
        InputComponent,
        LoaderComponent,
        LoadMoreFeedComponent,
        LogoComponent,
        ModalComponent,
        MultiInputComponent,
        PaginationComponent,
        ParticipantListComponent,
        ReplyBriefComponent,
        SwitchComponent,
        TabContentComponent,
        TabsComponent,
        TagListComponent,
        ThreadBriefComponent,
        UserListComponent,
        DropzoneComponent,
        DropdownComponent,
        CustomSelectSingleComponent,
        CustomSelectMultiComponent,
        CheckboxComponent,
        SidebarComponent,
        FileExpanderComponent,
        ExpandedFileComponent,
        CommentComponent,
        ThreadCommentEditComponent,
        RoomListComponent,
        ChatroomComponent,
        InboxEditorComponent,
        TimestampComponent,
        // Pipes
        ArrayFilterPipe,
        AvatarUrlPipe,
        FileBriefPipe,
        FilterPipe,
        HtmlSlicePipe,
        PluralPipe,
        ProfileUrlPipe,
        ThreadUrlPipe,
        PropFromArrayPipe,
        TimeAgoPipe,
        // Directives
        AutoFocusDirective,
        FormControlDirective,
        FormControlTypeLimitDirective,
        FormControlValidationDirective,
        SidebarHostDirective,
        ModalHostDirective,
        KeyupDebouncerDirective,
        TooltipDirective
    ],
    providers: [
        HtmlSlicePipe
    ]
})
export class SharedModule { }
