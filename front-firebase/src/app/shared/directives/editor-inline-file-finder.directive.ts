import {
    Directive,
    ElementRef,
    OnInit,
    Output,
    EventEmitter,
    Renderer2
} from '@angular/core'

import {
    AbstractControl,
    NgControl
} from '@angular/forms'

@Directive({
    selector: '[mrEditorInlineFileFinder]'
})
export class EditorInlineFileFinderDirective implements OnInit {

    @Output()
    unlink: EventEmitter<HTMLAnchorElement> = new EventEmitter<HTMLAnchorElement>()

    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private ngControl: NgControl
    ) { }

    ngOnInit(): void {
        (this.ngControl.control as AbstractControl).valueChanges
        .subscribe(() => {
            const files = this.el.nativeElement.querySelectorAll('a')
            files.forEach((f: HTMLAnchorElement) => {
                this.renderer.listen(f, 'click', (e: MouseEvent) => {
                    e.preventDefault()
                    this.unlink.emit(f)
                })
            })
        })
    }

}
