import {
    Directive,
    OnInit,
    ElementRef
} from '@angular/core'

@Directive({
    selector: '[mrAutoFocus]'
})
export class AutoFocusDirective implements OnInit {
    /**
     * Host
     */
    private inputElement: HTMLElement
    /**
     * Directive constructor
     * @param {ElementRef} elementRef ref of the host element
     */
    constructor(private elementRef: ElementRef) {
        this.inputElement = this.elementRef.nativeElement
    }
    /**
     * Directive initializer
     */
    ngOnInit(): void {
        this.inputElement.focus()
    }
}
