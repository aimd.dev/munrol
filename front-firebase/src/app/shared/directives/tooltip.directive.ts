import { DOCUMENT } from '@angular/common'
import {
    Directive,
    ElementRef,
    HostListener,
    Inject,
    Input,
    Renderer2
} from '@angular/core'

@Directive({
    selector: '[mrTooltip]'
})
export class TooltipDirective {
    /**
     * Tooltip html element
     */
    private tooltip: HTMLElement
    /**
     * Tooltip text
     */
    @Input() content: string | null
    /**
     * Y pos
     */
    @Input() set bottom(bottom: boolean) {
        if (bottom) this.renderer.addClass(this.host.nativeElement, 'bottom')
    }
    /**
     * Mouseenter event
     */
    @HostListener('mouseenter', ['$event']) mouseenter() {
        this.tooltip = this.document.createElement('span')
        this.tooltip.classList.add('tooltip')
        this.tooltip.innerHTML = this.content ? this.content : ''
        this.renderer.appendChild(this.host.nativeElement, this.tooltip)
    }
    /**
     * Mouseleave event
     */
    @HostListener('mouseleave', ['$event']) mouseleave() {
        this.renderer.removeChild(this.host.nativeElement, this.tooltip)
    }
    /**
     * Constructor de la directiva
     * @param host element ref
     */
    constructor(
        @Inject(DOCUMENT)
        private document: Document,
        private host: ElementRef,
        private renderer: Renderer2
    ) { }
}
