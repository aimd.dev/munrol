import { Directive, EventEmitter, HostListener, OnDestroy, OnInit, Output } from '@angular/core'
import { Subject } from 'rxjs'
import { debounceTime, filter, takeUntil, tap } from 'rxjs/operators'

@Directive({
    selector: '[mrKeyupDebouncer]'
})
export class KeyupDebouncerDirective implements OnInit, OnDestroy {

    ended = true

    keyup$: Subject<void> = new Subject<void>()

    unsubscriber$: Subject<void> = new Subject<void>()

    @Output()
    typingStart: EventEmitter<void> = new EventEmitter<void>()

    @Output()
    typingEnd: EventEmitter<void> = new EventEmitter<void>()

    @HostListener('keyup') checkTyping() {
        this.keyup$.next()
    }

    constructor() { }

    ngOnInit(): void {

        this.keyup$
        .pipe(
            takeUntil(this.unsubscriber$),
            filter(() => this.ended),
            tap(() => {
                this.ended = false
                this.typingStart.emit()
            })
        ).subscribe()

        this.keyup$
        .pipe(
            takeUntil(this.unsubscriber$),
            debounceTime(2000),
            tap(() => {
                this.ended = true
                this.typingEnd.emit()
            })
        ).subscribe()
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
