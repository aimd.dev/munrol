import {
    Directive,
    Input,
    OnInit,
    ElementRef,
    Renderer2,
    OnDestroy
} from '@angular/core'

import {
    AbstractControl,
    NgControl,
    Validators
} from '@angular/forms'

import { Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

import { constants } from '@app/core/config/constants'

@Directive({
    selector: '[mrFormControlTypeLimit]'
})
export class FormControlTypeLimitDirective implements OnInit, OnDestroy {
    /**
     * Subject of component destroyed
     */
    unsubscriber$: Subject<void> = new Subject()
    /**
     * Maxlength
     */
    @Input() maxLength: number = constants.summaryMaxLength
    /**
     * Directive constructor
     * @param {ElementRef} el ref of the host
     * @param {Renderer2} renderer angular renderer2
     * @param {NgControl} control form control
     */
    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private ngControl: NgControl
    ) { }
    /**
     * Directive initializer
     */
    ngOnInit(): void {
        this.insertCounterNode()
        this.configureValidation();
        (this.ngControl.control as AbstractControl).valueChanges
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe(data => this.updateCounter(data.length))
    }
    /**
     * Add counter HTML
     */
    insertCounterNode() {
        const legend = this.renderer.createElement('span')
        legend.classList.add('type-counter')
        this.renderer.appendChild(this.renderer.parentNode(this.el.nativeElement).querySelector('.control-validation'), legend)
    }
    /**
     * Add maxlength validation
     */
    configureValidation(): void {
        (this.ngControl.control as AbstractControl).addValidators(Validators.maxLength(this.maxLength));
        (this.ngControl.control as AbstractControl).updateValueAndValidity()
        this.updateCounter((this.ngControl.control as AbstractControl).value.length)
    }
    /**
     * Update counter HTML
     * @param {number} value input value
     */
    updateCounter(value: number): void {
        this.renderer.setProperty(
            this.renderer.parentNode(this.el.nativeElement).querySelector('.type-counter'), 'innerHTML', `${value}/${this.maxLength}`
        )
    }
    /**
     * Directive destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
