import {
    ComponentRef,
    Directive,
    OnInit,
    TemplateRef,
    ViewContainerRef
} from '@angular/core'

import { InputComponent } from '../components/input/input.component'

@Directive({
    selector: '[mrFormControl]'
})
export class FormControlDirective implements OnInit {
    /**
     * Ref of the host
     */
    compRef: ComponentRef<InputComponent>
    /**
     * Directive constructor
     * @param {TemplateRef<any>} templateRef template ref
     * @param {ViewContainerRef} view view ref
     */
    constructor(
        private templateRef: TemplateRef<any>,
        private view: ViewContainerRef
    ) { }
    /**
     * Directive initializer
     */
    ngOnInit(): void {
        this.compRef = this.view.createComponent(InputComponent)
        this.compRef.instance.template = this.templateRef
    }
}
