import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core'
import { AbstractControl, NgControl } from '@angular/forms'

@Directive({
    selector: '[mrEditorInlineMentionFinder]'
})
export class EditorInlineMentionFinderDirective implements OnInit {

    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private ngControl: NgControl
    ) {}

    ngOnInit(): void {
        (this.ngControl.control as AbstractControl).valueChanges
        .subscribe(() => {
        })
    }
}
