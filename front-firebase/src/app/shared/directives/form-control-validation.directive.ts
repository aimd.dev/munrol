import {
    Directive,
    ElementRef,
    OnDestroy,
    OnInit,
    Renderer2
} from '@angular/core'

import {
    AbstractControl,
    NgControl,
    ValidationErrors
} from '@angular/forms'

import {
    Observable,
    Subject
} from 'rxjs'

import {
    distinctUntilChanged,
    map,
    takeUntil
} from 'rxjs/operators'

import {
    getControlErrorMessage
} from '../util/helpers/get-control-error-message'

import {
    validatorFlattener
} from '../util/helpers/validator-flattener'

@Directive({
    selector: '[mrFormControlValidation]'
})
export class FormControlValidationDirective implements OnInit, OnDestroy {
    /**
     * Errors observable
     */
    errors$: Observable<{ [key: string]: any } | null>
    /**
     * Subject for directive destruction
     */
    unsubscriber$: Subject<void> = new Subject()
    /**
     * Directive constructor
     * @param {ElementRef} el host element ref
     * @param {Renderer2} renderer angular renderer
     * @param {FormControl} control form control
     */
    constructor(
        private el: ElementRef,
        private renderer: Renderer2,
        private ngControl: NgControl
    ) { }
    /**
     * Directive initializer
     */
    ngOnInit(): void {
        this.insertNode();
        (this.ngControl.control as AbstractControl).valueChanges
        .pipe(
            takeUntil(this.unsubscriber$),
            map(() => (this.ngControl.control as AbstractControl).errors),
            distinctUntilChanged((prev, curr) => validatorFlattener(prev) === validatorFlattener(curr))
        ).subscribe(() => {
            this.updateCSS()
            this.updateHTML()
        })
    }
    /**
     * Insert validation HTML
     */
    insertNode(): void {
        const legend = this.renderer.createElement('div')
        legend.classList.add('control-errors')
        this.renderer.appendChild(this.renderer.parentNode(this.el.nativeElement).querySelector('.control-validation'), legend)
    }
    /**
     * Handle validation classes
     */
    updateCSS(): void {
        if (this.hasErrors()) this.renderer.addClass(this.renderer.parentNode(this.el.nativeElement), 'error')
        else this.renderer.removeClass(this.renderer.parentNode(this.el.nativeElement), 'error')
    }
    /**
     * Handle errors HTML
     */
    updateHTML(): void {
        this.renderer.setProperty(this.renderer.parentNode(this.el.nativeElement).querySelector('.control-errors'), 'innerHTML', '')
        if (this.hasErrors()) {
            Object.keys((this.ngControl.control as AbstractControl).errors as ValidationErrors).forEach(e => {
                const p = this.renderer.createElement('p')
                this.renderer.setProperty(p, 'innerText', getControlErrorMessage(e, ((this.ngControl.control as AbstractControl).errors as ValidationErrors)[e]))
                this.renderer.appendChild(this.renderer.parentNode(this.el.nativeElement).querySelector('.control-errors'), p)
            })
        }
    }
    /**
     * Error helper
     * @returns if there are errors or not
     */
    hasErrors(): boolean {
        return (!!(this.ngControl.control as AbstractControl).touched || !!(this.ngControl.control as AbstractControl).dirty) && !!(this.ngControl.control as AbstractControl).errors
    }
    /**
     * Directive destroyer
     */
    ngOnDestroy(): void {
        this.unsubscriber$.next()
        this.unsubscriber$.complete()
    }
}
