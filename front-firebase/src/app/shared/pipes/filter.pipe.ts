import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    /**
     * Filters a string array
     * @param arr provided array
     * @param query query to filter
     * @returns filtered array
     */
    transform(arr: string[], query: string): string[] {
        if (query.trim() === '') return arr
        return arr.filter((item: string) => item.toLowerCase().includes(query.toLowerCase()));
    }
}
