import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'avatarUrl'
})
export class AvatarUrlPipe implements PipeTransform {
    /**
     * Returns the url for the user avatar
     * @param filename name of the file
     * @returns the avatar url
     */
    transform(username: string): string {
        return `users/${username}/${username}.png`
    }
}
