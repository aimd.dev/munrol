import { DOCUMENT } from '@angular/common'
import { Inject, Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'htmlSlice'
})
export class HtmlSlicePipe implements PipeTransform {
    /**
     * Constructor del pipe
     * @param document injected document
     */
    constructor(
        @Inject(DOCUMENT) private document: Document
    ) { }
    /**
     * Returs a sliced text from an HTML string
     * @param {string} body content to slice
     * @param {number} length length of the slice
     * @returns sliced text
     */
    transform(body: string, length: number): string {
        const aux = this.document.createElement('div')
        aux.innerHTML = body
        const content = aux.innerText.slice(0, length)
        aux.remove()
        return `${content}...`
    }
}
