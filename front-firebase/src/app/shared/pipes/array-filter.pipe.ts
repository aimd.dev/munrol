import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'arrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {
    /**
     * Filters an array of objects by prop
     * @param arr provided array
     * @param query query to filter
     * @param prop property to filter
     * @returns filtered array
     */
    transform(arr: any[], query: string, prop: string): any[] {
        if (query.trim() === '') return arr
        return arr.filter((item: any) => item[prop].toLowerCase().includes(query.toLowerCase()))
    }
}
