import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'fileBrief'
})
export class FileBriefPipe implements PipeTransform {

    transform(body: string): unknown {
        const text: string[] = body.split(/\${[^}]*::[a-zA-Z0-9]*}/)
        const references: any = body.match(/[^${}}]*::[a-zA-Z0-9]*/g)
        return body
    }
}
