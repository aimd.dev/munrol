import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {
    /**
     * Construye el time ago string
     * @param total cantidad de la unidad de tiempo
     * @param singular string para la unidad de tiempo en singular
     * @param plural string para la unidad de tiempo en plural
     * @returns time ago string
     */
    getTimeAgoString(total: number, singular: string, plural: string): string {
        return total > 1
            ? `hace ${total} ${plural}`
            : `hace ${total} ${singular}`;
    }
    /**
     * Calcula la unidad de tiempo y devuelve el string
     * con la cantidad de tiempo transcurrido
     * @param diff diferencia en segundos entre dos dates
     * @returns time ago string
     */
    getTimeAgo(diff: number): string {
        // < 60 segundos
        if (60 > diff) {
            return 'justo ahora';
        }
        // < 60 minutos
        if (3600 > diff) {
            const result = Math.round(diff / 60);
            return this.getTimeAgoString(result, 'minuto', 'minutos');
        }
        // < 24 horas
        if (86400 > diff) {
            const result = Math.round(diff / 3600);
            return this.getTimeAgoString(result, 'hora', 'horas');
        }
        // < 30 dias
        const days = Math.round(diff / 86400);
        if (30 > days) {
            return this.getTimeAgoString(days, 'día', 'días');
        }
        // < 345 dias
        if (365 > days) {
            const result = Math.round(days / 30);
            return this.getTimeAgoString(result, 'mes', 'meses');
        }
        // > 365 dias
        const result = Math.round(days / 365);
        return this.getTimeAgoString(result, 'año', 'años');
    }
    /**
     * Calcula el tiempo transcurrido entre la
     * fecha especificada y la fecha de hoy
     * @param date el iso string de la fecha
     * @returns time ago string
     */
    transform(date: string | number): string {
        const then = new Date(date).getTime();
        const now = new Date().getTime();
        const diff = (now - then) / 1000;
        return this.getTimeAgo(diff);
    }
}
