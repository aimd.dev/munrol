import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'threadUrl'
})
export class ThreadUrlPipe implements PipeTransform {
    /**
     * Returns the url for the thread
     * @param id thread id
     * @returns the thread url
     */
    transform(id: string): string {
        return `/threads/${id}`
    }
}
