import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'propFromArray'
})
export class PropFromArrayPipe implements PipeTransform {

    transform(id: string, arr: any[], prop: string): unknown {
        return arr.find(el => el.id === id)[prop]
    }
}
