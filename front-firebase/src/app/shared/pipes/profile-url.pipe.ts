import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'profileUrl'
})
export class ProfileUrlPipe implements PipeTransform {
    /**
     * Returns the url for the user profile
     * @param username username
     * @returns the profile url
     */
    transform(username: string): string {
        return `/users/${username}`
    }
}
