import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'plural'
})
export class PluralPipe implements PipeTransform {
    /**
     * Returns the plural version of the string if necessary
     * @param {number} num total
     * @param {string} singular word in singular
     * @returns plural version of the string if necessary
     */
    transform(num: number, singular: string): unknown {
        return num !== 1 ? `${num} ${singular}s` : `${num} ${singular}`
    }
}
