export interface HTMLMapElement {
    tagName: string
    text?: string
    children?: HTMLMapElement[]
    file?: string
}
