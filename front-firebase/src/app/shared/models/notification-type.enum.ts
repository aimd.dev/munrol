export enum NotificationType {
    INVITES = 'invites',
    REVIEWS = 'reviews',
    REQUESTS = 'requests'
}
