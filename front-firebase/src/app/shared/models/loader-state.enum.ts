export enum LoaderStateEnum {
    ON = 'ON',
    OFF = 'OFF'
}

export type loaderState = keyof typeof LoaderStateEnum