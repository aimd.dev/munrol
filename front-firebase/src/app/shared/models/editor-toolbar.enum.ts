export enum EditorToolbar {
    FULL = 'FULL',
    BASIC = 'BASIC',
    MIN = 'MIN'
}
