export interface PushNotifications {
    newInvites: boolean
    newRequests: boolean
    newMessages: boolean
}
