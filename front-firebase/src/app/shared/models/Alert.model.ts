import { AlertType } from './alert-type.enum'

export interface Alert {
    id: number
    type: AlertType
    message: string
}
