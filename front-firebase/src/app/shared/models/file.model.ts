export interface File {
    id: string
    author: string
    body: string
    createdAt: string
    lastEdit: string
    picture: string
    linkedThreads: string[]
    linkedFiles: string[]
    title: string

    inFile?: boolean
    isAuthor?: boolean
}
