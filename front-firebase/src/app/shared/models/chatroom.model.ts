export interface Chatroom {
    id: string,
    lastMessage: string
    lastUpdated: string
    newMessages: boolean
    nowTyping: boolean
}
