export interface Message {
    id: string
    author: string
    body: string
    createdAt: string
}
