export enum AllowedTagNames {
    H2 ='H2',
    H3 = 'H3',
    BR = 'BR',
    P ='P',
    UL = 'UL',
    OL = 'OL',
    LI = 'LI',
    DIV = 'DIV',
    SPAN = 'SPAN',
    B = 'B',
    U = 'U',
    I = 'I',
    STRIKE = 'STRIKE',
    A = 'A'
}

export type allowedTagName = keyof typeof AllowedTagNames

export const allowedTagNames: AllowedTagNames[] = Object.values(AllowedTagNames)
