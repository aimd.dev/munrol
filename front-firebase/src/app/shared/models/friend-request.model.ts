export interface FriendRequest {
    source: string
    target: string
}
