export enum AlertType {
    SUCCESS = 'success',
    INFO = 'info',
    ERROR = 'error'
}
