// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api: 'https://us-central1-munrol-dev.cloudfunctions.net',
  bucket: 'gs://munrol-dev.appspot.com/',
  firebase: {
    apiKey: 'AIzaSyAyZ9Kp4DH-ZbdrXWVPqFmMNJU55cpcmE4',
    authDomain: 'munrol-dev.firebaseapp.com',
    projectId: 'munrol-dev',
    storageBucket: 'munrol-dev.appspot.com',
    messagingSenderId: '960892715534',
    appId: '1:960892715534:web:a8ff637f0dbc0bdc52725f',
    measurementId: 'G-8NJ4JY8G7L'
  },
  captcha: {
    key: '6LdosWIdAAAAAITffz2d1Tq8QGAb7jsjdrrcwctz',
    secret: '6LdosWIdAAAAALG_EvPpgZ-mss2l2LK8tF-BrX0J'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
