"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAccount = exports.setDefaultAvatar = exports.signUp = void 0;
const functions = require("firebase-functions");
const auth_1 = require("./shared/auth");
const db_1 = require("./shared/db");
const storage_1 = require("./shared/storage");
exports.signUp = functions.https.onCall(async (data, context) => {
    const user = Object.assign({}, data.user);
    const document = await db_1.db.doc(`users/${user.username}`).get();
    if (document.exists) {
        functions.logger.error('Firestore Error', `Username is taken: ${user.username}`);
        throw new functions.https.HttpsError('already-exists', 'Username is taken');
    }
    return auth_1.auth.createUser({
        displayName: user.username,
        email: user.email,
        password: user.password,
        uid: user.username
    })
        .then((user) => {
        // auth.ve
        return { message: 'User Created' };
    })
        .catch((err) => {
        functions.logger.error('Error in Signup', err);
        throw new functions.https.HttpsError(err.code, err.errorInfo.message);
    });
});
exports.setDefaultAvatar = functions.auth.user().onCreate((user) => {
    storage_1.storage.bucket(process.env.BUCKET)
        .file('default/avatar.png')
        .copy(storage_1.storage.bucket(process.env.BUCKET).file(`users/${user.uid}/${user.uid}.png`));
});
exports.deleteAccount = functions.auth.user().onDelete((user) => {
    db_1.db.collection('users').doc(user.uid).collection('private').doc('config').delete();
    db_1.db.collection('notifications').doc(user.uid).delete();
    db_1.db.collection('userSaved').doc(user.uid).delete();
    db_1.db.collection('userFollowing').doc(user.uid).delete();
    db_1.db.collection('chats').doc(user.uid).delete();
    db_1.db.collection('users').doc(user.uid).delete();
});
//# sourceMappingURL=auth.js.map