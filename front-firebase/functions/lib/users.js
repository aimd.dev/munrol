"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = void 0;
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db_1 = require("./shared/db");
async function unlink(collection, username) {
    const queryRef = admin.firestore().collection(collection).where('author', '==', username);
    const query = queryRef.orderBy('__name__').limit(10);
    return new Promise((resolve, reject) => {
        unlinkBatch(query, resolve).catch(reject);
    });
}
async function unlinkBatch(query, resolve) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db_1.db.batch();
    snapshot.docs.forEach((doc) => {
        batch.update(doc.ref, { thread: 'deleted' });
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        unlinkBatch(query, resolve);
    });
}
exports.deleteUser = functions.firestore
    .document('users/{user}')
    .onDelete(async (snap, context) => {
    const doc = snap.data();
    const friends = Object.assign({}, doc.friends);
    const names = Object.keys(friends);
    while (names.length) {
        const batch = db_1.db.batch();
        const piece = names.splice(0, 20);
        piece.forEach((username) => {
            const ref = db_1.db.collection('users').doc(username);
            batch.update(ref, { [`friends.${snap.id}`]: admin.firestore.FieldValue.delete() });
        });
        await batch.commit();
    }
    return Promise.all([
        unlink('replies', snap.id),
        unlink('comments', snap.id),
        unlink('threads', snap.id),
        unlink('files', snap.id)
    ]);
});
//# sourceMappingURL=users.js.map