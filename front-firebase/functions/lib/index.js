"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.deleteThread = exports.deleteFile = exports.deleteRoom = exports.deleteAccount = exports.setDefaultAvatar = exports.signUp = void 0;
const admin = require("firebase-admin");
admin.initializeApp();
var auth_1 = require("./auth");
Object.defineProperty(exports, "signUp", { enumerable: true, get: function () { return auth_1.signUp; } });
Object.defineProperty(exports, "setDefaultAvatar", { enumerable: true, get: function () { return auth_1.setDefaultAvatar; } });
Object.defineProperty(exports, "deleteAccount", { enumerable: true, get: function () { return auth_1.deleteAccount; } });
var chats_1 = require("./chats");
Object.defineProperty(exports, "deleteRoom", { enumerable: true, get: function () { return chats_1.deleteRoom; } });
var files_1 = require("./files");
Object.defineProperty(exports, "deleteFile", { enumerable: true, get: function () { return files_1.deleteFile; } });
var threads_1 = require("./threads");
Object.defineProperty(exports, "deleteThread", { enumerable: true, get: function () { return threads_1.deleteThread; } });
var users_1 = require("./users");
Object.defineProperty(exports, "deleteUser", { enumerable: true, get: function () { return users_1.deleteUser; } });
//# sourceMappingURL=index.js.map