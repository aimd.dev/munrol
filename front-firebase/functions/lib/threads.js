"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteThread = void 0;
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db_1 = require("./shared/db");
async function unlink(collection, id) {
    const queryRef = admin.firestore().collection(collection).where('thread', '==', id);
    const query = queryRef.orderBy('__name__').limit(15);
    return new Promise((resolve, reject) => {
        unlinkBatch(query, resolve).catch(reject);
    });
}
async function unlinkBatch(query, resolve) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db_1.db.batch();
    snapshot.docs.forEach((doc) => {
        batch.update(doc.ref, { thread: 'deleted' });
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        unlinkBatch(query, resolve);
    });
}
exports.deleteThread = functions.firestore
    .document('threads/{thread}')
    .onDelete(async (snap, context) => {
    const doc = snap.data();
    const files = [...doc.linkedFiles];
    while (files.length) {
        const batch = db_1.db.batch();
        const piece = files.splice(0, 20);
        piece.forEach((file) => {
            const ref = db_1.db.collection('files').doc(file);
            batch.update(ref, { linkedThreads: admin.firestore.FieldValue.arrayRemove(snap.id) });
        });
        await batch.commit();
    }
    await db_1.db.collection('threadReplies').doc(snap.id).delete();
    return Promise.all([unlink('replies', snap.id), unlink('comments', snap.id)]);
});
//# sourceMappingURL=threads.js.map