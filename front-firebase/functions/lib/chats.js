"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteRoom = void 0;
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db_1 = require("./shared/db");
async function deleteCollection(path) {
    const collectionRef = admin.firestore().collection(path);
    const query = collectionRef.orderBy('__name__').limit(50);
    return new Promise((resolve, reject) => {
        deleteQueryBatch(query, resolve).catch(reject);
    });
}
async function deleteQueryBatch(query, resolve) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db_1.db.batch();
    snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref);
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        deleteQueryBatch(query, resolve);
    });
}
exports.deleteRoom = functions.firestore
    .document('chats/{from}/rooms/{to}')
    .onDelete(async (snap, context) => {
    return deleteCollection(`chats/${context.params.from}/rooms/${context.params.to}/messages`);
});
//# sourceMappingURL=chats.js.map