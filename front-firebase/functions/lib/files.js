"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFile = void 0;
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db_1 = require("./shared/db");
exports.deleteFile = functions.firestore
    .document('files/{file}')
    .onDelete(async (snap, context) => {
    const doc = snap.data();
    const threads = [...doc.linkedThreads];
    while (threads.length) {
        const batch = db_1.db.batch();
        const piece = threads.splice(0, 20);
        piece.forEach((thread) => {
            const ref = db_1.db.collection('threads').doc(thread);
            batch.update(ref, { linkedFiles: admin.firestore.FieldValue.arrayRemove(thread) });
        });
        await batch.commit();
    }
});
//# sourceMappingURL=files.js.map