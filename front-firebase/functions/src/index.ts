import * as admin from "firebase-admin";

admin.initializeApp()

export { signUp, setDefaultAvatar, deleteAccount } from './auth'
export { deleteRoom } from './chats'
export { deleteFile } from './files'
export { deleteThread } from './threads'
export { deleteUser } from './users'
