import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { db } from './shared/db';

async function unlink(collection: string, id: string) {
    const queryRef = admin.firestore().collection(collection).where('thread', '==', id);
    const query = queryRef.orderBy('__name__').limit(15);

    return new Promise((resolve, reject) => {
        unlinkBatch(query, resolve).catch(reject);
    })
}

async function unlinkBatch(query: any, resolve: any) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db.batch();
    snapshot.docs.forEach((doc: any) => {
        batch.update(doc.ref, { thread: 'deleted' });
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        unlinkBatch(query, resolve);
    });
}

export const deleteThread = functions.firestore
.document('threads/{thread}')
.onDelete(async (snap, context) => {
    const doc = snap.data()
    const files = [ ...doc!.linkedFiles ]

    while (files.length) {
        const batch = db.batch();
        const piece = files.splice(0, 20);
        piece.forEach((file) => {
            const ref = db.collection('files').doc(file)
            batch.update(ref, { linkedThreads: admin.firestore.FieldValue.arrayRemove(snap.id) })
        })
        await batch.commit()
    }

    await db.collection('threadReplies').doc(snap.id).delete()

    return Promise.all([unlink('replies', snap.id), unlink('comments', snap.id)])
})
