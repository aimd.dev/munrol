import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { db } from './shared/db'

async function deleteCollection(path: string) {
    const collectionRef = admin.firestore().collection(path);
    const query = collectionRef.orderBy('__name__').limit(50);

    return new Promise((resolve, reject) => {
        deleteQueryBatch(query, resolve).catch(reject);
    })
}

async function deleteQueryBatch(query: any, resolve: any) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db.batch();
    snapshot.docs.forEach((doc: any) => {
        batch.delete(doc.ref);
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        deleteQueryBatch(query, resolve);
    });
}

export const deleteRoom = functions.firestore
.document('chats/{from}/rooms/{to}')
.onDelete(async (snap, context) => {
    return deleteCollection(`chats/${context.params.from}/rooms/${context.params.to}/messages`);
})
