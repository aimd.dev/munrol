import * as functions from 'firebase-functions';

import { auth } from './shared/auth';
import { db } from './shared/db';
import { storage } from './shared/storage';

export const signUp = functions.https.onCall( async (data, context) => {

    const user = { ...data.user }
    const document = await db.doc(`users/${user.username}`).get()

    if (document.exists) {
        functions.logger.error('Firestore Error', `Username is taken: ${user.username}`)
        throw new functions.https.HttpsError('already-exists', 'Username is taken')
    }

    return auth.createUser({
        displayName: user.username,
        email: user.email,
        password: user.password,
        uid: user.username
    })
    .then((user) => {
        // auth.ve
        return { message: 'User Created' }
    })
    .catch((err) => {
        functions.logger.error('Error in Signup', err);
        throw new functions.https.HttpsError(err.code, err.errorInfo.message)
    })
})

export const setDefaultAvatar = functions.auth.user().onCreate((user) => {
    storage.bucket(process.env.BUCKET)
    .file('default/avatar.png')
    .copy(storage.bucket(process.env.BUCKET).file(`users/${user.uid}/${user.uid}.png`))
})

export const deleteAccount = functions.auth.user().onDelete((user) => {
    db.collection('users').doc(user.uid).collection('private').doc('config').delete()
    db.collection('notifications').doc(user.uid).delete()
    db.collection('userSaved').doc(user.uid).delete()
    db.collection('userFollowing').doc(user.uid).delete()
    db.collection('chats').doc(user.uid).delete()
    db.collection('users').doc(user.uid).delete()
})
