import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { db } from './shared/db';

async function unlink(collection: string, username: string) {
    const queryRef = admin.firestore().collection(collection).where('author', '==', username);
    const query = queryRef.orderBy('__name__').limit(10);

    return new Promise((resolve, reject) => {
        unlinkBatch(query, resolve).catch(reject);
    })
}

async function unlinkBatch(query: any, resolve: any) {
    const snapshot = await query.get();
    // Check documents
    const batchSize = snapshot.size;
    if (batchSize === 0) {
        // When there are no documents left, we are done
        resolve();
        return;
    }
    // Delete documents in a batch
    const batch = db.batch();
    snapshot.docs.forEach((doc: any) => {
        batch.update(doc.ref, { thread: 'deleted' });
    });
    await batch.commit();
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(() => {
        unlinkBatch(query, resolve);
    });
}

export const deleteUser = functions.firestore
.document('users/{user}')
.onDelete(async (snap, context) => {
    const doc = snap.data()
    const friends = { ...doc.friends }
    const names = Object.keys(friends)

    while (names.length) {
        const batch = db.batch();
        const piece = names.splice(0, 20);
        piece.forEach((username) => {
            const ref = db.collection('users').doc(username)
            batch.update(ref, { [`friends.${snap.id}`]: admin.firestore.FieldValue.delete() })
        })
        await batch.commit()
    }

    return Promise.all([
        unlink('replies', snap.id),
        unlink('comments', snap.id),
        unlink('threads', snap.id),
        unlink('files', snap.id)
    ])
})
