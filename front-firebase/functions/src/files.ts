import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import { db } from './shared/db'

export const deleteFile = functions.firestore
.document('files/{file}')
.onDelete(async (snap, context) => {
    const doc = snap.data()
    const threads = [ ...doc.linkedThreads ]

    while (threads.length) {
        const batch = db.batch();
        const piece = threads.splice(0, 20);
        piece.forEach((thread) => {
            const ref = db.collection('threads').doc(thread)
            batch.update(ref, { linkedFiles: admin.firestore.FieldValue.arrayRemove(thread)})
        })
        await batch.commit()
    }
})
