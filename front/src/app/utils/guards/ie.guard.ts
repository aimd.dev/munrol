import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IeGuard implements CanActivate {

  constructor (private router: Router) {}

  canActivate() {

    const ie = (window.navigator.userAgent.match(/MSIE|Trident/) !== null)
    return !ie;
  }
  
}
