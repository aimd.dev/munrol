import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-markdown-reader',
  templateUrl: './markdown-reader.component.html',
  styleUrls: ['./markdown-reader.component.css']
})
export class MarkdownReaderComponent implements OnInit {

  // @ViewChild('mdDisplay') mdDisplay:ElementRef
  // @ViewChild('editor') editor:ElementRef

  constructor() { }

  ngOnInit(): void {
    
  }

  modulesConfig = {
    toolbar: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote'],
      // [{ 'header': 1 }, { 'header': 2 }],   
      [{ 'align': [] }],            // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],     // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }]        // outdent/indent                        // text direction
    ]
      
  }

  check(e) {
    console.log(e)
  }

}
