import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarkdownReaderComponent } from './markdown-reader.component';
import { QuillModule } from 'ngx-quill';



@NgModule({
  declarations: [
    MarkdownReaderComponent
  ],
  imports: [
    CommonModule,
    QuillModule.forRoot()
  ],
  exports: [
    MarkdownReaderComponent
  ]
})
export class MarkdownReaderModule { }
