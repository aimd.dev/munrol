interface characterAuthor {
    id: string
    username: string
}
interface characterWorld {
    id: string
    title: string
}

export interface Character {
    id: string
    author: characterAuthor
    name: string
    worlds: characterWorld[]
}
