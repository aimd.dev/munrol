// interface UserCharacter {
//     id: string
//     name: string
// }
// interface UserShort {
//     id: string
//     brief: string
//     world: UserWorld
//     author: UserWorld
//     charactersInStory: UserCharacter[]
// }
// interface UserWorld {
//     id: string
//     title: string
//     isCreator?: boolean
// }

// export interface User {
//     id: string
//     username: string

//     email?: string
//     age?: number


//     characters?: UserCharacter[]
//     shorts?: UserShort[]
//     worlds?: UserWorld[]
// }

export interface User {
    id: string
    username: string
    profile_picture?: string
    description?: string
    created_at: Date
    password?: string
    worlds?: any
}
