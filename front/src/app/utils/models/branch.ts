interface BranchWorld {
    id: string
    title: string
}
interface BranchAuthor {
    id: string
    username: string
}
interface BranchCharacter {
    id: string
    name: string
}
interface BranchShort {
    id: string
    author: BranchAuthor
    characters: BranchCharacter[]
}

export interface Branch {
    id: string
    world: BranchWorld
    author: BranchAuthor
    characters: BranchCharacter[]
    shorts: BranchShort[]
}
