interface WorldUser {
    id: string
    username: string
    avatar?: string
}
interface WorldCharacter {
    id: string
    name: string
    picture?: string
    author?: WorldUser
}
interface WorldThread {
    id: string
    brief: string

    author: WorldUser
    charactersInStory: WorldCharacter[]
    replies: number,
    open: boolean
}
interface WorldEvent {
    id: string
    title: string
    description: string
    threads: WorldThread[]
}

export interface World {
    id: string
    title: string
    description: string
    picture: string

    usersInWorld: WorldUser[]
    charactersInWorld: WorldCharacter[]
    eventsInWorld: WorldEvent[]
}
