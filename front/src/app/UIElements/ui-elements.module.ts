import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalComponent } from './modal/modal.component';
import { LoaderComponent } from './loader/loader.component';
import { IconComponent } from './icon/icon.component';
import { GhostBoxComponent } from './ghost-box/ghost-box.component';
import { DaterangepickerComponent } from './datepicker/daterangepicker/daterangepicker.component';
import { PickerCalendarComponent } from './datepicker/picker-calendar/picker-calendar.component';



@NgModule({
  declarations: [
    ModalComponent,
    LoaderComponent,
    IconComponent,
    GhostBoxComponent,
    PickerCalendarComponent,
    DaterangepickerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ModalComponent,
    LoaderComponent,
    IconComponent,
    GhostBoxComponent,
    DaterangepickerComponent,
    PickerCalendarComponent
  ]
})
export class UIElementsModule { }
