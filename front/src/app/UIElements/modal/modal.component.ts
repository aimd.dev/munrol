import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input('title') title:string
  @Input('subtitle') subtitle:string
  @Input('scrollable') scrollable:boolean
  @Input('postNavigation') postNavigation:boolean
  @Input('world') world:string
  @Input('thread') thread:string
  @Input('hide-footer') hideFooter:boolean

  @Output()
  close:EventEmitter<void> = new EventEmitter()
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  open:boolean 
  show() {
    document.querySelector('body').classList.add('modal-opened')
    this.open = true
  }
  dismiss() {
    this.open = false
    document.querySelector('body').classList.remove('modal-opened')
    this.close.emit()
  }


  // Post Navigation
  openWorld() {
    this.router.navigate([`/worlds/${this.world}`])
  }
  openThread() {
    this.router.navigate([`/worlds/${this.world}`])
  }

}
