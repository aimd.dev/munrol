import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ghost-box',
  templateUrl: './ghost-box.component.html',
  styleUrls: ['./ghost-box.component.css']
})
export class GhostBoxComponent implements OnInit {

  @Input('width') width:number
  @Input('height') height:number
  @Input('mb') mb:number
  @Input('mx-auto') mxAuto:boolean

  constructor() { }

  ngOnInit(): void {
  }

}
