import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormValidationService } from 'src/app/services/form-validation.service';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authForm:FormGroup

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private formValidation: FormValidationService,
    private authService: AuthService,
    private userService: UsersService
  ) { }

  ngOnInit(): void {
    this.authForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', []],
      password: ['', [Validators.required]]
    })
  }

  get email() { return this.authForm.get('email'); }
  get username() { return this.authForm.get('username'); }
  get password() { return this.authForm.get('password'); }

  loading:boolean
  send(e) {

    e.preventDefault()

    if (this.loading) return
    if (this.authForm.invalid) return this.formValidation.validarFormulario(this.authForm)

    this.loading = true

    this.authService.signIn({
      username: this.authForm.get('username').value,
      email: this.authForm.get('email').value,
      password: this.authForm.get('password').value
    }, this.isLogin).subscribe(data => {
      console.log(data)

      // this.loading = false
      localStorage.setItem('USER', data['username'])
      localStorage.setItem('USERID', data['id'])
      localStorage.setItem('TOKEN', data['token'])
      this.router.navigate(['/'])

    }, error => {
      console.log(error)
      this.loading = false
    })

    
  }


  exists:boolean
  checkUsername(username:string) {
    this.userService.checkUsername(username).subscribe(data => {

      this.exists = data
      console.log(this.exists)

      if (this.exists)
      this.authForm.controls['username'].setErrors({'unique': true})

    }, error => {
      console.log(error)
    })
  }

  timeout:any
  debounce(username:string, wait:number) {

    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.checkUsername(username), wait)
  }


  isLogin:boolean = true
  switch() {
    this.isLogin = !this.isLogin

    if (this.isLogin) {
      this.authForm.controls['username'].setValidators([])
    } else {
      this.authForm.controls['username'].setValidators([Validators.required])
    }
    this.authForm.controls['username'].updateValueAndValidity()
  }

}
