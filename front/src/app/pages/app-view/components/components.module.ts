import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';

import { CharacterTagComponent } from './character-tag/character-tag.component';
import { ThreadPreviewComponent } from './thread-preview/thread-preview.component';
import { PostPreviewComponent } from './post-preview/post-preview.component';
import { LightboxComponent } from './lightbox/lightbox.component';
import { WorldTagComponent } from './world-tag/world-tag.component';
import { UserTagComponent } from './user-tag/user-tag.component';
import { EventPreviewComponent } from './event-preview/event-preview.component';


@NgModule({
  declarations: [
    CharacterTagComponent,
    ThreadPreviewComponent,
    PostPreviewComponent,
    LightboxComponent,
    WorldTagComponent,
    UserTagComponent,
    EventPreviewComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    UIElementsModule
  ],
  exports: [
    CharacterTagComponent,
    ThreadPreviewComponent,
    PostPreviewComponent,
    LightboxComponent,
    WorldTagComponent,
    UserTagComponent,
    EventPreviewComponent
  ]
})
export class ComponentsModule { }
