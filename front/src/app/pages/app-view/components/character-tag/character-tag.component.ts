import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: '[app-character-tag]',
  templateUrl: './character-tag.component.html',
  styleUrls: ['./character-tag.component.css']
})
export class CharacterTagComponent implements OnInit {

  @Input('id') id:string
  @Input('name') name:string
  @Input('avatar') avatar:string
  @Input('small') small:boolean
  @Input('author') author:any
  @Input('vertical') vertical:boolean

  @Input('character') character:any

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToCharacterPage(id:string) {
    this.router.navigate([`/c/${id}`])
  }

}
