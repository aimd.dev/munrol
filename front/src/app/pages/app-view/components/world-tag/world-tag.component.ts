import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-world-tag',
  templateUrl: './world-tag.component.html',
  styleUrls: ['./world-tag.component.css']
})
export class WorldTagComponent implements OnInit {

  @Input('world') world:any

  constructor() { }

  ngOnInit(): void {
    if (!this.world.picture) this.world.picture = 'https://i.imgur.com/I86rTVl.jpg'
  }

}
