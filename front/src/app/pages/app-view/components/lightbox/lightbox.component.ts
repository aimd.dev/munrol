import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.component.html',
  styleUrls: ['./lightbox.component.css']
})
export class LightboxComponent implements OnInit {

  @ViewChild('container') container:ElementRef
  @Input('src') src:string
  @Input('title') title:string
  @Input('brief') brief:string
  @Input('gallery') gallery:any[]

  constructor() { }

  ngOnInit(): void {
    this.gallery ? this.gallery[0].active = true : null
  }

  open:boolean
  display() {
    // document.querySelector('body').classList.add('modal-opened')
    this.open = true
  }

  close() {
    if (document.fullscreenElement) this.escFullScreen()
    this.open = false
  }




  fs:boolean
  goFullScreen() {
    this.fs = true
    document.documentElement.requestFullscreen()
  }
  escFullScreen() {
    this.fs = false
    document.exitFullscreen()
  }



  zoom:number = 75
  resize(dir:number) {
    if (dir > 0) {
      if (this.zoom <= 125) {
        this.zoom += 25
      }
    } else {
      if (this.zoom >= 100) {
        this.zoom -= 25
      }
    }

  }




  isDown:boolean
  startX:any
  startY:any
  scrollLeft:any
  scrollTop:any

  initDrag(e) {
    this.isDown = true
    this.startX = e.pageX - this.container.nativeElement.offsetLeft
    this.startY = e.pageY - this.container.nativeElement.offsetTop
    this.scrollLeft = this.container.nativeElement.scrollLeft
    this.scrollTop = this.container.nativeElement.scrollTop
  }
  moveDrag(e) {
    if (!this.isDown) return
    e.preventDefault()

    const x = e.pageX - this.container.nativeElement.offsetLeft
    const y = e.pageY - this.container.nativeElement.offsetTop
    const walkX = (x - this.startX) * 2
    const walkY = (y - this.startY) * 2

    this.container.nativeElement.scrollLeft = this.scrollLeft - walkX
    this.container.nativeElement.scrollTop = this.scrollTop - walkY
  }

  endDrag() {
    this.isDown = false
  }





  current:number = 0
  navigate(dir:number) {
    if (dir > 0) {
      if (this.current < this.gallery.length -1) {
        this.current += 1
      } else {
        this.current = 0
      }
    } else {
      if (this.current >= 1) {
        this.current -= 1
      } else {
        this.current = this.gallery.length -1
      }
    }

    this.gallery.forEach(g => g.active = false)
    this.gallery[this.current].active = true
  }

  setActive(i:number) {
    this.current = i
    this.gallery.forEach(g => g.active = false)
    this.gallery[this.current].active = true
  }

}
