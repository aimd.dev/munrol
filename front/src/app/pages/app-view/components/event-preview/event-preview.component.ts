import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-event-preview]',
  templateUrl: './event-preview.component.html',
  styleUrls: ['./event-preview.component.css']
})
export class EventPreviewComponent implements OnInit {

  @Input('event') event:any

  constructor() { }

  exc:string = ''
  ngOnInit(): void {
    this.exc = this.getExerpt(this.event.description)
  }

  getExerpt(text:string) {
    return text.slice(0, 300)
  }

}
