import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/services/character.service';
import { ModalComponent } from 'src/app/UIElements/modal/modal.component';
import { LightboxComponent } from '../../components/lightbox/lightbox.component';

@Component({
  selector: 'app-character-file',
  templateUrl: './character-file.component.html',
  styleUrls: ['./character-file.component.css']
})
export class CharacterFileComponent implements OnInit {

  @ViewChild('postModal') postModal:ModalComponent
  @ViewChild('lightbox') lightbox:LightboxComponent
  @ViewChild('galleryImgs') galleryImgs:LightboxComponent

  constructor(
    private route: ActivatedRoute,
    public charactersService: CharactersService
  ) { }

  gallery:any[] = [
    {
      src: 'https://bnetcmsus-a.akamaihd.net/cms/content_entry_media/C9L0G336EJSC1589912849760.jpg',
      title: 'Hi Im a title',
      brief: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis illo facilis, veritatis aut qui quae odio excepturi fugiat corrupti quia eveniet repellendus, consequuntur, aspernatur repellat architecto iste? Dolore, magnam dicta'
    },
    {
      src: 'https://image.freepik.com/vector-gratis/personaje-anime-girl_146237-78.jpg',
      title: 'Hi Im a title',
      brief: 'noooo qq jajajajasdhhss'
    },
    {
      src: 'https://assets.puzzlefactory.pl/puzzle/248/555/original.jpg',
    },
    {
      src: 'https://w7.pngwing.com/pngs/912/686/png-transparent-anime-female-manga-drawing-anime-girl-cg-artwork-black-hair-cartoon.png',
      title: 'Hi Im a title 343423 432 423 4324 3',
      brief: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis illo facilis, veritatis aut qui quae odio excepturi fugiat corrupti quia eveniet repellendus.'
    },
  ]

  ngOnInit(): void {
    this.getData()
  }

  getData() {
    this.route.params.subscribe(params => {
      this.getCharacter(params.url)
    })
  }

  character:any
  world:any
  getCharacter(url:string) {
    this.charactersService.getByUrl(url).subscribe(
      data => {
        console.log(data)
        this.character = data
        this.world = {
          title: this.character.worldTitle,
          url: this.character.worldUrl,
          picture: this.character.worldPicture,
          tags: this.character.worldTags
        }
      }, error => {
        console.log(error)
      }
    )
  }

  showPost() {
    this.postModal.show()
  }

  fullScreen() {
    this.lightbox.display()
  }

  openGallery() {
    this.galleryImgs.display()
  }

}
