import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { ComponentsModule } from '../components/components.module';

import { CharacterFileComponent } from './character-file/character-file.component';
import { CreateCharacterComponent } from './create-character/create-character.component';
import { CharactersFeedComponent } from './characters-feed/characters-feed.component';
import { QuillModule } from 'ngx-quill';

const routes: Routes = [
  {
    path: 'create',
    component: CreateCharacterComponent
  },
  {
    path: ':url',
    component: CharacterFileComponent
  },
  {
    path: '',
    component: CharactersFeedComponent
  }
]

@NgModule({
  declarations: [
    CharacterFileComponent,
    CreateCharacterComponent,
    CharactersFeedComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    UIElementsModule,
    ComponentsModule,
    QuillModule.forRoot()
  ]
})
export class CharactersModule { }
