import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuillModules } from 'ngx-quill';
import { FormValidationService } from 'src/app/services/form-validation.service';

@Component({
  selector: 'app-create-character',
  templateUrl: './create-character.component.html',
  styleUrls: ['./create-character.component.css']
})
export class CreateCharacterComponent implements OnInit {

  characterForm:FormGroup

  constructor(
    public fb: FormBuilder,
    private formValidation: FormValidationService
  ) { }

  modulesConfig:QuillModules = {
    toolbar: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote'],
      [{ 'align': [] }],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'indent': '-1'}, { 'indent': '+1' }]
    ]
  }

  ngOnInit(): void {
    this.characterForm = this.fb.group({
      name: ['', [Validators.required]],
      age: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
      bio: ['', [Validators.required]],
      body: ['', [Validators.required]],
      portrait: ['', [Validators.required]]
    })
  }


  loading:boolean
  save(e) {
    e.preventDefault()

    if (this.loading) return

    if (this.characterForm.invalid) {
      console.log('invalid')
      return this.formValidation.validarFormulario(this.characterForm)
    }

    this.loading = true
  }

}
