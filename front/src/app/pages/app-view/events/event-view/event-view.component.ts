import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/events.service';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public eventService: EventService
  ) { }

  ngOnInit(): void {
    this.getData()
  }

  eLoading:boolean
  getData() {
    this.eLoading = true
    this.route.params.subscribe(params => {
      const { url } = params
      url ? this.getEvent(url) : this.setNotFound()
    })
  }

  event:any
  getEvent(url:string) {
    this.eventService.getByUrl(url).subscribe(data => {
      this.event = data
      console.log(this.event)
    })
  }

  setNotFound() {
    console.log('not found')
  }

}
