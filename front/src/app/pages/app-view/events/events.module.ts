import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { ComponentsModule } from '../components/components.module';

import { EventViewComponent } from './event-view/event-view.component';


const routes: Routes = [
  {
    path: ':url',
    component: EventViewComponent
  }
]

@NgModule({
  declarations: [
    EventViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UIElementsModule,
    ComponentsModule
  ]
})
export class EventsModule { }
