import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { SharedModule } from './shared/shared.module';
import { ComponentsModule } from './components/components.module';

import { AppViewComponent } from './app-view.component';
import { BrowseComponent } from './browse/browse.component';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    component: AppViewComponent,
    children: [
      {
        path: 'u',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'c',
        loadChildren: () => import('./characters/characters.module').then(m => m.CharactersModule)
      },
      {
        path: 'w',
        loadChildren: () => import('./worlds/worlds.module').then(m => m.WorldsModule)
      },
      {
        path: 'e',
        loadChildren: () => import('./events/events.module').then(m => m.EventsModule)
      },
      {
        path: 't',
        loadChildren: () => import('./threads/threads.module').then(m => m.ThreadsModule)
      },
      {
        path: '',
        component: BrowseComponent
      }
    ]
  },
  
]

@NgModule({
  declarations: [
    AppViewComponent,
    BrowseComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UIElementsModule,
    SharedModule,
    ComponentsModule,
    FormsModule
  ]
})
export class AppViewModule { }
