import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThreadService } from 'src/app/services/threads.service';

@Component({
  selector: 'app-thread-view',
  templateUrl: './thread-view.component.html',
  styleUrls: ['./thread-view.component.css']
})
export class ThreadViewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public threadService: ThreadService
  ) { }

  thread:any
  ngOnInit(): void {
    this.getData()
  }

  getData() {
    this.route.params.subscribe(params => {
      const { url } = params
      url ? this.getThread(url) : this.setNotFound()
    })
  }

  getThread(url:string) {
    this.threadService.getByUrl(url).subscribe(data => {
      this.thread = data
      console.log(this.thread)
    })
  }

  setNotFound() {

  }

  scroll(index:string) {
    const element = document.querySelector(`#${index}`)
    const y = element.getBoundingClientRect().top + window.pageYOffset - 140
    window.scrollTo({top: y, behavior: 'smooth'})
  }

}
