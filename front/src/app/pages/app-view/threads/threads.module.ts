import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { ComponentsModule } from '../components/components.module';

import { ThreadViewComponent } from './thread-view/thread-view.component';


const routes: Routes = [
  {
    path: ':url',
    component: ThreadViewComponent
  }
]

@NgModule({
  declarations: [
    ThreadViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UIElementsModule,
    ComponentsModule
  ]
})
export class ThreadsModule { }
