import { Component, OnInit, ViewChild } from '@angular/core';

import { WorldsService } from 'src/app/services/worlds.service'
import { World } from 'src/app/utils/models/world';

import dummy_world from 'src/app/dummy_data/world_dummy';
import { ModalComponent } from 'src/app/UIElements/modal/modal.component';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'src/app/services/notifications.service';
import { TagsService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-world-view',
  templateUrl: './world-view.component.html',
  styleUrls: ['./world-view.component.css']
})
export class WorldViewComponent implements OnInit {

  @ViewChild('introModal') introModal:ModalComponent
  @ViewChild('postModal') postModal:ModalComponent
  @ViewChild('joinModal') joinModal:ModalComponent

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public worldsService: WorldsService,
    public tagService: TagsService,
    public notifications: NotificationsService
    ) { }

  dworld:World
  ngOnInit(): void {
    this.dworld = {...dummy_world}
    this.getData()
  }

  showPost() {
    this.postModal.show()
  }

  wLoading:boolean
  world:any

  getData() {
    this.wLoading = true
    this.route.params.subscribe(params => {
      const { url } = params
      url ? this.getWorld(url) : this.setNotFound()
    })
  }

  getWorld(url:string) {
  
    this.worldsService.getByUrl(url).subscribe(world => {
      
      console.log(world)
      if (world && world.id) {
        
        this.world = world
        if (!this.world.picture) this.world.picture = 'https://i.imgur.com/I86rTVl.jpg'

        this.getUserInWorld(world.id)

        this.wLoading = false

      } else {
        this.setNotFound()
      }
      
    })
  }

  user:any
  getUserInWorld(id:string) {
    this.worldsService.getUserInWorld(id).subscribe(data => {
      console.log(data)
      this.user = data
    }, error => {
      console.log(error)
    })
  }

  fLoading:boolean
  follow(action:boolean) {
    this.fLoading = true
    this.notifications.success('Serás notificado de las nuevas publicaciones de este mundo')
  }


  setNotFound() {
    console.log('not found')
  }


  // Like Interaction
  likeLoading:boolean
  like(like:boolean) {

    if (this.likeLoading) return
    this.likeLoading = true

    let obj = {
      wid: this.world.id,
      like: like
    }

    this.worldsService.likeWorld(obj).subscribe(data => {
      console.log(data)
      this.user.liked = like
      this.likeLoading = false
    }, error => {
      console.log(error)
      this.likeLoading = false
    })
  }

  // Join Interaction
  joinLoading:boolean
  join(join:boolean) {

    if (this.joinLoading) return
    this.joinLoading = true

    let obj = {
      wid: this.world.id,
      join: join
    }

    this.worldsService.likeWorld(obj).subscribe(data => {
      console.log(data)
      this.user.membership = join
      this.joinLoading = false
    }, error => {
      console.log(error)
      this.joinLoading = false
    })
  }

}
