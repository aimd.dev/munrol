import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { ComponentsModule } from '../components/components.module';

import { WorldViewComponent } from './world-view/world-view.component';
import { CreateWorldComponent } from './create-world/create-world.component';
import { RouterModule, Routes } from '@angular/router';
import { WorldsFeedComponent } from './worlds-feed/worlds-feed.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IeGuard } from 'src/app/utils/guards/ie.guard';
import { SharedModule } from '../shared/shared.module';


const routes: Routes = [
  {
    path: 'new',
    component: CreateWorldComponent,
    canActivate: [IeGuard]
  },
  {
    path: ':url',
    component: WorldViewComponent
  },
  {
    path: '',
    component: WorldsFeedComponent
  },
]

@NgModule({
  declarations: [
    WorldViewComponent,
    CreateWorldComponent,
    WorldsFeedComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    UIElementsModule,
    ComponentsModule,
    QuillModule.forRoot(),
    SharedModule
  ]
})
export class WorldsModule { }
