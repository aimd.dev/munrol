import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuillModules } from 'ngx-quill';
import { WorldsService } from 'src/app/services/worlds.service';
import { FormValidationService } from 'src/app/services/form-validation.service';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-world',
  templateUrl: './create-world.component.html',
  styleUrls: ['./create-world.component.css']
})
export class CreateWorldComponent implements OnInit {

  worldForm:FormGroup

  constructor(
    private fb: FormBuilder,
    public worldsService: WorldsService,
    private formValidation: FormValidationService,
    private authService: AuthService,
    private router: Router,
    public notify: NotificationsService
  ) { }

  generatedUrl:string = 'url_de_tu_mundo'
  exists:boolean
  ngOnInit(): void {

    this.worldForm = this.fb.group({
      title: ['', [Validators.required, Validators.pattern("^[\\w \\u00C0-\\u00FF]+$")]],
      description: ['', [Validators.required, Validators.minLength(30), Validators.maxLength(400)]],
      picture: ['', []],
      tags: [[], [Validators.required]]
    })

    // URL Generation
    // this.worldForm.get('title').valueChanges.subscribe(val => {

      // Remove accents & spaces
      // this.generatedUrl = val.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').toLowerCase()
      
      // if (this.exists)
      // this.worldForm.controls['title'].setErrors({'unique': true})
    // });
  }

  get title() { return this.worldForm.get('title'); }
  get description() { return this.worldForm.get('description'); }
  get tags() { return this.worldForm.get('tags'); }

  generateUrl(val:string) {
    this.generatedUrl = val.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').toLowerCase()
    this.debounce(700)
  }

  checkTitle() {
    this.worldsService.checkUrl(this.generatedUrl).subscribe(data => {

      this.exists = data
      console.log(this.exists)

      if (this.exists)
      this.worldForm.controls['title'].setErrors({'unique': true})

    }, error => {
      console.log(error)
    })
  }

  timeout:any
  debounce(wait:number) {

    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.checkTitle(), wait)
  }

  modulesConfig:QuillModules = {
    toolbar: [
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote'],
      [{ 'align': [] }],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'indent': '-1'}, { 'indent': '+1' }]
    ]
  }

  loading:boolean
  save(e) {
    console.log('submit')
    e.preventDefault()

    if (this.loading) return

    if (this.worldForm.invalid) {
      console.log('invalid')
      console.log(this.worldForm)
      return this.formValidation.validarFormulario(this.worldForm)
    }

    this.loading = true
    console.log(this.worldForm.value)

    this.worldsService.createWorld({
      title: this.worldForm.get('title').value,
      picture: this.worldForm.get('picture').value,
      description: this.worldForm.get('description').value,
      tags: this.worldForm.get('tags').value,
      created_by: this.authService.getLoggedUser(),
      user_id: this.authService.getLoggedInUserId()
    }).subscribe(data => {
      console.log(data)
      this.notify.success('Mundo creado exitosamente')
      this.router.navigate([`/w/${this.generatedUrl}`])
    }, error => {
      console.log(error)
      this.loading = false
    })
  }

}
