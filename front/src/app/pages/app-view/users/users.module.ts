import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { ComponentsModule } from '../components/components.module';

import { ProfileViewComponent } from './profile-view/profile-view.component';
import { UsersFeedComponent } from './users-feed/users-feed.component';


const routes: Routes = [
  {
    path: ':uid',
    component: ProfileViewComponent
  },
  {
    path: '',
    component: UsersFeedComponent
  }
]

@NgModule({
  declarations: [
    ProfileViewComponent,
    UsersFeedComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    UIElementsModule,
    ComponentsModule
  ]
})
export class UsersModule { }
