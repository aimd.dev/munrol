import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UsersService } from 'src/app/services/users.service';
import { NotificationsService } from 'src/app/services/notifications.service';

import { ModalComponent } from 'src/app/UIElements/modal/modal.component';
import { User } from 'src/app/utils/models/user';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {

  @ViewChild('postModal') postModal:ModalComponent

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      public usersService: UsersService,
      public notify: NotificationsService
  ) { }

  user:User
  ngOnInit(): void {

    this.getUser()

  }

  uLoading:boolean
  getUser() {
    this.uLoading = true
    this.route.params.subscribe(p => {
      if (p.uid) {
        this.usersService.getByUsername(p.uid).subscribe(user => {
          console.log(user)
          if (user) {
            
            this.user = user
            if (!this.user.profile_picture) this.user.profile_picture = 'https://i.imgur.com/I86rTVl.jpg'

            this.getUserToUser(user.id)

            this.uLoading = false

          } else {
            this.router.navigate(['/404'])
          }
          
        })
      } else {
        
      }
    })
    
  }

  status:any
  getUserToUser(id:string) {
    this.usersService.getUserToUserData(id).subscribe(data => {
      console.log(data)
      this.status = data
    }, error => {
      console.log(error)
    })
  }

  currentPost:any = {}
  showPost(post) {
    this.currentPost = post
    this.postModal.show()
  }

  
  followLoading:boolean
  followInteraction(follow:boolean) {

    if (this.followLoading) return
    this.followLoading = true

    let obj = {
      lid: this.user.id,
      f: follow
    }

    this.usersService.followInteraction(obj).subscribe(data => {
      console.log(data)
      this.followLoading = false
      this.status.isFollowing = obj.f
    }, error => {
      console.log(error)
      this.followLoading = false
    })
  }

}
