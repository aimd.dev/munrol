import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

  constructor() { }

  authLink:string
  ngOnInit(): void {
    
    const user = localStorage.getItem('USER')
    user ? this.authLink = '/u/'+user : this.authLink = '/auth'
  }


  cat:string = '0'
  filter:string



  fs:boolean
  goFullScreen() {
    this.fs = true
    document.documentElement.requestFullscreen()
  }
  escFullScreen() {
    this.fs = false
    document.exitFullscreen()
  }


  @HostBinding('class.hidden') ht:boolean
  hideTop() {
    this.ht = true
  }
  showTop() {
    this.ht = false
  }

}
