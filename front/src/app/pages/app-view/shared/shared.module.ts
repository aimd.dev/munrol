import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';

import { TopNavComponent } from './top-nav/top-nav.component';
import { FooterComponent } from './footer/footer.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { GoTopComponent } from './go-top/go-top.component';
import { UiSelectComponent } from './ui-select/ui-select.component';


@NgModule({
  declarations: [
    TopNavComponent,
    FooterComponent,
    NotificationsComponent,
    GoTopComponent,
    UiSelectComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UIElementsModule,
    // ComponentsModule
  ],
  exports: [
    TopNavComponent,
    FooterComponent,
    NotificationsComponent,
    GoTopComponent,
    UiSelectComponent
  ]
})
export class SharedModule { }
