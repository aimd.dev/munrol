import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-go-top',
  templateUrl: './go-top.component.html',
  styleUrls: ['./go-top.component.css']
})
export class GoTopComponent implements OnInit {

  constructor() { }

  showBtn:boolean
  ngOnInit(): void {
    document.addEventListener('scroll', () => { this.showBtn = document.documentElement.scrollTop > 20 })
  }

  scroll() {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }

}
