import { Component, Self, Optional, Input, Output, ViewChild, HostListener, ElementRef, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

import { TagsService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-ui-select',
  templateUrl: './ui-select.component.html',
  styleUrls: ['./ui-select.component.css'],
  // providers: [
  //   {
  //     provide: NG_VALUE_ACCESSOR,
  //     useExisting: forwardRef(() => UiSelectComponent),
  //     multi: true
  //   }
  // ]
})
export class UiSelectComponent implements ControlValueAccessor {

  @Input('placeholder') placeholder:string = ''
  @Input('disabled') disabled:boolean
  @Input('max') max:number

  @Output()
  selection: EventEmitter<any> = new EventEmitter()

  @Output()
  deselect: EventEmitter<any> = new EventEmitter()

  @Output()
  selectionchange: EventEmitter<any> = new EventEmitter()

  @Output()
  closed: EventEmitter<any> = new EventEmitter()

  @ViewChild('search') search:ElementRef

  @HostListener('document:click') 
  clickout() {
    this.toggleDrop(false)
  }


  constructor(
    @Self()
    @Optional()
    public ngControl: NgControl,
    public tagService: TagsService) {

    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }


  // private _value:any
  onChangeCallback: any = () => {}
  onTouchedCallback: any = () => {}

  writeValue(value:any) {
    this.selected = value 
    this.onChangeCallback(value)
  }
  registerOnChange(fn: (_: any) => void): void {
    this.onChangeCallback = fn
  }
  registerOnTouched(fn: () => void): void {
    this.onTouchedCallback = fn
  }
  setDisabledState(disabled:boolean) {
    // this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', disabled);
  }





  toggleDrop(state:boolean) {
    this.open = state
    if (!this.open) {
      this.closed.emit()
    }
  }

  open:boolean
  items:any[]
  active:number = 0
  valor:string
  filter(e) {

    // this.subject.next();

    if ((e.key === 'Enter' || e.key === ',') && this.open) {

      this.select(this.items[this.active])
      return
    }
    if ((e.key === 'Enter' || e.key === ',') && !this.open && this.filterValue.length > 2) {

      if (!this.selected.includes(this.filterValue)) {
        this.select(this.filterValue)
      }
      return
    }
    // console.log(this.filterValue)
    // console.log(this.filterValue.length)
    if (e.key === 'Backspace') {
      if (this.selected.length > 0 && this.filterValue.length == 0) {
        console.log(this.filterValue)
        this.remove(this.selected.length - 1)
      }
    }
    
    if (e.key === 'ArrowUp') {
      
      if (this.active - 1 < 0) {
        this.active = this.items.length -1
      } else {
        this.active -= 1
      }
      return
    }
    if (e.key === 'ArrowDown') {
      if (this.active + 1 === this.items.length) {
        this.active = 0
      } else {
        this.active += 1
      }
      return
    }

    if (e.target.value.length > 2) {

      this.debounce('getTags', e.target.value, 500)

    } else {
      this.open = false
    }

    // this.valor = e.target.value
  }

  // Debounce keyup
  timeout:any
  debounce(func:string, value:string, wait:number) {

    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this[func](value), wait)
  }

  getTags(val:string) {

    // if (this.valor.length > 2) { 
    this.tagService.getByString(val).subscribe(data => {

      this.items = data.filter(val => !this.selected.includes(val))

      if (this.items.length > 0) {
        this.active = 0
        this.toggleDrop(true)
      } else {
        this.open = false
      }

    }, error => {
      console.log(error)
    })
  }

  

  selected:any[] = []
  filterValue:string
  select(item:any) {

    this.open = false
    this.filterValue = ''
    this.selected.push(item)
    this.selectionchange.emit(this.selected)
    this.writeValue(this.selected)

    if (this.selected.length > this.max) {
      this.ngControl.control.setErrors({'max': true})
    }
  }
  remove(index:number) {

    this.selected.splice(index, 1)
    this.selectionchange.emit(this.selected)
    this.writeValue(this.selected)
  }
}
