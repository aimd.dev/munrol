import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/events.service';
import { PostsService } from 'src/app/services/posts.service';
import { WorldsService } from 'src/app/services/worlds.service';

import world_dummy from '../../../dummy_data/world_dummy'

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {

  world: any = { ...world_dummy }

  constructor(
    public worldsService: WorldsService,
    public eventService: EventService,
    public postsService: PostsService
  ) { }

  ngOnInit(): void {
    this.getTop()
    this.getTrending()
    this.getRecentEvents()
    this.getRecentPosts()
  }

  topWorlds:any[]
  getTop() {
    this.worldsService.getPopular().subscribe(data => {
      console.log(data)
      this.topWorlds = data
    }, error => {
      console.log(error)
    })
  }

  trendingWorlds:any[]
  getTrending() {
    this.worldsService.getTrending().subscribe(data => {
      console.log(data)
      this.trendingWorlds = data
    }, error => {
      console.log(error)
    })
  }

  events:any
  getRecentEvents() {
    this.eventService.getRecent().subscribe(data => {
      console.log(data)
      this.events = data
    }, error => {
      console.log(error)
    })
  }

  posts:any
  getRecentPosts() {
    this.postsService.getRecent().subscribe(data => {
      console.log(data)
      this.posts = data
    }, error => {
      console.log(error)
    })
  }

}
