import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  code:number = 404
  description:string = 'Not Found'

  constructor(private location:Location) { }

  ngOnInit(): void {
  }

  return() {
    this.location.back()
  }

}
