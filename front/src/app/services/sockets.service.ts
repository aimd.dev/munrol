import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketsService {

  socket;

  constructor() { }

  setupSocketConnection() {
    this.socket = io(environment.url_services);
    this.socket.emit('my message', 'Hello there from Angular.');
  }

}
