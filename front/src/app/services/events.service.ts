import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    constructor(private http: HttpClient) { }

    private urlGetByUrl = environment.url_services + '/api/events/';
    private urlGetRecent = environment.url_services + '/api/events/';

    getByUrl(url:string) {
        return this.http.get<any>(this.urlGetByUrl + url)
    }

    getRecent() {
        return this.http.get<any>(this.urlGetRecent)
    }
}
