import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(private http: HttpClient) { }

    private urlGetByUsername = environment.url_services + '/api/users/';
    private urlCheckUsername = environment.url_services + '/api/users/q/';

    private urlGetUserToUserData = environment.url_services + '/api/user-to-user/';
    private urlFollowUnfollow = environment.url_services + '/api/user-follow/';

    getByUsername(username:string) {
        return this.http.get<any>(this.urlGetByUsername + username)
    }

    checkUsername(username:string) {
        return this.http.get<any>(this.urlCheckUsername + username)
    }


    getUserToUserData(id:string) {
        return this.http.get<any>(this.urlGetUserToUserData + id)
    }
    followInteraction(obj) {
        return this.http.post(this.urlFollowUnfollow, obj)
    }
}
