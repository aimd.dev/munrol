import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TagsService {

    constructor(private http: HttpClient) { }

    private urlGetTagsByString = environment.url_services + '/api/tags/';

    getByString(string:string) {
        return this.http.get<any>(this.urlGetTagsByString + string)
    }
}
