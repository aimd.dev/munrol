import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PostsService {

    constructor(private http: HttpClient) { }

    private urlGetRecent = environment.url_services + '/api/posts/';

    getRecent() {
        return this.http.get<any>(this.urlGetRecent)
    }
}
