import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  private urlSignIn = 'http://localhost:3000/api/users/'
  private urlLogin = 'http://localhost:3000/api/users/auth/'

  signIn(user:any, isLogin:boolean) {

    if (isLogin) {
      return this.http.post(this.urlLogin, {email: user.email, password: user.password})
    } else {
      return this.http.post(this.urlSignIn, user)
    }
    
  }

  getLoggedUser() {
    return localStorage.getItem('USER')
  }

  getLoggedInUserId() {
    return localStorage.getItem('USERID')
  }
}
