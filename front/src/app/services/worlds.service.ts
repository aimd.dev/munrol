import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment'

@Injectable({
    providedIn: 'root'
})
export class WorldsService {

    constructor(private http: HttpClient) { }

    private urlGetPopular = environment.url_services + '/api/browse/worlds/popular/'
    private urlGetTrending = environment.url_services + '/api/browse/worlds/trending/'

    private urlCheckUrl = environment.url_services + '/api/worlds/q/'
    private urlInsertWorld = environment.url_services + '/api/worlds/'
    
    private urlGetByUrl = environment.url_services + '/api/worlds/'
    private urlGetEventsInWorld = environment.url_services + '/api/events/world/'
    private urlGetUserInWorld = environment.url_services + '/api/user-in-world/'

    private urlLikeInteraction = environment.url_services + '/api/world-likes/'
    private urlJoinInteraction = environment.url_services + '/api/world-joins/'


    // Browsing
    getPopular() {
        return this.http.get<any>(this.urlGetPopular)
    }
    getTrending() {
        return this.http.get<any>(this.urlGetTrending)
    }


    // Creation
    checkUrl(url:string) {
        return this.http.get<boolean>(this.urlCheckUrl + url)
    }
    createWorld(world) {
        return this.http.post(this.urlInsertWorld, world)
    }

    
    // Navigation
    getByUrl(url:string) {
        return this.http.get<any>(this.urlGetByUrl + url)
    }
    getEventsInWorld(id:string) {
        return this.http.get<any>(this.urlGetEventsInWorld + id)
    }
    getUserInWorld(wid:string) {
        return this.http.get<any>(this.urlGetUserInWorld + wid)
    }


    // Interactions
    likeWorld(obj) {
        return this.http.post(this.urlLikeInteraction, obj)
    }
    joinWorld(obj) {
        return this.http.post(this.urlJoinInteraction, obj)
    }
}
