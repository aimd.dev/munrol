import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor() { }

  esCampoValido(form: FormGroup, campo: string) {
    return form.get(campo).invalid  && (form.get(campo).dirty || form.get(campo).touched);
  }

  validarFormulario(formulario: FormGroup) {
    Object.keys(formulario.controls).forEach(field => {
      const control = formulario.get(field);
      if (control instanceof FormControl) { 
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validarFormulario(control);
      }
    });
  }
}
