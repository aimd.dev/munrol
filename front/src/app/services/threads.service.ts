import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ThreadService {

    constructor(private http: HttpClient) { }

    private urlGetByUrl = environment.url_services + '/api/threads/';

    getByUrl(url:string) {
        return this.http.get<any>(this.urlGetByUrl + url)
    }
}
