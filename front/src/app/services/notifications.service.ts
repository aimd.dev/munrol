import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor() { }

  notifications: any[] = []

  success(msg) {
    this.notificacion('success', 'Éxito', msg, null)
  }

  error(msg) {
    this.notificacion('error', 'Error', msg, null)
  }

  info(msg) {
    this.notificacion('info', 'Aviso', msg, null)
  }

  // msg(msg, url) {

  //   let audio = new Audio();
  //   audio.src = "../../../assets/audio/not3.mp3";
  //   audio.load();
  //   audio.play();

  //   this.notificacion('message', 'ti-email', 'Nuevo Documento', msg, url)
  // }

  notificacion(tipo: string, titulo: string, mensaje: string, url: string, items:any[] = []) {

    let show = true

    const nuevo:any = { tipo, titulo, mensaje, show, url }
    if (items.length > 0) nuevo.items = items
    this.notifications = [...this.notifications, nuevo]

    // if (tipo != 'contrato' && tipo != 'formulario') {
    nuevo.autoCerrar = setTimeout(() => {
      this.close(0)
    }, 5000);
    // }
    

  }

  close(i:number) {
    if (this.notifications[i]) {
      this.notifications[i].show = false;
      setTimeout(() => {
        this.notifications = this.notifications.filter( n => n != this.notifications[i])
      }, 450);
    }
  }


  enter(i:number) {
    clearTimeout(this.notifications[i].autoCerrar)
  }
  leave(i:number) {
    this.notifications[i].autoCerrar = setTimeout(() => {
      this.close(i)
    }, 3000);
  }

}
