import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TokenInterceptor } from './security/token.interceptor';

import { UIElementsModule } from 'src/app/UIElements/ui-elements.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SocketsService } from './services/sockets.service';
import { TestingComponent } from './utils/testing/testing.component';
import { MarkdownReaderModule } from './utils/markdown-reader/markdown-reader.module';

import { AppComponent } from './app.component';
import { ErrorPageComponent } from './pages/error-view/error-page.component';


const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./pages/auth-view/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'testing',
    component: TestingComponent
  },
  {
    path: '',
    loadChildren: () => import('./pages/app-view/app-view.module').then(m => m.AppViewModule)
  },
  {
    path: '**',
    component: ErrorPageComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    TestingComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
    UIElementsModule,
    HttpClientModule,
    MarkdownReaderModule
  ],
  providers: [
    SocketsService, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
