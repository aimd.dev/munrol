import { Character } from '../utils/models/character'

const character_dummy:any = {
    id: 'c1',
    author: { id: 'u1', username: 'kiki' },
    name: 'esmeralda',
    worlds: [ {id: 'cdyet15', title: 'Once upon a december'} ]
}

export default character_dummy