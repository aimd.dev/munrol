import { World } from '../utils/models/world'

const world_dummy:any = {
    id: 'cdyet15',
    title: 'Once upon a time',
    picture: 'https://am-a.akamaihd.net/image?f=https%3A%2F%2Funiverse-meeps.leagueoflegends.com%2Fv1%2Fassets%2Fimages%2Ffactions%2Fimage-gallery%2Fdemacia-silvermere.jpg&resize=:1200',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod a arcu vitae fermentum. Maecenas pellentesque ante vel risus bibendum laoreet. Maecenas varius mattis metus, sed vestibulum libero bibendum et. Aliquam erat volutpat. Nullam viverra magna est, sed dignissim orci imperdiet eget. Mauris porttitor sem quis cursus ullamcorper.',
    usersInWorld: [
        {
            id: 'u1',
            username: 'kiki',
            avatar: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.counterstats.net%2Fleague-of-legends%2Firelia&psig=AOvVaw0WjGncChZj48EEXTyvtzNi&ust=1600031553085000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKje05fE5OsCFQAAAAAdAAAAABAD'
        }
    ],
    charactersInWorld: [ 
        {id: 'c1', name: 'esmeralda', picture: 'https://cdna.artstation.com/p/assets/images/images/020/679/830/large/rina-samoylova-.jpg?1568744294', author: {id: 'u1', username: 'kiki'}},
        {id: 'c2', name: 'leia', picture: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/8173394d-de95-430a-a448-f57211a61abf/d77t2fj-73fe4a45-d213-4069-9884-96329297ad9c.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvODE3MzM5NGQtZGU5NS00MzBhLWE0NDgtZjU3MjExYTYxYWJmXC9kNzd0MmZqLTczZmU0YTQ1LWQyMTMtNDA2OS05ODg0LTk2MzI5Mjk3YWQ5Yy5wbmcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.DR_8_mxkcz-W_7pjXGFPB3XYk_xB-xw78e0owBN3g9Y', author: {id: 'u1', username: 'kiki'}},
        {id: 'c3', name: 'victoria', picture: 'https://wallpapermemory.com/uploads/589/riven-league-of-legends-wallpaper-hd-1366x768-173050.jpg', author: {id: 'u1', username: 'kiki'}} 
    ],
    eventsInWorld: [ 
        {
            id: 'e1', 
            title: 'The Winter Festival',
            description: 'Veniam mollit aliqua anim duis officia dolor mollit nulla aliqua pariatur eu dolore. Exercitation sit dolore sunt laboris labore anim officia elit consectetur velit cillum amet exercitation id. Proident ut anim Lorem ea labore cillum aute ad velit....',
            picture: 'https://images.hdqwalls.com/wallpapers/dragon-winter-snow-anime-manga-4k-9k.jpg',
            threads: [ 
                { 
                    id: 's1', 
                    charactersInStory: [
                        {
                            id: 'c1', 
                            name: 'esmeralda',
                            picture: 'https://cdna.artstation.com/p/assets/images/images/020/679/830/large/rina-samoylova-.jpg?1568744294'
                        },
                        {
                            id: 'c2',
                            name: 'leia',
                            picture: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/8173394d-de95-430a-a448-f57211a61abf/d77t2fj-73fe4a45-d213-4069-9884-96329297ad9c.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvODE3MzM5NGQtZGU5NS00MzBhLWE0NDgtZjU3MjExYTYxYWJmXC9kNzd0MmZqLTczZmU0YTQ1LWQyMTMtNDA2OS05ODg0LTk2MzI5Mjk3YWQ5Yy5wbmcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.DR_8_mxkcz-W_7pjXGFPB3XYk_xB-xw78e0owBN3g9Y'
                        }
                    ], 
                    brief: 'I was walking on the beach when...', 
                    author: {id: 'u1', username: 'kiki'}, 
                    replies: 23,
                    open: true
                },
                { 
                    id: 's2', 
                    charactersInStory: [
                        {
                            id: 'c3', 
                            name: 'victoria',
                            picture: 'https://wallpapermemory.com/uploads/589/riven-league-of-legends-wallpaper-hd-1366x768-173050.jpg'
                        },
                    ], 
                    brief: 'Nobody knows what is hidden beneath the surface of the legendary moon', 
                    author: {id: 'u1', username: 'kiki'}, 
                    replies: 23,
                    open: true
                }  
            ] 
        },
        {
            id: 'e1', 
            title: "The Emperor's Assasination",
            description: 'Fugiat officia aliqua eiusmod Lorem in. Pariatur aute ex esse culpa qui dolor non. In labore adipisicing occaecat aliqua ea dolor proident dolore nulla exercitation. Nulla pariatur deserunt ullamco sit et sit pariatur pariatur laborum...',
            picture: 'https://img2.wikia.nocookie.net/__cb20110809011136/katanagatari/images/a/a8/Gekoku_Castle.jpg'
        } 
    ]
}

export default world_dummy