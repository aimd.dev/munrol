import { Component, OnInit } from '@angular/core';
import { SocketsService } from './services/sockets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    public socketService: SocketsService
  ) {

  }

  title = 'munrol';

  ngOnInit() {
    this.socketService.setupSocketConnection();
  }
}
