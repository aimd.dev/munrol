SELECT * FROM characters
JOIN worlds ON characters.world = worlds.id

-- Character & world join
SELECT * FROM characters JOIN worlds ON characters.world = worlds.id WHERE characters.id = '457ed55d-3f74-4940-bc6a-03e7304825b9';

-- Get characters from world
SELECT * FROM characters WHERE world = '4108db4b-f465-4892-81d6-b11592d87e5c';






-- World with tags view
CREATE VIEW tagged_worlds AS 
	SELECT 
        worlds.id,
		title, 
		url,
		description, 
		picture, 
		created_at, 
		ARRAY_AGG (tag_title) AS tags
	FROM worlds
	JOIN world_tags ON worlds.id = world_id
	GROUP BY worlds.id, title, url, description, picture, created_at;



-- Trending worlds
SELECT 
	title, 
	url,
	picture,
	tags
FROM tagged_worlds AS worlds
FULL JOIN likes on worlds.id = likes.world_id
GROUP BY title, url, picture, tags
ORDER BY MAX(likes.created_at) DESC
LIMIT 4

-- Popular worlds
SELECT 
	title, 
	url,
	picture,
	tags
FROM tagged_worlds AS worlds
FULL JOIN likes on worlds.id = likes.world_id
GROUP BY title, url, picture, tags
ORDER BY COUNT(likes.id) DESC
LIMIT 4






-- Trending urls
SELECT 
url
FROM worlds
JOIN likes on worlds.id = likes.world_id
GROUP BY url
ORDER BY MAX(likes.created_at) DESC
LIMIT 3

-- Popular urls
SELECT 
url, COUNT(*)
FROM worlds
JOIN likes on worlds.id = likes.world_id
GROUP BY url
ORDER BY COUNT(*) DESC
LIMIT 3


-- Full world view
CREATE OR REPLACE VIEW full_world AS (
	WITH 
	grouped_tags AS (
		SELECT world_id, ARRAY_AGG (tag_title) AS tags FROM world_tags GROUP BY world_id
	),
	cte_total_events AS (
		SELECT world_id, COUNT(id) FROM events GROUP BY world_id
	),
	cte_total_posts AS (
		SELECT world_id, COUNT(id) FROM posts GROUP BY world_id
	),
	cte_total_members AS (
		SELECT world_id, COUNT(id) FROM world_users GROUP BY world_id
	)
	SELECT 
		worlds.id,
		worlds.title,
		url,
		worlds.description, 
		worlds.picture, 
		worlds.created_at, 
		tags,
		COALESCE(cte_total_events.count, 0) AS total_events,
		COALESCE(cte_total_posts.count, 0) AS total_posts,
		COALESCE(cte_total_members.count, 0) AS total_members
	FROM worlds
	FULL JOIN grouped_tags ON worlds.id = world_id
	FULL JOIN cte_total_events ON worlds.id = cte_total_events.world_id
	FULL JOIN cte_total_posts ON worlds.id = cte_total_posts.world_id
	FULL JOIN cte_total_members ON worlds.id = cte_total_members.world_id
	GROUP BY 
		worlds.id, 
		worlds.title,
		url, 
		worlds.description, 
		worlds.picture, 
		worlds.created_at,
		tags,
		total_events,
		total_posts,
		total_members
);	



CREATE OR REPLACE VIEW recent_events AS (
	WITH
	threads_total AS (
		SELECT event_id, COUNT(*) FROM threads GROUP BY event_id
	),
	posts_total AS (
		SELECT event_id, COUNT(*) FROM posts GROUP BY event_id
	)
	SELECT 
		id, 
		created_at,
		updated_at,
		title, 
		description,
		picture,
		open,
		COALESCE(threads_total.count, 0) as total_threads,
		COALESCE(posts_total.count, 0) as total_posts,
 		events.world_id,
		events.nano_id
	FROM events
	FULL JOIN threads_total ON threads_total.event_id = events.id
	FULL JOIN posts_total ON posts_total.event_id = events.id
);



CREATE FUNCTION get_world_data(url VARCHAR)
RETURNS SETOF full_world AS
$$
SELECT * FROM full_world WHERE url = $1
$$
LANGUAGE SQL

SELECT * FROM get_world_data('isekai_generico_para_degenerados');




CREATE OR REPLACE VIEW tagged_worlds_with_likes AS (
	SELECT 
		title, 
		url,
		picture,
		tags,
		COUNT(likes.id) AS total_likes
	FROM tagged_worlds AS worlds
	FULL JOIN likes on worlds.id = likes.world_id
	GROUP BY title, url, picture, tags
);



CREATE FUNCTION get_popular_worlds(qty integer DEFAULT 0)
RETURNS SETOF tagged_worlds_with_likes AS
$$
SELECT *
FROM tagged_worlds_with_likes
ORDER BY total_likes DESC
LIMIT qty
$$
LANGUAGE SQL






CREATE OR REPLACE VIEW trending_tagged_worlds_with_likes AS (
	SELECT 
		title, 
		url,
		picture,
		tags,
		COUNT(likes.id) AS total_likes
	FROM tagged_worlds AS worlds
	FULL JOIN likes on worlds.id = likes.world_id
	WHERE likes.created_at > NOW() - INTERVAL '48 HOUR'
	GROUP BY title, url, picture, tags
);

CREATE FUNCTION get_trending_worlds(qty integer DEFAULT 0)
RETURNS SETOF trending_tagged_worlds_with_likes AS
$$
SELECT *
FROM trending_tagged_worlds_with_likes
ORDER BY total_likes DESC
LIMIT qty
$$
LANGUAGE SQL






CREATE OR REPLACE VIEW posts_data AS (
	SELECT 
		posts.id, 
		posts.created_at,
		posts.updated_at,
		posts.user_id,
		threads.nano_id AS thread_url,
		events.nano_id AS event_url,
		events.title AS event_title,
		posts.contents,
		username,
		posts.world_id
	FROM posts
	JOIN events ON events.id = posts.event_id
	JOIN users ON users.id = posts.user_id
	JOIN threads ON threads.id = posts.thread_id
);


CREATE FUNCTION get_world_recent_posts(wid UUID, qty integer)
RETURNS SETOF posts_data AS
$$
SELECT *
FROM posts_data
WHERE world_id = wid
LIMIT qty
$$
LANGUAGE SQL






CREATE OR REPLACE VIEW event_data AS (
	SELECT 
		worlds.url AS world_url,
		worlds.title AS world_title,
		events.created_at,
		events.updated_at,
		events.open,
		events.title,
		events.description,
		events.picture,
		username,
		events.nano_id,
		events.id
	FROM events 
	FULL JOIN worlds ON worlds.id = events.world_id
	JOIN users ON users.id = events.user_id
);

CREATE OR REPLACE FUNCTION get_event_data(url VARCHAR(15))
RETURNS SETOF event_data AS
$$
SELECT *
FROM event_data
WHERE nano_id = get_event_data.url
$$
LANGUAGE SQL






CREATE OR REPLACE VIEW event_threads AS (
    WITH
	total_posts AS (
		SELECT thread_id, COUNT(*) FROM posts GROUP BY thread_id
	)
	SELECT
		threads.created_at,
		threads.updated_at,
		threads.nano_id AS url,
		threads.open,
		threads.guests_only,
		threads.contents,
		username,
		threads.event_id AS event_id,
		avatar AS user_avatar,
	    COALESCE(total_posts.count, 0) as total_posts
	FROM threads
	JOIN events ON events.id = threads.event_id
	JOIN users ON users.id = threads.user_id
    FULL JOIN total_posts ON total_posts.thread_id = threads.id
	ORDER BY threads.created_at DESC
);

CREATE OR REPLACE FUNCTION get_event_threads(id UUID, qty INTEGER)
RETURNS SETOF event_threads AS
$$
SELECT *
FROM event_threads
WHERE event_id = get_event_threads.id
LIMIT qty
$$
LANGUAGE SQL;




CREATE OR REPLACE VIEW thread_data AS (
    SELECT
        worlds.title AS world_title,
        worlds.url AS world_url,
        events.title AS event_title,
        events.nano_id AS event_url,
        threads.contents,
        threads.created_at,
        threads.updated_at,
        threads.open,
        threads.guests_only,
        users.username,
        users.avatar AS user_avatar,
		threads.id,
		threads.nano_id AS url
    FROM threads
    JOIN worlds ON worlds.id = threads.world_id
    JOIN events ON events.id = threads.event_id
    JOIN users ON users.id = threads.user_id
);

CREATE FUNCTION get_thread_data(url VARCHAR(15))
RETURNS SETOF thread_data AS
$$
SELECT *
FROM thread_data
WHERE thread_data.url = get_thread_data.url
$$
LANGUAGE SQL;


CREATE OR REPLACE VIEW thread_posts AS (
    SELECT
        posts.created_at,
        posts.updated_at,
        posts.contents,
        users.username,
        users.avatar AS user_avatar,
        posts.thread_id AS thread_id
    FROM posts
    JOIN users ON users.id = posts.user_id
	ORDER BY posts.created_at ASC
);


CREATE FUNCTION get_thread_posts(id UUID, qty INTEGER)
RETURNS SETOF thread_posts AS
$$
SELECT *
FROM thread_posts
WHERE thread_posts.thread_id = (id)
LIMIT (qty)
$$
LANGUAGE SQL;





CREATE OR REPLACE VIEW user_data AS (
    WITH
    post_count AS (SELECT COUNT(*), user_id FROM posts GROUP BY posts.user_id),
    character_count AS (SELECT COUNT(*), user_id FROM characters WHERE NOT pending GROUP BY characters.user_id)
    SELECT
		users.id
        users.created_at,
        username,
        avatar,
        bio,
        COALESCE(post_count.count, 0) AS total_posts,
        COALESCE(character_count.count, 0) AS total_characters
    FROM users
    FULL JOIN post_count ON post_count.user_id = users.id
    FULL JOIN character_count ON character_count.user_id = users.id
);


CREATE OR REPLACE FUNCTION get_user_data(username VARCHAR(50))
RETURNS SETOF user_data AS
$$
    SELECT *
    FROM user_data
    WHERE user_data.username = get_user_data.username
$$
LANGUAGE SQL;







CREATE OR REPLACE VIEW world_users_data AS (
    SELECT
        worlds.title,
        worlds.url,
        worlds.picture,
        worlds.tags,
        world_users.world_id,
        users.username
    FROM tagged_worlds AS worlds
    JOIN world_users ON world_users.world_id = worlds.id
    JOIN users ON users.id = world_users.user_id
);


CREATE OR REPLACE FUNCTION get_user_worlds(username VARCHAR(50))
RETURNS SETOF world_users_data AS
    $$
        SELECT * FROM world_users_data WHERE world_users_data.username = get_user_worlds.username
    $$
LANGUAGE SQL;



INSERT INTO friends (user_id, friend_id, pending)
VALUES ((SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM users WHERE username = 'lillia'), false);
INSERT INTO friends (user_id, friend_id)
VALUES ((SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM users WHERE username = 'yokoki'));
INSERT INTO friends (user_id, friend_id)
VALUES ((SELECT id FROM users WHERE username = 'lillia'), (SELECT id FROM users WHERE username = 'sett'));





CREATE OR REPLACE FUNCTION get_friend_count(uname VARCHAR(50))
RETURNS BIGINT AS
    $$
        SELECT COALESCE(COUNT(*), 0) AS friend_count
        FROM friends
        JOIN users ON users.id = friends.user_id OR users.id = friends.friend_id
        WHERE users.username = uname AND NOT friends.pending;
    $$
LANGUAGE SQL;




CREATE OR REPLACE FUNCTION get_user_friends(id UUID)
RETURNS TABLE(username VARCHAR(50), avatar VARCHAR(150), created_at TIMESTAMP WITH TIME ZONE) AS
    $$
    (SELECT
        users.username,
        users.avatar,
		friends.created_at
    FROM friends
    JOIN users ON users.id = friends.friend_id
    WHERE friends.user_id = get_user_friends.id AND NOT pending)
    UNION
    (SELECT
        users.username,
        users.avatar,
		friends.created_at
    FROM friends
    JOIN users ON users.id = friends.user_id
    WHERE friends.friend_id = get_user_friends.id AND NOT pending)
	ORDER BY created_at DESC
    $$
LANGUAGE SQL;



CREATE OR REPLACE FUNCTION get_user_posts(username VARCHAR(150))
RETURNS SETOF posts_data AS
    $$
        SELECT 
		posts_data.*,
		full_world.tags
		FROM posts_data WHERE posts_data.username = get_user_posts.username
		JOIN full_world ON full_world.id = posts_data.world_id
        ORDER BY created_at DESC
    $$
LANGUAGE SQL






CREATE OR REPLACE VIEW full_posts_data AS (
    SELECT
		posts.id,
		posts.created_at,
		posts.updated_at,
		posts.user_id,
		threads.nano_id AS thread_url,
		events.nano_id AS event_url,
		events.title AS event_title,
		posts.contents,
		username,
		posts.world_id,
		tagged_worlds.title AS world_title,
        tagged_worlds.tags AS world_tags,
        tagged_worlds.url AS world_url
	FROM posts
	JOIN events ON events.id = posts.event_id
	JOIN users ON users.id = posts.user_id
	JOIN threads ON threads.id = posts.thread_id
	JOIN tagged_worlds ON tagged_worlds.id = posts.world_id
);




CREATE OR REPLACE FUNCTION get_user_posts(username VARCHAR(150))
RETURNS SETOF full_posts_data AS
    $$
        SELECT *
		FROM full_posts_data
        WHERE full_posts_data.username = get_user_posts.username
        ORDER BY created_at DESC
    $$
LANGUAGE SQL





INSERT INTO user_follows (leader_id, follower_id)
VALUES
       ((SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM users WHERE username = 'lillia')),
       ((SELECT id FROM users WHERE username = 'lillia'), (SELECT id FROM users WHERE username = 'yokoki')),
       ((SELECT id FROM users WHERE username = 'lillia'), (SELECT id FROM users WHERE username = 'sett'));








CREATE OR REPLACE FUNCTION get_user_followers(id UUID)
RETURNS TABLE (username VARCHAR(50), avatar VARCHAR(150), created_at TIMESTAMP WITH TIME ZONE) AS
    $$
    SELECT
        users.username,
        users.avatar,
        user_follows.created_at
        FROM user_follows
        JOIN users ON users.id = user_follows.follower_id
        WHERE user_follows.leader_id = get_user_followers.id
        ORDER BY user_follows.created_at DESC
    $$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_user_following(id UUID)
RETURNS TABLE (username VARCHAR(50), avatar VARCHAR(150), created_at TIMESTAMP WITH TIME ZONE) AS
    $$
    SELECT
        users.username,
        users.avatar,
        user_follows.created_at
        FROM user_follows
        JOIN users ON users.id = user_follows.leader_id
        WHERE user_follows.follower_id = get_user_following.id
        ORDER BY user_follows.created_at DESC
    $$
LANGUAGE SQL;





CREATE OR REPLACE FUNCTION get_user_characters(id UUID)
RETURNS TABLE(created_at TIMESTAMP WITH TIME ZONE, name VARCHAR(50), nano_id VARCHAR(12), picture VARCHAR(150)) AS
    $$
    SELECT
        created_at,
        name,
        nano_id,
        picture
    FROM characters
    WHERE user_id = get_user_characters.id
    $$
LANGUAGE SQL;





CREATE OR REPLACE FUNCTION get_character(url VARCHAR(12))
RETURNS TABLE(id UUID, created_at TIMESTAMP WITH TIME ZONE, name VARCHAR(50), bio TEXT, picture VARCHAR(150)) AS
    $$
    SELECT
        id,
        created_at,
        name,
        bio,
        picture
    FROM characters
    WHERE nano_id = get_character.url
    $$
LANGUAGE SQL;







CREATE OR REPLACE VIEW character_data AS (
    SELECT 
        characters.created_at,
        characters.updated_at,
        characters.picture,
        characters.name,
        characters.bio,
        characters.nano_id,
        characters.pending,
        worlds.title AS world_title,
        worlds.url AS world_url,
        worlds.picture AS world_picture,
        worlds.tags AS world_tags,
        users.username
    FROM characters
    JOIN tagged_worlds AS worlds ON worlds.id = characters.world_id
    JOIN users ON users.id = characters.user_id
);

CREATE OR REPLACE FUNCTION get_character_data(url VARCHAR(12))
RETURNS SETOF character_data AS
    $$
        SELECT * 
        FROM character_data
        WHERE nano_id = get_character_data.url
    $$
LANGUAGE SQL






CREATE OR REPLACE FUNCTION get_user_in_world_data(uid UUID, wid UUID)
RETURNS TABLE(is_member BOOLEAN) AS
    $$
    SELECT
        COUNT(*)::INTEGER::BOOLEAN AS is_member
    FROM world_users
    WHERE world_users.world_id = wid AND world_users.user_id = uid
    $$
LANGUAGE SQL;





CREATE OR REPLACE FUNCTION get_user_in_world_moderation_data(uid UUID, wid UUID)
RETURNS TABLE(is_mod BOOLEAN, is_admin BOOLEAN) AS
    $$
    SELECT
        CASE WHEN world_moderation.is_admin IS FALSE THEN TRUE ELSE FALSE END AS is_mod,
        CASE WHEN world_moderation.is_admin IS TRUE THEN TRUE ELSE FALSE END AS is_admin
    FROM users
    LEFT JOIN world_moderation ON world_moderation.user_id = users.id
    WHERE world_moderation.world_id = wid AND users.id = uid
    $$
LANGUAGE SQL;





CREATE OR REPLACE FUNCTION get_world_moderation_data(wid UUID)
RETURNS TABLE(username VARCHAR(50), is_admin BOOLEAN) AS
    $$
    SELECT
        username,
        world_moderation.is_admin AS is_admin
    FROM world_moderation
    JOIN users ON users.id = world_moderation.user_id
    WHERE world_moderation.world_id = wid
    $$
LANGUAGE SQL;





CREATE OR REPLACE FUNCTION get_world_characters(wid UUID)
RETURNS SETOF characters AS
    $$
    SELECT *
    FROM characters
    WHERE world_id = wid
    $$
LANGUAGE SQL;






CREATE OR REPLACE FUNCTION get_user_in_world_like_data(uid UUID, wid UUID)
RETURNS TABLE(has_liked BOOLEAN) AS
    $$
    SELECT
        COUNT(*)::INTEGER::BOOLEAN AS has_liked
    FROM likes
    WHERE likes.world_id = wid AND likes.user_id = uid
    $$
LANGUAGE SQL;






CREATE OR REPLACE FUNCTION world_like_interaction(uid UUID, wid UUID, L BOOLEAN)
RETURNS VOID AS
    $$
        BEGIN
            IF L THEN
                INSERT INTO likes (user_id, post_id, world_id)
                VALUES (uid, null, wid);
            ELSE
                DELETE FROM likes
                WHERE user_id = uid AND world_id = wid;
            END IF;
        END
    $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION world_join_interaction(uid UUID, wid UUID, J BOOLEAN)
RETURNS VOID AS
    $$
        BEGIN
            IF J THEN
                INSERT INTO world_users (user_id, pending, world_id)
                VALUES (uid, false, wid);
            ELSE
                DELETE FROM world_users
                WHERE user_id = uid AND world_id = wid;
            END IF;
        END
    $$
LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION user_follow_interaction(lid UUID, fid UUID, F BOOLEAN)
RETURNS VOID AS
    $$
        BEGIN
            IF F THEN
                INSERT INTO user_follows (leader_id, follower_id)
                VALUES (lid, fid);
            ELSE
                DELETE FROM user_follows
                WHERE leader_id = lid AND follower_id = fid;
            END IF;
        END
    $$
LANGUAGE plpgsql;







CREATE OR REPLACE FUNCTION get_user_to_user_data(lid UUID, fid UUID)
RETURNS TABLE (is_following BOOLEAN, friends BOOLEAN) AS
    $$
        SELECT
            (
                SELECT COALESCE(COUNT(*), 0)::INTEGER::BOOLEAN
                FROM user_follows
                WHERE leader_id = lid AND follower_id = fid
            ) AS is_following,
           (
                SELECT COALESCE(COUNT(*), 0)::INTEGER::BOOLEAN
                FROM friends
                WHERE (user_id = lid AND friend_id = fid) OR (user_id = fid AND friend_id = lid) AND NOT pending
            ) AS friends
    $$
LANGUAGE SQL;