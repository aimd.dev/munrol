-- Create Table

CREATE TABLE users ( 
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	username VARCHAR(50) NOT NULL UNIQUE,
	email VARCHAR(50) NOT NULL UNIQUE,
	bio VARCHAR(400),
	avatar VARCHAR(150) DEFAULT 'https://munrol-static.s3.amazonaws.com/default.jpg',
 	password VARCHAR(100) NOT NULL,
	premium BOOLEAN DEFAULT false,
	verified BOOLEAN DEFAULT false,
	PRIMARY KEY (username)
);

-- Add User Function

CREATE OR REPLACE FUNCTION
insert_user(
	_username VARCHAR(50),
	_email VARCHAR(50),
	_password VARCHAR(100)
)
RETURNS TABLE(
	username VARCHAR(50), 
	email VARCHAR(40)
) AS
$$
	INSERT INTO users (username, email, password)
	VALUES (_username, _email, _password)
	RETURNING username, email;
$$
LANGUAGE SQL;

-- Update User Functions

CREATE OR REPLACE FUNCTION
update_user_email(_username VARCHAR(50), _email VARCHAR(50))
RETURNS VOID AS
$$
	UPDATE users
	SET email = _email
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
update_user_avatar(_username VARCHAR(50), _avatar VARCHAR(50))
RETURNS VOID AS
$$
	UPDATE users
	SET avatar = _avatar
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
update_user_bio(_username VARCHAR(50), _bio VARCHAR(50))
RETURNS VOID AS
$$
	UPDATE users
	SET bio = _bio
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
update_user_password(_username VARCHAR(50), _password VARCHAR(100))
RETURNS VOID AS
$$
	UPDATE users
	SET password = _password
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
update_user_birthday(_username VARCHAR(50), _birthday DATE)
RETURNS VOID AS
$$
	UPDATE users
	SET birthday = _birthday
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
deactivate_user(_username VARCHAR(50))
RETURNS VOID AS
$$
	UPDATE users
	SET active = false
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
change_account_type(_username VARCHAR(50), _premium BOOLEAN)
RETURNS VOID AS
$$
	UPDATE users
	SET premium = _premium
	WHERE username = _username;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION
verify_account(_username VARCHAR(50))
RETURNS VOID AS
$$
	UPDATE users
	SET verified = true
	WHERE username = _username;
$$
LANGUAGE SQL;

-- Get User By Username

CREATE OR REPLACE FUNCTION
get_user_by_username(_username VARCHAR(50))
RETURNS TABLE (
	username VARCHAR(50),
	email VARCHAR(40),
	bio VARCHAR(400),
	avatar VARCHAR(150),
	created_at TIMESTAMP WITH TIME ZONE,
	birthday DATE
) AS
$$
	SELECT username, email, bio, avatar, created_at, birthday
	FROM users
	WHERE username = _username;
$$
LANGUAGE SQL;

-- Get User Credentials

CREATE OR REPLACE FUNCTION
get_user_credentials(_username VARCHAR(50))
RETURNS TABLE(
	username VARCHAR(50),
	email VARCHAR(40),
	password VARCHAR(100),
	avatar VARCHAR(150)
) AS
$$
	SELECT username, email, password, avatar
	FROM users
	WHERE username = _username;
$$
LANGUAGE SQL;

-- Get User Avatar

CREATE OR REPLACE FUNCTION
get_user_avatar(_username VARCHAR(50))
RETURNS TABLE (
	avatar VARCHAR(150)
) AS
$$
	SELECT avatar FROM users WHERE username = _username;
$$
LANGUAGE SQL;