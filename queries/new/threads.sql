-- Create Table

CREATE TABLE threads (
    id VARCHAR(12) NOT NULL UNIQUE,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_update TIMESTAMP WITH TIME ZONE,
    last_edit TIMESTAMP WITH TIME ZONE,
	title VARCHAR(100) NOT NULL,
    summary VARCHAR(600) NOT NULL,
	body TEXT NOT NULL,
    open BOOLEAN DEFAULT true,
    restricted BOOLEAN,
    author VARCHAR(50) REFERENCES users (username) ON DELETE SET NULL,
    PRIMARY KEY (id)
);



-- Insert Thread FUNCTION

CREATE OR REPLACE FUNCTION
create_thread(
	_id VARCHAR(12),
	_title VARCHAR(100),
    _summary VARCHAR(600),
    _restricted BOOLEAN,
	_body TEXT,
    _author VARCHAR(50)
)
RETURNS SETOF threads AS
$$
	INSERT INTO threads (id, title, summary, restricted, body, author)
	VALUES (_id, _title, _summary, _restricted, _body, _author)
    RETURNING *;
$$
LANGUAGE SQL;



-- Update Thread

CREATE OR REPLACE FUNCTION
update_thread(
	_id VARCHAR(12),
	_title VARCHAR(100),
    _summary VARCHAR(600),
    _restricted BOOLEAN,
	_body TEXT,
    _author VARCHAR(50)
)
RETURNS VOID AS
$$
	UPDATE threads
	SET title = _title,
        summary = _summary,
        restricted = _restricted,
        body = _body
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;

-- Update Thread Status

CREATE OR REPLACE FUNCTION
update_thread_status(
	_id VARCHAR(12),
	_open BOOLEAN,
    _author VARCHAR(50)
)
RETURNS VOID AS
$$
	UPDATE threads
	SET open = _open
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;

-- Update Thread Restrictions

CREATE OR REPLACE FUNCTION
update_thread_restrictions(
	_id VARCHAR(12),
	_restricted BOOLEAN,
    _author VARCHAR(50)
)
RETURNS VOID AS
$$
	UPDATE threads
	SET restricted = _restricted
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;



-- Get Thread By Id

CREATE OR REPLACE VIEW v_threads AS (
    SELECT
        threads.id,
        threads.created_at,
        threads.last_edit,
        threads.last_update,
        title,
        summary,
        body,
        open,
        restricted,
        author,
        users.avatar
    FROM threads
    JOIN users ON threads.author = users.username
);

CREATE OR REPLACE FUNCTION
get_thread_by_id( _id VARCHAR(12), _username VARCHAR(50) )
RETURNS TABLE (
    id VARCHAR(12),
    created_at TIMESTAMP WITH TIME ZONE,
    last_update TIMESTAMP WITH TIME ZONE,
    last_edit TIMESTAMP WITH TIME ZONE,
    title VARCHAR(100),
    summary VARCHAR(600),
    body TEXT,
    open BOOLEAN,
    restricted BOOLEAN,
    author VARCHAR(50),
    avatar VARCHAR(150),
    saved BOOLEAN,
    fav BOOLEAN
) AS
$$
	SELECT 
        v_threads.*,
        EXISTS(SELECT id FROM saved_threads WHERE element = _id AND username = _username) AS saved,
        EXISTS(SELECT id FROM fav_threads WHERE element = _id AND username = _username) AS fav
    FROM v_threads
    LEFT JOIN saved_threads ON saved_threads.element = v_threads.id AND saved_threads.username = _username
    LEFT JOIN fav_threads ON fav_threads.element = v_threads.id AND saved_threads.username = _username
    WHERE v_threads.id = _id;
$$
LANGUAGE SQL;

-- Get Threads By User

CREATE OR REPLACE FUNCTION
get_threads_by_user( _author VARCHAR(50) )
RETURNS SETOF threads AS
$$
	SELECT * FROM threads
    WHERE author = _author
    ORDER BY created_at DESC;
$$
LANGUAGE SQL;

-- Get Threads By Title Filtering






-- DROP FUNCTION get_threads_by_user;
-- DROP FUNCTION get_thread_by_id;
-- DROP FUNCTION update_thread_restrictions;
-- DROP FUNCTION update_thread_status;
-- DROP FUNCTION update_thread;
-- DROP FUNCTION create_thread;
-- DROP TABLE threads;




-- Infinite Scrolling (New Threads)

CREATE OR REPLACE FUNCTION get_new_threads(
    _start TIMESTAMP WITH TIME ZONE,
    _username VARCHAR(50),
    _limit INT,
    _offset INT
)
RETURNS TABLE (
    id VARCHAR(12),
    created_at TIMESTAMP WITH TIME ZONE,
    last_update TIMESTAMP WITH TIME ZONE,
    last_edit TIMESTAMP WITH TIME ZONE,
    title VARCHAR(100),
    summary VARCHAR(600),
    body TEXT,
    open BOOLEAN,
    restricted BOOLEAN,
    author VARCHAR(50),
    avatar VARCHAR(150),
    saved BOOLEAN,
    fav BOOLEAN
) AS
$$
    SELECT 
        v_threads.*,
        EXISTS(SELECT id FROM saved_threads WHERE element = v_threads.id AND username = _username) AS saved,
        EXISTS(SELECT id FROM fav_threads WHERE element = v_threads.id AND username = _username) AS fav 
    FROM v_threads
    LEFT JOIN saved_threads ON saved_threads.element = v_threads.id AND saved_threads.username = _username
    LEFT JOIN fav_threads ON fav_threads.element = v_threads.id AND saved_threads.username = _username
    WHERE v_threads.created_at < _start
    ORDER BY v_threads.created_at DESC
    LIMIT _limit OFFSET _offset;
$$
LANGUAGE SQL;

-- Infinite Scrolling (Updated Threads)

CREATE OR REPLACE FUNCTION get_active_threads(
    _start TIMESTAMP WITH TIME ZONE,
    _username VARCHAR(50),
    _limit INT,
    _offset INT
)
RETURNS TABLE (
    id VARCHAR(12),
    created_at TIMESTAMP WITH TIME ZONE,
    last_update TIMESTAMP WITH TIME ZONE,
    last_edit TIMESTAMP WITH TIME ZONE,
    title VARCHAR(100),
    summary VARCHAR(600),
    body TEXT,
    open BOOLEAN,
    restricted BOOLEAN,
    author VARCHAR(50),
    avatar VARCHAR(150),
    saved BOOLEAN,
    fav BOOLEAN
) AS
$$
    SELECT 
        v_threads.*,
        EXISTS(SELECT id FROM saved_threads WHERE element = v_threads.id AND username = _username) AS saved,
        EXISTS(SELECT id FROM fav_threads WHERE element = v_threads.id AND username = _username) AS fav 
    FROM v_threads
    LEFT JOIN saved_threads ON saved_threads.element = v_threads.id AND saved_threads.username = _username
    LEFT JOIN fav_threads ON fav_threads.element = v_threads.id AND saved_threads.username = _username
    WHERE v_threads.last_update < _start
    ORDER BY v_threads.last_update DESC
    LIMIT _limit OFFSET _offset;
$$
LANGUAGE SQL;