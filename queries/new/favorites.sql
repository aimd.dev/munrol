-- Create Tables fav

CREATE TABLE fav_threads (
    id SERIAL PRIMARY KEY,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    element VARCHAR(12) REFERENCES threads (id) ON DELETE CASCADE,
    UNIQUE (element, username)
);

CREATE TABLE fav_replies (
    id SERIAL PRIMARY KEY,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    element VARCHAR(12) REFERENCES replies (id) ON DELETE CASCADE,
    UNIQUE (element, username)
);

-- Insert 

CREATE OR REPLACE FUNCTION insert_fav_thread(
    _username VARCHAR(50),
    _element VARCHAR(12)
)
RETURNS VOID AS
$$
    INSERT INTO fav_threads (username, element)
    VALUES (_username, _element);
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION insert_fav_reply(
    _username VARCHAR(50),
    _element VARCHAR(12)
)
RETURNS VOID AS
$$
    INSERT INTO fav_replies (username, element)
    VALUES (_username, _element);
$$
LANGUAGE SQL;

-- Delete 

CREATE OR REPLACE FUNCTION delete_fav_thread(
    _username VARCHAR(50),
    _element VARCHAR(12)
)
RETURNS VOID AS
$$
    DELETE FROM fav_threads
    WHERE username = _username AND element = _element;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION delete_fav_reply(
    _username VARCHAR(50),
    _element VARCHAR(12)
)
RETURNS VOID AS
$$
    DELETE FROM fav_replies
    WHERE username = _username AND element = _element;
$$
LANGUAGE SQL;