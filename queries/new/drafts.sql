CREATE TABLE thread_drafts (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    title VARCHAR(100) NOT NULL,
    summary VARCHAR(600) NOT NULL,
	body TEXT NOT NULL
);

CREATE TABLE reply_drafts (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    body TEXT
);

-- Insert

CREATE OR REPLACE FUNCTION insert_thread_draft(
    _author VARCHAR(50),
    _title VARCHAR(100),
    _summary VARCHAR(600),
    _body TEXT
)
RETURNS TABLE (
    id INT,
    created_at TIMESTAMP WITH TIME ZONE,
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50),
    title VARCHAR(100),
    summary VARCHAR(600),
	body TEXT
) AS
$$
    INSERT INTO thread_drafts (author, title, summary, body)
    VALUES (_author, _title, _summary, _body)
    RETURNING *;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION insert_reply_draft(
    _author VARCHAR(50),
    _body TEXT
)
RETURNS TABLE (
    id INT,
    created_at TIMESTAMP WITH TIME ZONE,
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50),
    body TEXT
) AS
$$
    INSERT INTO reply_drafts (author, body)
    VALUES (_author, _body)
    RETURNING *;
$$
LANGUAGE SQL;

-- Update

CREATE OR REPLACE FUNCTION update_thread_draft(
    _id INT,
    _last_updated TIMESTAMP WITH TIME ZONE,
    _author VARCHAR(50),
    _title VARCHAR(100),
    _summary VARCHAR(600),
    _body TEXT
)
RETURNS TABLE (
    id INT,
    created_at TIMESTAMP WITH TIME ZONE,
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50),
    title VARCHAR(100),
    summary VARCHAR(600),
	body TEXT
) AS
$$
    UPDATE thread_drafts
    SET
        last_updated = _last_updated,
        title = _title,
        summary = _summary,
        body = _body
    WHERE id = _id AND author = _author
    RETURNING *;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION update_reply_draft(
    _id INT,
    _last_updated TIMESTAMP WITH TIME ZONE,
    _author VARCHAR(50),
    _body TEXT
)
RETURNS VOID AS
$$
    UPDATE reply_drafts
    SET
        last_updated = _last_updated,
        body = _body
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;

-- Delete

CREATE OR REPLACE FUNCTION delete_thread_draft(
    _id INT,
    _author VARCHAR(50)
)
RETURNS VOID AS
$$
    DELETE FROM thread_drafts
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION delete_reply_draft(
    _id INT,
    _author VARCHAR(50)
)
RETURNS VOID AS
$$
    DELETE FROM reply_drafts
    WHERE id = _id AND author = _author;
$$
LANGUAGE SQL;

-- Get By Author

CREATE OR REPLACE FUNCTION get_thread_drafts(
    _author VARCHAR(50)
)
RETURNS TABLE (
    id INT,
    created_at TIMESTAMP WITH TIME ZONE,
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50),
    title VARCHAR(100),
    summary VARCHAR(600),
	body TEXT
) AS
$$
    SELECT * FROM thread_drafts
    WHERE author = _author;
$$
LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_reply_drafts(
    _author VARCHAR(50)
)
RETURNS TABLE (
    id INT,
    created_at TIMESTAMP WITH TIME ZONE,
    last_updated TIMESTAMP WITH TIME ZONE,
    author VARCHAR(50),
    body TEXT
) AS
$$
    SELECT * FROM reply_drafts
    WHERE author = _author;
$$
LANGUAGE SQL;
