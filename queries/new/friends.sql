DROP FUNCTION insert_friendship;
DROP FUNCTION accept_friendship;
DROP FUNCTION remove_friendship;
DROP FUNCTION get_friendships_by_user;
DROP TABLE friendships;

CREATE TABLE friendships (
    id VARCHAR(12) NOT NULL UNIQUE,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    friend VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    befriends VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    pending BOOLEAN DEFAULT true,
    seen BOOLEAN DEFAULT false,
    PRIMARY KEY (id),
    UNIQUE (friend, befriends)
);

-- Insert Friendship

CREATE OR REPLACE FUNCTION
insert_friendship(
	_id VARCHAR(12),
	_friend VARCHAR(50),
	_befriends VARCHAR(50)
)
RETURNS VOID AS
$$
	INSERT INTO friendships (id, friend, befriends)
	VALUES (_id, _friend, _befriends);
$$
LANGUAGE SQL;

-- Accept Friendship

CREATE OR REPLACE FUNCTION
accept_friendship(
	_id VARCHAR(12),
    _username VARCHAR(50)
)
RETURNS SETOF friendships AS
$$
	UPDATE friendships
	SET pending = false
    WHERE id = _id AND _username = befriends
    RETURNING *;
$$
LANGUAGE SQL;

-- Remove Friendship

CREATE OR REPLACE FUNCTION
remove_friendship(
	_id VARCHAR(12),
    _username VARCHAR(50)
)
RETURNS VOID AS
$$
	DELETE FROM friendships
	WHERE id = _id AND (_username = friend OR _username = befriends);
$$
LANGUAGE SQL;

-- Get Friendship Requests by User

CREATE OR REPLACE FUNCTION
get_friendship_requests(
	_username VARCHAR(50)
)
RETURNS TABLE(
    id VARCHAR(12),
    created_at TIMESTAMP WITH TIME ZONE,
	username VARCHAR(50),
	avatar VARCHAR(150),
    seen BOOLEAN
) AS
$$
	SELECT
        friendships.id,
        friendships.created_at,
        users.username,
	    users.avatar,
        friendships.seen
    FROM friendships
    JOIN users ON users.username = friendships.friend
    WHERE friendships.befriends = _username AND friendships.pending
$$
LANGUAGE SQL

-- Get Friendships by User

CREATE OR REPLACE FUNCTION
get_friendships_by_user(
	_username VARCHAR(50)
)
RETURNS TABLE(
    id VARCHAR(12),
	username VARCHAR(50),
	avatar VARCHAR(150)
) AS
$$
SELECT
    friendships.id,
    users.username,
    users.avatar
FROM friendships
JOIN users ON users.username = friendships.friend OR users.username = friendships.befriends
WHERE 
    (friendships.friend = _username OR friendships.befriends = _username) 
    AND users.username != _username 
    AND NOT friendships.pending;
$$
LANGUAGE SQL;

-- Check Friendship

CREATE OR REPLACE FUNCTION
check_friendship(
    _username1 VARCHAR(50),
    _username2 VARCHAR(50)
)
RETURNS SETOF friendships AS
$$
    SELECT * FROM friendships
    WHERE (friend = _username1 AND befriends = _username2) OR (friend = _username2 AND befriends = _username1) AND NOT PENDING;
$$
LANGUAGE SQL;

-- Get Request Alerts

CREATE OR REPLACE FUNCTION
get_friendship_requests_alerts(
    _username VARCHAR(50)
)
RETURNS TABLE (count BIGINT) AS
$$
    SELECT COUNT(*) as count
    FROM friendships 
    WHERE befriends = _username AND pending AND NOT seen;
$$
LANGUAGE SQL;

-- Clear Request Alerts

CREATE OR REPLACE FUNCTION
clear_friendship_requests_alerts(
    _username VARCHAR(50)
)
RETURNS VOID AS
$$
    UPDATE friendships
    SET seen = true
    WHERE befriends = _username AND pending AND NOT seen;
$$
LANGUAGE SQL;