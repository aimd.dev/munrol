-- Create Table

CREATE TABLE participants (
    id SERIAL PRIMARY KEY,
    thread VARCHAR(12) REFERENCES threads (id) ON DELETE CASCADE,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    UNIQUE (thread, username)
);

-- Insert Participant

CREATE OR REPLACE FUNCTION
insert_participant(
	_thread VARCHAR(100),
	_username VARCHAR(50)
)
RETURNS VOID AS
$$
	INSERT INTO participants (thread, username)
	VALUES (_thread, _username);
$$
LANGUAGE SQL;

-- Remove Participant

CREATE OR REPLACE FUNCTION
remove_participant(
	_id INT
)
RETURNS VOID AS
$$
	DELETE FROM participants
	WHERE id = _id;
$$
LANGUAGE SQL;

-- Get Thread Participants

CREATE OR REPLACE FUNCTION
get_thread_participants(
	_id VARCHAR(12)
)
RETURNS TABLE(
    id INT,
	username VARCHAR(50),
	avatar VARCHAR(150)
) AS
$$
	SELECT
	    participants.id,
	    users.username,
	    users.avatar
	FROM participants
    JOIN users ON users.username = participants.username
    WHERE thread = _id
$$
LANGUAGE SQL;