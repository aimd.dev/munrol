-- Create Table

CREATE TABLE verification_codes (
    id SERIAL PRIMARY KEY,
    code VARCHAR(120),
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    UNIQUE (code, username)
);

-- Insert Code

CREATE OR REPLACE FUNCTION insert_verification_code(
    _username VARCHAR(50),
    _code VARCHAR(120)
)
RETURNS VOID AS
$$
    INSERT INTO verification_codes (username, code)
    VALUES (_username, _code);
$$
LANGUAGE SQL;

-- Get User Code

CREATE OR REPLACE FUNCTION get_verification_code(
    _username VARCHAR(50)
)
RETURNS TABLE (
    email VARCHAR(40),
    code VARCHAR(120)
) AS
$$
    SELECT 
        users.email, 
        verification_codes.code
    FROM verification_codes
    JOIN users ON users.username = verification_codes.username
    WHERE verification_codes.username = _username;
$$
LANGUAGE SQL;

-- Delete Code

CREATE OR REPLACE FUNCTION delete_verification_code(
    _username VARCHAR(50)
)
RETURNS VOID AS
$$
    DELETE FROM verification_codes
    WHERE username = _username;
$$
LANGUAGE SQL;



-- drop function delete_verification_code;
-- drop function get_verification_code;
-- drop function insert_verification_code;
-- drop table verification_codes;