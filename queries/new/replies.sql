-- Create Table

CREATE TABLE replies (
    id VARCHAR(12) NOT NULL UNIQUE,
    thread VARCHAR(12) REFERENCES threads (id) ON DELETE SET NULL,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    last_edit TIMESTAMP WITH TIME ZONE,
    approval_date TIMESTAMP WITH TIME ZONE,
	body TEXT NOT NULL,
    author VARCHAR(50) REFERENCES users (username) ON DELETE SET NULL,
    approved BOOLEAN,
    seen BOOLEAN DEFAULT false,
    hidden BOOLEAN DEFAULT false,
    PRIMARY KEY (id)
);

-- Insert Reply FUNCTION

CREATE OR REPLACE FUNCTION
insert_reply(
	_id VARCHAR(12),
	_thread VARCHAR(100),
	_body TEXT,
    _author VARCHAR(50),
    _approval_date TIMESTAMP WITH TIME ZONE,
    _approved BOOLEAN
)
RETURNS VOID AS
$$
	INSERT INTO replies (id, thread, approval_date, body, author, approved)
	VALUES (_id, _thread, _approval_date, _body, _author, _approved);
$$
LANGUAGE SQL;

-- Approve Reply

CREATE OR REPLACE FUNCTION
approve_reply(
	_id VARCHAR(12),
    _approval_date TIMESTAMP WITH TIME ZONE
)
RETURNS VOID AS
$$
    UPDATE replies
    SET 
        approved = true, 
        approval_date = _approval_date
    WHERE id = _id;
$$
LANGUAGE SQL;

-- Reject Reply

CREATE OR REPLACE FUNCTION
reject_reply(
	_id VARCHAR(12)
)
RETURNS VOID AS
$$
    DELETE FROM replies
    WHERE id = _id;
$$
LANGUAGE SQL;

-- Replies View

CREATE OR REPLACE VIEW v_replies AS (
    SELECT
        replies.id,
        thread,
        replies.created_at,
        replies.last_edit,
        replies.approval_date,
        body,
        approved,
        author,
        users.avatar
    FROM replies
    JOIN users ON replies.author = users.username
);

-- Get Approved Replies by Thread

CREATE OR REPLACE FUNCTION
get_replies_by_thread( _id VARCHAR(12), _username VARCHAR(50) )
RETURNS TABLE (
    id VARCHAR(12),
    thread VARCHAR(12),
    created_at TIMESTAMP WITH TIME ZONE,
    last_edit TIMESTAMP WITH TIME ZONE,
    approval_date TIMESTAMP WITH TIME ZONE,
    body TEXT,
    approved BOOLEAN,
    author VARCHAR(50),
    avatar VARCHAR(50),
    saved BOOLEAN,
    fav BOOLEAN
) AS
$$
	SELECT 
        v_replies.*,
        EXISTS(SELECT id FROM saved_replies WHERE element = _id AND username = _username) AS saved,
        EXISTS(SELECT id FROM fav_replies WHERE element = _id AND username = _username) AS fav 
    FROM v_replies
    LEFT JOIN saved_replies ON saved_replies.element = v_replies.id AND saved_replies.username = _username
    LEFT JOIN fav_replies ON fav_replies.element = v_replies.id AND saved_replies.username = _username
    WHERE thread = _id AND approved
    ORDER BY approval_date;
$$
LANGUAGE SQL;

-- Reply Requests View

-- drop function get_reply_requests_by_author;
-- drop view v_reply_requests;

CREATE OR REPLACE VIEW v_reply_requests AS (
    SELECT
        replies.id as reply_id,
        thread,
        replies.created_at,
        replies.body,
        threads.author as thread_author,
        threads.id as thread_id,
        threads.title,
        users.avatar,
        replies.author as reply_author,
        replies.seen
    FROM replies
    JOIN users ON replies.author = users.username
    JOIN threads ON threads.id = replies.thread
    WHERE NOT approved
);

-- Get Reply Requests by Author

CREATE OR REPLACE FUNCTION
get_reply_requests_by_author( _username VARCHAR(50) )
RETURNS SETOF v_reply_requests AS
$$
	SELECT * FROM v_reply_requests
    WHERE thread_author = _username
    ORDER BY created_at;
$$
LANGUAGE SQL;

-- select * from get_reply_requests_by_author('Lillia');

-- Get Request Alerts

CREATE OR REPLACE FUNCTION
get_reply_requests_alerts(
    _username VARCHAR(50)
)
RETURNS TABLE (count BIGINT) AS
$$
    SELECT COUNT(*) as count
    FROM v_reply_requests
    WHERE thread_author = _username AND NOT seen;
$$
LANGUAGE SQL;

-- Clear Request Alerts

CREATE OR REPLACE FUNCTION
clear_reply_requests_alerts(
    _username VARCHAR(50)
)
RETURNS VOID AS
$$
    UPDATE replies
    SET seen = true
    FROM threads
    WHERE threads.author = _username AND NOT seen
$$
LANGUAGE SQL;