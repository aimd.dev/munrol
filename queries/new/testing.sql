CREATE TABLE participants (
    id VARCHAR(12) NOT NULL UNIQUE,
    thread VARCHAR(12) REFERENCES threads (id) ON DELETE CASCADE,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION
insert_participant(
	_id VARCHAR(12),
	_thread VARCHAR(100),
	_username VARCHAR(50)
)
RETURNS VOID AS
$$
	INSERT INTO participants (id, thread, username)
	VALUES (_id, _thread, _username);
$$
LANGUAGE SQL;