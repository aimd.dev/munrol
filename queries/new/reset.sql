-- Create Table

CREATE TABLE reset_codes (
    code VARCHAR(5) PRIMARY KEY,
 	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    username VARCHAR(50) REFERENCES users (username) ON DELETE CASCADE,
    UNIQUE (code, username)
);

-- Insert Code

CREATE OR REPLACE FUNCTION insert_reset_code(
    _username VARCHAR(50),
    _code VARCHAR(5)
)
RETURNS VOID AS
$$
    INSERT INTO reset_codes (username, code)
    VALUES (_username, _code);
$$
LANGUAGE SQL;

-- Get User Code

CREATE OR REPLACE FUNCTION get_reset_code(
    _username VARCHAR(50)
)
RETURNS TABLE (
    email VARCHAR(40),
    code VARCHAR(5),
    created_at TIMESTAMP WITH TIME ZONE
) AS
$$
    SELECT 
        users.email, 
        reset_codes.code,
        reset_codes.created_at
    FROM reset_codes
    JOIN users ON users.username = reset_codes.username
    WHERE reset_codes.username = _username;
$$
LANGUAGE SQL;

-- Delete Code

CREATE OR REPLACE FUNCTION delete_reset_code(
    _username VARCHAR(50)
)
RETURNS VOID AS
$$
    DELETE FROM reset_codes
    WHERE username = _username;
$$
LANGUAGE SQL;

-- drop function delete_reset_code;
-- drop function get_reset_code;
-- drop function insert_reset_code;
-- drop table reset_codes;