SELECT 
    username, 
    created_at 
FROM users
ORDER BY created_at DESC;