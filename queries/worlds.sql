/* 
SELECT * FROM characters
JOIN worlds ON characters.world = worlds.id

SELECT * FROM characters JOIN worlds ON characters.world = worlds.id WHERE characters.id = '457ed55d-3f74-4940-bc6a-03e7304825b9';
*/

create table worlds (
	id UUID DEFAULT gen_random_uuid(),
	w_url VARCHAR(50),
	title VARCHAR(50),
	description TEXT,
	w_picture VARCHAR(50),
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	PRIMARY KEY (id)
);

CREATE TABLE characters (
	id UUID DEFAULT gen_random_uuid(),
	name VARCHAR(80) NOT NULL,
	picture VARCHAR(200),
	PRIMARY KEY (id),
	world UUID REFERENCES worlds (id)
);

INSERT INTO characters (name, picture, world) VALUES (
'Araragi Sakura', 
'https://pbs.twimg.com/media/EQknjA8XUAEH38a?format=jpg&name=4096x4096', 
'4108db4b-f465-4892-81d6-b11592d87e5c');

insert into worlds (w_url, title, description, w_picture) values ('lkeelan0', 'Polarised client-server customer loyalty', 'felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla', 'http://dummyimage.com/1920x819.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('xmaycock1', null, 'in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel', null);
insert into worlds (w_url, title, description, w_picture) values ('iboon2', 'Multi-tiered heuristic emulation', null, 'http://dummyimage.com/1920x887.png/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('idun3', 'Function-based exuding analyzer', 'donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis', 'http://dummyimage.com/1920x823.png/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('glagadu4', 'Future-proofed multi-state parallelism', null, 'http://dummyimage.com/1920x893.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('sbrave5', 'Seamless fresh-thinking application', null, 'http://dummyimage.com/1920x806.bmp/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('hwickenden6', 'Managed multi-state hierarchy', 'magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi', 'http://dummyimage.com/1920x802.jpg/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('vales7', 'Robust explicit encoding', 'massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer', 'http://dummyimage.com/1920x881.bmp/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('edavitti8', 'Managed secondary model', null, 'http://dummyimage.com/1920x811.jpg/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('sdigiacomettino9', 'Cross-group contextually-based benchmark', 'nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam', 'http://dummyimage.com/1920x805.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('grevelanda', null, 'scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi', null);
insert into worlds (w_url, title, description, w_picture) values ('estanlackb', 'Cross-platform high-level architecture', 'mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in', 'http://dummyimage.com/1920x895.jpg/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('mgolbornc', null, 'in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam', null);
insert into worlds (w_url, title, description, w_picture) values ('hansteysd', 'Upgradable transitional infrastructure', null, 'http://dummyimage.com/1920x900.png/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('rthezee', null, null, null);
insert into worlds (w_url, title, description, w_picture) values ('gwimpressf', 'Cloned radical extranet', 'proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus', 'http://dummyimage.com/1920x824.bmp/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('spallaschg', 'Triple-buffered tangible open architecture', null, 'http://dummyimage.com/1920x842.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('debertzh', 'Visionary discrete model', 'et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non', 'http://dummyimage.com/1920x813.png/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('talstoni', 'Virtual 5th generation support', null, 'http://dummyimage.com/1920x854.jpg/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('mrozecj', 'Cloned methodical adapter', null, 'http://dummyimage.com/1920x835.bmp/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('jgoodingk', 'Profound logistical monitoring', null, 'http://dummyimage.com/1920x832.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('gwhicherl', 'Sharable exuding algorithm', 'sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend', 'http://dummyimage.com/1920x872.jpg/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('gmackleym', 'Secured even-keeled customer loyalty', 'vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec', 'http://dummyimage.com/1920x850.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('gbarnsdalen', 'Phased exuding hierarchy', 'dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue', 'http://dummyimage.com/1920x893.bmp/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('cellinoro', 'Intuitive logistical open architecture', 'eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus', 'http://dummyimage.com/1920x887.bmp/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('aelletsonp', 'Ergonomic composite data-warehouse', null, 'http://dummyimage.com/1920x858.bmp/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('wtomekq', 'Digitized intermediate paradigm', null, 'http://dummyimage.com/1920x877.jpg/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('mnairyr', 'User-friendly asynchronous productivity', 'viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus', 'http://dummyimage.com/1920x885.png/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('tcliffts', 'Switchable incremental productivity', null, 'http://dummyimage.com/1920x833.png/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('amitrikhint', 'Horizontal next generation collaboration', null, 'http://dummyimage.com/1920x848.png/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('fbroweru', 'Business-focused transitional knowledge base', null, 'http://dummyimage.com/1920x859.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('bjullianv', null, null, null);
insert into worlds (w_url, title, description, w_picture) values ('fallsoppw', 'Up-sized discrete throughput', null, 'http://dummyimage.com/1920x842.bmp/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('mshimmanx', 'Optional zero tolerance synergy', 'mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget', 'http://dummyimage.com/1920x813.png/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('rpopleyy', null, null, null);
insert into worlds (w_url, title, description, w_picture) values ('ssutherlandz', 'Organized 24 hour firmware', 'volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus', 'http://dummyimage.com/1920x830.png/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('cmattiussi10', 'Pre-emptive 5th generation utilisation', 'libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis', 'http://dummyimage.com/1920x856.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('bcometson11', null, null, null);
insert into worlds (w_url, title, description, w_picture) values ('fjinkin12', 'Inverse full-range contingency', null, 'http://dummyimage.com/1920x871.png/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('uhars13', 'Automated foreground model', 'tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere', 'http://dummyimage.com/1920x827.jpg/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('nmacaindreis14', 'Optimized local challenge', 'in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc', 'http://dummyimage.com/1920x836.png/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('cdyet15', 'Reactive multimedia info-mediaries', 'platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla', 'http://dummyimage.com/1920x806.bmp/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('sbusch16', 'Open-source empowering encoding', null, 'http://dummyimage.com/1920x864.jpg/ff4444/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('nwallman17', 'Persevering bifurcated alliance', null, 'http://dummyimage.com/1920x845.png/dddddd/000000');
insert into worlds (w_url, title, description, w_picture) values ('svannuchi18', 'Multi-channelled heuristic collaboration', null, 'http://dummyimage.com/1920x869.png/5fa2dd/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('mearthfield19', null, 'pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et', null);
insert into worlds (w_url, title, description, w_picture) values ('jbaldree1a', 'Devolved human-resource project', 'dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia', 'http://dummyimage.com/1920x831.jpg/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('scockayne1b', 'Synergistic responsive knowledge base', null, 'http://dummyimage.com/1920x856.bmp/cc0000/ffffff');
insert into worlds (w_url, title, description, w_picture) values ('hhatchette1c', null, 'mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac', null);
insert into worlds (w_url, title, description, w_picture) values ('bbeeden1d', 'Re-engineered national toolset', null, 'http://dummyimage.com/1920x870.jpg/cc0000/ffffff');
