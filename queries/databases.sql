CREATE TABLE users ( 
	id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	username VARCHAR(50) NOT NULL UNIQUE,
	email VARCHAR(50) NOT NULL UNIQUE,
	bio VARCHAR(400),
	avatar VARCHAR(150),
    password VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE worlds (
	id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    url VARCHAR(50) NOT NULL UNIQUE,
	title VARCHAR(50) NOT NULL,
	description VARCHAR(400) NOT NULL,
	picture VARCHAR(150),
    created_by VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE characters (
	id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    nano_id VARCHAR(10) NOT NULL UNIQUE,
	name VARCHAR(50) NOT NULL,
    bio TEXT,
	picture VARCHAR(150),
	PRIMARY KEY (id),
    user_id UUID REFERENCES users (id) ON DELETE SET NULL,
	world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE
);

CREATE TABLE events (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    nano_id VARCHAR(10) NOT NULL UNIQUE,
    title VARCHAR(70) NOT NULL,
    description TEXT NOT NULL,
    picture VARCHAR(150),
    open BOOLEAN DEFAULT true,
    user_id UUID REFERENCES users (id) ON DELETE SET NULL,
    world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE threads (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    nano_id VARCHAR(10) NOT NULL UNIQUE,
    open BOOLEAN DEFAULT true,
    guests_only BOOLEAN DEFAULT false,
    contents TEXT NOT NULL,
    PRIMARY KEY (id),
    user_id UUID REFERENCES users (id) ON DELETE SET NULL,
    event_id UUID NOT NULL REFERENCES events (id) ON DELETE CASCADE,
    world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE
);

CREATE TABLE posts (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    contents TEXT NOT NULL,
    user_id UUID REFERENCES users (id) ON DELETE SET NULL,
    thread_id UUID NOT NULL REFERENCES threads (id) ON DELETE CASCADE,
    event_id UUID NOT NULL REFERENCES events (id) ON DELETE CASCADE,
    world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE comments (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    contents TEXT NOT NULL,
    user_id UUID REFERENCES users (id) ON DELETE SET NULL,
    post_id UUID NOT NULL REFERENCES posts (id) ON DELETE CASCADE,
    world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE
);

CREATE TABLE likes (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    post_id UUID REFERENCES posts (id) ON DELETE CASCADE,
    world_id UUID REFERENCES worlds (id) ON DELETE CASCADE,
    CHECK (
        COALESCE(post_id IS NOT NULL::INTEGER)
         +
         COALESCE(world_id IS NOT NULL::INTEGER)
        = 1
    ),
    UNIQUE (user_id, post_id),
    UNIQUE (user_id, world_id)
);

-- CREATE TABLE world_likes (
--     id UUID DEFAULT gen_random_uuid(),
--     created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
--     user_id UUID REFERENCES users (id) ON DELETE SET NULL,
--     world_id UUID NOT NULL REFERENCES worlds (id) ON DELETE CASCADE,
--     UNIQUE (user_id, world_id)
-- );

CREATE TABLE tags (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    title VARCHAR(50) NOT NULL UNIQUE,
    created_by VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);




CREATE TABLE world_users (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    pending BOOLEAN DEFAULT true,
    world_id UUID REFERENCES worlds (id) ON DELETE CASCADE,
    user_id UUID REFERENCES users (id) ON DELETE CASCADE,
    UNIQUE (world_id, user_id)
);

CREATE TABLE world_moderation (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    world_id UUID REFERENCES worlds (id) ON DELETE CASCADE,
    user_id UUID REFERENCES users (id) ON DELETE CASCADE,
    is_admin BOOLEAN DEFAULT false,
    UNIQUE (world_id, user_id)
);

CREATE TABLE world_tags (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    world_url VARCHAR(50) REFERENCES worlds (url) ON DELETE CASCADE,
    tag_title VARCHAR(50) REFERENCES tags (title) ON DELETE CASCADE,
    UNIQUE (world_url, tag_title)
);

CREATE TABLE thread_characters (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    thread_id UUID REFERENCES threads (id) ON DELETE CASCADE,
    character_id UUID REFERENCES characters (id) ON DELETE CASCADE,
    UNIQUE (thread_id, character_id)
);











/* Following */
CREATE TABLE user_follows (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    leader_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    follower_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    UNIQUE (leader_id, follower_id)
);
CREATE TABLE character_follows (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    character_id UUID NOT NULL REFERENCES characters (id) ON DELETE CASCADE,
    UNIQUE (user_id, character_id)
);
CREATE TABLE event_follows (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    event_id UUID NOT NULL REFERENCES events (id) ON DELETE CASCADE,
    UNIQUE (user_id, event_id)
);
CREATE TABLE thread_follows (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    thread_id UUID NOT NULL REFERENCES threads (id) ON DELETE CASCADE,
    UNIQUE(user_id, thread_id)
);

/* Thread invites */
CREATE TABLE thread_invites (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    thread_id UUID NOT NULL REFERENCES threads (id) ON DELETE CASCADE,
    UNIQUE(user_id, thread_id)
);

/* Saved */
CREATE TABLE saved_posts (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    post_id UUID NOT NULL REFERENCES posts (id) ON DELETE CASCADE,
    UNIQUE (user_id, post_id) 
);

/* Friends */
CREATE TABLE friends (
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    id UUID DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    friend_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    pending BOOLEAN DEFAULT true,
    UNIQUE (user_id, friend_id)
);

/* Bans */
CREATE TABLE world_bans (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    updated_at TIMESTAMP WITH TIME ZONE,
    duration INTERVAL,
    revoked BOOLEAN DEFAULT false,
    auditor_id UUID REFERENCES users (id) ON DELETE SET NULL,
    revoker_id UUID REFERENCES users (id) ON DELETE SET NULL,
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    world_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE
);

/* Blocks */
CREATE TABLE blocks (
    id UUID DEFAULT gen_random_uuid(),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
    user_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    blocked_id UUID NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    UNIQUE (user_id, blocked_id)
);








/* Inserts */
INSERT INTO users (username, email, password) VALUES 
('flamesofblue', 'ailia.vmd@gmail.com', '013363309'),
('lillia', 'lillia@gmail.com', '123'),
('dev', 'dev@gmail.com', '123'),
('yokoki', 'yoko@gmail.com', '123'),
('sett', 'sett@gmail.com', '123');

UPDATE users
SET bio = 'To love is to give what you haven''t got'
WHERE username = 'flamesofblue';

UPDATE users
SET bio = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ac enim varius, volutpat nulla nec, laoreet mi. Fusce ac consectetur quam. Donec euismod, arcu eu vulputate scelerisque, nulla mi cursus lorem, vitae ultricies tellus nunc et mi. Mauris vel lorem porttitor, gravida eros non, mattis eros. Phasellus ac elit sapien.',
avatar = 'https://i.pinimg.com/originals/d7/41/1c/d7411c1ac54c062a6f94cb0bf247c902.jpg'
WHERE username = 'lillia';

UPDATE users
SET bio = 'Nulla mi cursus lorem, vitae ultricies tellus nunc et mi. Mauris vel lorem porttitor, gravida eros non, mattis eros. Phasellus ac elit sapien.'
WHERE username = 'dev';

UPDATE users
SET avatar = 'https://i.ytimg.com/vi/YK2dMCgU0Uw/maxresdefault.jpg'
WHERE username = 'yokoki';



INSERT INTO worlds (url, title, description, picture, created_by) VALUES
('isekai_generico_para_degenerados', 'Isekai Genérico para Degenerados', 'Donec nisi augue, viverra ut condimentum accumsan, rhoncus vitae urna. In ac pulvinar erat. Suspendisse iaculis commodo metus, in dapibus massa maximus porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed pretium laoreet iaculis. Ut nec egestas mi. Proin vehicula. Fusce porttitor purus ut odio tincidunt, eget posuere metus sollicitudin.', 'https://www.beahero.gg/wp-content/uploads/2019/06/1007781.jpg', 'flamesofblue'),
('dramas_vampiricos', 'Dramas Vampíricos', 'Proin vehicula, leo at luctus sagittis, neque justo tempor orci, sit amet commodo velit diam in lectus. Fusce porttitor purus ut odio tincidunt', null, 'lillia');

INSERT INTO worlds (url, title, description, picture, created_by) VALUES
('chistes_ratos', 'Chistes Raros', 'Donec nisi augue', null, 'flamesofblue'),
('lmao', 'lmao', 'Proin vehicula, leo at luctus sagittis, neque justo tempor orci, sit amet commodo velit diam in lectus. Fusce porttitor purus ut odio tincidunt', null, 'sett'),
('novelas', 'Novelas', 'Proin vehicula, leo at luctus sagittis, neque', null, 'yokoki');
('random', 'Random', 'Ola ke ace', null, 'yokoki');

INSERT INTO tags (title, created_by) VALUES
('drama', 'flamesofblue'),
('isekai', 'flamesofblue'),
('comedia', 'flamesofblue'),
('anime', 'flamesofblue'),
('angst', 'lillia'),
('fantasía', 'lillia'),
('romance', 'flamesofblue');

INSERT INTO world_tags (world_url, tag_title) VALUES
('isekai_generico_para_degenerados', 'isekai'),
('isekai_generico_para_degenerados', 'comedia'),
('isekai_generico_para_degenerados', 'anime'),
('isekai_generico_para_degenerados', 'fantasía'),
('dramas_vampiricos', 'drama'),
('dramas_vampiricos', 'angst');

INSERT INTO world_users (world_id, user_id) VALUES
((SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), (SELECT id FROM users WHERE username = 'flamesofblue')),
((SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), (SELECT id FROM users WHERE username = 'lillia')),
((SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), (SELECT id FROM users WHERE username = 'yokoki')),
((SELECT id FROM worlds WHERE url = 'dramas_vampiricos'), (SELECT id FROM users WHERE username = 'lillia'));

UPDATE world_users
SET pending = FALSE
WHERE user_id = (SELECT id FROM users WHERE username = 'flamesofblue');

UPDATE world_users
SET pending = FALSE
WHERE user_id = (SELECT id FROM users WHERE username = 'lillia');


INSERT INTO characters (name, bio, picture, user_id, world_id)
VALUES
('Kotoko', 'Praesent sit amet leo quis sem lobortis semper a at arcu. Nulla finibus lorem fermentum metus blandit porttitor. Vivamus sagittis semper leo a suscipit. Integer tristique, lorem sit amet facilisis convallis, nisi leo eleifend erat, nec suscipit eros sem quis ex. Aenean magna augue, consectetur nec nunc pharetra, ultricies maximus est. Aliquam ullamcorper leo libero, et vestibulum arcu mollis in. Aenean eget orci viverra orci gravida ornare vitae eu mi. Pellentesque ornare lorem sit amet congue scelerisque. Integer facilisis velit et sapien hendrerit, in consequat nisi elementum. Praesent efficitur, justo sollicitudin malesuada viverra, risus urna varius erat, eu tempus turpis nulla sed nisi. Praesent et sem hendrerit, scelerisque purus non, sodales turpis. Sed consectetur ipsum imperdiet dui finibus, ut venenatis tortor ullamcorper. Vivamus mattis gravida ligula laoreet ultricies. Cras ultrices fermentum risus, interdum efficitur lectus posuere iaculis.', 'https://cdn.sketchmob.com/uploads/2019/11/234106_1fbb9862c-720x728.jpeg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados')),
('Hachi', 'Nulla scelerisque metus nec neque maximus, feugiat tincidunt massa porttitor. Vestibulum vulputate faucibus ipsum eget tristique. Quisque rhoncus lacinia magna, ac egestas turpis pretium ac. Integer sagittis, augue vitae pretium sollicitudin, dui lectus lobortis massa, vel mollis augue orci nec nulla. Donec ac feugiat urna. Etiam vitae sem non augue interdum vehicula et ac felis. Curabitur hendrerit aliquam ipsum, non semper turpis mollis eget. Phasellus sed nulla ante. Duis et rutrum dui. Phasellus ac elementum erat. Praesent tempus vel felis sit amet efficitur.', 'https://c4.wallpaperflare.com/wallpaper/94/600/32/anime-anime-girls-digital-art-artwork-portrait-display-hd-wallpaper-preview.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados')),
('Araragi Sakura', 'Phasellus ut elementum nunc, vitae luctus augue. Ut eu suscipit nunc. Sed a ante fermentum, sagittis ligula et, feugiat massa. Duis vehicula dignissim lacus eget efficitur. Proin sollicitudin ornare iaculis. Proin nec arcu purus. Etiam vulputate feugiat libero ac porttitor. Vestibulum et blandit lorem. Integer dolor ipsum, elementum lacinia efficitur ac, luctus id neque. Nam ac nisl euismod eros auctor vulputate. Ut ullamcorper arcu vitae neque tempus, et vulputate nisl semper. Fusce cursus metus massa, eget posuere enim euismod id.', 'https://img5.goodfon.com/wallpaper/nbig/6/78/wang-bu-duo-lian-xi-art-anime-portret-devushka.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados')),
('Jisuz', 'Praesent sit amet leo quis sem lobortis semper a at arcu. Nulla finibus lorem fermentum metus blandit porttitor. Vivamus sagittis semper leo a suscipit. Integer tristique, lorem sit amet facilisis convallis, nisi leo eleifend erat, nec suscipit eros sem quis ex. Aenean magna augue, consectetur nec nunc pharetra, ultricies maximus est. Aliquam ullamcorper leo libero, et vestibulum arcu mollis in. Aenean eget orci viverra orci gravida ornare vitae eu mi. Pellentesque ornare lorem sit amet congue scelerisque. Integer facilisis velit et sapien hendrerit, in consequat nisi elementum. Praesent efficitur, justo sollicitudin malesuada viverra, risus urna varius erat, eu tempus turpis nulla sed nisi. Praesent et sem hendrerit, scelerisque purus non, sodales turpis. Sed consectetur ipsum imperdiet dui finibus, ut venenatis tortor ullamcorper. Vivamus mattis gravida ligula laoreet ultricies. Cras ultrices fermentum risus, interdum efficitur lectus posuere iaculis.', 'https://i.etsystatic.com/16287606/r/il/ae5118/1336398744/il_570xN.1336398744_kfzi.jpg', (SELECT id FROM users WHERE username = 'lillia'), (SELECT id FROM worlds WHERE url = 'dramas_vampiricos')),
('Lucian', 'Praesent consectetur eros massa, a finibus lectus dapibus sit amet. Donec sagittis laoreet libero, eu posuere justo sodales ac. Vivamus ut sapien vel erat tristique feugiat sed ac enim. Sed ultrices, dui non varius venenatis, arcu lacus aliquam neque, et condimentum lectus odio id ligula. Fusce tristique mi elementum quam consequat, ac pulvinar metus elementum. Vestibulum efficitur lorem consectetur sapien hendrerit, sit amet mollis justo fringilla. Nam luctus lectus eget suscipit porta. Aenean iaculis vehicula ipsum.', 'https://www.wallpapertip.com/wmimgs/240-2406698_photo-wallpaper-look-portrait-anime-art-guy-fairy.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'novelas')),
('Helena', 'Praesent sit amet leo quis sem lobortis semper a at arcu. Nulla finibus lorem fermentum metus blandit porttitor. Vivamus sagittis semper leo a suscipit. Integer tristique, lorem sit amet facilisis convallis, nisi leo eleifend erat, nec suscipit eros sem quis ex. Aenean magna augue, consectetur nec nunc pharetra.', 'https://64.media.tumblr.com/59d7925db18eefe621d9d10979481242/tumblr_ovw711gW2q1r92482o1_640.png', (SELECT id FROM users WHERE username = 'lillia'), (SELECT id FROM worlds WHERE url = 'dramas_vampiricos')),
('Lilith', 'Praesent sit amet leo quis sem lobortis semper a at arcu. Nulla finibus lorem fermentum metus blandit porttitor. Vivamus sagittis semper leo a suscipit. Integer tristique, lorem sit amet facilisis convallis, nisi leo eleifend erat, nec suscipit eros sem quis ex. Aenean magna augue, consectetur nec nunc pharetra, ultricies maximus est. Aliquam ullamcorper leo libero, et vestibulum arcu mollis in. Aenean eget orci viverra orci gravida ornare vitae eu mi. Pellentesque ornare lorem sit amet congue scelerisque. Integer facilisis velit et sapien hendrerit, in consequat nisi elementum. Praesent efficitur, justo sollicitudin malesuada viverra, risus urna varius erat, eu tempus turpis nulla sed nisi. Praesent et sem hendrerit, scelerisque purus non, sodales turpis. Sed consectetur ipsum imperdiet dui finibus, ut venenatis tortor ullamcorper. Vivamus mattis gravida ligula laoreet ultricies. Cras ultrices fermentum risus, interdum efficitur lectus posuere iaculis.', 'https://static2.aniimg.com/upload/20170530/634/N/N/4/NN4FEF.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'));


INSERT INTO events (title, description, picture, user_id, world_id)
VALUES ('The Winter Festival', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at purus id felis congue faucibus. Vestibulum quis nibh iaculis, rutrum eros in, viverra arcu. In velit tellus, tincidunt id ex maximus, tincidunt auctor urna. Nam dictum non tellus eget aliquet. Curabitur ipsum felis, sollicitudin ac tincidunt eget, vehicula at diam. In euismod sapien non nunc cursus placerat nec eget sapien. In sed ipsum est. Nulla et vulputate risus. Nullam convallis, lectus sit amet maximus vestibulum, justo nisl aliquam ipsum, sit amet vestibulum lacus ipsum non tortor. Pellentesque ultricies erat vel lectus dapibus, ut varius nisi finibus. Nullam consectetur orci vel finibus tincidunt. Proin cursus magna sed leo tincidunt vulputate. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas non posuere mi.', 'https://images6.alphacoders.com/868/868599.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'));
INSERT INTO events (title, description, picture, user_id, world_id)
VALUES ('The Solar Eclipse', 'Aenean ut sagittis diam. Quisque ac malesuada eros. Phasellus suscipit nisi ut iaculis rutrum. Sed interdum mi ut ligula interdum aliquet. Cras sodales pharetra rutrum. Nunc sit amet consequat odio, id pulvinar elit. Fusce euismod interdum pulvinar. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut consectetur hendrerit purus, quis maximus magna malesuada a.', 'https://wallpapercave.com/wp/qnrE2td.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'));

INSERT INTO events (title, description, picture, user_id, world_id, nano_id)
VALUES ('Holiday', 'Praesent sit amet leo quis sem lobortis semper a at arcu. Nulla finibus lorem fermentum metus blandit porttitor. Vivamus sagittis semper leo a suscipit. Integer tristique, lorem sit amet facilisis convallis, nisi leo eleifend erat, nec suscipit eros sem quis ex. Aenean magna augue, consectetur nec nunc pharetra, ultricies maximus est. Aliquam ullamcorper leo libero, et vestibulum arcu mollis in. Aenean eget orci viverra orci gravida ornare vitae eu mi. Pellentesque ornare lorem sit amet congue scelerisque. Integer facilisis velit et sapien hendrerit, in consequat nisi elementum. Praesent efficitur, justo sollicitudin malesuada viverra, risus urna varius erat, eu tempus turpis nulla sed nisi. Praesent et sem hendrerit, scelerisque purus non, sodales turpis. Sed consectetur ipsum imperdiet dui finibus, ut venenatis tortor ullamcorper.', 'https://images.hdqwalls.com/wallpapers/dragon-winter-snow-anime-manga-4k-9k.jpg', (SELECT id FROM users WHERE username = 'flamesofblue'), (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), 'FQI1yzCpN2');


INSERT INTO posts (contents, user_id, thread_id, event_id, world_id)
VALUES ('Aenean ut sagittis diam. Quisque ac malesuada eros. Phasellus suscipit nisi ut iaculis rutrum. Sed interdum mi ut ligula interdum aliquet. Cras sodales pharetra rutrum. Nunc sit amet consequat odio, id pulvinar elit. Fusce euismod interdum pulvinar. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut consectetur hendrerit purus, quis maximus magna malesuada a.', (SELECT id FROM users WHERE username = 'lillia'), '9c51cb0a-7d4a-4a70-b799-ab974297cb6b', 'c23b9f82-d09e-47a5-9736-c1a310030ce6', (SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'));



INSERT INTO world_moderation (world_id, user_id, is_admin)
VALUES
       ((SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), (SELECT id FROM users WHERE username = 'flamesofblue'), true),
       ((SELECT id FROM worlds WHERE url = 'isekai_generico_para_degenerados'), (SELECT id FROM users WHERE username = 'lillia'), false)
