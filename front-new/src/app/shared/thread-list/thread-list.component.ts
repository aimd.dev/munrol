import { Component, OnInit, Input } from '@angular/core';
import { Thread } from 'src/app/interfaces/Thread.model';

@Component({
  selector: 'app-thread-list',
  templateUrl: './thread-list.component.html',
  styleUrls: ['./thread-list.component.css']
})
export class ThreadListComponent implements OnInit {

  @Input('threads') threads: Thread[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
