import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  @Input('inline') inline:boolean
  @Input('height') height:number = 400
  @Input('grey') grey:boolean

  constructor() { }

  ngOnInit(): void {
  }

}
