import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tab } from 'src/app/interfaces/Tab.model';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  @Input('tabs') tabs:Tab[] = []
  @Input('active') active:number = 0
  @Input('select-on-load') selectOnLoad:boolean = false

  @Output('onleave') 
  onleave: EventEmitter<number> = new EventEmitter()
  @Output('onselect') 
  onselect: EventEmitter<number> = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
    if (this.selectOnLoad) this.onselect.emit(this.active)
  }

  select(index:number) {
    this.onleave.emit(this.active)
    this.active = index
    this.onselect.emit(this.active)
  }

}
