import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { InputComponent } from './input/input.component';
import { ModalComponent } from './modal/modal.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TabsComponent } from './tabs/tabs.component';
import { LoaderComponent } from './loader/loader.component';
import { EditorComponent } from './editor/editor.component';
import { ThreadListComponent } from './thread-list/thread-list.component';
import { SwitchComponent } from './switch/switch.component';
import { FormsModule } from '@angular/forms';
import { CountdownComponent } from './countdown/countdown.component';
import { BlinkingDotComponent } from './blinking-dot/blinking-dot.component';
import { InfiniteScrollerComponent } from './infinite-scroller/infinite-scroller.component';
import { SelectComponent } from './select/select.component';


@NgModule({
  declarations: [
    InputComponent, 
    ModalComponent, 
    NotificationsComponent, 
    TabsComponent, 
    LoaderComponent,
    EditorComponent,
    ThreadListComponent,
    SwitchComponent,
    CountdownComponent,
    BlinkingDotComponent,
    InfiniteScrollerComponent,
    SelectComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    InputComponent, 
    ModalComponent, 
    NotificationsComponent,
    TabsComponent,
    LoaderComponent,
    EditorComponent,
    ThreadListComponent,
    SwitchComponent,
    CountdownComponent,
    BlinkingDotComponent,
    InfiniteScrollerComponent,
    SelectComponent
  ]
})
export class SharedModule { }
