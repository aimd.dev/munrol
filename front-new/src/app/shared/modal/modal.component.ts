import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input('title') title:string
  @Input('subtitle') subtitle:string
  @Input('no-footer') noFooter:boolean
  @Input('close-on-confirm') closeOnConfirm:boolean = true
  @Input('disable-confirm') disableConfirm:boolean
  @Input('hide-cancel') hideCancel:boolean
  @Input('loading') loading:boolean
  @Input('confirm-text') confirmText:string = 'Confirmar'
  @Input('cancel-text') cancelText:string = 'Cancelar'
  @Input('confirm-class') confirmClass:string = 'primary'
  @Input('cancel-class') cancelClass:string = 'secondary'

  @Input('large') large:boolean
  @Input('scroller') scroller:boolean

  @Output()
  onopen: EventEmitter<any> = new EventEmitter()
  @Output() 
  onclose: EventEmitter<any> = new EventEmitter()
  @Output()
  onconfirm: EventEmitter<any> = new EventEmitter()

  isOpen:boolean

  constructor() { }

  ngOnInit(): void {
  }

  open() {
    this.isOpen = true
    this.onopen.emit()
  }
  close() {
    this.isOpen = false
    this.onclose.emit()
  }
  confirm() {
    this.onconfirm.emit()
    if (this.closeOnConfirm) this.isOpen = false
  }

}
