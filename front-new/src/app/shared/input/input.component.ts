import { Component, ElementRef, EventEmitter, Input, OnInit, Optional, Output, Renderer2, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';


type ControlType = 'text' | 'email' | 'number' | 'tel' | 'password' | 'select' | 'textarea' | 'checkbox'

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements ControlValueAccessor, OnInit {

  @ViewChild('element') element:ElementRef

  @Input('type') type:ControlType
  @Input('label') label:string
  @Input('initialValue') value:any
  @Input('required') required:boolean
  @Input('selectOptions') options:any[] = []
  @Input('selectOptionsKey') optionsKey:string
  @Input('selectOptionsName') optionsName:string
  @Input('readonly') readonly:boolean
  @Input('placeholder') placeholder:string = ''
  @Input('step') step:number
  @Input('login-style') loginStyle:boolean
  @Input('hide-error-text') hideErrorText:boolean

  @Output()
  onfocus: EventEmitter<any> = new EventEmitter()
  @Output()
  change: EventEmitter<any> = new EventEmitter()
  @Output()
  keyup: EventEmitter<any> = new EventEmitter()

  constructor(
    private renderer: Renderer2,
    @Self()
    @Optional()
    public ngControl: NgControl) {
      if (this.ngControl) { this.ngControl.valueAccessor = this }
  }

  onChangeCallback: any = () => {}
  onTouchedCallback: any = () => {}

  writeValue(value:any) {
    this.value = value?.target ? value.target.value : value
    this.onChangeCallback(value)
  }
  registerOnChange(fn: (_: any) => void): void {
    this.onChangeCallback = fn
  }
  registerOnTouched(fn: () => void): void {
    this.onTouchedCallback = fn
  }
  setDisabledState(disabled:boolean) {
    this.renderer.setProperty(this.element.nativeElement, 'disabled', disabled);
  }

  ngOnInit(): void {
  }

  handleChange(e) {

    let val = e.target.value

    this.writeValue(val)
    this.change.emit(val)
    this.keyup.emit(val)
  }

}