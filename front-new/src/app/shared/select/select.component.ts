import { Component, ElementRef, OnInit, Optional, Renderer2, Self, ViewChild, Input, Output, TemplateRef, EventEmitter, HostListener } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements ControlValueAccessor, OnInit {

  @ViewChild('element') element:ElementRef
  @ViewChild('filterInput') filterInput:ElementRef

  @Input('label') label:string
  @Input('placeholder') placeholder:string = 'Seleccionar...'
  @Input('value') value:any
  @Input('options') options:any[] = []
  @Input('text-filter') textFilter:boolean = true

  @Input('display-field') displayField:string
  @Input('value-field') valueField:string
  @Input('filter-filed') filterField:string

  @Input('list-template') listTemplate:TemplateRef<any>
  @Input('selected-template') selectedTemplate:TemplateRef<any>

  @Output()
  onselect: EventEmitter<any> = new EventEmitter<any>()

  constructor(
    private renderer: Renderer2,
    @Self()
    @Optional()
    public ngControl: NgControl) {
      if (this.ngControl) { this.ngControl.valueAccessor = this }
  }

  onChangeCallback: any = () => {}
  onTouchedCallback: any = () => {}

  writeValue(value:any) {
    this.value = value
    this.onselect.emit(value)
    this.onChangeCallback(value)
  }
  registerOnChange(fn: (_: any) => void): void {
    this.onChangeCallback = fn
  }
  registerOnTouched(fn: () => void): void {
    this.onTouchedCallback = fn
  }
  setDisabledState() {
  }

  @HostListener('document:click')
  clickout() {
    if (this.open) this.open = false
  }

  ngOnInit(): void {
  }

  filter(event:KeyboardEvent) {
    const value:string = this.filterInput.nativeElement.value
    // if (!this.listTemplate) this.options[this.filterField]
    // else 
  }

  open:boolean = false
  toggleDropdown(event:MouseEvent):void {
    event.stopPropagation()
    this.open = !this.open
  }

}
