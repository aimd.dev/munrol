import { Component, OnInit, Input, Output, ViewChild, ElementRef, HostListener, ViewEncapsulation, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EditorComponent implements OnInit {

  constructor() { }

  show:boolean

  @Input('value') value:string
  @Input('min-height') minHeight:number = 300

  @Output()
  onchange: EventEmitter<any> = new EventEmitter()

  @ViewChild('toolbar') toolbar:ElementRef
  @ViewChild('editor') editor:ElementRef

  @HostListener('document:click')
  clickout(e) {
    if (this.show) {
      this.rendered = false
      this.show = false
    }
  }

  placeholderHidden:boolean
  ngOnInit(): void {
    document.execCommand('defaultParagraphSeparator', false, 'p');
  }

  toolbarX:number
  toolbarY:number
  rendered:boolean
  mouseUp(e:MouseEvent) {
    // console.log(document.getSelection())
    const selection = document.getSelection()
    const show:boolean = selection.anchorOffset !== selection.focusOffset
    if (show) {
      this.rendered = true
      const bounds = this.editor.nativeElement.getBoundingClientRect();
      this.toolbarX = (e.clientX - bounds.left) - 130
      this.toolbarY = (e.clientY - bounds.top) - 100
      this.show = show
    }
  }

  format(command:string) {
    document.execCommand(command)
    this.onchange.emit(this.editor.nativeElement.innerHTML)
  }
  convert(tag:string) {
    document.execCommand('formatBlock', false, `<${tag}>`)
    this.onchange.emit(this.editor.nativeElement.innerHTML)
  }

  checkLength(e) {
    e.stopPropagation()
  }

  checkSelection(e) {
    if (e.ctrlKey && e.key === 'a') {
      this.rendered = true
      this.toolbarX = 0
      this.toolbarY = -40
      this.show = true
    }
  }

  handlePaste(e) {
    console.log(e)
    e.preventDefault()
    const data = e.clipboardData
    const text = data.getData('text')
    document.execCommand('insertText', false, text)
    this.onchange.emit(this.editor.nativeElement.innerHTML)
  }

  handleInput(event) {

    event.stopPropagation()
    // console.log(event.srcElement.innerHTML)

    // Show placeholder
    if (event.srcElement.innerText.trim() !== '' || event.data === ' ') this.placeholderHidden = true
    else this.placeholderHidden = false

    // Add first paragraph
    if (event.srcElement.innerHTML.length === 1) {
      document.execCommand('formatBlock', false, '<p>')
    }

    this.onchange.emit(this.editor.nativeElement.innerHTML)
  }

  setValue(html:string, emitEvent:boolean = true) {
    this.editor.nativeElement.innerHTML = html
    
    // Show placeholder
    if (this.editor.nativeElement.innerText.trim() !== '' || html === ' ') this.placeholderHidden = true
    else this.placeholderHidden = false

    // Add first paragraph
    if (this.editor.nativeElement.innerHTML.length === 1) {
      document.execCommand('formatBlock', false, '<p>')
    }

    if (emitEvent) this.onchange.emit(this.editor.nativeElement.innerHTML)
  }

}
