import { Time } from '@angular/common';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.css']
})
export class CountdownComponent implements OnInit {

  @Input('minutes') minutes:number = 5

  @Output()
  onend: EventEmitter<any> = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

  countDown: Time = { hours: 0, minutes: 0 };
  seconds: number = 0;
  countEnd: boolean = false
  count: any = null
  
  start() {
    
    this.stop()

    this.count = setInterval(() => {

      if (this.seconds == 0 && this.countDown.minutes == 0) {

        clearInterval(this.count)
        this.countEnd = true
        this.count = null
        this.onend.emit()

      } else {
        this.seconds --
        if (this.seconds == -1) {
          if (this.countDown.minutes > 0) this.countDown.minutes --
          this.seconds = 59
        }
      }
    }, 1000);
  }

  stop() {
    clearInterval(this.count)

    this.countDown = { hours: 0, minutes: this.minutes-1 }
    this.seconds = 59;
  }

}
