import { Component, OnInit, Output, HostListener, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-infinite-scroller]',
  templateUrl: './infinite-scroller.component.html',
  styleUrls: ['./infinite-scroller.component.css']
})
export class InfiniteScrollerComponent implements OnInit {

  @Output()
  onscrollend: EventEmitter<any> = new EventEmitter()

  constructor() { }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    this.debounce()
  }

  timeout:any
  debounce() {
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) this.onscrollend.emit()
    }, 300)
  }

  ngOnInit(): void {
  }

}
