import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { User } from 'src/app/interfaces/User.model';
import { Router } from '@angular/router';
import { CurrentUser } from '../interfaces/CurrentUser.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( 
    private http: HttpClient,
    private router: Router
  ) { console.log(this.currentUser) }

  private urlAuth:string = `${environment.api}/users/auth/`;

  currentUser:CurrentUser | User
  avatar:string

  getCurrentUser():Promise<any> {
    return this.http.get(`${environment.api}/auth/current/`).toPromise()
  }
  async setCurrentUser():Promise<void> {
    this.currentUser = await this.getCurrentUser()
    console.log('set',this.currentUser)
    if (this.currentUser) this.saveUserData(this.currentUser)
  }

  login(body:User):Observable<User> {
    return this.http.post<User>(this.urlAuth, body)
  }

  saveUserData(user:User):void {
    this.saveToken(user.token)
    this.avatar = user.avatar
    this.currentUser = {...user}
  }

  getAvatar():string {
    return this.avatar
  }
  async setAvatar(username:string):Promise<void> {
    let res = await this.http.get<{avatar:string}>(`${environment.api}/users/avatar/${username}`).toPromise<{avatar:string}>()
    this.avatar = res?.avatar
  }

  saveToken(token:string) {
    localStorage.setItem('token', token)
  }
  getToken():string {
    return localStorage.getItem('token')
  }

  saveEmail(email:string):void {
    localStorage.setItem('email', email)
  }
  getEmail():string {
    return this.currentUser.email
  }
  
  logout() {
    localStorage.clear()
    console.log('logout')
    this.currentUser = null
    this.avatar = null
    this.router.navigate(['/login'])
  }
}
