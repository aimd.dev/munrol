import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Thread } from 'src/app/interfaces/Thread.model';

@Injectable({
  providedIn: 'root'
})
export class ThreadService {

  constructor( private http: HttpClient ) { }

  private urlThreads:string = `${environment.api}/threads/`;

  createThread(body:Thread):Observable<Thread> {
    return this.http.post<Thread>(this.urlThreads, body)
  }

  getByUser(username:string):Observable<any> {
    return this.http.get(`${this.urlThreads}by-author/${username}`)
  }

  getById(id:string):Observable<Thread> {
    return this.http.get<Thread>(`${this.urlThreads}id/${id}`)
  }

  getParticipants(id:string):Observable<any> {
    return this.http.get(`${this.urlThreads}participants/${id}`)
  }

  like(id:string) {
    return this.http.post(`${this.urlThreads}likes`, {id:id})
  }

  dislike(id:string) {
    return this.http.delete(`${this.urlThreads}likes/${id}`)
  }

  save(id:string) {
    return this.http.post(`${this.urlThreads}saved`, {id:id})
  }

  unsave(id:string) {
    return this.http.delete(`${this.urlThreads}saved/${id}`)
  }

  getNew(time:number, pos:number):Observable<Thread[]> {
    return this.http.get<Thread[]>(`${this.urlThreads}new?ts=${time}&pos=${pos}`)
  }

  getActive(time:number, pos:number):Observable<Thread[]> {
    return this.http.get<Thread[]>(`${this.urlThreads}active?ts=${time}&pos=${pos}`)
  }

}
