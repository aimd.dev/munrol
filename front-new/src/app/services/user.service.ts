import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { User } from 'src/app/interfaces/User.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient ) { }

  private urlUsers:string = `${environment.api}/users`;

  checkUsername(username:string):Observable<any> {
    return this.http.get(this.urlUsers + '/q/' + username)
  }

  createUser(body:User):Observable<User> {
    return this.http.post<User>(this.urlUsers, body)
  }

  getByUsername(username:string):Observable<User> {
    return this.http.get<User>(`${this.urlUsers}/u/${username}`)
  }

  alterUser(username:string, prop:string, value:string) {
    return this.http.put(`${this.urlUsers}/${username}/${prop}`, {value:value})
  }

  alterAvatar(username:string, key:string, file:any, type:string) {
    return this.http.post(`${this.urlUsers}/${username}/avatar`, {key:key, file:file, type:type})
  }

  sendResetCode(username:string) {
    return this.http.get(`${this.urlUsers}/pr?u=${username}`)
  }

  resetPassword(body:{username:string, code:string, password:string}) {
    return this.http.post(`${this.urlUsers}/pr`, body)
  }

  verifyAccount(username:string, code:string) {
    return this.http.post(`${this.urlUsers}/account-verify`, {username:username,code:code})
  }

}
