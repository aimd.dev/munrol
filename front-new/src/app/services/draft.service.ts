import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Draft } from '../interfaces/Draft.model';


@Injectable({
  providedIn: 'root'
})
export class DraftService {

  constructor(private http: HttpClient) { }

  private url:string = `${environment.api}/drafts/`;

  getThreadDraftsByUsername():Observable<Draft[]> {
    return this.http.get<Draft[]>(this.url + 'threads')
  }

  insertDraft(body:Draft):Observable<Draft> {
    return this.http.post<Draft>(this.url + 'threads', body)
  }

  updateDraft(body:Draft):Observable<Draft> {
    return this.http.put<Draft>(this.url + 'threads', body)
  }

  deleteThreadDraft(id:number) {
    return this.http.delete(this.url + 'threads/' + id)
  }
}
