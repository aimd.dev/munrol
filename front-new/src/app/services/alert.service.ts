import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private urlAlerts:string = `${environment.api}/users/alerts`;

  constructor( private http: HttpClient ) { }

  f:number = null
  r:number = null

  private getAlerts() {
    return this.http.get(this.urlAlerts).toPromise()
  }

  async setAlerts() {
    return new Promise( async (resolve, reject) => {
      const data:any = await this.getAlerts()
      this.f = parseInt(data.friendships)
      this.r = parseInt(data.replies)
      resolve(true)
    })
  }

  clear(param:string) {
    return this.http.patch(this.urlAlerts, {param:param}).toPromise()
  }
}
