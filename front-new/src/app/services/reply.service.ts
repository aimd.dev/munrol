import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Reply } from 'src/app/interfaces/Reply.model';
import { ReplyRequest } from 'src/app/interfaces/ReplyRequest.model';

@Injectable({
  providedIn: 'root'
})
export class ReplyService {

  constructor( private http: HttpClient ) { }

  private urlReplies:string = `${environment.api}/replies/`;

  insertReply(body:Reply):Observable<Reply> {
    return this.http.post<Reply>(this.urlReplies, body)
  }

  getReplyRequestsByAuthor():Observable<ReplyRequest[]> {
    return this.http.get<ReplyRequest[]>(this.urlReplies + 'requests')
  }

  approveReply(id:string, threadId:string, replyAuthor:string) {
    return this.http.patch(this.urlReplies + 'requests', {id:id, threadId:threadId, username:replyAuthor})
  }

  rejectReply(id:string, threadId:string) {
    return this.http.delete(this.urlReplies + 'requests/' + threadId + '/' +id)
  }

  like(id:string) {
    return this.http.post(this.urlReplies + 'likes', {id:id})
  }

  dislike(id:string) {
    return this.http.delete(this.urlReplies + 'likes/' + id)
  }

  save(id:string) {
    return this.http.post(this.urlReplies + 'saved', {id:id})
  }

  unsave(id:string) {
    return this.http.delete(this.urlReplies + 'saved/' + id)
  }

}
