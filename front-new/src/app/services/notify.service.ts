import { Injectable } from '@angular/core';

type notification = {
  type:string,
  title:string,
  message:string,
  timestamp:number,
  show?:boolean,
  autoClose?:ReturnType<typeof setTimeout>
}

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor() { }

  notifications: notification[] = []

  success(msg:string) {
    this.notify('success', 'Éxito', msg)
  }

  error(msg:string) {
    this.notify('error', 'Error', msg)
  }

  info(msg:string) {
    this.notify('info', 'Aviso', msg)
  }

  notify(type: string, title: string, message: string) {

    const show:boolean = true
    const timestamp:number = new Date().getTime()

    const notification:notification = { type, title, message, timestamp, show }
    this.notifications = [...this.notifications, notification]

    notification.autoClose = setTimeout(() => {
      this.close(notification)
    }, 8000);
        
  }
  

  close(n:notification) {
    n.show = false;
    setTimeout(() => {
      const notifications:notification[] = [...this.notifications]
      this.notifications = notifications.filter((item:notification) => item.timestamp !== n.timestamp)
    }, 450);
  }


  enter(n:notification) {
    clearTimeout(n.autoClose)
  }
  leave(n:notification) {
    n.autoClose = setTimeout(() => {
      this.close(n)
    }, 8000);
  }

  closeAll() {
    this.notifications = []
  }

}
