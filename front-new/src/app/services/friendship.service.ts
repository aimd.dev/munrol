import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { User } from '../interfaces/User.model';
import { FriendshipRequest } from '../interfaces/FriendshipRequest.model';

@Injectable({
  providedIn: 'root'
})
export class FriendshipService {

  constructor( private http: HttpClient ) { }

  private urlFriendships:string = `${environment.api}/friendships/`;

  checkFriendship(username:string):Observable<any> {
    return this.http.get(this.urlFriendships + 'check?target=' + username)
  }

  sendRequest(username:string):Observable<any> {
    return this.http.post(this.urlFriendships, { user:username })
  }

  getRequests():Observable<FriendshipRequest[]> {
    return this.http.get<FriendshipRequest[]>(`${this.urlFriendships}requests/`)
  }

  getMutuals(username:string):Observable<User[]> {
    return this.http.get<User[]>(`${this.urlFriendships}mutuals/${username}`)
  }

  acceptRequest(id:string) {
    return this.http.patch(`${this.urlFriendships}${id}`, {})
  }

  deleteFriendship(id:string) {
    return this.http.delete(`${this.urlFriendships}${id}`)
  }

}
