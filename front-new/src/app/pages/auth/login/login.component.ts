import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { FormValidationService } from 'src/app/services/form-validation.service';
import { User } from 'src/app/interfaces/User.model';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { CountdownComponent } from 'src/app/shared/countdown/countdown.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('modalRecover') modalRecover:ModalComponent
  @ViewChild('countdown') countdown:CountdownComponent

  constructor(
    private fb: FormBuilder,
    private fv: FormValidationService,
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ) { }

  register: boolean
  authForm: FormGroup
  resetForm: FormGroup

  ngOnInit(): void {
    this.authForm = this.fb.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.email]],
      password: ['', [Validators.required]]
    })
    this.resetForm = this.fb.group({
      code: ['', Validators.required],
      password: ['', Validators.required, Validators.minLength(6)]
    })
  }

  switchForms(register:boolean) {

    if (register) {
      this.debounce(this.authForm.get('username').value, 10)
      this.authForm.get('email').setValidators([Validators.required, Validators.email])
      this.authForm.get('username').setValidators([Validators.required, Validators.minLength(3), Validators.maxLength(50)])
      this.authForm.get('password').setValidators([Validators.required, Validators.minLength(6)])
    } else {
      this.authForm.get('username').setErrors(null)
      this.authForm.get('email').setValidators([Validators.email])
      this.authForm.get('username').setValidators([Validators.required])
      this.authForm.get('password').setValidators([Validators.required])
    }
    this.authForm.get('email').updateValueAndValidity()
    this.authForm.get('username').updateValueAndValidity()
    this.authForm.get('password').updateValueAndValidity()
    this.register = register

  }

  checkDebounce(value:string) {
    if (this.register) this.debounce(value, 700)
  }

  timeout:any
  debounce(value:string, wait:number) {
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.checkUsername(value), wait)
  }

  checkUsername(value:string) {
    this.userService.checkUsername(value).subscribe(data => {
      if (data.exists)
      this.authForm.controls['username'].setErrors({'notUnique': true})
    })
  }

  loading:boolean
  submit() {

    if (this.loading) return

    if (this.authForm.invalid) {
      return this.fv.validateForm(this.authForm)
    }

    this.loading = true

    const body:User = {
      username: this.authForm.get('username').value,
      password: this.authForm.get('password').value,
      email: this.authForm.get('email').value
    }

    this.register ? this.createUser(body) : this.login(body)
  }

  createUser(body:User) {
    this.userService.createUser(body).subscribe(data => {
      console.log(data)
      this.loading = false
      this.router.navigate(['/u/'+data.username])
    }, error => {
      console.log(error)
      this.loading = false
    })
  }


  recoverConfirm() {
    switch (this.recoverView) {
      case 'recover':
        this.recover()
        break
      case 'reset':
        this.reset()
        break
    }
  }

  recoverUsername:string
  recoverLoading:boolean
  recoverView: 'recover' | 'reset' = 'recover'
  recoverError:string
  recover() {
    this.recoverLoading = true
    this.userService.sendResetCode(this.recoverUsername).subscribe(data => {
      console.log(data)
      this.recoverLoading = false
      this.recoverView = 'reset'
      setTimeout(() => {this.countdown.start()}, 10)
    }, error => {
      console.log(error)

      switch (error.status) {
        case 404:
          console.log('USER DOESNT EXIST')
          this.recoverError = 'Este usuario no existe'
          break
        default:
          console.log('UNKNOWN ERROR')
          this.recoverError = 'Ha ocurrido un error inesperado'
          break
      }

      this.recoverLoading = false
    })
  }

  reset() {
    this.recoverLoading = true
    const body = {...this.resetForm.value, username: this.recoverUsername}
    this.userService.resetPassword(body).subscribe(data => {
      console.log(data)
      this.login({username:this.recoverUsername,password:this.resetForm.get('password').value})
    }, error => {
      console.log(error)
      if (error.status === 403) this.resetForm.get('code').setErrors({authenticationError:true})
      this.recoverLoading = false
    })
  }

  restartRecover() {
    this.recoverUsername = null
    this.recoverView = 'recover'
    this.resetForm.reset()
    this.countdown.stop()
  }



  login(body:User) {
    this.authService.login(body).subscribe(data => {
      
      console.log(data)
      this.authService.saveUserData(data)
      this.router.navigate(['/u/'+data.username])

    }, error => {
      console.log(error)

      switch (error.status) {
        case 404:
          console.log('USER DOESNT EXIST')
          this.authForm.get('username').setErrors({doesNotExist:true})
          break
        case 401:
          console.log('WRONG PASSWORD')
          this.authForm.get('password').setErrors({authenticationError:true})
          break
        default:
          console.log('UNKNOWN ERROR')
          break
      }

      this.loading = false
    })
  }

}
