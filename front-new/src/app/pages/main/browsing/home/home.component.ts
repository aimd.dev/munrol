import { Component, OnInit } from '@angular/core';
import { Thread } from 'src/app/interfaces/Thread.model';
import { ThreadService } from 'src/app/services/thread.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public threadService: ThreadService
  ) { }

  now:number

  first:boolean = true
  threads:Thread[] = []
  ngOnInit(): void {
    this.now = new Date().getTime()
    this.getNew()
  }

  loading:boolean
  end:boolean
  getNew() {
    this.loading = true
    this.threadService.getNew(this.now, this.threads.length).subscribe(
      data => {
        
        if (!data.length) {
          this.end = true
          this.loading = false
          return
        }

        const threads:Thread[] = this.threads.concat(data)
        this.threads = [...threads]
        this.first = false

      }, error => {
        console.log(error)
        this.loading = false
      }
    )
  }

  getPosts() {
    if (!this.end) this.getNew()
  }

}
