import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Draft } from 'src/app/interfaces/Draft.model';
import { Thread } from 'src/app/interfaces/Thread.model';
import { AuthService } from 'src/app/services/auth.service';
import { DraftService } from 'src/app/services/draft.service';
import { FormValidationService } from 'src/app/services/form-validation.service';
import { FriendshipService } from 'src/app/services/friendship.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ThreadService } from 'src/app/services/thread.service';
import { EditorComponent } from 'src/app/shared/editor/editor.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-new-thread',
  templateUrl: './new-thread.component.html',
  styleUrls: ['./new-thread.component.css']
})
export class NewThreadComponent implements OnInit {

  @ViewChild('body') body:EditorComponent
  @ViewChild('modalFriends') modalFriends:ModalComponent
  @ViewChild('filterInput') filterInput:ElementRef

  constructor(
    private fb: FormBuilder,
    private fv: FormValidationService,
    private router: Router,
    public authService: AuthService,
    public service: ThreadService,
    public draftService: DraftService,
    public friendshipService: FriendshipService,
    public notify: NotifyService
  ) { }

  threadForm: FormGroup

  ngOnInit(): void {
    this.threadForm = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(100)]],
      summary: ['', [Validators.required, Validators.maxLength(600)]],
      restricted: [false]
    })

    this.getDrafts()
    this.obtenerAmigos()

    this.threadForm.valueChanges.subscribe(val => { this.debounce() })
  }

  drafts:Draft[] = []
  getDrafts() {
    this.draftService.getThreadDraftsByUsername().subscribe(data => {
      console.log(data)
      data.forEach((d:Draft) => { d.raw = this.getRawText(d.body) })
      this.drafts = data.sort((a:Draft, b:Draft) => {
        const dateOne:number = new Date(a.lastUpdated || a.createdAt).getTime()
        const dateTwo:number = new Date(b.lastUpdated || b.createdAt).getTime()
        return dateTwo - dateOne
      })
    })
  }

  getRawText(html:string):string {
    const div = document.createElement('div')
    div.innerHTML = html
    const text:string = div.innerText
    div.remove()
    return text
  }

  friends:any[] = []
  filteredFriends:any[] = []
  obtenerAmigos() {
    this.friendshipService.getMutuals(this.authService.currentUser?.username).subscribe(data => {
      this.friends = data

      this.friends.forEach(f => f.checked = false)
      this.filteredFriends = this.friends
      
    }, error => {
      console.log(error)
    })
  }

  filter(filter:string) {
    const value = filter.trim()
    this.filteredFriends = this.friends.filter(f => f.username.toLowerCase().indexOf(value) !== -1)
  }

  setRestricted() {
    this.threadForm.get('restricted').setValue(!!this.friends.find(f => f.checked))
    this.clear()
  }

  clear() {
    setTimeout(() => {
      this.filterInput.nativeElement.value = ''
      this.filteredFriends = this.friends
    }, 1000)
  }

  currentDraft:Draft
  draftLoading:boolean
  saveDraft() {

    console.log('save draft')
    this.draftLoading = true
    const request:Draft = {
      title: this.threadForm.get('title').value,
      summary: this.threadForm.get('summary').value,
      body: this.body.editor.nativeElement.innerHTML
    }

    this.draftService.insertDraft(request).subscribe(data => {
      data.raw = this.getRawText(data.body)
      this.currentDraft = data
      this.draftLoading = false
    }, error => {
      console.log(error)
      this.draftLoading = false
    })
  }

  timeout:any
  debounce() {
    console.log('debounce')
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {if (this.currentDraft) {this.updateDraft()} else {this.saveDraft()}}, 5000)
  }

  loadDraft(draft:Draft) {
    if (!draft) return
    this.threadForm.get('title').setValue(draft.title, {emitEvent:false})
    this.threadForm.get('summary').setValue(draft.summary, {emitEvent:false})
    this.body.setValue(draft.body, false)
  }

  updateDraft() {

    console.log('update draft')
    this.draftLoading = true
    const request:Draft = {
      id: this.currentDraft.id,
      title: this.threadForm.get('title').value,
      summary: this.threadForm.get('summary').value,
      body: this.body.editor.nativeElement.innerHTML
    }

    this.draftService.updateDraft(request).subscribe(data => {
      data.raw = this.getRawText(data.body)
      this.currentDraft = {...data}
      this.draftLoading = false
    }, error => {
      console.log(error)
      this.draftLoading = false
    })
  }

  removeDraft(id:number, event:MouseEvent) {

    console.log('remove draft')
    event.stopPropagation()
    this.draftService.deleteThreadDraft(id).subscribe(data => {
      console.log(data)
      this.getDrafts()
    }, error => {
      console.log(error)
    })
  }

  loading:boolean
  send() {

    if (this.loading) return
    if (this.threadForm.invalid) {
      return this.fv.validateForm(this.threadForm)
    }

    this.loading = true
    const request:Thread = {
      title: this.threadForm.get('title').value,
      summary: this.threadForm.get('summary').value,
      restricted: this.threadForm.get('restricted').value,
      body: this.body.editor.nativeElement.innerHTML,
      draftId: this.currentDraft.id
    }

    if (request.restricted) request.participants = this.friends.filter(f => f.checked).map(f => f.username)
    console.log(request)

    clearTimeout(this.timeout)

    this.service.createThread(request).subscribe(data => {
      console.log(data)
      this.loading = false
      this.notify.success('Publicación creada')
      this.router.navigate(['/t/'+data.id])
    }, error => {
      console.log(error)
      this.loading = false
      this.notify.error('Hubo un problema al crear su publicación')
    })

  }

}
