import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';
import { NewThreadComponent } from './new-thread/new-thread.component';
import { ThreadComponent } from './thread/thread.component';
import { LoggedInGuard } from 'src/app/guards/logged-in.guard';
import { EditThreadComponent } from './edit-thread/edit-thread.component';


const routes: Routes = [
  {
    path: 'new',
    component: NewThreadComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'edit/:id',
    component: EditThreadComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: ':id',
    component: ThreadComponent
  }
]

@NgModule({
  declarations: [
    NewThreadComponent,
    ThreadComponent,
    EditThreadComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ThreadsModule { }
