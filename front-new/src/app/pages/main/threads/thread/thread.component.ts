import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Reply } from 'src/app/interfaces/Reply.model';
import { Thread } from 'src/app/interfaces/Thread.model';
import { User } from 'src/app/interfaces/User.model';
import { AuthService } from 'src/app/services/auth.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ReplyService } from 'src/app/services/reply.service';
import { ThreadService } from 'src/app/services/thread.service';
import { EditorComponent } from 'src/app/shared/editor/editor.component';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})
export class ThreadComponent implements OnInit {

  @ViewChild('reply') reply: EditorComponent

  constructor(
    private route: ActivatedRoute,
    public service: ThreadService,
    public authService: AuthService,
    public replyService: ReplyService,
    public notify: NotifyService
  ) { }

  isAuthor:boolean

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.getThread(params.id)
    })
  }

  loading:boolean = true
  thread:Thread
  getThread(id:string) {
    this.service.getById(id).subscribe(data => {
      console.log(data)
      this.thread = {...data}
      this.isAuthor = this.authService.currentUser?.username === this.thread.author
      this.getParticipants(id)
    }, error => {
      console.log(error)
    })
  }

  isParticipant:boolean
  participants:User[] = []
  getParticipants(id:string) {
    this.service.getParticipants(id).subscribe(data => {
      console.log(data)
      const participants = [...data]
      participants.unshift({username:this.thread.author, avatar:this.thread.avatar})
      this.participants = [...participants]
      this.isParticipant = this.participants.map(p => p.username).includes(this.authService.currentUser?.username)
      this.loading = false
    }, error => {
      console.log(error)
    })
  }

  handleThreadLike(like:boolean) {
    this.thread = {...this.thread, fav: like}
    this.service[like ? 'like' : 'dislike'](this.thread.id).subscribe(data => {
      console.log(data)
      console.log(this.thread)
      this.notify.success('Cambios Guardados')
    }, error => {
      console.log(error)
      this.thread = {...this.thread, fav: !like}
    })
  }

  handleThreadSave(save:boolean) {
    this.thread = {...this.thread, saved: save}
    this.service[save ? 'save' : 'unsave'](this.thread.id).subscribe(data => {
      console.log(data)
      console.log(this.thread)
      this.notify.success('Cambios Guardados')
    }, error => {
      console.log(error)
      this.thread = {...this.thread, saved: !save}
    })
  }

  handleReplyLike(reply:Reply, like:boolean) {
    reply.fav = like
    this.replyService[like ? 'like' : 'dislike'](reply.id).subscribe(data => {
      console.log(data)
      this.notify.success('Cambios Guardados')
    }, error => {
      console.log(error)
      reply.fav = !like
    })
  }

  handleReplySave(reply:Reply, save:boolean) {
    reply.saved = save
    this.replyService[save ? 'save' : 'unsave'](reply.id).subscribe(data => {
      console.log(data)
      this.notify.success('Cambios Guardados')
    }, error => {
      console.log(error)
      reply.saved = !save
    })
  }

  replyLoading:boolean
  insertReply() {

    const reply:Reply = {
      thread: this.thread.id,
      body: this.reply.editor.nativeElement.innerHTML,
      approved: this.isParticipant
    }

    this.replyLoading = true

    this.replyService.insertReply(reply).subscribe(data => {
      console.log(data)
      this.replyLoading = false
    }, error => {
      console.log(error)
      this.replyLoading = false
    })
  }

}
