import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { ProfileSettingsComponent } from './settings/profile-settings.component';
import { DeskComponent } from './desk/desk.component';
import { RequestsComponent } from './requests/requests.component';
import { LoggedInGuard } from 'src/app/guards/logged-in.guard';


const routes: Routes = [
  {
    path: ':username',
    component: ProfileComponent
  },
  {
    path: ':username/desk',
    component: DeskComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: ':username/settings',
    component: ProfileSettingsComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: ':username/requests',
    component: RequestsComponent,
    canActivate: [LoggedInGuard]
  }
]

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileSettingsComponent,
    DeskComponent,
    RequestsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
