import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FriendshipRequest } from 'src/app/interfaces/FriendshipRequest.model';
import { ReplyRequest } from 'src/app/interfaces/ReplyRequest.model';
import { Tab } from 'src/app/interfaces/Tab.model';
import { AlertService } from 'src/app/services/alert.service';
import { FriendshipService } from 'src/app/services/friendship.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ReplyService } from 'src/app/services/reply.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { TabsComponent } from 'src/app/shared/tabs/tabs.component';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  @ViewChild('requestsTabs') requestsTabs:TabsComponent
  @ViewChild('replyModal') replyModal:ModalComponent

  constructor(
    private route: ActivatedRoute,
    public friendshipService: FriendshipService,
    public replyService: ReplyService,
    public alertService: AlertService,
    public notify: NotifyService
  ) { }

  setTabs():Tab[] {
    return [
      {title:'Solicitudes de Amistad', alert:!!this.alertService.f},
      {title:'Solicitudes de Participación', alert:!!this.alertService.r}
    ]
  }

  tabs:Tab[] = this.setTabs()

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.username) {
        this.getFriendshipRequests()
        this.getReplyRequests()
        this.checkAlerts()
        this.clearAlerts(0)
      }
    })
  }

  async checkAlerts() {
    await this.alertService.setAlerts()
    this.tabs = this.setTabs()
    // console.log(this.tabs)
  }

  clearAlerts(index:number) {
    let param = index === 0 ? 'f' : 'r'
    // console.log(this.alertService[param])
    if (this.alertService[param] === null) {return}
    // this.alertService[param] = null

    // const requests:any = [...this[param+'requests']]
    // requests.forEach((r) => {r[param === 'f' ? 'fav' : 'saved'] = false})
    // this[param+'requests'] = [...requests]

    // this.tabs = this.setTabs()
    this.alertService.clear(param)
  }




  loading:boolean
  frequests:FriendshipRequest[] = []
  getFriendshipRequests() {
    this.loading = true
    this.friendshipService.getRequests().subscribe(data => {
      console.log(data)
      this.frequests = data
      this.loading = false
    }, error => {
      console.log(error)
    })
  }

  acceptFriendship(req:any) {
    req.loading = true
    this.friendshipService.acceptRequest(req.id).subscribe(data => {
      console.log(data)
      req.loading = false
      req.hide = true
      this.notify.success(`¡${req.username} y tú ahora son amigos!`)
      this.updateList('frequests')
    }, error => {
      console.log(error)
      req.loading = false
      this.notify.success('Ha ocurrido un error')
    })
  }

  deleteFriendship(req:any) {
    req.loading = true
    this.friendshipService.deleteFriendship(req.id).subscribe(data => {
      console.log(data)
      req.loading = false
      req.hide = true
      this.notify.success('La solicitud ha sido rechazada')
      this.updateList('frequests')
    }, error => {
      console.log(error)
      req.loading = false
      this.notify.success('Ha ocurrido un error')
    })
  }




  rrequests:ReplyRequest[] = []
  getReplyRequests() {
    this.loading = true
    this.replyService.getReplyRequestsByAuthor().subscribe(data => {
      console.log(data)
      this.rrequests = data
      this.loading = false
    }, error => {
      console.log(error)
    })
  }

  currentReply:any
  ver(req:any) {
    this.currentReply = req
    this.replyModal.open()
  }

  approveReply(req:any) {
    req.loading = true
    this.replyService.approveReply(req.replyId, req.threadId, req.replyAuthor).subscribe(data => {
      console.log(data)
      req.hide = true
      this.notify.success(`${req.replyAuthor} ha sido incluído como participante y su respuesta ha sido aprobada y publicada`)
      this.updateList('rrequests')
    }, error => {
      req.loading = false
      console.log(error)
      this.notify.error('Ha ocurrido un error')
    })
  }

  rejectReply(req:any) {
    req.loading = true
    this.replyService.rejectReply(req.replyId, req.threadId).subscribe(data => {
      console.log(data)
      req.hide = true
      this.notify.success('Se ha rechazado la solicitud')
      this.updateList('rrequests')
    }, error => {
      req.loading = false
      console.log(error)
      this.notify.error('Ha ocurrido un error')
    })
  }


  updateList(list:string) {
    this[list] = this[list].filter((el:any) => !el.hide)
  }

}
