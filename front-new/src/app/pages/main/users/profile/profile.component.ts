import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Thread } from 'src/app/interfaces/Thread.model';
import { User } from 'src/app/interfaces/User.model';
import { AuthService } from 'src/app/services/auth.service';
import { ThreadService } from 'src/app/services/thread.service';
import { FriendshipService } from 'src/app/services/friendship.service';
import { UserService } from 'src/app/services/user.service';
import { TabsComponent } from 'src/app/shared/tabs/tabs.component';
import { NotifyService } from 'src/app/services/notify.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Tab } from 'src/app/interfaces/Tab.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @ViewChild('profileTabs') profileTabs:TabsComponent
  @ViewChild('uploadModal') uploadModal:ModalComponent
  @ViewChild('avatarEl') avatarEl:ElementRef

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public userService: UserService,
    public threadService: ThreadService,
    public friendshipService: FriendshipService,
    public authService: AuthService,
    public notify: NotifyService
  ) { }

  loading:boolean = true
  isUser:boolean
  tabs:Tab[] = [{title:'Hilos'}, {title:'Respuestas'}, {title:'Comentarios'}]
  ngOnInit(): void {
   
    this.route.params.subscribe(params => {

      if (params.username) {
        
        this.isUser = this.authService.currentUser?.username === params.username
        this.obtenerUsuario(params.username)
        this.obtenerHilos(params.username)
        this.obtenerAmigos(params.username)
        this.confirmarAmistad(params.username)
        
      }
      
    })
  }

  isFriend:boolean
  confirmarAmistad(username:string) {
    if (this.authService.currentUser) {
      this.friendshipService.checkFriendship(username).subscribe(data => {
        console.log(data)
        this.isFriend = data.friendship
      }, error => {
        console.log(error)
      })
    }
  }

  user:User
  obtenerUsuario(username:string) {
    this.userService.getByUsername(username).subscribe(data =>{
      this.user = {...data}
      this.loading = false
      console.log(this.user)
    }, error => {
      console.log(error)
    })
  }

  threads:Thread[] = []
  obtenerHilos(username:string) {
    this.threadService.getByUser(username).subscribe(data =>{
      this.threads = [...data]
      this.loading = false
      console.log(this.threads)
    }, error => {
      console.log(error)
    })
  }

  friends:User[] = []
  obtenerAmigos(username:string) {
    this.friendshipService.getMutuals(username).subscribe(data => {
      console.log(data)
      this.friends = data
    }, error => {
      console.log(error)
    })
  }

  bioEdit:boolean = false
  bioReference:string
  editBio() {
    this.bioReference = this.user.bio
    this.bioEdit = !this.bioEdit
  }

  guardarBio() {
    this.userService.alterUser(this.user.username, 'bio', this.bioReference).subscribe(
      data => {
        console.log(data)
        this.user.bio = this.bioReference
        this.bioEdit = false
        this.notify.success('Cambios guardados')
      }, error => {
        console.log(error)
      }
    )
  }

  follow() {
    if (!this.authService.currentUser) {
      this.router.navigate(['/login'])
      return
    }
  }

  friendship() {
    if (!this.authService.currentUser) {
      this.router.navigate(['/login'])
      return
    }

    this.friendshipService.sendRequest(this.user.username).subscribe(data => {
      console.log(data)
      this.notify.success('Solicitud de amistad enviada')
    }, error => {
      console.log(error)
    })
  }

  unfriend(id:string) {
    this.friendshipService.deleteFriendship(id).subscribe(data => {
      console.log(data)
      this.notify.success('Operación exitosa')
    }, error => {
      console.log(error)
    })
  }

  getFiles(event) {
    this.avatar = event.target.files[0]
  }

  uploadLoading:boolean
  avatar:File
  async upload() {
    this.uploadLoading = true
    const reader = new FileReader();

    reader.onload = () => {
      const file = reader.result

      this.userService.alterAvatar(
        this.authService.currentUser.username,
        'users/'+this.avatar.name,
        file,
        this.avatar.type
      ).subscribe(data => {

        this.authService.avatar = data['key']
        this.avatarEl.nativeElement.src = data['key']
        this.uploadModal.close()
        this.notify.success('Cambios guardados')
        this.uploadLoading = false

      }, error => {
        console.log(error)
        this.uploadLoading = false
      })
    }

    reader.readAsDataURL(this.avatar)

  }

}
