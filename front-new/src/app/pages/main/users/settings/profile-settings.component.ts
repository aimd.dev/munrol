import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {

  @ViewChild('email') emailModal:ModalComponent
  @ViewChild('password') passwordModal:ModalComponent

  constructor() { }

  ngOnInit(): void {
  }

  newEmail:string = ''
  actualPassword:string = ''
  newPassword:string = ''

  reset() {
    console.log('close')
    this.actualPassword = ''
    this.newEmail = ''
    this.newPassword = ''
  }

}
