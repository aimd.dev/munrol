import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-account-verify',
  templateUrl: './account-verify.component.html',
  styleUrls: ['./account-verify.component.css']
})
export class AccountVerifyComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params.u && params.c) this.verifyAccount(params.u, params.c)
    })
  }

  loading:boolean = true
  success:boolean
  error:boolean
  verifyAccount(username:string, code:string) {
    this.userService.verifyAccount(username, code).subscribe(data => {
      console.log(data)
      this.loading = false
      this.success = true
    }, error => {
      console.log(error)
      this.loading = false
      this.error = true
    })
  }

}
