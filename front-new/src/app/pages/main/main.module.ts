import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { MainComponent } from './main.component';
import { AccountVerifyComponent } from './account-verify/account-verify.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./browsing/browsing.module').then(m => m.BrowsingModule)
      },
      {
        path: 't',
        loadChildren: () => import('./threads/threads.module').then(m => m.ThreadsModule)
      },
      {
        path: 'u',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'account-verify',
        component: AccountVerifyComponent
      }
    ]
  }
]


@NgModule({
  declarations: [
    MainComponent, 
    AccountVerifyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CoreModule,
    SharedModule
  ]
})
export class MainModule { }
