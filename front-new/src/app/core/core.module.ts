import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TopNavComponent } from './top-nav/top-nav.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ActionsComponent } from './actions/actions.component';
import { NotifyComponent } from './notify/notify.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    TopNavComponent,
    FooterComponent,
    SidebarComponent,
    ActionsComponent,
    NotifyComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    TopNavComponent,
    FooterComponent,
    SidebarComponent,
    ActionsComponent,
    NotifyComponent
  ]
})
export class CoreModule { }
