import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public alertService: AlertService
  ) { }

  username:string
  avatar:string
  ngOnInit(): void {
    if (this.authService.currentUser) {
      this.username = this.authService.currentUser.username
      this.alertService.setAlerts()
    }
  }

  logout() {
    this.authService.logout()
  }

}
