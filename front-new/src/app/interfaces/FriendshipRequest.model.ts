export interface FriendshipRequest {
    id:string
    createdAt:string
    username:string
    avatar?:string
    seen?:boolean
}