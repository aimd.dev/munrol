export interface User {
    username:string
    bio?:string
    createdAt?:string
    email?:string
    avatar?:string
    token?:string
    password?:string
}