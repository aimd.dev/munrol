import { Reply } from "../interfaces/Reply.model";

export interface Thread {
    id?:string
    createdAt?:string
    lastUpdate?:string
    lastEdit?:string
    author?:string
    avatar?:string
    title:string
    summary:string
    body:string
    open?:boolean
    restricted?:boolean
    fav?:boolean
    saved?:boolean
    replies?:Reply[]
    participants?:string[]
    draftId?:number
}