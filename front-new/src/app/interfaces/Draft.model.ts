export interface Draft {
    id?:number
    lastUpdated?:string
    createdAt?:string
    title?:string
    summary?:string
    body?:string
    raw?:string
}