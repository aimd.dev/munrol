export interface Reply {
    id?:string
    thread?:string
    createdAt?:string
    lastEdit?:string
    approvalDate?:string
    approved?:boolean
    author?:string
    avatar?:string
    body:string
    fav?:boolean
    saved?:boolean
}