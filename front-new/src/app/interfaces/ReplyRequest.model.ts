export interface ReplyRequest {
    replyId:string
    createdAt?:string
    replyAuthor:string
    avatar?:string
    thread:string
    threadAuthor?:string
    threadId?:string
    title?:string
    body:string
    seen?:boolean
}